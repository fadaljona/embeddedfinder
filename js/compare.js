//highlight row that on hover
$("tr").not(':first').hover(
  function () {
    $(this).css("background","#00FFD9");
  }, 
  function () {
    $(this).css("background","");
  }
);

//$("tr:odd").addClass("odd");

// add header rows -- row_section_header
$('#compare_table tbody').prepend('<tr class="row_section_header"/>').children('tr:first').append('<td colspan="5">System</td>') 
$("<tr class='row_section_header'><td colspan='5'>Storage</td></tr>").insertAfter(".rowid_maxmemory");
$("<tr class='row_section_header'><td colspan='5'>I/O</td></tr>").insertAfter(".rowid_raidsupport");
$("<tr class='row_section_header'><td colspan='5'>Video</td></tr>").insertAfter(".rowid_canbus");
$("<tr class='row_section_header'><td colspan='5'>Expansion</td></tr>").insertAfter(".rowid_displayport");
$("<tr class='row_section_header'><td colspan='5'>Power</td></tr>").insertAfter(".rowid_pci");
$("<tr class='row_section_header'><td colspan='5'>Mechanical</td></tr>").insertAfter(".rowid_poweradapter");
$("<tr class='row_section_header'><td colspan='5'>Environmental</td></tr>").insertAfter(".rowid_weight");
$("<tr class='row_section_header'><td colspan='5'>Operating System</td></tr>").insertAfter(".rowid_vibration");
