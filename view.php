<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="robots" content="noindex,nofollow">

  <title>Embedded Grid</title>
  
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" />
  <link rel="stylesheet" href="css/unifilter.css" />
  <link rel="stylesheet" href="css/view.css" />

  <style>
  		body {font-size: .9rem;}
  </style>
</head>
<body>
  <div class="container">
    <div class="row"><div class="col-12"><a href="table.php">All Products (Table)</a>&nbsp;|&nbsp;<a href="compare.php?id=127,106,146">Compare Products</a></div></div>
  <div class="row">
  <div class="col-12">
  <h2>Product Grid</h2>
  <p>Sort products by status, manufacturer, and more.</p>
  	<?php /* ?>
  	<h5>Status</h5>
	<div class="row">
		<div class="col">
			<button type="button" class="btn btn-secondary btn-active">Active</button>
			<button type="button" class="btn btn-secondary btn-featured">Featured</button>
			<button type="button" class="btn btn-secondary btn-new">New</button>
			<button type="button" class="btn btn-secondary btn-coming-soon">Coming Soon</button>
			<button type="button" class="btn btn-secondary btn-eol">EOL</button>
			<button type="button" class="btn btn-secondary btn-no-sell">Do Not Sell</button>
			<button type="button" class="btn btn-secondary btn-discontinued">Discontinued</button>
		</div>
	</div>

	<h5>Manufacturer</h5>
	<div class="row">
		<div class="col">
			<button type="button" class="btn btn-secondary btn-industrialpc">Industrial PC</button>
			<button type="button" class="btn btn-secondary btn-adlink">ADLink</button>
			<button type="button" class="btn btn-secondary btn-advantech">Advantech</button>
			<button type="button" class="btn btn-secondary btn-arestech">Arestech</button>
			<button type="button" class="btn btn-secondary btn-gigaipc">GIGAIPC</button>
			<button type="button" class="btn btn-secondary btn-lex">Lex</button>
			<button type="button" class="btn btn-secondary btn-msi">MSI</button>
			<button type="button" class="btn btn-secondary btn-neousys">Neousys</button>
			<button type="button" class="btn btn-secondary btn-sintrones">Sintrones</button>
			<button type="button" class="btn btn-secondary btn-winmate">Winmate</button>
		</div>
	</div>
	<?php */ ?>
		
<?php

$servername = "localhost";
$username = "embeddh3_de";
$password = "dremel29001";
$dbname = "embeddh3_entry";

	// Create connection
	$conn = new mysqli($servername, $username, $password,$dbname);

	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	} 

	if (isset($_GET['cores'])) {
    $ip_cores = $_GET['cores'];
		}
		else
		{
	  $ip_cores = '1,2,3,4,5,6,7,8,9,10';
		}

if (isset($_GET['apps'])) {
    $ip_apps = $_GET['apps'];
    $app_cond = "and app_id in ('".$ip_apps."')";
		}
		else
		{	
    $app_cond = "";
		}


	// starting code to display Input result on screen 
		$sql = "SELECT tb6.app_id_grp,tb6.app_id,tb1.`f_name`,tb1.`f_status`,tb1.`id`,tb1.`f_sata35_in`, tb1.`f_sata25_hot`,tb3.`processor_id`,tb3.`cores`,tb5.`os_id`, tb1.`f_manufacturer`, tb1.`f_source`, tb1.`f_series`, tb1.`f_chipset`, tb1.`f_graphics`, tb1.`f_graphics_card`, tb1.`f_max_memory`, tb1.`f_memory_slots`, tb1.`f_memory_type`, tb1.`f_memory_speed`, tb1.`f_sata25_in`, tb1.`f_msata`, tb1.`f_m2`, tb1.`f_raid`, tb1.`f_gbe`, tb1.`f_poe`, tb1.`f_usb2`, tb1.`f_usb3`, tb1.`f_rs2`, tb1.`f_rs244`, tb1.`f_dio`, tb1.`f_can`, tb1.`f_mdisplay`, tb1.`f_vga`, tb1.`f_dvi`, tb1.`f_hdmi`, tb1.`f_dp`, tb1.`f_pcie16`, tb1.`f_pcie8`, tb1.`f_pcie4`, tb1.`f_pcie1`, tb1.`f_mpcie`, tb1.`f_pci`, tb1.`f_sim`, tb1.`f_voltage`, tb1.`f_connector`, tb1.`f_adapter`, tb1.`f_dim_l`, tb1.`f_dim_w`, tb1.`f_dim_d`, tb1.`f_weight`, tb1.`f_optemp_min`, tb1.`f_optemp_max`, tb1.`f_sttemp_min`, tb1.`f_sttemp_max`, tb1.`f_humidity`, tb1.`f_shock`, tb1.`f_vibration`, tb1.`f_notes`, tb1.`f_cost`, tb1.`f_odoo` FROM `comp_specs` tb1 left outer join (select comp_spec_id,GROUP_CONCAT(mount_id SEPARATOR ' ') as mount_id from mount_link group by comp_spec_id)tb2 on tb1.id = tb2.comp_spec_id  left outer join (select comp_spec_id,proc_family,proc_number,cores,threads,base_freq,max_freq,cache,group_concat(concat('Intel ',proc_family,' ', proc_number,' (',base_freq,case when max_freq is not null then concat('/',max_freq) else '' end,' GHz, ',cache,'M)',' - ',cores,' Cores') SEPARATOR '<br>') as processor_id from process_link pl left outer join dim_processor dp on pl.processor_id = dp.proc_id group by comp_spec_id)tb3 on tb1.id = tb3.comp_spec_id  left outer join (select comp_spec_id,group_concat(cert_id) as cert_id from cert_link group by comp_spec_id)tb4 on tb1.id = tb4.comp_spec_id  left outer join (select comp_spec_id,group_concat(os_id) as os_id from os_link group by comp_spec_id)tb5 on tb1.id = tb5.comp_spec_id left outer join (select comp_spec_id,app_id,group_concat(app_id) as app_id_grp from app_link group by comp_spec_id)tb6 on tb1.id = tb6.comp_spec_id where tb1.id > 17 AND cores in ($ip_cores) $app_cond ORDER BY  tb1.`f_name`";



		//working sql fron nitin
		//$sql = "SELECT tb6.app_id_grp,tb6.app_id,tb1.`f_name`, tb3.`processor_id`,tb3.`cores`,tb5.`os_id`, tb1.`f_manufacturer`, tb1.`f_source`, tb1.`f_chipset`, tb1.`f_graphics`, tb1.`f_max_memory`, tb1.`f_memory_slots`, tb1.`f_memory_type`, tb1.`f_memory_speed`, tb1.`f_sata25_in`, tb1.`f_msata`, tb1.`f_m2`, tb1.`f_raid`, tb1.`f_gbe`, tb1.`f_poe`, tb1.`f_usb2`, tb1.`f_usb3`, tb1.`f_rs2`, tb1.`f_rs244`, tb1.`f_dio`, tb1.`f_can`, tb1.`f_vga`, tb1.`f_dvi`, tb1.`f_hdmi`, tb1.`f_dp`, tb1.`f_pcie16`, tb1.`f_pcie8`, tb1.`f_pcie4`, tb1.`f_pcie1`, tb1.`f_mpcie`, tb1.`f_pci`, tb1.`f_voltage`, tb1.`f_connector`, tb1.`f_adapter`, tb1.`f_dim_l`, tb1.`f_dim_w`, tb1.`f_dim_d`, tb1.`f_weight`, tb1.`f_optemp_min`, tb1.`f_optemp_max`, tb1.`f_sttemp_min`, tb1.`f_sttemp_max`, tb1.`f_humidity`, tb1.`f_shock`, tb1.`f_vibration`, tb1.`f_notes` FROM `comp_specs` tb1 left outer join (select comp_spec_id,GROUP_CONCAT(mount_id SEPARATOR ' ') as mount_id from mount_link group by comp_spec_id)tb2 on tb1.id = tb2.comp_spec_id  left outer join (select comp_spec_id,proc_family,proc_number,cores,threads,cache,group_concat(concat('Intel ',proc_family,' ',proc_number,' Cores(',cores,') Threads(',threads,')')) as processor_id from process_link pl left outer join dim_processor dp on pl.processor_id = dp.proc_id group by comp_spec_id)tb3 on tb1.id = tb3.comp_spec_id  left outer join (select comp_spec_id,group_concat(cert_id) as cert_id from cert_link group by comp_spec_id)tb4 on tb1.id = tb4.comp_spec_id  left outer join (select comp_spec_id,group_concat(os_id) as os_id from os_link group by comp_spec_id)tb5 on tb1.id = tb5.comp_spec_id left outer join (select comp_spec_id,app_id,group_concat(app_id) as app_id_grp from app_link group by comp_spec_id)tb6 on tb1.id = tb6.comp_spec_id where cores in ($ip_cores) $app_cond ORDER BY  tb1.`f_manufacturer`";

		$result = $conn->query($sql);
	?>

	<div class='row'>
		<?php

		if ($result->num_rows > 0) {
			echo "<div class='col-lg-4'><div id='filters' class='unifilter'></div></div>";
			echo"<div class='col-lg-8'>";
				echo"<div class='row' id='shop'>";
			    	// output data of each row
				    while($row = $result->fetch_assoc()) {
				    	$p_name=  $row["f_name"];
				    	$p_status = $row["f_status"];

				    	$p_manufacturer = $row["f_manufacturer"];
				    	if ($p_manufacturer == "Iwill") {
				    		$p_manufacturer = "Industrial PC, Inc.";
				    	} else {
				    		$p_manufacturer = $p_manufacturer;
				    	}

						$p_series = $row["f_series"];

						//Display USB
				    	$p_usb2 = $row["f_usb2"];
				    	$p_usb3 = $row["f_usb3"];
				    	$p_usbt = $p_usb2 + $p_usb3;
				    	
				    	$app_id_grp = $row["app_id_grp"];

						//Display Serial
				    	$p_rs2 = $row["f_rs2"];
				    	$p_rs244 = $row["f_rs244"];
				    	$p_rst = $p_rs2 + $p_rs244;

				    	//Display Video
				    	$p_graphics_card = $row["f_graphics_card"];
				    	$p_vga = $row["f_vga"];
				    	$p_dvi = $row["f_dvi"];
				    	$p_hdmi = $row["f_hdmi"];
				    	$p_dp = $row["f_dp"];
				    	$p_mdisplay = $row["f_mdisplay"];
				    	$p_display_tot = $p_vga + $p_dvi + $p_hdmi + $p_dp;

				    	$p_vid = "";
				    	if ($p_vga > 0){$p_vid = $p_vga."x VGA,";}
				    	if ($p_dvi > 0){$p_vid = $p_vid." ".$p_dvi."x DVI,";}
				    	if ($p_hdmi > 0){$p_vid = $p_vid." ".$p_hdmi."x HDMI,";}
				    	if ($p_dp > 0){$p_vid = $p_vid." ".$p_dp."x DisplayPort";}
				    	$p_vid = rtrim($p_vid,',');


				    	//Display Expansion
				    	$p_pcie16 = $row["f_pcie16"];
				    	$p_pcie8 = $row["f_pcie8"];
				    	$p_pcie4 = $row["f_pcie4"];
				    	$p_pcie1 = $row["f_pcie1"];
				    	$p_pciet = $p_pcie16 + $p_pcie8 + $p_pcie4 + $p_pcie1;
				    	$p_pci = $row["f_pci"];
				    	$p_mpcie = $row["f_mpcie"];
				    	$p_expansion = "";
				    	if ($p_pciet > 0){$p_expansion = $p_pciet."x PCIe,";}
				    	if ($p_mpcie > 0){$p_expansion = $p_expansion." ".$p_mpcie."x Mini PCIe,";}
				    	if ($p_pci > 0){$p_expansion = $p_expansion." ".$p_pci."x PCI";}
				    	$p_expansion = rtrim($p_expansion,',');

				    	$p_sim = $row["f_sim"];

				    	//Display Ethernet
				    	$p_gbe = $row["f_gbe"];
				    	$p_poe = $row["f_poe"];
				    	$p_lan = "";
				    	if ($p_gbe > 0 and $p_poe > 0){
				    		$p_lan = $p_gbe."x GbE, ".$p_poe."x PoE, ";
				    	} elseif ($p_poe > 0 and $p_gbe == 0) {
				    		$p_lan = $p_poe."x PoE, ";
				    	} else {
				    		$p_lan = $p_gbe."x GbE, ";
				    	}

				    	//Storage
				    	$p_sata25_in = $row["f_sata25_in"];
				    	$p_sata25_hot = $row["f_sata25_hot"];
				    	$p_sata35_in = $row["f_sata35_in"];
				    	$p_sata25_tot = $row["f_sata25_hot"];
				    	$p_sata_tot = $p_sata25_in + $p_sata25_hot;
				    	$p_msata = $row["f_msata"];
				    	$p_m2 = $row["f_m2"];
				    	$p_raid = $row["f_raid"];	
				    	$p_storage = "";
				    	if ($p_sata_tot > 0){$p_storage = $p_sata_tot."x SATA,";}
				    	if ($p_msata > 0){$p_storage = $p_storage." ".$p_msata."x mSATA,";}
				    	if ($p_m2 > 0){$p_storage = $p_storage." ".$p_m2."x m.2";}
				    	$p_storage = rtrim($p_storage,',');

				    	//$p_storage = $p_sata_tot."x SATA";

				    	$p_bullet_ports = $row["f_gbe"]."x LAN, <span class='data_usb'>".$p_usbt."</span>x USB, ".$p_rst."x Serial";

				    	//Display Processors
				    	$p_processor_list_raw = $row["processor_id"];

						if (strpos($p_processor_list_raw, 'i7-6') !== false) {
						    $p_processor_list = "Dual/Quad-Core 6th Gen. Intel® Core™ Processors";
						    $p_data_cpu = "core";
						} elseif (strpos($p_processor_list_raw, 'i7-8') !== false) {
							$p_processor_list = "Quad/Hexa-Core 8th Gen. Intel® Core™ Processors";
							$p_data_cpu = "core";
						} elseif (strpos($p_processor_list_raw, 'i7-3') !== false) {
							$p_processor_list = "Dual/Quad-Core 3rd Gen. Intel® Core™ Processors";
							$p_data_cpu = "core";
						} elseif (strpos($p_processor_list_raw, 'i7-7') !== false) {
							$p_processor_list = "Dual/Quad-Core 7th Gen. Intel® Core™ Processors";
							$p_data_cpu = "core";
						} elseif (strpos($p_processor_list_raw, 'i7-4') !== false) {
							$p_processor_list = "Dual/Quad-Core 4th Gen. Intel® Core™ Processors";
							$p_data_cpu = "core";
						} elseif (strpos($p_processor_list_raw, 'Atom') !== false) {
							$p_processor_list = str_replace("Intel Atom","Intel® Atom®",$p_processor_list_raw);
							$p_data_cpu = "atom";
						} elseif (strpos($p_processor_list_raw, 'Celeron') !== false) {
							$p_processor_list = str_replace("Intel Celeron","Intel® Celeron®",$p_processor_list_raw);
							$p_data_cpu = "celeron";
						} elseif (strpos($p_processor_list_raw, 'Pentium') !== false) {
							$p_processor_list = str_replace("Intel Pentium","Intel® Pentium®",$p_processor_list_raw);
							$p_data_cpu = "pentium";
						} 
						else {
							$p_processor_list = str_replace("Intel","Intel®",$p_processor_list_raw);
							if (strpos($p_processor_list_raw, 'i7') !== false OR strpos($p_processor_list_raw, 'i5') !== false OR strpos($p_processor_list_raw, 'i3') !== false) {
								$p_data_cpu = "core";
							} else {
								$p_data_cpu = "other";
							}							
						};

				    	//$p_processor_list = str_replace(",",", ",$row["app_id_grp"]);

				    	

						//Dimensions
						$p_dim_l = $row["f_dim_l"];
						$p_dim_w = $row["f_dim_w"];
						$p_dim_h = $row["f_dim_d"];
						$p_dim_l_in = $p_dim_l*0.0393701;
					    $p_dim_w_in = $p_dim_w*0.0393701;
					    $p_dim_h_in = $p_dim_h*0.0393701;
					    $p_dim_t = $p_dim_l*$p_dim_w*$p_dim_h;

					    $p_weight = $row["f_weight"];
					    $p_weight_lb = $p_weight*2.20462;	
					
					    $p_mounting = $row["mount_id"];
					    $p_voltage = $row["f_voltage"];


						//Display Picture
						if (file_exists ("img/products/".$row["f_name"].".jpg") == true) {
							$p_img_name = $row["f_name"].".jpg";
						} elseif (strpos($row["f_name"], 'Nuvo-3') !== false AND strpos($row["f_name"], 'LP') !== false ) {
							$p_img_name = "Nuvo-3000LP.jpg";
						} elseif (strpos($row["f_name"], 'Nuvo-5') !== false AND strpos($row["f_name"], 'LP') !== false ) {
							$p_img_name = "Nuvo-5000LP.jpg";
						} elseif (strpos($row["f_name"], 'Nuvo-7') !== false AND strpos($row["f_name"], 'LP') !== false ) {
							$p_img_name = "Nuvo-7000LP.jpg";
						}  elseif (file_exists ("img/products/".$row["f_series"].".jpg") == true) {
							$p_img_name = $row["f_series"].".jpg";
						} else {
							$p_img_name = "no-image.jpg";
						}

				    	//Bullet CPU
						$p_bullet_cpu = "";
						if (strpos($p_processor_list, '- 2 Cores') !== false) {
						    $p_bullet_cpu = "Dual-Core ".substr($p_processor_list, 0, strpos($p_processor_list, "("));
						} elseif (strpos($p_processor_list, '- 4 Cores') !== false) {
						    $p_bullet_cpu = "Quad-Core ".substr($p_processor_list, 0, strpos($p_processor_list, "("));
						} elseif (strpos($p_processor_list, '- 1 Cores') !== false) {
						    $p_bullet_cpu = "Single-Core ".substr($p_processor_list, 0, strpos($p_processor_list, "("));
						}
						else {
							$p_bullet_cpu = $p_processor_list;
						}

						//Bullet Points
						


						if ($p_rst > 0) {
							$p_bullet_ports = $p_lan.$p_usbt."x USB, ".$p_rst."x Serial";
						} else {
							$p_bullet_ports = $p_lan.$p_usbt."x USB";
						}

						if ($p_pciet > 0 and $p_pci == 0 and $p_mpcie == 0) {
							$p_bullets_expansion = " with PCIe expansion";
						}
						elseif ($p_pciet == 2 and $p_pci == 0 and $p_mpcie > 0) {
							$p_bullets_expansion = " with Dual PCIe and Mini-PCIe expansion";
						}
						elseif ($p_pciet > 0 and $p_pci == 0 and $p_mpcie > 0) {
							$p_bullets_expansion = " with PCIe and Mini-PCIe expansion";
						}
						elseif ($p_pciet == 0 and $p_pci > 0 and $p_mpcie > 0) {
							$p_bullets_expansion =  " with PCI and Mini-PCIe expansion";
						}
						elseif ($p_pciet > 0 and $p_pci > 0 and $p_mpcie > 0) {
							$p_bullets_expansion = " with PCI, PCIe, and Mini-PCIe expansion";
						} 
						elseif ($p_mpcie == 0 and $p_pciet == 0 and $p_pci > 0) {
							$p_bullets_expansion = " with PCI expansion";
						} 
						elseif ($p_mpcie == 0 and $p_pciet > 0 and $p_pci > 0) {
							$p_bullets_expansion = " with PCI and PCIe expansion";
						} 
						elseif ($p_mpcie > 0 and $p_pciet == 0 and $p_pci == 0) {
							$p_bullets_expansion = " with Mini-PCIe expansion";
						} 
						else {$p_bullets_expansion = "";}

						//Bullet - Chassis
						if ($p_dim_t < 1000000) {
							$p_bullet_size = "Ultra-Compact, Fanless Chassis";
							$p_data_form_factor = "Ultra-Compact";
						} elseif ($p_dim_t < 3000000) {
							$p_bullet_size = "Compact, Fanless Chassis";
						} elseif (strpos($row["f_name"], 'LP') !== false ) {
							$p_bullet_size = "Low-Profile Fanless Chassis";
							$p_data_form_factor = "Low-Profile";
						}  else {
							$p_bullet_size = "Fanless Chassis";
							$p_data_form_factor = "Fanless";
						};

						//Bullet - Video -- Dual/Triple Display? Graphics Card
						if ($p_display_tot > 1){
							$p_bullet_video = "Multi Display (".$p_vid.")";
						} elseif ($p_display_tot == 0) {
							$p_bullet_video = "No Video Output";
						} else {
							$p_bullet_video = $p_vid;
						}

						$p_bullet_video = str_replace("( ", "(", $p_bullet_video);

						//Bullet - Storage -- Add mSATA and m.2
						if ($p_sata35_in > 0 and $p_sata_tot == 0) {
							$p_bullet_storage = $p_sata35_in."x 3.5\" SATA";
						} elseif ($p_sata_tot > 0) {
							$p_bullet_storage = $p_sata_tot."x 2.5\" SATA";
						} elseif ($p_sata_tot == 0 AND $p_series == "UNO-1000") {
							$p_bullet_storage = "microSD slot";
						} else {
							$p_bullet_storage =  $p_storage;
						}

						if ($p_raid == "Y") {$p_bullet_storage = $p_bullet_storage." (RAID)";}

						//Application Class 
						$p_data_application = "";
						if (strpos($row["app_id_grp"], 'In-Vehicle') !== false) {
							$p_data_application = $app_class_string.", In-Vehicle";
						} elseif (strpos($row["app_id_grp"], 'General') !== false) {
							$p_data_application = $app_class_string.", General";
						} elseif (strpos($row["app_id_grp"], 'Automation') !== false) {
							$p_data_application = $app_class_string.", Automation";
						} elseif (strpos($row["app_id_grp"], 'Machine Learning') !== false) {
							$p_data_application = $app_class_string.", Machine Learning";
						} elseif (strpos($row["app_id_grp"], 'Surveillance') !== false) {
							$p_data_application = $app_class_string.", Surveillance";
						} 
						$p_data_application = substr($p_data_application, 2);


						$p_data_manufacturer = $p_manufacturer;
						if ($p_manufacturer == "Industrial PC, Inc.") {
							$p_data_manufacturer = "industrial pc";
						} else {
							$p_data_manufacturer = $p_manufacturer;
						}


						$p_data_features = "";
						if ($p_pciet > 0 OR $p_pci > 0) {$p_data_features = $p_data_features.", Expansion Slot";}
						if (strpos($p_name, 'VTC') !== FALSE) {$p_data_features = $p_data_features.", Ignition Control";}
						if ($p_poe > 0) {$p_data_features = $p_data_features.", PoE";}
						if ($p_graphics_card != "None") {$p_data_features = $p_data_features.", Graphics Card";}
						if ($p_raid == "Y") {$p_data_features = $p_data_features.", RAID";}
						if ($p_sata35_in > 0) {$p_data_features = $p_data_features.", 3.5 in Drive";}
						if ($p_sata25_hot > 0) {$p_data_features = $p_data_features.", Hot-Swappable Drive";}
						if ($p_msata > 0 AND $p_sim > 0 AND ($p_manufacturer == "Neousys" OR $p_manufacturer == "Sintrones")) {$p_data_features = $p_data_features.", LTE";}
						if ($p_msata > 0) {$p_data_features = $p_data_features.", WIFI";}
						if ($p_m2 > 0) {$p_data_features = $p_data_features.", M.2";}



						

						$p_data_features = substr($p_data_features, 2);;


						/* May be a little too broad */
						$p_data_voltage = "";
						if ($p_voltage == "12V" OR strpos($p_voltage, '~19V') !== FALSE) {
							$p_data_voltage = "12V";
						} else {
							$p_data_voltage = "12/24V";
						}


						echo "<div class='col-xs col-md-4 p-1 product_".strtolower($p_status)."' data-cpu='".$p_data_cpu."' data-application='".$p_data_application."' data-manufacturer='".$p_data_manufacturer."' data-status='".strtolower($p_status)."' data-usb='".$p_usbt."' data-ethernet='".$p_gbe."' data-serial='".$p_rst."' data-sata='".$p_sata_tot."' data-power='".$p_data_voltage."' data-features='".$p_data_features."'>";
							echo "<div class='product_card p-2'>";	
								if ($p_status == "Featured") {
									echo "<div class='ribbon ribbon-featured'>Featured</div>";
								} else if($p_status == "New"){
									echo "<div class='ribbon ribbon-new'>New</div>";
								} else if($p_status == "Coming-Soon"){
									echo "<div class='ribbon ribbon-comingsoon'>Coming Soon</div>";
								}  else if($p_status == "Discontinued"){
									echo "<div class='ribbon ribbon-discontinued'>Discontinued</div>";
								} else if($p_status == "EOL"){
									echo "<div class='ribbon ribbon-eol'>EOL</div>";
								} else if($p_status == "No-Sell"){
									echo "<div class='ribbon ribbon-discontinued'>Do Not Sell</div>";
								}  else {
									echo "<div class='ribbon ribbon-active'>Active</div>";
								}
								echo "<div class='product_card_img' ><a href='product.php?id=".$row["id"]."'><img src='img/products/".$p_img_name."' class='img-fluid' /></a></div>";
								echo "<div class='product_card_head'>";
									echo "<h4>".$row["f_series"]."</h4>";
									echo "<h3 class='name'><a class='name' href='product.php?id=".$row["id"]."'>".$row["f_name"]."</a></h3>";
									echo "<h5>".$p_manufacturer."</h5>";
								echo "</div>";
								echo "<div class='product_card_details'>";
									echo "<div class='product_card_bullets'>";
										echo "<ul>";
											echo "<li>".$p_bullet_cpu."</li>";
											echo "<li>".$p_bullet_ports."</li>";

											echo "<li>".$p_bullet_video."</li>";
											echo "<li>".$p_bullet_size.$p_bullets_expansion."</li>";
											echo "<li>".$p_bullet_storage."</li>";
											echo "<li>".$p_voltage." DC Input</li>";
											//echo "<li>".$p_mounting."</li>";
										echo "</ul>";
									echo "</div>";
									//echo "<div class='product_card_price'>Starting at $".round($row["f_cost"])."</div>";
									//echo "<div class='product_card_status'>In Stock</div>";
									echo "<div class='product_card_more'><a href='product.php?id=".$row["id"]."'><button type='button' class='btn btn-viewmore'>More Info</button></a></div>";
								echo "</div>";
								//echo "<div class='product_card_app indev' style='display: none;'>".str_replace(",",", ",$row["app_id_grp"])."</div>";
							echo "</div>";	
						echo "</div>";
				    }
		    	echo "</div>";
		    echo "</div>";
		} else {
		    echo "<div class='col'>0 results</div>";
		}

		$conn->close();

		?>
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  	<script type="text/javascript" src="js/jquery.unifilter.min.js"></script>
    <script type="text/javascript">
  		$(document).ready(function(){
  		
  			// our online shop UL list
  			var shop = $('#shop');
  			
  			// Masonry Options
			var masonry_options = {
	  			itemSelector: 'li:not(.uf-clone)',
	  			percentPosition: true,
	  			columnWidth: '.masonry-sizer'
	  		};

	  		var default_options = { 
	  			animationType: 'rotate',
	  			animationTime: 500
	  		};
  		
  			// filter options
  			var filter_options = {
  				"application": {
					title: "Application",
					leading: true
				},
				"cpu": {
					title: "CPU",
					leading: true
				},
				"manufacturer" : {	
					title: 'Manufacturer',
					tooltip: true,
				},
				"status": {
					title: 'Status',
					tooltip: true,
					multiple: true
				},
				"power": {
					title: 'Voltage',
					tooltip: true,
					multiple: true
				},
				"features": {
					title: 'Features',
					tooltip: true,
					leading: true
				}
			};
			
			// search options
			var search_options = {
				"name": {
					title: "Quick Search",
					placeholder: 'Eg. POC-120',
				}
			};
 

			// range options
			var range_options = {
				"ethernet": {
					title: "Ethernet Ports",
					scale: '0-10'
				},
				"usb": {
					title: "USB Ports",
					scale: '0-10'
				},
				"serial": {
					title: "Serial Ports",
					scale: '0-10'
				},
				"sata": {
					title: "SATA Ports",
					scale: '0-4'
				}
			};

		    // sort options  
		    //var sort_options = {  
		        //title: 'Sorting',  
		        //subtitle: 'Please select an option to sort',  
		        //options: {  
		            //"status": {  
		                //label: 'Status',  
		                //order: 'DESC'  
		            //}  
		        //}  
		    //};  
			
  			var unifilter_options = {  
		        order: "search, filters, range",
		        filters: filter_options,
  				search: search_options,
  				range: range_options
		    }; 



		    $('#filters').unifilter("#shop", unifilter_options);
  		});
  	
  	</script>
   	<?php /*<script type="text/javascript" src="/js/filter.js"></script> */ ?>
  </div>
</body>
</html>