<section class="b-search">
			<div class="container">
				<form action="listings.html" method="POST" class="b-search__main">
					<div class="b-search__main-title wow zoomInUp" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInUp;">
						<h2>UNSURE WHICH COMPUTER YOU ARE LOOKING FOR? FIND IT HERE</h2>
					</div>
					<div class="b-search__main-type wow zoomInUp" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInUp;">
						<div class="col-xs-12 col-md-2 s-noLeftPadding">
							<h4>Form Factor</h4>
						</div>
						<div class="col-xs-12 col-md-10">
							<div class="row">
								<div class="col-xs-2">
									<input id="type1" type="radio" name="type">
									<label for="type1" class="b-search__main-type-svg">
										<img src="images/icons/icon_rackmount.png">
									</label>
									<h5><label for="type1">Rackmount</label></h5>
								</div>
								<div class="col-xs-2">
									<input id="type2" type="radio" name="type">
									<label for="type2" class="b-search__main-type-svg">
										<img src="images/icons/icon_embedded.png">
									</label>
									<h5><label for="type2">Fanless Embedded</label></h5>
								</div>
								<div class="col-xs-2">
									<input id="type3" type="radio" name="type">
									<label for="type3" class="b-search__main-type-svg">
										<img src="images/icons/icon_panel.png">
									</label>
									<h5><label for="type3">Panel PCs</label></h5>
								</div>
								<div class="col-xs-2">
									<input id="type4" type="radio" name="type">
									<label for="type4" class="b-search__main-type-svg">
										<img src="images/icons/icon_board.png">
									</label>
									<h5><label for="type4">Boards</label></h5>
								</div>
								
								
							</div>
						</div>
					</div>
					<div class="b-search__main-form wow zoomInUp" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomInUp;">
						<div class="row">
							<div class="col-xs-12 col-md-8">
								<div class="m-firstSelects">
									<div class="col-xs-4">
										<select name="select1">
											<option value="">Any Manufacturer</option>
											<option value="ADLINK">ADLINK</option>
											<option value="Advantech">Advantech</option>
											<option value="Arestech">Arestech</option>
											<option value="GIGAIPC">GIGAIPC</option>
											<option value="Industrial PC">Industrial PC</option>
											<option value="Lex">Lex</option>
											<option value="MSI">MSI</option>
											<option value="Neousys">Neousys</option>
											<option value="Sintrones">Sintrones</option>
											<option value="Winmate">Winmate</option>
										</select>
										<span class="fa fa-caret-down"></span>
										<p>MISSING MANUFACTURER?</p>
									</div>
									<div class="col-xs-4">
										<select name="select2">
											<option value="" selected="">Any Processor</option>
											<option value="Atom">Atom</option>
											<option value="Celeron">Celeron</option>
											<option value="Pentium">Pentium</option>
											<option value="Core">Core i3/i5/i7</option>
										</select>
										<span class="fa fa-caret-down"></span>
										<p>MISSING PROCESSOR?</p>
									</div>
									<div class="col-xs-4">
										<select name="select3">
											<option value="" selected="">Vehicle Status</option>
										</select>
										<span class="fa fa-caret-down"></span>
										<p>E.G:  NEW, USED, CERTIFIED</p>
									</div>
								</div>
								<div class="m-secondSelects">
									<div class="col-xs-4">
										<select name="select4">
											<option value="" selected="">Min Year</option>
										</select>
										<span class="fa fa-caret-down"></span>
									</div>
									<div class="col-xs-4">
										<select name="select5">
											<option value="" selected="">Max Year</option>
										</select>
										<span class="fa fa-caret-down"></span>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-12 text-left s-noPadding">
								<div class="b-search__main-form-range">
									<label>PRICE RANGE</label>
									<div class="slider ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" aria-disabled="false"><div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 0%; width: 100%;"></div><a class="ui-slider-handle ui-state-default ui-corner-all" href="#" style="left: 0%;"><span class="min">100</span></a><a class="ui-slider-handle ui-state-default ui-corner-all" href="#" style="left: 100%;"><span class="max">5000</span></a></div>
									<input type="hidden" name="min" class="j-min">
									<input type="hidden" name="max" class="j-max">
								</div>
								<div class="b-search__main-form-submit">
									<a href="#">Advanced search</a>
									<button type="submit" class="btn m-btn">Search Computers<span class="fa fa-angle-right"></span></button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</section>