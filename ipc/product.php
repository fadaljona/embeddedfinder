<!DOCTYPE html>
<?php include('connect.php'); ?>

<html>
	<head>
		<meta charset="utf-8"/>
		<meta name="robots" content="noindex,nofollow">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<title>Computer Store</title>

		<link rel="shortcut icon" type="image/x-icon" href="images/favicon.png" />

		<link href="css/master.css" rel="stylesheet">

		<!-- SWITCHER -->
		<link rel="stylesheet" id="switcher-css" type="text/css" href="assets/switcher/css/switcher.css" media="all" />
		<link rel="alternate stylesheet" type="text/css" href="assets/switcher/css/color1.css" title="color1" media="all" data-default-color="true" />
		<link rel="alternate stylesheet" type="text/css" href="assets/switcher/css/color2.css" title="color2" media="all" />
		<link rel="alternate stylesheet" type="text/css" href="assets/switcher/css/color3.css" title="color3" media="all" />
		<link rel="alternate stylesheet" type="text/css" href="assets/switcher/css/color4.css" title="color4" media="all" />
		<link rel="alternate stylesheet" type="text/css" href="assets/switcher/css/color5.css" title="color5" media="all" />
		<link rel="alternate stylesheet" type="text/css" href="assets/switcher/css/color6.css" title="color6" media="all" />

		<!--[if lt IE 9]>
		<script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

	</head>
	<body class="m-detail" data-scrolling-animations="true" data-equal-height=".b-auto__main-item">

		<!-- Loader -->
		<div id="page-preloader"><span class="spinner"></span></div>
		<!-- Loader end -->

		<section class="b-modal">
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="myModalLabel">Video</h4>
						</div>
						<div class="modal-body">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/a_ugz7GoHwY" allowfullscreen></iframe>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
		</section><!--b-modal-->

		<?php include_once('header.php');?>

		<section class="b-pageHeader">
			<div class="container">
				<h1 class="wow zoomInLeft" data-wow-delay="0.5s">Vehicle Details Page</h1>
			</div>
		</section><!--b-pageHeader-->

		<div class="b-breadCumbs s-shadow wow zoomInUp" data-wow-delay="0.5s">
			<div class="container">
				<a href="home.html" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="listings.html" class="b-breadCumbs__page">Luxury Cars</a><span class="fa fa-angle-right"></span><a href="listingsTwo.html" class="b-breadCumbs__page">Nissan</a><span class="fa fa-angle-right"></span><a href="detail.html" class="b-breadCumbs__page m-active">Nissan Maxima</a>
			</div>
		</div><!--b-breadCumbs-->

		<div class="b-infoBar">
			<div class="container">
				<div class="row wow zoomInUp" data-wow-delay="0.5s">
					<div class="col-xs-3">
						<div class="b-infoBar__premium">Premium Listing</div>
					</div>
					<div class="col-xs-9">
						<div class="b-infoBar__btns">
							<a href="#" class="btn m-btn m-infoBtn">SHARE THIS VEHICLE<span class="fa fa-angle-right"></span></a>
							<a href="#" class="btn m-btn m-infoBtn">ADD TO FAVOURITES<span class="fa fa-angle-right"></span></a>
							<a href="#" class="btn m-btn m-infoBtn">PRINT THIS PAGE<span class="fa fa-angle-right"></span></a>
							<a href="#" class="btn m-btn m-infoBtn">DOWNLOAD MANUAL<span class="fa fa-angle-right"></span></a>
						</div>
					</div>
				</div>
			</div>
		</div><!--b-infoBar-->

		<?php

			if (isset($_GET['id'])) {$last_id = $_GET['id'];} else {$last_id = 40;}

			$sql = "SELECT `f_name`, `f_series`, `f_manufacturer`, `f_cpu_broad`, `f_status`, `f_source`, `f_chipset`, `f_graphics`, `f_graphics_card`, `f_max_memory`, `f_memory_slots`, `f_memory_type`, `f_memory_speed`,  `f_sata25_in`,  `f_sata25_hot`,  `f_sata35_in`, `f_m2`, `f_msata`, `f_cfast`, `f_compact_flash`, `f_raid`, `f_mdisplay`,`f_gbe`, `f_poe`, `f_usb2`, `f_usb3`, `f_rs2`, `f_rs244`, `f_dio`,  `f_vga`, `f_vga_max_res`, `f_dvi`, `f_dvi_max_res`, `f_hdmi`, `f_hdmi_max_res`, `f_dp`, `f_dp_max_res`, `f_audio_in`, `f_audio_out`, `f_pcie16`, `f_pcie8`, `f_pcie4`, `f_pcie1`, `f_mpcie`, `f_pci`, `f_sim`, `f_m2e`, `f_mezio`, `f_voltage`, `f_connector`, `f_adapter`, `f_dim_l`, `f_dim_w`, `f_dim_d`, `f_weight`, `f_optemp_min`, `f_optemp_max`, `f_sttemp_min`, `f_sttemp_max`, `f_humidity`, `f_shock`, `f_vibration`,tb2.mount_id,tb3.processor_id,tb4.cert_id,tb5.os_id, tb1.`insert_ts` FROM `comp_specs` tb1 left outer join (select comp_spec_id,GROUP_CONCAT(mount_id SEPARATOR '<br>') as mount_id from mount_link where comp_spec_id=$last_id group by comp_spec_id)tb2 on tb1.id = tb2.comp_spec_id  left outer join (select pl.comp_spec_id,group_concat(concat('Intel ',proc_family,' ', proc_number,' (',base_freq,'/',coalesce(max_freq,'-'),' GHz, ',cache,'M)',' - ',cores,' Cores') SEPARATOR '<br>') as processor_id from process_link pl left outer join dim_processor dp on pl.processor_id = dp.proc_id where pl.comp_spec_id=$last_id group by pl.comp_spec_id)tb3 on tb1.id = tb3.comp_spec_id  left outer join (select comp_spec_id,group_concat(cert_id) as cert_id from cert_link where comp_spec_id=$last_id group by comp_spec_id)tb4 on tb1.id = tb4.comp_spec_id  left outer join (select comp_spec_id,group_concat(os_id) as os_id from os_link where comp_spec_id=$last_id group by comp_spec_id)tb5 on tb1.id = tb5.comp_spec_id where tb1.id=$last_id";
			$result = $conn->query($sql);

			if ($result->num_rows > 0) {
			    // output data of each row
			    while($row = $result->fetch_assoc()) {
			    	$p_name = $row["f_name"];
			    	if ($row["f_manufacturer"] == "Iwill") {$p_manufacturer = "Industrial PC, Inc.";} else {$p_manufacturer = $row["f_manufacturer"];}
			    	$p_series = $row["f_series"];
    				$p_status = $row["f_status"];

    				$p_cpu_broad = $row["f_cpu_broad"];

    				$p_chipset = $row["f_chipset"];

					//Dimensions
					$p_dim_l = $row["f_dim_l"];
					$p_dim_w = $row["f_dim_w"];
					$p_dim_h = $row["f_dim_d"];
					$p_dim_l_in = $p_dim_l*0.0393701;
					$p_dim_w_in = $p_dim_w*0.0393701;
					$p_dim_h_in = $p_dim_h*0.0393701;
					$p_dim_t = $p_dim_l*$p_dim_w*$p_dim_h;

					$p_weight = $row["f_weight"];
					$p_weight_lb = $p_weight*2.20462;

					$p_name = $row["f_name"];
					$p_max_memory = $row["f_max_memory"];
					$p_max_type = $row["f_max_type"];

					//Display USB
			    	$p_usb2 = $row["f_usb2"];
			    	$p_usb3 = $row["f_usb3"];
			    	$p_usbt = $p_usb2 + $p_usb3;
			    	
			    	$app_id_grp = $row["app_id_grp"];

					//Display Serial
			    	$p_rs2 = $row["f_rs2"];
			    	$p_rs244 = $row["f_rs244"];
			    	$p_rst = $p_rs2 + $p_rs244;

			    	//Display Video
			    	$p_graphics = $row["f_graphics"];
			    	$p_graphics_card = $row["f_graphics_card"];
			    	$p_vga = $row["f_vga"];
			    	$p_dvi = $row["f_dvi"];
			    	$p_hdmi = $row["f_hdmi"];
			    	$p_dp = $row["f_dp"];
			    	$p_mdisplay = $row["f_mdisplay"];
			    	$p_display_tot = $p_vga + $p_dvi + $p_hdmi + $p_dp;

			    	$p_vid = "";
			    	if ($p_vga > 0){$p_vid = $p_vga."x VGA,";}
			    	if ($p_dvi > 0){$p_vid = $p_vid." ".$p_dvi."x DVI,";}
			    	if ($p_hdmi > 0){$p_vid = $p_vid." ".$p_hdmi."x HDMI,";}
			    	if ($p_dp > 0){$p_vid = $p_vid." ".$p_dp."x DisplayPort";}
			    	$p_vid = rtrim($p_vid,',');


			    	//Display Expansion
			    	$p_pcie16 = $row["f_pcie16"];
			    	$p_pcie8 = $row["f_pcie8"];
			    	$p_pcie4 = $row["f_pcie4"];
			    	$p_pcie1 = $row["f_pcie1"];
			    	$p_pciet = $p_pcie16 + $p_pcie8 + $p_pcie4 + $p_pcie1;
			    	$p_pci = $row["f_pci"];
			    	$p_mpcie = $row["f_mpcie"];
			    	$p_expansion = "";
			    	if ($p_pciet > 0){$p_expansion = $p_pciet."x PCIe,";}
			    	if ($p_mpcie > 0){$p_expansion = $p_expansion." ".$p_mpcie."x Mini PCIe,";}
			    	if ($p_pci > 0){$p_expansion = $p_expansion." ".$p_pci."x PCI";}
			    	$p_expansion = rtrim($p_expansion,',');

			    	//Display Ethernet
			    	$p_gbe = $row["f_gbe"];
			    	$p_poe = $row["f_poe"];
			    	$p_lan = "";
			    	if ($p_gbe > 0 and $p_poe > 0){
			    		$p_lan = $p_gbe."x GbE, ".$p_poe."x PoE, ";
			    	} elseif ($p_poe > 0 and $p_gbe == 0) {
			    		$p_lan = $p_poe."x PoE, ";
			    	} else {
			    		$p_lan = $p_gbe."x GbE, ";
			    	}

			    	//Storage
			    	$p_sata25_in = $row["f_sata25_in"];
			    	$p_sata25_hot = $row["f_sata25_hot"];
			    	$p_sata35_in = $row["f_sata35_in"];
			    	$p_sata25_tot = $p_sata25_in + $p_sata25_hot;
			    	$p_sata_tot = $p_sata25_in + $p_sata25_hot + $p_sata35_in;
			    	$p_msata = $row["f_msata"];
			    	$p_m2 = $row["f_m2"];
			    	$p_raid = $row["f_raid"];	
			    	$p_storage = "";
			    	if ($p_sata_tot > 0){$p_storage = $p_sata_tot."x SATA,";}
			    	if ($p_msata > 0){$p_storage = $p_storage." ".$p_msata."x mSATA,";}
			    	if ($p_m2 > 0){$p_storage = $p_storage." ".$p_m2."x m.2";}
			    	$p_storage = rtrim($p_storage,',');

			    	$p_adapter = $row["f_adapter"];
			    	if ($p_adapter == "12V5A") {
			    		$p_adapter = "12V/5A";
			    	} elseif ($p_adapter == "12V3A") {
			    		$p_adapter = "12V/5A";
			    	}

			    	$p_voltage = $row["f_voltage"];

					$p_processor_list = $row["processor_id"];

					$p_optemp_min = $row["f_optemp_min"];
					$p_optemp_max = $row["f_optemp_max"];

					$p_mount = $row["mount_id"];

					//img source
					$img_path_prod = "/img/products/".$row["f_name"].".jpg";
					$img_path_series = "/img/products/".$row["f_series"].".jpg";
					$img_path_to_display = "";

				    if (file_exists($img_path_prod)) {
				        $img_path_to_display = $img_path_prod;
				    } elseif (file_exists($img_path_series)) {
				    	$img_path_to_display = $img_path_series;
				    } else {
				        $img_path_to_display = "/img/products/no-image.jpg";
				    }


					//Display Picture
					if (file_exists ("img/products/".$row["f_name"].".jpg") == true) {
						$p_img_name = $row["f_name"].".jpg";
					} elseif (strpos($row["f_name"], 'Nuvo-3') !== false AND strpos($row["f_name"], 'LP') !== false ) {
						$p_img_name = "Nuvo-3000LP.jpg";
					} elseif (strpos($row["f_name"], 'Nuvo-5') !== false AND strpos($row["f_name"], 'LP') !== false ) {
						$p_img_name = "Nuvo-5000LP.jpg";
					} elseif (strpos($row["f_name"], 'Nuvo-7') !== false AND strpos($row["f_name"], 'LP') !== false ) {
						$p_img_name = "Nuvo-7000LP.jpg";
					}  elseif (file_exists ("img/products/".$row["f_series"].".jpg") == true) {
						$p_img_name = $row["f_series"].".jpg";
					} else {
						$p_img_name = "no-image.jpg";
					}

			    	//Bullet CPU
					$p_bullet_cpu = "";
					if (strpos($p_processor_list, '- 2 Cores') !== false) {
					    $p_bullet_cpu = substr($p_processor_list, 0, strpos($p_processor_list, "("));
					} elseif (strpos($p_processor_list, '- 4 Cores') !== false) {
					    $p_bullet_cpu = substr($p_processor_list, 0, strpos($p_processor_list, "("));
					} elseif (strpos($p_processor_list, '- 1 Cores') !== false) {
					    $p_bullet_cpu = substr($p_processor_list, 0, strpos($p_processor_list, "("));
					}
					else {
						$p_bullet_cpu = $p_processor_list;
					}

					//Bullet Ports
					$p_bullet_ports = $p_gbe."x LAN, ".$p_usbt."x USB, ".$p_rst."x Serial";

					if ($p_pciet > 0 and $p_pci == 0 and $p_mpcie == 0) {
						$p_bullets_expansion = " with PCIe expansion";
					}
					elseif ($p_pciet == 2 and $p_pci == 0 and $p_mpcie > 0) {
						$p_bullets_expansion = " with Dual PCIe and Mini-PCIe expansion";
					}
					elseif ($p_pciet > 0 and $p_pci == 0 and $p_mpcie > 0) {
						$p_bullets_expansion = " with PCIe and Mini-PCIe expansion";
					}
					elseif ($p_pciet == 0 and $p_pci > 0 and $p_mpcie > 0) {
						$p_bullets_expansion =  " with PCI and Mini-PCIe expansion";
					}
					elseif ($p_pciet > 0 and $p_pci > 0 and $p_mpcie > 0) {
						$p_bullets_expansion = " with PCI, PCIe, and Mini-PCIe expansion";
					} 
					elseif ($p_mpcie == 0 and $p_pciet == 0 and $p_pci > 0) {
						$p_bullets_expansion = " with PCI expansion";
					} 
					elseif ($p_mpcie == 0 and $p_pciet > 0 and $p_pci > 0) {
						$p_bullets_expansion = " with PCI and PCIe expansion";
					} 
					elseif ($p_mpcie > 0 and $p_pciet == 0 and $p_pci == 0) {
						$p_bullets_expansion = " with Mini-PCIe expansion";
					} 
					else {$p_bullets_expansion = "";}

					//Bullet - Chassis
					if ($p_dim_t < 1000000) {
						$p_bullet_size = "Ultra-Compact, Fanless Computer";
					} elseif ($p_dim_t < 3000000) {
						$p_bullet_size = "Compact, Fanless Chassis";
					} elseif (strpos($row["f_name"], 'LP') !== false ) {
						$p_bullet_size = "Low-Profile Fanless Computer";
					}  else {
						$p_bullet_size = "Fanless Computer";
					};

					//Bullet - Video -- Dual/Triple Display? Graphics Card
					if ($p_display_tot > 1){
						$p_bullet_video = "Multi Display (".$p_vid.")";
					} else {
						$p_bullet_video = $p_vid;
					}

					//Bullet - Storage -- Add mSATA and m.2
					if ($p_sata35_in > 0 and $p_sata_tot == 0) {
						$p_bullet_storage = $p_sata35_in."x 3.5\" SATA";
					} elseif ($p_sata_tot > 0) {
						$p_bullet_storage = $p_sata_tot."x 2.5\" SATA";
					} else {
						$p_bullet_storage =  $p_storage;
					}

					if ($p_raid == "Y") {$p_bullet_storage = $p_bullet_storage." (RAID)";}


					//Bullet - Connectivity
					if ($p_manufacturer == "Neousys" AND $p_mpcie > 1 AND $row["processor_id"] > 0) {
						$p_bullet_connectivity = "Certified for Verizon and AT&amp;T connectivity via optional cellular card";
					} elseif ($p_manufacturer == "Neousys" AND $p_mpcie =1) {
						$p_bullet_connectivity = "Certified for Verizon and AT&amp;T connectivity via optional cellular card";
					} else {
						$p_bullet_connectivity = "Optional WiFi/Bluetooth";
					}

				    //Build Product Description	
				    $prod_description = "";

				    //if ($p_dim_t > 3000000) {
				    	//$prod_description = "Compact, "	+ "Fanless PC from " + $row["f_manufacturer"];
				    //} else {
						//$prod_description = "Fanless PC from " + $row["f_manufacturer"];
				    //}

				    //Not working
				    if (strpos($row["mount_id"], "DIN") > 0) {$prod_description = "DIN Rail".$prod_description;}

				    if (!empty($row["f_series"])) {
				    	$prod_description = $p_series." Series from ".$p_manufacturer;
				    } else {
				    	$prod_description = "Fanless Computer from ".$p_manufacturer;
				    }
				    if ($p_graphics_card != "None" AND $p_graphics_card != ""){$prod_description = $prod_description." with Graphics Card";}

			    }
			}
		?>

		<section class="b-detail s-shadow">
			<div class="container">
				<header class="b-detail__head s-lineDownLeft wow zoomInUp" data-wow-delay="0.5s">
					<div class="row">
						<div class="col-sm-9 col-xs-12">
							<div class="b-detail__head-title">
								<h1><?php echo $p_name ;?></h1>
								<h3><?php echo $p_bullet_size." from ".$p_manufacturer ;?></h3>
							</div>
						</div>
						<div class="col-sm-3 col-xs-12">
							<div class="b-detail__head-price">
								<div class="b-detail__head-price-num">$2,000</div>
								<p>Includes base processor, memory, &amp; storage</p>
							</div>
						</div>
					</div>
				</header>
				<div class="b-detail__main">
					<div class="row">
						<div class="col-md-8 col-xs-12">
							<div class="b-detail__main-info">
								<div class="b-detail__main-info-images wow zoomInUp" data-wow-delay="0.5s">
									<div class="row m-smallPadding">
										<div class="col-xs-10">
											<ul class="b-detail__main-info-images-big bxslider enable-bx-slider" data-pager-custom="#bx-pager" data-mode="horizontal" data-pager-slide="true" data-mode-pager="vertical" data-pager-qty="5">
												<li class="s-relative">                                        
													<a data-toggle="modal" data-target="#myModal" href="#" class="b-items__cars-one-img-video"><span class="fa fa-film"></span>VIDEO</a>
													<img class="img-responsive center-block" src="img/products/<?php echo $p_img_name;?>" alt="nissan" />
												</li>
												<li class="s-relative">                                        
													<a data-toggle="modal" data-target="#myModal" href="#" class="b-items__cars-one-img-video"><span class="fa fa-film"></span>VIDEO</a>
													<img class="img-responsive center-block" src="media/620x485/big2.jpg" alt="nissan" />
												</li>
												<li class="s-relative">                                        
													<a data-toggle="modal" data-target="#myModal" href="#" class="b-items__cars-one-img-video"><span class="fa fa-film"></span>VIDEO</a>
													<img class="img-responsive center-block" src="media/620x485/big3.jpg" alt="nissan" />
												</li>
												<li class="s-relative">                                        
													<a data-toggle="modal" data-target="#myModal" href="#" class="b-items__cars-one-img-video"><span class="fa fa-film"></span>VIDEO</a>
													<img class="img-responsive center-block" src="media/620x485/big4.jpg" alt="nissan" />
												</li>
												<li class="s-relative">                                        
													<a data-toggle="modal" data-target="#myModal" href="#" class="b-items__cars-one-img-video"><span class="fa fa-film"></span>VIDEO</a>
													<img class="img-responsive center-block" src="media/620x485/big5.jpg" alt="nissan" />
												</li>
												<li class="s-relative">                                        
													<a data-toggle="modal" data-target="#myModal" href="#" class="b-items__cars-one-img-video"><span class="fa fa-film"></span>VIDEO</a>
													<img class="img-responsive center-block" src="media/620x485/big1.jpg" alt="nissan" />
												</li>
												<li class="s-relative">                                        
													<a data-toggle="modal" data-target="#myModal" href="#" class="b-items__cars-one-img-video"><span class="fa fa-film"></span>VIDEO</a>
													<img class="img-responsive center-block" src="media/620x485/big2.jpg" alt="nissan" />
												</li>
												<li class="s-relative">                                        
													<a data-toggle="modal" data-target="#myModal" href="#" class="b-items__cars-one-img-video"><span class="fa fa-film"></span>VIDEO</a>
													<img class="img-responsive center-block" src="media/620x485/big3.jpg" alt="nissan" />
												</li>
												
											</ul>
										</div>
										<div class="col-xs-2 pagerSlider pagerVertical">
											<div class="b-detail__main-info-images-small" id="bx-pager">
												<a href="#" data-slide-index="0" class="b-detail__main-info-images-small-one">
													<img class="img-responsive" src="media/115x85/small1.jpg" alt="nissan" />
												</a>
												<a href="#" data-slide-index="1" class="b-detail__main-info-images-small-one">
													<img class="img-responsive" src="media/115x85/small2.jpg" alt="nissan" />
												</a>
												<a href="#" data-slide-index="2" class="b-detail__main-info-images-small-one">
													<img class="img-responsive" src="media/115x85/small3.jpg" alt="nissan" />
												</a>
												<a href="#" data-slide-index="3" class="b-detail__main-info-images-small-one">
													<img class="img-responsive" src="media/115x85/small4.jpg" alt="nissan" />
												</a>
												<a href="#" data-slide-index="4" class="b-detail__main-info-images-small-one">
													<img class="img-responsive" src="media/115x85/small5.jpg" alt="nissan" />
												</a>
												<a href="#" data-slide-index="5" class="b-detail__main-info-images-small-one">
													<img class="img-responsive" src="media/115x85/small1.jpg" alt="nissan" />
												</a>
												<a href="#" data-slide-index="6" class="b-detail__main-info-images-small-one">
													<img class="img-responsive" src="media/115x85/small2.jpg" alt="nissan" />
												</a>
												<a href="#" data-slide-index="7" class="b-detail__main-info-images-small-one">
													<img class="img-responsive" src="media/115x85/small3.jpg" alt="nissan" />
												</a>
												
											</div>
										</div>
									</div>
								</div>
								<div class="b-detail__main-info-characteristics wow zoomInUp" data-wow-delay="0.5s">
									<div class="b-detail__main-info-characteristics-one">
										<div class="b-detail__main-info-characteristics-one-top">
											<div><span class="fa fa-car"></span></div>
											<p><?php echo $p_status; ?></p>
										</div>
										<div class="b-detail__main-info-characteristics-one-bottom">
											Status
										</div>
									</div>
									<div class="b-detail__main-info-characteristics-one">
										<div class="b-detail__main-info-characteristics-one-top">
											<div><span class="fa fa-trophy"></span></div>
											<p>2 Years</p>
										</div>
										<div class="b-detail__main-info-characteristics-one-bottom">
											Warranty
										</div>
									</div>
									<div class="b-detail__main-info-characteristics-one">
										<div class="b-detail__main-info-characteristics-one-top">
											<div><span class="fa fa-at"></span></div>
											<p>Processor</p>
										</div>
										<div class="b-detail__main-info-characteristics-one-bottom">
											<p><?php echo $p_cpu_broad; ?></p>
										</div>
									</div>
									<div class="b-detail__main-info-characteristics-one">
										<div class="b-detail__main-info-characteristics-one-top">
											<div><span class="fa fa-car"></span></div>
											<p>FWD</p>
										</div>
										<div class="b-detail__main-info-characteristics-one-bottom">
											Drivetrain
										</div>
									</div>
									<div class="b-detail__main-info-characteristics-one">
										<div class="b-detail__main-info-characteristics-one-top">
											<div><span class="fa fa-user"></span></div>
											<p>5</p>
										</div>
										<div class="b-detail__main-info-characteristics-one-bottom">
											Passangers
										</div>
									</div>
									<div class="b-detail__main-info-characteristics-one">
										<div class="b-detail__main-info-characteristics-one-top">
											<div><span class="fa fa-fire-extinguisher"></span></div>
											<p>10.8L</p>
										</div>
										<div class="b-detail__main-info-characteristics-one-bottom">
											In City
										</div>
									</div>
									<div class="b-detail__main-info-characteristics-one">
										<div class="b-detail__main-info-characteristics-one-top">
											<div><span class="fa fa-fire-extinguisher"></span></div>
											<p>7.5L</p>
										</div>
										<div class="b-detail__main-info-characteristics-one-bottom">
											On Highway
										</div>
									</div>
								</div>
								<div class="b-detail__main-info-text wow zoomInUp" data-wow-delay="0.5s">
									<div class="b-detail__main-aside-about-form-links">
										<a href="#" class="j-tab m-active s-lineDownCenter" data-to='#info1'>DESCRIPTION</a>
										<a href="#" class="j-tab" data-to='#info2'>SPECIFICATIONS</a>
										<a href="#" class="j-tab" data-to='#info3'>DOWNLOADS</a>
										<!--<a href="#" class="j-tab" data-to='#info4'>SCHEDULE TEST DRIVE</a>-->
									</div>
									<div id="info1">
										<p>The 2016 Nissan Maxima is powered by a 3.5-liter V6 engine with 300 horsepower, 10 more than the engine in the outgoing
											model. A continuously variable transmission and front-wheel drive are standard in all models. Nissan expects the 2016 Maxima
											to return 22/30 mpg city/highway, which is an improvement over the previous model's EPA-estimated 19/26 mpg.</p>
										<p>
											The 2016 Nissan Maxima seats five and comes with a power-adjustable driver,seat, an eight-speaker audio system, Bluetooth,
											satellite radio, HD Radio, push-button start, a rearview camera, two USB ports, the NissanConnect infotainment system,
											navigation, an 8-inch color display screen and voice controls for phone, audio and navigation functions. Leather upholstery,
											heated and ventilated front seats, an 11-speaker Bose audio system, a 360-degree parking camera system, adaptive cruise
											control, blind spot warning, rear cross traffic alert, front and rear parking sensors and forward collision warning with automatic
											braking are available. The 2016 Nissan Maxima starts at $33,235 including destination fees.</p>
										<p>The full review of the 2016 Nissan Maxima is coming soon. In the meantime, you can see pictures, research prices or view and
											compare specs for the 2016 Nissan Maxima. If you, considering the 2014 Nissan Maxima, you can read our review.</p>
										<p>Vestibulum auctor lacinia nunc. Nunc ut turpis.Sed libero magna, fermentum viverra, egestas non, fermentum sed, elit. Aenean
											erat orci, mollis quis gravida sed, mollis a, quam. Integer fermentum neque egestas orci. Nunc posuere, felis sit amet faucibus
											convallis tortor enim viverra quam, hendrerit interdum dui quam ut lacus. Donec quis quam in ante condimentum blan erdit.
											Integer et urna. Vestibulum nisl. Ut ante est, imperdiet dignissim eleifend sit amet lacinia tempor justo. Nunc ornare atm nibh.
											Fusce ut felis. </p>
										<p>Donec ullamcorper nisi ac lectus. Proin at orci. Suspendisse nec orci nec elit convallis porttitor. Praesent sit amet turpis eu nisl
											faucibus pharetra. Sed eu felis. Etiam eleifend nisl nec lectus. Ut suscipit pede eu diam. Aenean vitae quam. Cras felis. Sed utdw
											nibh. Duis libero. Vivamus pharetra libero non facilisis imperdiet mi augue feugiat nisl.</p>
									</div>
									<div id="info2">
										<p>The full review of the 2016 Nissan Maxima is coming soon. In the meantime, you can see pictures, research prices or view and
											compare specs for the 2016 Nissan Maxima. If you‚considering the 2014 Nissan Maxima, you can read our review.</p>
										<p>Vestibulum auctor lacinia nunc. Nunc ut turpis.Sed libero magna, fermentum viverra, egestas non, fermentum sed, elit. Aenean
											erat orci, mollis quis gravida sed, mollis a, quam. Integer fermentum neque egestas orci. Nunc posuere, felis sit amet faucibus
											convallis tortor enim viverra quam, hendrerit interdum dui quam ut lacus. Donec quis quam in ante condimentum blan erdit.
											Integer et urna. Vestibulum nisl. Ut ante est, imperdiet dignissim eleifend sit amet lacinia tempor justo. Nunc ornare atm nibh.
											Fusce ut felis. </p>
										<p>Donec ullamcorper nisi ac lectus. Proin at orci. Suspendisse nec orci nec elit convallis porttitor. Praesent sit amet turpis eu nisl
											faucibus pharetra. Sed eu felis. Etiam eleifend nisl nec lectus. Ut suscipit pede eu diam. Aenean vitae quam. Cras felis. Sed utdw
											nibh. Duis libero. Vivamus pharetra libero non facilisis imperdiet mi augue feugiat nisl.</p>
									</div>
									<div id="info3">
										<p>Vestibulum auctor lacinia nunc. Nunc ut turpis.Sed libero magna, fermentum viverra, egestas non, fermentum sed, elit. Aenean
											erat orci, mollis quis gravida sed, mollis a, quam. Integer fermentum neque egestas orci. Nunc posuere, felis sit amet faucibus
											convallis tortor enim viverra quam, hendrerit interdum dui quam ut lacus. Donec quis quam in ante condimentum blan erdit.
											Integer et urna. Vestibulum nisl. Ut ante est, imperdiet dignissim eleifend sit amet lacinia tempor justo. Nunc ornare atm nibh.
											Fusce ut felis. </p>
										<p>Donec ullamcorper nisi ac lectus. Proin at orci. Suspendisse nec orci nec elit convallis porttitor. Praesent sit amet turpis eu nisl
											faucibus pharetra. Sed eu felis. Etiam eleifend nisl nec lectus. Ut suscipit pede eu diam. Aenean vitae quam. Cras felis. Sed utdw
											nibh. Duis libero. Vivamus pharetra libero non facilisis imperdiet mi augue feugiat nisl.</p>
									</div>
									<!--<div id="info4">
										<p>Donec ullamcorper nisi ac lectus. Proin at orci. Suspendisse nec orci nec elit convallis porttitor. Praesent sit amet turpis eu nisl
											faucibus pharetra. Sed eu felis. Etiam eleifend nisl nec lectus. Ut suscipit pede eu diam. Aenean vitae quam. Cras felis. Sed utdw
											nibh. Duis libero. Vivamus pharetra libero non facilisis imperdiet mi augue feugiat nisl.</p>
									</div>-->
								</div>
								<div class="b-detail__main-info-extra wow zoomInUp" data-wow-delay="0.5s">
									<h2 class="s-titleDet">EXTRA FEATURES</h2>
									<div class="row">
										<div class="col-xs-4">
											<ul>
												<li><span class="fa fa-check"></span>Graphics Card</li>
												<li><span class="fa fa-check"></span>Dual Video</li>
												<li><span class="fa fa-check"></span>Triple Video</li>
												<li><span class="fa fa-check"></span>PoE Ports</li>
												<li><span class="fa fa-check"></span>M12 Connectors</li>
												<li><span class="fa fa-check"></span>Legacy I/O</li>
											</ul>
										</div>
										<div class="col-xs-4">
											<ul>
												<li><span class="fa fa-check"></span>3.5" Drive Capacity</li>
												<li><span class="fa fa-check"></span>RAID</li>
												<li><span class="fa fa-check"></span>Hot-Swappable Bays</li>
												<li><span class="fa fa-check"></span>M.2 Storage</li>
												<li><span class="fa fa-check"></span>Cellular Connectivity<!-- - AT&T or Verizon--></li>
												<li><span class="fa fa-check"></span>WiFi/Bluetooth</li>
											</ul>
										</div>
										<div class="col-xs-4">
											<ul>
												<li><span class="fa fa-check"></span>PCIe Slot</li>
												<li><span class="fa fa-check"></span>PCI Slot</li>
												<li><span class="fa fa-check"></span>Mini PCIe Slot</li>
												<li><span class="fa fa-check"></span>M.2 Expansion</li>
												<li><span class="fa fa-check"></span>DIN-Rail Mount</li>
												<li><span class="fa fa-check"></span>DIN-Rail Mount</li>
												<li><span class="fa fa-check"></span>VESA Mount</li>
												<li><span class="fa fa-check"></span>Ignition Control</li>
												<li><span class="fa fa-check"></span>Legacy OS</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-xs-12">
							<aside class="b-detail__main-aside">
								<div class="b-detail__main-aside-desc wow zoomInUp" data-wow-delay="0.5s">
									<h2 class="s-titleDet">Description</h2>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Manufacturer</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo $p_manufacturer; ?></p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Series</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo $p_series; ?></p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Processors</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo $p_bullet_cpu;?></p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Chipset</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo $p_chipset;?></p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Memory</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo "Up to ".$p_max_memory."GB ".$p_max_type;?></p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Video</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo $p_graphics;?></p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Ethernet</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value">
											<?php 
												if ($p_poe > 0) {
													echo $p_poe."x PoE";
													if ($p_gbe > 0) {echo ", ";}
												}
												if ($p_gbe > 0) {echo $p_gbe."x GbE";}
											?>
										</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">USB</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo $p_usbt;?>x USB</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Serial</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo $p_rst;?>x Serial</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Expansion</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value">
											<?php 
												if ($p_pciet > 0 ){echo $p_pciet."x PCIe";}
												if ($p_pci > 0 ){
													if ($p_pciet > 0 ){echo ", ";}
													echo $p_pci."x PCI";
												}
												if ($p_pciet < 0 and $p_pcie < 0){echo "-";}
											?>
											</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">DC Input</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo $p_voltage;?></p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Dimensions</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo number_format((float)$p_dim_l_in, 2, '.', '');?>" x <?php echo number_format((float)$p_dim_w_in, 2, '.', '');?>" x <?php echo number_format((float)$p_dim_h_in, 2, '.', '');?>"</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Mounting</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo $p_mount;?></p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6">
											<h4 class="b-detail__main-aside-desc-title">Operating Temp.</h4>
										</div>
										<div class="col-xs-6">
											<p class="b-detail__main-aside-desc-value"><?php echo $p_optemp_min." ~ ".$p_optemp_max."°C";?></p>
										</div>
									</div>
								</div>
								<div class="b-detail__main-aside-about wow zoomInUp" data-wow-delay="0.5s">
									<h2 class="s-titleDet">INQUIRE ABOUT THIS VEHICLE</h2>
									<div class="b-detail__main-aside-about-call">
										<span class="fa fa-phone"></span>
										<div>1-888-378-4027</div>
										<p>Call the seller 24/7 and they would help you.</p>
									</div>
									<div class="b-detail__main-aside-about-seller">
										<p>Seller Info: <span>NissanCarDealer</span></p>
									</div>
									<div class="b-detail__main-aside-about-form">
										<div class="b-detail__main-aside-about-form-links">
											<a href="#" class="j-tab m-active s-lineDownCenter" data-to='#form1'>REQUEST A QUOTE</a>
											<!--<a href="#" class="j-tab" data-to='#form2'>SCHEDULE TEST DRIVE</a>-->
										</div>
										<form id="form1" action="/" method="post">
											<input type="text" placeholder="YOUR NAME" value="" name="name" />
											<input type="email" placeholder="EMAIL ADDRESS" value="" name="email" />
											<input type="tel" placeholder="PHONE NO." value="" name="name" />
											<textarea name="text" placeholder="message"></textarea>
											<div><input type="checkbox" name="one" value="" /><label>Send me a copy of this message</label></div>
											<div><input type="checkbox" name="two" value="" /><label>Send me a copy of this message</label></div>
											<button type="submit" class="btn m-btn">SEND MESSAGE<span class="fa fa-angle-right"></span></button>
										</form>
										<!--
										<form id="form2" action="/" method="post">
											<input type="text" placeholder="YOUR NAME" value="" name="name" />
											<textarea name="text" placeholder="message"></textarea>
											<div><input type="checkbox" name="one" value="" /><label>Send me a copy of this message</label></div>
											<div><input type="checkbox" name="two" value="" /><label>Send me a copy of this message</label></div>
											<button type="submit" class="btn m-btn">SEND MESSAGE<span class="fa fa-angle-right"></span></button>
										</form>
										-->
									</div>
								</div>
								<div class="b-detail__main-aside-payment wow zoomInUp" data-wow-delay="0.5s">
									<h2 class="s-titleDet">CAR PAYMENT CALCULATOR</h2>
									<div class="b-detail__main-aside-payment-form">
										<form action="/" method="post">
											<input type="text" placeholder="TOTAL VALUE/LOAN AMOUNT" value="" name="name" />
											<input type="text" placeholder="DOWN PAYMENT" value="" name="name" />
											<div class="s-relative">
												<select name="select" class="m-select">
													<option value="">LOAN TERM IN MONTHS</option>
												</select>
												<span class="fa fa-caret-down"></span>
											</div>
											<input type="text" placeholder="INTEREST RATE IN %" value="" name="name" />
											<button type="submit" class="btn m-btn">ESTIMATE PAYMENT<span class="fa fa-angle-right"></span></button>
										</form>
									</div>
									<div class="b-detail__main-aside-about-call">
										<span class="fa fa-calculator"></span>
										<div>$250 <p>PER MONTH</p></div>
										<p>Total Number of Payments: <span>50</span></p>
									</div>
								</div>
							</aside>
						</div>
					</div>
				</div>
			</div>
		</section><!--b-detail-->

		<section class="b-related m-home">
			<div class="container">
				<h5 class="s-titleBg wow zoomInUp" data-wow-delay="0.5s">FIND OUT MORE</h5><br />
				<h2 class="s-title wow zoomInUp" data-wow-delay="0.5s">RELATED VEHICLES ON SALE</h2>
				<div class="row">
					<div class="col-md-3 col-xs-6">
						<div class="b-auto__main-item wow zoomInLeft" data-wow-delay="0.5s">
							<img class="img-responsive center-block"  src="media/270x150/LandRover.jpg" alt="LandRover" />
							<div class="b-world__item-val">
								<span class="b-world__item-val-title">REGISTERED <span>2014</span></span>
							</div>
							<h2><a href="detail.html">Land Rover Range Rover</a></h2>
							<div class="b-auto__main-item-info s-lineDownLeft">
								<span class="m-price">
									$44,380
								</span>
								<span class="m-number">
									<span class="fa fa-tachometer"></span>35,000 KM
								</span>
							</div>
							<div class="b-featured__item-links m-auto">
								<a href="#">Used</a>
								<a href="#">2014</a>
								<a href="#">Manual</a>
								<a href="#">Orange</a>
								<a href="#">Petrol</a>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-xs-6">
						<div class="b-auto__main-item wow zoomInLeft" data-wow-delay="0.5s">
							<img class="img-responsive center-block"  src="media/270x150/nissanGT.jpg" alt="nissan" />
							<div class="b-world__item-val">
								<span class="b-world__item-val-title">REGISTERED <span>2014</span></span>
							</div>
							<h2><a href="detail.html">Nissan GT-R NISMO</a></h2>
							<div class="b-auto__main-item-info s-lineDownLeft">
								<span class="m-price">
									$10,857
								</span>
								<span class="m-number">
									<span class="fa fa-tachometer"></span>35,000 KM
								</span>
							</div>
							<div class="b-featured__item-links m-auto">
								<a href="#">Used</a>
								<a href="#">2014</a>
								<a href="#">Manual</a>
								<a href="#">Orange</a>
								<a href="#">Petrol</a>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-xs-6">
						<div class="b-auto__main-item wow zoomInRight" data-wow-delay="0.5s">
							<img class="img-responsive center-block"  src="media/270x150/bmw.jpg" alt="bmw" />
							<div class="b-world__item-val">
								<span class="b-world__item-val-title">REGISTERED <span>2014</span></span>
							</div>
							<h2><a href="detail.html">BMW 650i Coupe</a></h2>
							<div class="b-auto__main-item-info s-lineDownLeft">
								<span class="m-price">
									$95,900
								</span>
								<span class="m-number">
									<span class="fa fa-tachometer"></span>12,000 KM
								</span>
							</div>
							<div class="b-featured__item-links m-auto">
								<a href="#">Used</a>
								<a href="#">2014</a>
								<a href="#">Manual</a>
								<a href="#">Orange</a>
								<a href="#">Petrol</a>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-xs-6">
						<div class="b-auto__main-item wow zoomInRight" data-wow-delay="0.5s">
							<img class="img-responsive center-block"  src="media/270x150/camaro.jpg" alt="camaro" />
							<div class="b-world__item-val">
								<span class="b-world__item-val-title">REGISTERED <span>2014</span></span>
							</div>
							<h2><a href="detail.html">Chevrolet Corvette Z06</a></h2>
							<div class="b-auto__main-item-info s-lineDownLeft">
								<span class="m-price">
									$95,900
								</span>
								<span class="m-number">
									<span class="fa fa-tachometer"></span>12,000 KM
								</span>
							</div>
							<div class="b-featured__item-links m-auto">
								<a href="#">Used</a>
								<a href="#">2014</a>
								<a href="#">Manual</a>
								<a href="#">Orange</a>
								<a href="#">Petrol</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section><!--"b-related-->

		<section class="b-brands s-shadow">
			<div class="container">
				<h5 class="s-titleBg wow zoomInUp" data-wow-delay="0.5s">FIND OUT MORE</h5><br />
				<h2 class="s-title wow zoomInUp" data-wow-delay="0.5s">BRANDS WE OFFER</h2>
				<div class="">
					<div class="b-brands__brand wow rotateIn" data-wow-delay="0.5s">
						<img src="media/brands/bmwLogo.png" alt="brand" />
					</div>
					<div class="b-brands__brand wow rotateIn" data-wow-delay="0.5s">
						<img src="media/brands/ferrariLogo.png" alt="brand" />
					</div>
					<div class="b-brands__brand wow rotateIn" data-wow-delay="0.5s">
						<img src="media/brands/volvo.png" alt="brand" />
					</div>
					<div class="b-brands__brand wow rotateIn" data-wow-delay="0.5s">
						<img src="media/brands/mercLogo.png" alt="brand" />
					</div>
					<div class="b-brands__brand wow rotateIn" data-wow-delay="0.5s">
						<img src="media/brands/audiLogo.png" alt="brand" />
					</div>
					<div class="b-brands__brand wow rotateIn" data-wow-delay="0.5s">
						<img src="media/brands/honda.png" alt="brand" />
					</div>
					<div class="b-brands__brand wow rotateIn" data-wow-delay="0.5s">
						<img src="media/brands/peugeot.png" alt="brand" />
					</div>
				</div>
			</div>
		</section><!--b-brands-->

		<div class="b-features">
			<div class="container">
				<div class="row">
					<div class="col-md-9 col-md-offset-3 col-xs-6 col-xs-offset-6">
						<ul class="b-features__items">
							<li class="wow zoomInUp" data-wow-delay="0.3s" data-wow-offset="100">Low Prices, No Haggling</li>
							<li class="wow zoomInUp" data-wow-delay="0.3s" data-wow-offset="100">Largest Car Dealership</li>
							<li class="wow zoomInUp" data-wow-delay="0.3s" data-wow-offset="100">Multipoint Safety Check</li>
						</ul>
					</div>
				</div>
			</div>
		</div><!--b-features-->

		<?php include_once('footer.php');?>
		<!--b-footer-->

		<!--Main-->   
		<script src="js/jquery-1.11.3.min.js"></script>
		<script src="js/jquery-ui.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/modernizr.custom.js"></script>

		<script src="assets/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
		<script src="js/waypoints.min.js"></script>
		<script src="js/jquery.easypiechart.min.js"></script>
		<script src="js/classie.js"></script>

		<!--Switcher-->
		<script src="assets/switcher/js/switcher.js"></script>
		<!--Owl Carousel-->
		<script src="assets/owl-carousel/owl.carousel.min.js"></script>
		<!--bxSlider-->
		<script src="assets/bxslider/jquery.bxslider.js"></script>
		<!-- jQuery UI Slider -->
		<script src="assets/slider/jquery.ui-slider.js"></script>

		<!--Theme-->
		<script src="js/jquery.smooth-scroll.js"></script>
		<script src="js/wow.min.js"></script>
		<script src="js/jquery.placeholder.min.js"></script>
		<script src="js/theme.js"></script>
	</body>
</html>