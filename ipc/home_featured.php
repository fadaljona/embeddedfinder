<?php 
include('connect.php'); 
include('functions.php'); 
?>

<?php
	if (isset($_GET['cores'])) {
    $ip_cores = $_GET['cores'];
		}
		else
		{
	  $ip_cores = '1,2,3,4,5,6,7,8,9,10';
		}

	if (isset($_GET['apps'])) {
    $ip_apps = $_GET['apps'];
    $app_cond = "and app_id in ('".$ip_apps."')";
		}
		else
		{	
    		$app_cond = "";
		}


	// starting code to display Input result on screen 
		$sql = "SELECT tb6.app_id_grp,tb6.app_id,tb1.`f_name`,tb1.`f_status`,tb1.`id`,tb1.`f_cpu_broad`,tb1.`f_sata35_in`, tb1.`f_sata25_hot`,tb3.`processor_id`,tb3.`cores`,tb5.`os_id`, tb1.`f_manufacturer`, tb1.`f_series`, tb1.`f_max_memory`, tb1.`f_memory_type`, tb1.`f_sata25_in`, tb1.`f_msata`, tb1.`f_m2`, tb1.`f_raid`, tb1.`f_gbe`, tb1.`f_poe`, tb1.`f_usb2`, tb1.`f_usb3`, tb1.`f_rs2`, tb1.`f_rs244`, tb1.`f_dio`, tb1.`f_can`, tb1.`f_mdisplay`, tb1.`f_vga`, tb1.`f_dvi`, tb1.`f_hdmi`, tb1.`f_dp`, tb1.`f_pcie16`, tb1.`f_pcie8`, tb1.`f_pcie4`, tb1.`f_pcie1`, tb1.`f_mpcie`, tb1.`f_pci`, tb1.`f_voltage`, tb1.`f_connector`, tb1.`f_adapter`, tb1.`f_dim_l`, tb1.`f_dim_w`, tb1.`f_dim_d`, tb1.`f_weight`, tb1.`f_optemp_min`, tb1.`f_optemp_max`, tb1.`f_sttemp_min`, tb1.`f_sttemp_max`, tb1.`f_humidity`, tb1.`f_shock`, tb1.`f_vibration`, tb1.`f_notes`, tb1.`f_cost`, tb1.`f_odoo` FROM `comp_specs` tb1 left outer join (select comp_spec_id,GROUP_CONCAT(mount_id SEPARATOR ' ') as mount_id from mount_link group by comp_spec_id)tb2 on tb1.id = tb2.comp_spec_id  left outer join (select comp_spec_id,proc_family,proc_number,cores,threads,base_freq,max_freq,cache,group_concat(concat('Intel ',proc_family,' ', proc_number,' (',base_freq,case when max_freq is not null then concat('/',max_freq) else '' end,' GHz, ',cache,'M)',' - ',cores,' Cores') SEPARATOR '<br>') as processor_id from process_link pl left outer join dim_processor dp on pl.processor_id = dp.proc_id group by comp_spec_id)tb3 on tb1.id = tb3.comp_spec_id  left outer join (select comp_spec_id,group_concat(cert_id) as cert_id from cert_link group by comp_spec_id)tb4 on tb1.id = tb4.comp_spec_id  left outer join (select comp_spec_id,group_concat(os_id) as os_id from os_link group by comp_spec_id)tb5 on tb1.id = tb5.comp_spec_id left outer join (select comp_spec_id,app_id,group_concat(app_id) as app_id_grp from app_link group by comp_spec_id)tb6 on tb1.id = tb6.comp_spec_id where tb1.id > 17 AND tb1.f_status = 'Featured' ORDER BY  tb1.`f_name`";

		$result = $conn->query($sql);
	?>

		<section class="b-featured">
			<div class="container">
				<h2 class="s-title wow zoomInUp" data-wow-delay="0.3s">Featured Computers</h2>
				<div id="carousel-small" class="owl-carousel enable-owl-carousel" data-items="4" data-navigation="true" data-auto-play="true" data-stop-on-hover="true" data-items-desktop="4" data-items-desktop-small="4" data-items-tablet="3" data-items-tablet-small="2">
					<!-- each -->
					<?php
						$total_embedded = $result->num_rows;

						if ($result->num_rows > 0) {

							while($row = $result->fetch_assoc()) {

							if ($row["f_cost"] > 0) {
								$price = round(($row["f_cost"]+140)*1.67, 0);
							} else {
								$price = round(940*1.67, 0);
							}

							$p_id = $row["id"];
							$p_name = $row["f_name"];
							$p_series = $row["f_series"];
							$p_manufacturer = $row["f_manufacturer"];
							$p_cpu_broad = $row["f_cpu_broad"];

							$p_form_factor = "Fanless";

							//Dimensions
							$p_dim_l = $row["f_dim_l"];
							$p_dim_w = $row["f_dim_w"];
							$p_dim_h = $row["f_dim_d"];
							$p_dim_t = $p_dim_l*$p_dim_w*$p_dim_h;

							//Expansion
					    	$p_pcie16 = $row["f_pcie16"];
					    	$p_pcie8 = $row["f_pcie8"];
					    	$p_pcie4 = $row["f_pcie4"];
					    	$p_pcie1 = $row["f_pcie1"];
					    	$p_pciet = $p_pcie16 + $p_pcie8 + $p_pcie4 + $p_pcie1;
					    	$p_pci = $row["f_pci"];

					    	//Graphics Card
					    	$p_graphics_card = $row["f_graphics_card"];

					    	$p_poe = $row["f_poe"];

					    	$p_mount = $row["mount_id"];



							//Display Picture
							if (file_exists ("../img/products/".$row["f_name"].".jpg") == true) {
								$p_img_name = $row["f_name"].".jpg";
							} elseif (strpos($row["f_name"], 'Nuvo-3') !== false AND strpos($row["f_name"], 'LP') !== false ) {
								$p_img_name = "Nuvo-3000LP.jpg";
							} elseif (strpos($row["f_name"], 'Nuvo-5') !== false AND strpos($row["f_name"], 'LP') !== false ) {
								$p_img_name = "Nuvo-5000LP.jpg";
							} elseif (strpos($row["f_name"], 'Nuvo-7') !== false AND strpos($row["f_name"], 'LP') !== false ) {
								$p_img_name = "Nuvo-7000LP.jpg";
							}  elseif (file_exists ("../img/products/".$row["f_series"].".jpg") == true) {
								$p_img_name = $row["f_series"].".jpg";
							} else {
								$p_img_name = "no-image.jpg";
							} 

							echo "<div>";
								echo "<div class='b-featured__item wow rotateIn' data-wow-delay='0.3s' data-wow-offset='150'>";
									echo "<a href='product.php?id=".$p_id."'>";
										echo "<img src='../img/products/".$p_img_name."' alt='".$p_name."' height='170px' />";
										echo "<span class='m-premium'>".$row["f_status"]."</span>";
									echo "</a>";
									echo "<div class='b-featured__item-price'>$".$price."</div>";
									echo "<div class='clearfix'></div>";
									echo "<h5><a href='product.php?id=".$p_id."'>".$p_name."</a></h5>";
									echo "<div class='b-featured__item-count'>";
										productDescription($p_name, $p_dim_t, $p_form_factor, $p_pciet, $p_pci, $p_graphics_card, $p_poe, $p_manufacturer, $p_mount);
									echo "</div>";
									echo "<div class='b-featured__item-links'>";
										echo "<a href='#'>".$p_cpu_broad."</a>";
										echo "<a href='#'>".$p_mount."</a>";
										//echo "<a href='#'>2014</a>";
										//echo "<a href='#'>Manual</a>";
										//echo "<a href='#'>Orange</a>";
										//echo "<a href='#'>Petrol</a>";
									echo "</div>";
								echo "</div>";
							echo "</div>";
						}
					} else {
		   				 echo "<div class='col'>0 results</div>";
					}

					$conn->close();

					?>
					<!-- each -->
				</div>
			</div>
		</section><!--b-featured-->