<!DOCTYPE html>
<?php 
include('connect.php'); 
include('functions.php'); 
?>

<html>
	<head>
		<meta charset="utf-8"/>
		<meta name="robots" content="noindex,nofollow">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<title>Computer Store</title>

		<link rel="shortcut icon" type="image/x-icon" href="images/favicon.png" />

		<link href="css/master.css" rel="stylesheet">
		<link rel="stylesheet" href="css/unifilter.css" />

		<!-- SWITCHER -->
		<link rel="stylesheet" id="switcher-css" type="text/css" href="assets/switcher/css/switcher.css" media="all" />
		<link rel="alternate stylesheet" type="text/css" href="assets/switcher/css/color1.css" title="color1" media="all" data-default-color="true" />
		<link rel="alternate stylesheet" type="text/css" href="assets/switcher/css/color2.css" title="color2" media="all" />
		<link rel="alternate stylesheet" type="text/css" href="assets/switcher/css/color3.css" title="color3" media="all" />
		<link rel="alternate stylesheet" type="text/css" href="assets/switcher/css/color4.css" title="color4" media="all" />
		<link rel="alternate stylesheet" type="text/css" href="assets/switcher/css/color5.css" title="color5" media="all" />
		<link rel="alternate stylesheet" type="text/css" href="assets/switcher/css/color6.css" title="color6" media="all" />


		<!--[if lt IE 9]>
		<script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

	</head>
	<body class="m-listTableTwo" data-scrolling-animations="true" data-equal-height=".b-items__cell">

		<!-- Loader -->
		<div id="page-preloader"><span class="spinner"></span></div>
		<!-- Loader end -->

		<section class="b-modal">
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog ">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="myModalLabel">Video</h4>
						</div>
						<div class="modal-body">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/a_ugz7GoHwY" allowfullscreen></iframe>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
		</section><!--b-modal-->

		<?php include_once('header.php');?>


		<section class="b-pageHeader">
			<div class="container">
				<h1 class="wow zoomInLeft" data-wow-delay="0.5s">Auto Listings</h1>
				<div class="b-pageHeader__search wow zoomInRight" data-wow-delay="0.5s">
					<h3>Your search returned 28 results</h3>
				</div>
			</div>
		</section><!--b-pageHeader-->

		<div class="b-breadCumbs s-shadow">
			<div class="container wow zoomInUp" data-wow-delay="0.5s">
				<a href="home.html" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="listTableTwo.html" class="b-breadCumbs__page m-active">Search Results</a>
			</div>
		</div><!--b-breadCumbs-->

		<div class="b-infoBar">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 col-xs-12">
						<div class="b-infoBar__compare wow zoomInUp" data-wow-delay="0.5s">
							<div class="dropdown">
								<a href="#" class="dropdown-toggle b-infoBar__compare-item" data-toggle='dropdown'><span class="fa fa-clock-o"></span>RECENTLY VIEWED<span class="fa fa-caret-down"></span></a>
								<ul class="dropdown-menu">
									<li><a href="#">Item</a></li>
									<li><a href="#">Item</a></li>
									<li><a href="#">Item</a></li>
									<li><a href="#">Item</a></li>
								</ul>
							</div>
							<a href="#" class="b-infoBar__compare-item"><span class="fa fa-compress"></span>COMPARE VEHICLES: 2</a>
						</div>
					</div>
					<div class="col-lg-8 col-xs-12">
						<div class="b-infoBar__select wow zoomInUp" data-wow-delay="0.5s">
							<form method="post" action="/">
								<div class="b-infoBar__select-one">
									<span class="b-infoBar__select-one-title">SELECT VIEW</span>
									<a href="listings.html" class="m-list"><span class="fa fa-list"></span></a>
									<a href="listTableTwo.html" class="m-table m-active"><span class="fa fa-table"></span></a>
								</div>
								<div class="b-infoBar__select-one">
									<span class="b-infoBar__select-one-title">SHOW ON PAGE</span>
									<select name="select1" class="m-select">
										<option value="" selected="">10 items</option>
									</select>
									<span class="fa fa-caret-down"></span>
								</div>
								<div class="b-infoBar__select-one">
									<span class="b-infoBar__select-one-title">SORT BY</span>
									<select name="select2" class="m-select">
										<option value="" selected="">Last Added</option>
									</select>
									<span class="fa fa-caret-down"></span>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div><!--b-infoBar-->

		<div class="b-items">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-sm-4 col-xs-12">
						<aside class="b-items__aside">
							<h2 class="s-title wow zoomInUp" data-wow-delay="0.5s">REFINE YOUR SEARCH</h2>
							<div class="b-items__aside-main wow zoomInUp" data-wow-delay="0.5s">
								<div id='filters' class='unifilter'></div>
								<!--
								<form>
									<div class="b-items__aside-main-body">
										<div class="b-items__aside-main-body-item">
											<label>SELECT A MAKE</label>
											<div>
												<select name="select1" class="m-select">
													<option value="" selected="">Any Make</option>
												</select>
												<span class="fa fa-caret-down"></span>
											</div>
										</div>
										<div class="b-items__aside-main-body-item">
											<label>SELECT A MODEL</label>
											<div>
												<select name="select1" class="m-select">
													<option value="" selected="">Any Make</option>
												</select>
												<span class="fa fa-caret-down"></span>
											</div>
										</div>
										<div class="b-items__aside-main-body-item">
											<label>PRICE RANGE</label>
											<div class="slider"></div>
											<input type="hidden" name="min" value="100" class="j-min" />
											<input type="hidden" name="max" value="1000" class="j-max" />
										</div>
										<div class="b-items__aside-main-body-item">
											<label>VEHICLE TYPE</label>
											<div>
												<select name="select1" class="m-select">
													<option value="" selected="">Any Type</option>
												</select>
												<span class="fa fa-caret-down"></span>
											</div>
										</div>
										<div class="b-items__aside-main-body-item">
											<label>VEHICLE STATUS</label>
											<div>
												<select name="select1" class="m-select">
													<option value="" selected="">Any Status</option>
												</select>
												<span class="fa fa-caret-down"></span>
											</div>
										</div>
										<div class="b-items__aside-main-body-item">
											<label>FUEL TYPE</label>
											<div>
												<select name="select1" class="m-select">
													<option value="" selected="">All Fuel Types</option>
												</select>
												<span class="fa fa-caret-down"></span>
											</div>
										</div>
									</div>
									<footer class="b-items__aside-main-footer">
										<button type="submit" class="btn m-btn">FILTER VEHICLES<span class="fa fa-angle-right"></span></button><br />
										<a href="">RESET ALL FILTERS</a>
									</footer>
								</form>
								-->
							</div>
							<div class="b-items__aside-sell wow zoomInUp" data-wow-delay="0.5s">
								<div class="b-items__aside-sell-img">
									<h3>REQUEST A QUOTE</h3>
								</div>
								<div class="b-items__aside-sell-info">
									<p>
										Nam tellus enimds eleifend dignis lsim
										biben edum tristique sed metus fusce
										Maecenas lobortis.
									</p>
									<a href="submit1.html" class="btn m-btn">REGISTER NOW<span class="fa fa-angle-right"></span></a>
								</div>
							</div>
						</aside>
					</div>
					<div class="col-lg-9 col-sm-8 col-xs-12">
						<div class="row m-border">

							<?php
								if (isset($_GET['cores'])) {
							    $ip_cores = $_GET['cores'];
									}
									else
									{
								  $ip_cores = '1,2,3,4,5,6,7,8,9,10';
									}

							if (isset($_GET['apps'])) {
							    $ip_apps = $_GET['apps'];
							    $app_cond = "and app_id in ('".$ip_apps."')";
									}
									else
									{	
							    $app_cond = "";
									}


								// starting code to display Input result on screen 
									$sql = "SELECT tb6.app_id_grp,tb6.app_id,tb1.`f_name`,tb1.`f_status`,tb1.`f_cpu_broad`,tb1.`id`,tb1.`f_sata35_in`, tb1.`f_sata25_hot`,tb3.`processor_id`,tb3.`cores`,tb5.`os_id`, tb1.`f_manufacturer`, tb1.`f_source`, tb1.`f_series`, tb1.`f_chipset`, tb1.`f_graphics`, tb1.`f_max_memory`, tb1.`f_memory_slots`, tb1.`f_memory_type`, tb1.`f_memory_speed`, tb1.`f_sata25_in`, tb1.`f_msata`, tb1.`f_m2`, tb1.`f_raid`, tb1.`f_gbe`, tb1.`f_poe`, tb1.`f_usb2`, tb1.`f_usb3`, tb1.`f_rs2`, tb1.`f_rs244`, tb1.`f_dio`, tb1.`f_can`, tb1.`f_mdisplay`, tb1.`f_vga`, tb1.`f_dvi`, tb1.`f_hdmi`, tb1.`f_dp`, tb1.`f_pcie16`, tb1.`f_pcie8`, tb1.`f_pcie4`, tb1.`f_pcie1`, tb1.`f_mpcie`, tb1.`f_pci`, tb1.`f_voltage`, tb1.`f_connector`, tb1.`f_adapter`, tb1.`f_dim_l`, tb1.`f_dim_w`, tb1.`f_dim_d`, tb1.`f_weight`, tb1.`f_optemp_min`, tb1.`f_optemp_max`, tb1.`f_sttemp_min`, tb1.`f_sttemp_max`, tb1.`f_humidity`, tb1.`f_shock`, tb1.`f_vibration`, tb1.`f_notes`, tb1.`f_cost`, tb1.`f_odoo` FROM `comp_specs` tb1 left outer join (select comp_spec_id,GROUP_CONCAT(mount_id SEPARATOR ' ') as mount_id from mount_link group by comp_spec_id)tb2 on tb1.id = tb2.comp_spec_id  left outer join (select comp_spec_id,proc_family,proc_number,cores,threads,base_freq,max_freq,cache,group_concat(concat('Intel ',proc_family,' ', proc_number,' (',base_freq,case when max_freq is not null then concat('/',max_freq) else '' end,' GHz, ',cache,'M)',' - ',cores,' Cores') SEPARATOR '<br>') as processor_id from process_link pl left outer join dim_processor dp on pl.processor_id = dp.proc_id group by comp_spec_id)tb3 on tb1.id = tb3.comp_spec_id  left outer join (select comp_spec_id,group_concat(cert_id) as cert_id from cert_link group by comp_spec_id)tb4 on tb1.id = tb4.comp_spec_id  left outer join (select comp_spec_id,group_concat(os_id) as os_id from os_link group by comp_spec_id)tb5 on tb1.id = tb5.comp_spec_id left outer join (select comp_spec_id,app_id,group_concat(app_id) as app_id_grp from app_link group by comp_spec_id)tb6 on tb1.id = tb6.comp_spec_id where tb1.id > 17 AND cores in ($ip_cores) $app_cond ORDER BY  tb1.`f_name`";

									$result = $conn->query($sql);
							?>

				<?php
				 while($row = $result->fetch_assoc()) {

				    	$p_status = $row["f_status"];

				    	$p_manufacturer = $row["f_manufacturer"];
				    	if ($p_manufacturer == "Iwill") {
				    		$p_manufacturer = "Industrial PC, Inc.";
				    	} else {
				    		$p_manufacturer = $p_manufacturer;
				    	}

						$p_series = $row["f_series"];
						$p_cpu_broad = $row["f_cpu_broad"];

						//Display USB
				    	$p_usb2 = $row["f_usb2"];
				    	$p_usb3 = $row["f_usb3"];
				    	$p_usbt = $p_usb2 + $p_usb3;
				    	
				    	$app_id_grp = $row["app_id_grp"];

						//Display Serial
				    	$p_rs2 = $row["f_rs2"];
				    	$p_rs244 = $row["f_rs244"];
				    	$p_rst = $p_rs2 + $p_rs244;

				    	//Display Video
				    	$p_vga = $row["f_vga"];
				    	$p_dvi = $row["f_dvi"];
				    	$p_hdmi = $row["f_hdmi"];
				    	$p_dp = $row["f_dp"];
				    	$p_mdisplay = $row["f_mdisplay"];
				    	$p_display_tot = $p_vga + $p_dvi + $p_hdmi + $p_dp;

				    	$p_vid = "";
				    	if ($p_vga > 0){$p_vid = $p_vga."x VGA,";}
				    	if ($p_dvi > 0){$p_vid = $p_vid." ".$p_dvi."x DVI,";}
				    	if ($p_hdmi > 0){$p_vid = $p_vid." ".$p_hdmi."x HDMI,";}
				    	if ($p_dp > 0){$p_vid = $p_vid." ".$p_dp."x DisplayPort";}
				    	$p_vid = rtrim($p_vid,',');


				    	//Display Expansion
				    	$p_pcie16 = $row["f_pcie16"];
				    	$p_pcie8 = $row["f_pcie8"];
				    	$p_pcie4 = $row["f_pcie4"];
				    	$p_pcie1 = $row["f_pcie1"];
				    	$p_pciet = $p_pcie16 + $p_pcie8 + $p_pcie4 + $p_pcie1;
				    	$p_pci = $row["f_pci"];
				    	$p_mpcie = $row["f_mpcie"];
				    	$p_expansion = "";
				    	if ($p_pciet > 0){$p_expansion = $p_pciet."x PCIe,";}
				    	if ($p_mpcie > 0){$p_expansion = $p_expansion." ".$p_mpcie."x Mini PCIe,";}
				    	if ($p_pci > 0){$p_expansion = $p_expansion." ".$p_pci."x PCI";}
				    	$p_expansion = rtrim($p_expansion,',');

				    	//Display Ethernet
				    	$p_gbe = $row["f_gbe"];
				    	$p_poe = $row["f_poe"];
				    	$p_lan = "";
				    	if ($p_gbe > 0 and $p_poe > 0){
				    		$p_lan = $p_gbe."x GbE, ".$p_poe."x PoE, ";
				    	} elseif ($p_poe > 0 and $p_gbe == 0) {
				    		$p_lan = $p_poe."x PoE, ";
				    	} else {
				    		$p_lan = $p_gbe."x GbE, ";
				    	}

				    	//Storage
				    	$p_sata25_in = $row["f_sata25_in"];
				    	$p_sata25_hot = $row["f_sata25_hot"];
				    	$p_sata35_in = $row["f_sata35_in"];
				    	$p_sata25_tot = $row["f_sata25_hot"];
				    	$p_sata_tot = $p_sata25_in + $p_sata25_hot;
				    	$p_msata = $row["f_msata"];
				    	$p_m2 = $row["f_m2"];
				    	$p_raid = $row["f_raid"];	
				    	$p_storage = "";
				    	if ($p_sata_tot > 0){$p_storage = $p_sata_tot."x SATA,";}
				    	if ($p_msata > 0){$p_storage = $p_storage." ".$p_msata."x mSATA,";}
				    	if ($p_m2 > 0){$p_storage = $p_storage." ".$p_m2."x m.2";}
				    	$p_storage = rtrim($p_storage,',');

				    	//$p_storage = $p_sata_tot."x SATA";

				    	$p_bullet_ports = $row["f_gbe"]."x LAN, <span class='data_usb'>".$p_usbt."</span>x USB, ".$p_rst."x Serial";

				    	//Display Processors
				    	$p_processor_list_raw = $row["processor_id"];

						if (strpos($p_processor_list_raw, 'i7-6') !== false) {
						    $p_processor_list = "Dual/Quad-Core 6th Gen. Intel® Core™ Processors";
						    $p_data_cpu = "core";
						} elseif (strpos($p_processor_list_raw, 'i7-8') !== false) {
							$p_processor_list = "Quad/Hexa-Core 8th Gen. Intel® Core™ Processors";
							$p_data_cpu = "core";
						} elseif (strpos($p_processor_list_raw, 'i7-3') !== false) {
							$p_processor_list = "Dual/Quad-Core 3rd Gen. Intel® Core™ Processors";
							$p_data_cpu = "core";
						} elseif (strpos($p_processor_list_raw, 'i7-7') !== false) {
							$p_processor_list = "Dual/Quad-Core 7th Gen. Intel® Core™ Processors";
							$p_data_cpu = "core";
						} elseif (strpos($p_processor_list_raw, 'i7-4') !== false) {
							$p_processor_list = "Dual/Quad-Core 4th Gen. Intel® Core™ Processors";
							$p_data_cpu = "core";
						} elseif (strpos($p_processor_list_raw, 'Atom') !== false) {
							$p_processor_list = str_replace("Intel Atom","Intel® Atom®",$p_processor_list_raw);
							$p_data_cpu = "atom";
						} elseif (strpos($p_processor_list_raw, 'Celeron') !== false) {
							$p_processor_list = str_replace("Intel Celeron","Intel® Celeron®",$p_processor_list_raw);
							$p_data_cpu = "celeron";
						} elseif (strpos($p_processor_list_raw, 'Pentium') !== false) {
							$p_processor_list = str_replace("Intel Pentium","Intel® Pentium®",$p_processor_list_raw);
							$p_data_cpu = "pentium";
						} 
						else {
							$p_processor_list = str_replace("Intel","Intel®",$p_processor_list_raw);
							if (strpos($p_processor_list_raw, 'i7') !== false OR strpos($p_processor_list_raw, 'i5') !== false OR strpos($p_processor_list_raw, 'i3') !== false) {
								$p_data_cpu = "core";
							} else {
								$p_data_cpu = "other";
							}							
						};

				    	//$p_processor_list = str_replace(",",", ",$row["app_id_grp"]);

				    	

						//Dimensions
						$p_dim_l = $row["f_dim_l"];
						$p_dim_w = $row["f_dim_w"];
						$p_dim_h = $row["f_dim_d"];
						$p_dim_l_in = $p_dim_l*0.0393701;
					    $p_dim_w_in = $p_dim_w*0.0393701;
					    $p_dim_h_in = $p_dim_h*0.0393701;
					    $p_dim_t = $p_dim_l*$p_dim_w*$p_dim_h;

					    $p_weight = $row["f_weight"];
					    $p_weight_lb = $p_weight*2.20462;	
					
					    $p_mounting = $row["mount_id"];
					    $p_voltage = $row["f_voltage"];


						//Display Picture
						if (file_exists ("img/products/".$row["f_name"].".jpg") == true) {
							$p_img_name = $row["f_name"].".jpg";
						} elseif (strpos($row["f_name"], 'Nuvo-3') !== false AND strpos($row["f_name"], 'LP') !== false ) {
							$p_img_name = "Nuvo-3000LP.jpg";
						} elseif (strpos($row["f_name"], 'Nuvo-5') !== false AND strpos($row["f_name"], 'LP') !== false ) {
							$p_img_name = "Nuvo-5000LP.jpg";
						} elseif (strpos($row["f_name"], 'Nuvo-7') !== false AND strpos($row["f_name"], 'LP') !== false ) {
							$p_img_name = "Nuvo-7000LP.jpg";
						}  elseif (file_exists ("img/products/".$row["f_series"].".jpg") == true) {
							$p_img_name = $row["f_series"].".jpg";
						} else {
							$p_img_name = "no-image.jpg";
						}

				    	//Bullet CPU
						$p_bullet_cpu = "";
						if (strpos($p_processor_list, '- 2 Cores') !== false) {
						    $p_bullet_cpu = "Dual-Core ".substr($p_processor_list, 0, strpos($p_processor_list, "("));
						} elseif (strpos($p_processor_list, '- 4 Cores') !== false) {
						    $p_bullet_cpu = "Quad-Core ".substr($p_processor_list, 0, strpos($p_processor_list, "("));
						} elseif (strpos($p_processor_list, '- 1 Cores') !== false) {
						    $p_bullet_cpu = "Single-Core ".substr($p_processor_list, 0, strpos($p_processor_list, "("));
						}
						else {
							$p_bullet_cpu = $p_processor_list;
						}

						//Bullet Points
						


						if ($p_rst > 0) {
							$p_bullet_ports = $p_lan.$p_usbt."x USB, ".$p_rst."x Serial";
						} else {
							$p_bullet_ports = $p_lan.$p_usbt."x USB";
						}

						if ($p_pciet > 0 and $p_pci == 0 and $p_mpcie == 0) {
							$p_bullets_expansion = " with PCIe expansion";
						}
						elseif ($p_pciet == 2 and $p_pci == 0 and $p_mpcie > 0) {
							$p_bullets_expansion = " with Dual PCIe and Mini-PCIe expansion";
						}
						elseif ($p_pciet > 0 and $p_pci == 0 and $p_mpcie > 0) {
							$p_bullets_expansion = " with PCIe and Mini-PCIe expansion";
						}
						elseif ($p_pciet == 0 and $p_pci > 0 and $p_mpcie > 0) {
							$p_bullets_expansion =  " with PCI and Mini-PCIe expansion";
						}
						elseif ($p_pciet > 0 and $p_pci > 0 and $p_mpcie > 0) {
							$p_bullets_expansion = " with PCI, PCIe, and Mini-PCIe expansion";
						} 
						elseif ($p_mpcie == 0 and $p_pciet == 0 and $p_pci > 0) {
							$p_bullets_expansion = " with PCI expansion";
						} 
						elseif ($p_mpcie == 0 and $p_pciet > 0 and $p_pci > 0) {
							$p_bullets_expansion = " with PCI and PCIe expansion";
						} 
						elseif ($p_mpcie > 0 and $p_pciet == 0 and $p_pci == 0) {
							$p_bullets_expansion = " with Mini-PCIe expansion";
						} 
						else {$p_bullets_expansion = "";}

						//Bullet - Chassis
						if ($p_dim_t < 1000000) {
							$p_bullet_size = "Ultra-Compact, Fanless Chassis";
						} elseif ($p_dim_t < 3000000) {
							$p_bullet_size = "Compact, Fanless Chassis";
						} elseif (strpos($row["f_name"], 'LP') !== false ) {
							$p_bullet_size = "Low-Profile Fanless Chassis";
						}  else {
							$p_bullet_size = "Fanless Chassis";
						};

						//Bullet - Video -- Dual/Triple Display? Graphics Card
						if ($p_display_tot > 1){
							$p_bullet_video = "Multi Display (".$p_vid.")";
						} elseif ($p_display_tot == 0) {
							$p_bullet_video = "No Video Output";
						} else {
							$p_bullet_video = $p_vid;
						}

						$p_bullet_video = str_replace("( ", "(", $p_bullet_video);

						//Bullet - Storage -- Add mSATA and m.2
						if ($p_sata35_in > 0 and $p_sata_tot == 0) {
							$p_bullet_storage = $p_sata35_in."x 3.5\" SATA";
						} elseif ($p_sata_tot > 0) {
							$p_bullet_storage = $p_sata_tot."x 2.5\" SATA";
						} elseif ($p_sata_tot == 0 AND $p_series == "UNO-1000") {
							$p_bullet_storage = "microSD slot";
						} else {
							$p_bullet_storage =  $p_storage;
						}

						if ($p_raid == "Y") {$p_bullet_storage = $p_bullet_storage." (RAID)";}

						//Application Class 
						if (strpos($row["app_id_grp"], 'In-Vehicle') !== false) {
							$app_class_string = $app_class_string + "invehicle";
						} elseif (strpos($row["app_id_grp"], 'General') !== false) {
							$app_class_string = $app_class_string + "general";
						} else {
							$app_class_string = "";
						}

						$p_data_manufacturer = $p_manufacturer;
						if ($p_manufacturer == "Industrial PC, Inc.") {
							$p_data_manufacturer = "industrial pc";
						} else {
							$p_data_manufacturer = $p_manufacturer;
						}



						echo "<div class='col-lg-4 col-md-6 col-xs-12 wow zoomInUp' data-wow-delay='0.5s' data-cpu='".$p_data_cpu."' data-manufacturer='".$p_data_manufacturer."' data-status='".strtolower($p_status)."' data-usb='".$p_usbt."' data-ethernet='".$p_gbe."' data-serial='".$p_rst."' data-sata='".$p_sata_tot."'>";
							echo "<div class='b-items__cell'>";
								echo "<div class='b-items__cars-one-img'>";
									echo "<img class='img-responsive' src='img/products/".$p_img_name."' alt='".$row["f_name"]."'/>";
									//echo "<a href='#' data-toggle='modal' data-target='#myModal' class='b-items__cars-one-img-video'><span class='fa fa-film'></span>VIDEO</a>";
									if ($p_status == "New") {
										echo "<span class='b-items__cars-one-img-type m-premium'>".$p_status."</span>";
									}
									echo "<form action='/' method='post'>";
										echo "<input type='checkbox' name='check1' id='check1'/>";
										echo "<label for='check1' class='b-items__cars-one-img-check'><span class='fa fa-check'></span></label>";
									echo "</form>";
								echo "</div>";
								echo "<div class='b-items__cell-info'>";
									echo "<div class='s-lineDownLeft b-items__cell-info-title'>";
										echo "<h2 class=''><a href='product.php?id=".$row["id"]."'>".$row["f_name"]."</a></h2>";
									echo "</div>";
									echo "<p>Description</p>";
									echo "<div>";
										echo "<div class='row m-smallPadding'>";
											echo "<div class='col-xs-5'>";
												echo "<span class='b-items__cars-one-info-title'>Processor:</span>";
												echo "<span class='b-items__cars-one-info-title'>I/O:</span>";
												echo "<span class='b-items__cars-one-info-title'>Video:</span>";
												
												echo "<span class='b-items__cars-one-info-title'>Dimensions:</span>";
											echo "</div>";
											echo "<div class='col-xs-7'>";
												echo "<span class='b-items__cars-one-info-value'>".$p_bullet_cpu."</span>";
												echo "<span class='b-items__cars-one-info-value'>".$p_bullet_ports."</span>";
												echo "<span class='b-items__cars-one-info-value'>".$p_bullet_video."</span>";
												
												echo "<span class='b-items__cars-one-info-value'>".number_format((float)$p_dim_l_in, 2, '.', '');?>" x <?php echo number_format((float)$p_dim_w_in, 2, '.', '');?>" x <?php echo number_format((float)$p_dim_h_in, 2, '.', '')."</span>";
											echo "</div>";
										echo "</div>";
									echo "</div>";
									//echo "<h5 class='b-items__cell-info-price'><span>Price:</span></h5>";
								echo "</div>";
							echo "</div>";
						echo "</div>";
	 				}
				?>


							<!--
							<div class="col-lg-4 col-md-6 col-xs-12 wow zoomInUp" data-wow-delay="0.5s">
								<div class="b-items__cell">
									<div class="b-items__cars-one-img">
										<img class='img-responsive' src="media/260x230/mersTable.jpg" alt='mers'/>
										<a data-toggle="modal" data-target="#myModal" href="#" class="b-items__cars-one-img-video"><span class="fa fa-film"></span>VIDEO</a>
										<form action="/" method="post">
											<input type="checkbox" name="check4" id='check4'/>
											<label for="check4" class="b-items__cars-one-img-check"><span class="fa fa-check"></span></label>
										</form>
									</div>
									<div class="b-items__cell-info">
										<div class="s-lineDownLeft b-items__cell-info-title">
											<h2 class=""><a href="detail.html">Mercedes-Benz GL63 AMG</a></h2>
										</div>
										<p>Lorem ipsum dolor sit amet consec let radipisicing elit, sed do eiusmod  ...</p>
										<div>
											<div class="row m-smallPadding">
												<div class="col-xs-5">
													<span class="b-items__cars-one-info-title">Body Style:</span>
													<span class="b-items__cars-one-info-title">Mileage:</span>
													<span class="b-items__cars-one-info-title">Transmission:</span>
													<span class="b-items__cars-one-info-title">Specs:</span>
												</div>
												<div class="col-xs-7">
													<span class="b-items__cars-one-info-value">Sedan</span>
													<span class="b-items__cars-one-info-value">35,000 KM</span>
													<span class="b-items__cars-one-info-value">6-Speed Auto</span>
													<span class="b-items__cars-one-info-value">2-Passenger, 2-Door</span>
												</div>
											</div>
										</div>
										<h5 class="b-items__cell-info-price"><span>Price:</span>$29,415</h5>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-6 col-xs-12 wow zoomInUp" data-wow-delay="0.5s">
								<div class="b-items__cell">
									<div class="b-items__cars-one-img">
										<img class='img-responsive' src="media/260x230/audiTable.jpg" alt='audi'/>
										<a data-toggle="modal" data-target="#myModal" href="#" class="b-items__cars-one-img-video"><span class="fa fa-film"></span>VIDEO</a>
										<span class="b-items__cars-one-img-type m-listing">NEW LISTING</span>
										<form action="/" method="post">
											<input type="checkbox" name="check5" id='check5'/>
											<label for="check5" class="b-items__cars-one-img-check"><span class="fa fa-check"></span></label>
										</form>
									</div>
									<div class="b-items__cell-info">
										<div class="s-lineDownLeft b-items__cell-info-title">
											<h2 class=""><a href="detail.html">Audi S4 3.0L V6 Turbo</a></h2>
										</div>
										<p>Lorem ipsum dolor sit amet consec let radipisicing elit, sed do eiusmod  ...</p>
										<div>
											<div class="row m-smallPadding">
												<div class="col-xs-5">
													<span class="b-items__cars-one-info-title">Body Style:</span>
													<span class="b-items__cars-one-info-title">Mileage:</span>
													<span class="b-items__cars-one-info-title">Transmission:</span>
													<span class="b-items__cars-one-info-title">Specs:</span>
												</div>
												<div class="col-xs-7">
													<span class="b-items__cars-one-info-value">Sedan</span>
													<span class="b-items__cars-one-info-value">35,000 KM</span>
													<span class="b-items__cars-one-info-value">6-Speed Auto</span>
													<span class="b-items__cars-one-info-value">2-Passenger, 2-Door</span>
												</div>
											</div>
										</div>
										<h5 class="b-items__cell-info-price"><span>Price:</span>$29,415</h5>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-6 col-xs-12 wow zoomInUp" data-wow-delay="0.5s">
								<div class="b-items__cell">
									<div class="b-items__cars-one-img">
										<img class='img-responsive' src="media/260x230/toyotaTable.jpg" alt='toyota'/>
										<a href="#" class="b-items__cars-one-img-video"><span class="fa fa-film"></span>VIDEO</a>
										<span class="b-items__cars-one-img-type m-premium">PREMIUM</span>
										<form action="/" method="post">
											<input type="checkbox" name="check6" id='check6'/>
											<label for="check6" class="b-items__cars-one-img-check"><span class="fa fa-check"></span></label>
										</form>
									</div>
									<div class="b-items__cell-info">
										<div class="s-lineDownLeft b-items__cell-info-title">
											<h2 class=""><a href="detail.html">Toyota RAV4 A Class</a></h2>
										</div>
										<p>Lorem ipsum dolor sit amet consec let radipisicing elit, sed do eiusmod  ...</p>
										<div>
											<div class="row m-smallPadding">
												<div class="col-xs-5">
													<span class="b-items__cars-one-info-title">Body Style:</span>
													<span class="b-items__cars-one-info-title">Mileage:</span>
													<span class="b-items__cars-one-info-title">Transmission:</span>
													<span class="b-items__cars-one-info-title">Specs:</span>
												</div>
												<div class="col-xs-7">
													<span class="b-items__cars-one-info-value">Sedan</span>
													<span class="b-items__cars-one-info-value">35,000 KM</span>
													<span class="b-items__cars-one-info-value">6-Speed Auto</span>
													<span class="b-items__cars-one-info-value">2-Passenger, 2-Door</span>
												</div>
											</div>
										</div>
										<h5 class="b-items__cell-info-price"><span>Price:</span>$29,415</h5>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-6 col-xs-12 wow zoomInUp" data-wow-delay="0.5s">
								<div class="b-items__cell">
									<div class="b-items__cars-one-img">
										<img class='img-responsive' src="media/260x230/jaguarTable.jpg" alt='jaguar'/>
										<a data-toggle="modal" data-target="#myModal" href="#" class="b-items__cars-one-img-video"><span class="fa fa-film"></span>VIDEO</a>
										<span class="b-items__cars-one-img-type m-owner">1 OWNER</span>
										<form action="/" method="post">
											<input type="checkbox" name="check7" id='check7'/>
											<label for="check7" class="b-items__cars-one-img-check"><span class="fa fa-check"></span></label>
										</form>
									</div>
									<div class="b-items__cell-info">
										<div class="s-lineDownLeft b-items__cell-info-title">
											<h2 class=""><a href="detail.html">Jaugar XF 250</a></h2>
										</div>
										<p>Lorem ipsum dolor sit amet consec let radipisicing elit, sed do eiusmod  ...</p>
										<div>
											<div class="row m-smallPadding">
												<div class="col-xs-5">
													<span class="b-items__cars-one-info-title">Body Style:</span>
													<span class="b-items__cars-one-info-title">Mileage:</span>
													<span class="b-items__cars-one-info-title">Transmission:</span>
													<span class="b-items__cars-one-info-title">Specs:</span>
												</div>
												<div class="col-xs-7">
													<span class="b-items__cars-one-info-value">Sedan</span>
													<span class="b-items__cars-one-info-value">35,000 KM</span>
													<span class="b-items__cars-one-info-value">6-Speed Auto</span>
													<span class="b-items__cars-one-info-value">2-Passenger, 2-Door</span>
												</div>
											</div>
										</div>
										<h5 class="b-items__cell-info-price"><span>Price:</span>$29,415</h5>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-6 col-xs-12 wow zoomInUp" data-wow-delay="0.5s">
								<div class="b-items__cell">
									<div class="b-items__cars-one-img">
										<img class='img-responsive' src="media/260x230/mercTable.jpg" alt='merc'/>
										<a data-toggle="modal" data-target="#myModal" href="#" class="b-items__cars-one-img-video"><span class="fa fa-film"></span>VIDEO</a>
										<form action="/" method="post">
											<input type="checkbox" name="check8" id='check8'/>
											<label for="check8" class="b-items__cars-one-img-check"><span class="fa fa-check"></span></label>
										</form>
									</div>
									<div class="b-items__cell-info">
										<div class="s-lineDownLeft b-items__cell-info-title">
											<h2 class=""><a href="detail.html">Mercedes Benz GL Class</a></h2>
										</div>
										<p>Lorem ipsum dolor sit amet consec let radipisicing elit, sed do eiusmod  ...</p>
										<div>
											<div class="row m-smallPadding">
												<div class="col-xs-5">
													<span class="b-items__cars-one-info-title">Body Style:</span>
													<span class="b-items__cars-one-info-title">Mileage:</span>
													<span class="b-items__cars-one-info-title">Transmission:</span>
													<span class="b-items__cars-one-info-title">Specs:</span>
												</div>
												<div class="col-xs-7">
													<span class="b-items__cars-one-info-value">Sedan</span>
													<span class="b-items__cars-one-info-value">35,000 KM</span>
													<span class="b-items__cars-one-info-value">6-Speed Auto</span>
													<span class="b-items__cars-one-info-value">2-Passenger, 2-Door</span>
												</div>
											</div>
										</div>
										<h5 class="b-items__cell-info-price"><span>Price:</span>$29,415</h5>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-6 col-xs-12 wow zoomInUp" data-wow-delay="0.5s">
								<div class="b-items__cell">
									<div class="b-items__cars-one-img">
										<img class='img-responsive' src="media/260x230/lexusTable.jpg" alt='lexus'/>
										<a data-toggle="modal" data-target="#myModal" href="#" class="b-items__cars-one-img-video"><span class="fa fa-film"></span>VIDEO</a>
										<span class="b-items__cars-one-img-type m-listing">NEW LISTING</span>
										<form action="/" method="post">
											<input type="checkbox" name="check9" id='check9'/>
											<label for="check9" class="b-items__cars-one-img-check"><span class="fa fa-check"></span></label>
										</form>
									</div>
									<div class="b-items__cell-info">
										<div class="s-lineDownLeft b-items__cell-info-title">
											<h2 class=""><a href="detail.html">Lexus LS460F Sport</a></h2>
										</div>
										<p>Lorem ipsum dolor sit amet consec let radipisicing elit, sed do eiusmod  ...</p>
										<div>
											<div class="row m-smallPadding">
												<div class="col-xs-5">
													<span class="b-items__cars-one-info-title">Body Style:</span>
													<span class="b-items__cars-one-info-title">Mileage:</span>
													<span class="b-items__cars-one-info-title">Transmission:</span>
													<span class="b-items__cars-one-info-title">Specs:</span>
												</div>
												<div class="col-xs-7">
													<span class="b-items__cars-one-info-value">Sedan</span>
													<span class="b-items__cars-one-info-value">35,000 KM</span>
													<span class="b-items__cars-one-info-value">6-Speed Auto</span>
													<span class="b-items__cars-one-info-value">2-Passenger, 2-Door</span>
												</div>
											</div>
										</div>
										<h5 class="b-items__cell-info-price"><span>Price:</span>$29,415</h5>
									</div>
								</div>
							</div>
						-->
						</div>
						<div class="b-items__pagination">
							<div class="b-items__pagination-main wow zoomInUp" data-wow-delay="0.5s">
								<a data-toggle="modal" data-target="#myModal" href="#" class="m-left"><span class="fa fa-angle-left"></span></a>
								<span class="m-active"><a href="#">1</a></span>
								<span><a href="#">2</a></span>
								<span><a href="#">3</a></span>
								<span><a href="#">4</a></span>
								<a href="#" class="m-right"><span class="fa fa-angle-right"></span></a>    
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!--b-items-->

		<div class="b-features">
			<div class="container">
				<div class="row">
					<div class="col-md-9 col-md-offset-3 col-xs-6 col-xs-offset-6">
						<ul class="b-features__items">
							<li class="wow zoomInUp" data-wow-delay="0.3s" data-wow-offset="100">Low Prices, No Haggling</li>
							<li class="wow zoomInUp" data-wow-delay="0.3s" data-wow-offset="100">Largest Car Dealership</li>
							<li class="wow zoomInUp" data-wow-delay="0.3s" data-wow-offset="100">Multipoint Safety Check</li>
						</ul>
					</div>
				</div>
			</div>
		</div><!--b-features-->

		<?php include_once('footer.php');?>
		<!--b-footer-->

		<!--Main-->   
		<script src="js/jquery-1.11.3.min.js"></script>
		<script src="js/jquery-ui.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/modernizr.custom.js"></script>

		<script src="assets/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
		<script src="js/waypoints.min.js"></script>
		<script src="js/jquery.easypiechart.min.js"></script>
		<script src="js/classie.js"></script>

		<!--Switcher-->
		<script src="assets/switcher/js/switcher.js"></script>
		<!--Owl Carousel-->
		<script src="assets/owl-carousel/owl.carousel.min.js"></script>
		<!--bxSlider-->
		<script src="assets/bxslider/jquery.bxslider.js"></script>
		<!-- jQuery UI Slider -->
		<script src="assets/slider/jquery.ui-slider.js"></script>

		<!--Theme-->
		<script src="js/jquery.smooth-scroll.js"></script>
		<script src="js/wow.min.js"></script>
		<script src="js/jquery.placeholder.min.js"></script>
		<script src="js/theme.js"></script>


	  	<script type="text/javascript" src="js/jquery.unifilter.min.js"></script>
	    <script type="text/javascript">
	  		$(document).ready(function(){
	  		
	  			// our online shop UL list
	  			var shop = $('#shop');
	  			
	  			// Masonry Options
				var masonry_options = {
		  			itemSelector: 'li:not(.uf-clone)',
		  			percentPosition: true,
		  			columnWidth: '.masonry-sizer'
		  		};

		  		var default_options = { 
		  			animationType: 'rotate',
		  			animationTime: 500
		  		};
	  		
	  			// filter options
	  			var filter_options = {
	  				"cpu": {
						title: "CPU",
						leading: true
					},
					"manufacturer" : {	
						title: 'Manufacturer',
						tooltip: true,
					},
					"status": {
						title: 'Status',
						tooltip: true,
						multiple: true
					}
				};
				
				// search options
				var search_options = {
					"name": {
						title: "Quick Search",
						placeholder: 'Eg. POC-120',
					}
				};
	 

				// range options
				var range_options = {
					"ethernet": {
						title: "Ethernet Ports",
						scale: '0-10'
					},
					"usb": {
						title: "USB Ports",
						scale: '0-10'
					},
					"serial": {
						title: "Serial Ports",
						scale: '0-10'
					},
					"sata": {
						title: "SATA Ports",
						scale: '0-4'
					}
				};

			    // sort options  
			    //var sort_options = {  
			        //title: 'Sorting',  
			        //subtitle: 'Please select an option to sort',  
			        //options: {  
			            //"status": {  
			                //label: 'Status',  
			                //order: 'DESC'  
			            //}  
			        //}  
			    //};  
				
	  			var unifilter_options = {  
			        order: "search, filters, range",
			        filters: filter_options,
	  				search: search_options,
	  				range: range_options
			    }; 



			    $('#filters').unifilter("#shop", unifilter_options);
	  		});
	  	
	  	</script>
	</body>
</html>