<!DOCTYPE html>
<?php include('connect.php'); ?>

<?php
	//Get compare IDs
	if (isset($_GET['id'])) {
    $proc_id_set = $_GET['id'];
    $array = explode(',', $proc_id_set);
    if (isset($array[0])) {
      $proc_id_1=$array[0];
  		}
      else
      {
      $proc_id_1=-1;
	}
	if (isset($array[1])) {
      $proc_id_2=$array[1];
  	} else
      {$proc_id_2=-1;}
	if (isset($array[2])) {
      $proc_id_3=$array[2];
  	} else
      {$proc_id_3=-1;}
	if (isset($array[3])) {
      $proc_id_4=$array[3];
  	} else
      {$proc_id_4=-1;}	
		}
		else
		{
	      $proc_id_1=-1;
	      $proc_id_2=-1;
	      $proc_id_3=-1;
	      $proc_id_4=-1;
		}

?>
<html>
	<head>
		<meta charset="utf-8"/>
		<meta name="robots" content="noindex,nofollow">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<title>Computer Store</title>

		<link rel="shortcut icon" type="image/x-icon" href="images/favicon.png" />

		<link href="css/master.css" rel="stylesheet">

		<!-- SWITCHER -->
		<link rel="stylesheet" id="switcher-css" type="text/css" href="assets/switcher/css/switcher.css" media="all" />
		<link rel="alternate stylesheet" type="text/css" href="assets/switcher/css/color1.css" title="color1" media="all" data-default-color="true" />
		<link rel="alternate stylesheet" type="text/css" href="assets/switcher/css/color2.css" title="color2" media="all" />
		<link rel="alternate stylesheet" type="text/css" href="assets/switcher/css/color3.css" title="color3" media="all" />
		<link rel="alternate stylesheet" type="text/css" href="assets/switcher/css/color4.css" title="color4" media="all" />
		<link rel="alternate stylesheet" type="text/css" href="assets/switcher/css/color5.css" title="color5" media="all" />
		<link rel="alternate stylesheet" type="text/css" href="assets/switcher/css/color6.css" title="color6" media="all" />

		<!--[if lt IE 9]>
		<script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

	</head>
	<body class="m-compare" data-scrolling-animations="true">

		<!-- Loader -->
		<div id="page-preloader"><span class="spinner"></span></div>
		<!-- Loader end -->

		<?php include_once('header.php');?>

		<section class="b-pageHeader">
			<div class="container">
				<h1 class=" wow zoomInLeft" data-wow-delay="0.3s">Listings</h1>
				<div class="b-pageHeader__search wow zoomInRight" data-wow-delay="0.3s">
					<h3>Compare Computers</h3>
				</div>
			</div>
		</section><!--b-pageHeader-->

		<div class="b-breadCumbs s-shadow">
			<div class="container wow zoomInUp" data-wow-delay="0.3s">
				<a href="home.html" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="compare.html" class="b-breadCumbs__page m-active">Compare Vehicles</a>
			</div>
		</div><!--b-breadCumbs-->

		<div class="b-infoBar">
			<div class="container">
				<div class="row">
					<div class="col-sm-5 col-xs-12 wow zoomInUp" data-wow-delay="0.3s">
						<h5>QUESTIONS? CALL US  :  <span>1-800- 624-5462</span></h5>
					</div>
					<div class="col-sm-7 col-xs-12">
						<div class="b-infoBar__btns wow zoomInUp" data-wow-delay="0.3s">
							<a href="#" class="btn m-btn m-infoBtn">SHARE THIS COMPARISON<span class="fa fa-angle-right"></span></a>
							<a href="#" class="btn m-btn m-infoBtn">ADD A VEHICLE<span class="fa fa-angle-right"></span></a>
						</div>
					</div>
				</div>
			</div>
		</div><!--b-infoBar-->

		
		<section class="b-compare s-shadow">
			<div class="container">
				<!--
				<div class="b-compare__images">
					<div class="row">
						<div class="col-md-3 col-sm-4 col-xs-12 col-md-offset-3">
							<div class="b-compare__images-item s-lineDownCenter wow zoomInUp" data-wow-delay="0.3s">
								<h3>Jaguar XJ 2015</h3>
								<img class="img-responsive center-block" src="media/270x180/jaguarComp.jpg" alt="jaguar" />
								<div class="b-compare__images-item-price m-right"><div class="b-compare__images-item-price-vs">vs</div>$90,600</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-4 col-xs-12 ">
							<div class="b-compare__images-item s-lineDownCenter wow zoomInUp" data-wow-delay="0.3s">
								<h3>Mercedes E-Class 2015</h3>
								<img class="img-responsive center-block" src="media/270x180/mercComp.jpg" alt="merc" />
								<div class="b-compare__images-item-price m-right m-left"><div class="b-compare__images-item-price-vs">vs</div>$52,650</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-4 col-xs-12">
							<div class="b-compare__images-item s-lineDownCenter wow zoomInUp" data-wow-delay="0.3s">
								<h3>Lexus LS 2015</h3>
								<img class="img-responsive center-block" src="media/270x180/lexusComp.jpg" alt="lexus" />
								<div class="b-compare__images-item-price m-left">$120,440</div>
							</div>
						</div>
					</div>
				</div>
				-->
				
				<?php 

					$specifications = array("Applications","Name","Processors","Manufacturer","Series","Chipset","Graphics","Max Memory","Internal 2.5 SATA","Hot-Swap 2.5 SATA","Interal 3.5 SATA","mSATA","M.2","RAID Support","Ethernet (GbE, LAN)","PoE","USB 2.0","USB 3.0","RS-232","RS-232/422/485","DIO/GPIO","CAN Bus","VGA","DVI","HDMI","DisplayPort","PCIe x16","PCIe x8","PCIe x4","PCIe x1","Mini PCIe","PCI","DC Input","DC Connector","Power Adapter","Dimensions","Weight","Operating Temp.","Storage Temp.","Humidity","Shock","Vibration", "OS", "Notes");
					$sql = "select tb1.op as col1,tb2.op as col2,tb3.op as col3 ,tb4.op as col4 from (SELECT id,n.seq, SUBSTRING_INDEX(SUBSTRING_INDEX(vals, '^', n.seq), '^', -1) op FROM (select id,concat_ws('^',coalesce(app_id_grp ,'-'),coalesce(`f_name` ,'-'),coalesce(`processor_id` ,'-'),coalesce(`f_manufacturer` ,'-'),coalesce(`f_series` ,'-'),coalesce(`f_chipset` ,'-'),coalesce(`f_graphics` ,'-'),concat(coalesce(`f_max_memory`,'-'),'GB ',coalesce(`f_memory_type`,'-')),coalesce(`f_sata25_in` ,'-'),coalesce(`f_sata25_hot` ,'-'),coalesce(`f_sata35_in` ,'-'),coalesce(`f_msata` ,'-'),coalesce(`f_m2` ,'-'),coalesce(`f_raid` ,'-'),coalesce(`f_gbe` ,'-'),coalesce(`f_poe` ,'-'),coalesce(`f_usb2` ,'-'),coalesce(`f_usb3` ,'-'),coalesce(`f_rs2` ,'-'),coalesce(`f_rs244` ,'-'),coalesce(`f_dio` ,'-'),coalesce(`f_can` ,'-'),coalesce(`f_vga` ,'-'),coalesce(`f_dvi` ,'-'),coalesce(`f_hdmi` ,'-'),coalesce(`f_dp` ,'-'),coalesce(`f_pcie16` ,'-'),coalesce(`f_pcie8` ,'-'),coalesce(`f_pcie4` ,'-'),coalesce(`f_pcie1` ,'-'),coalesce(`f_mpcie` ,'-'),coalesce(`f_pci` ,'-'),coalesce(`f_voltage` ,'-'),coalesce(`f_connector` ,'-'),coalesce(`f_adapter` ,'-'),concat(coalesce(`f_dim_l`,'-'),' mm x ',coalesce(`f_dim_w`,'-'),' mm x ',coalesce(`f_dim_d`,'-'), ' mm'),concat(coalesce(`f_weight` ,'-'), ' kg'),concat(coalesce(`f_optemp_min`,'-'),'~',coalesce(`f_optemp_max`,'-')),concat(coalesce(`f_sttemp_min`,'-'),'~',coalesce(`f_sttemp_max`,'-')),coalesce(`f_humidity` ,'-'),coalesce(`f_shock` ,'-'),coalesce(`f_vibration` ,'-'),coalesce(`os_id` ,'-'),coalesce(`f_notes`  ,'-')) as vals from vw_allspecs) tt1 INNER JOIN vw_seq n ON char_length(vals) - char_length(replace(vals, '^', '')) >= seq - 1 where id = $proc_id_1)tb1 left outer join (SELECT id,n.seq, SUBSTRING_INDEX(SUBSTRING_INDEX(vals, '^', n.seq), '^', -1) op FROM (select id,concat_ws('^',coalesce(app_id_grp ,'-'),coalesce(`f_name` ,'-'),coalesce(`processor_id` ,'-'),coalesce(`f_manufacturer` ,'-'),coalesce(`f_series` ,'-'),coalesce(`f_chipset` ,'-'),coalesce(`f_graphics` ,'-'),concat(coalesce(`f_max_memory`,'-'),'GB ',coalesce(`f_memory_type`,'-')),coalesce(`f_sata25_in` ,'-'),coalesce(`f_sata25_hot` ,'-'),coalesce(`f_sata35_in` ,'-'),coalesce(`f_msata` ,'-'),coalesce(`f_m2` ,'-'),coalesce(`f_raid` ,'-'),coalesce(`f_gbe` ,'-'),coalesce(`f_poe` ,'-'),coalesce(`f_usb2` ,'-'),coalesce(`f_usb3` ,'-'),coalesce(`f_rs2` ,'-'),coalesce(`f_rs244` ,'-'),coalesce(`f_dio` ,'-'),coalesce(`f_can` ,'-'),coalesce(`f_vga` ,'-'),coalesce(`f_dvi` ,'-'),coalesce(`f_hdmi` ,'-'),coalesce(`f_dp` ,'-'),coalesce(`f_pcie16` ,'-'),coalesce(`f_pcie8` ,'-'),coalesce(`f_pcie4` ,'-'),coalesce(`f_pcie1` ,'-'),coalesce(`f_mpcie` ,'-'),coalesce(`f_pci` ,'-'),coalesce(`f_voltage` ,'-'),coalesce(`f_connector` ,'-'),coalesce(`f_adapter` ,'-'),concat(coalesce(`f_dim_l`,'-'),' mm x ',coalesce(`f_dim_w`,'-'),' mm x ',coalesce(`f_dim_d`,'-'), ' mm'),concat(coalesce(`f_weight` ,'-'), ' kg'),concat(coalesce(`f_optemp_min`,'-'),'~',coalesce(`f_optemp_max`,'-')),concat(coalesce(`f_sttemp_min`,'-'),'~',coalesce(`f_sttemp_max`,'-')),coalesce(`f_humidity` ,'-'),coalesce(`f_shock` ,'-'),coalesce(`f_vibration` ,'-'),coalesce(`os_id` ,'-'),coalesce(`f_notes`  ,'-')) as vals from vw_allspecs) tt1 INNER JOIN vw_seq n ON char_length(vals) - char_length(replace(vals, '^', '')) >= seq - 1 where id = $proc_id_2)tb2 on tb1.seq = tb2.seq left outer join (SELECT id,n.seq, SUBSTRING_INDEX(SUBSTRING_INDEX(vals, '^', n.seq), '^', -1) op FROM (select id,concat_ws('^',coalesce(app_id_grp ,'-'),coalesce(`f_name` ,'-'),coalesce(`processor_id` ,'-'),coalesce(`f_manufacturer` ,'-'),coalesce(`f_series` ,'-'),coalesce(`f_chipset` ,'-'),coalesce(`f_graphics` ,'-'),concat(coalesce(`f_max_memory`,'-'),'GB ',coalesce(`f_memory_type`,'-')),coalesce(`f_sata25_in` ,'-'),coalesce(`f_sata25_hot` ,'-'),coalesce(`f_sata35_in` ,'-'),coalesce(`f_msata` ,'-'),coalesce(`f_m2` ,'-'),coalesce(`f_raid` ,'-'),coalesce(`f_gbe` ,'-'),coalesce(`f_poe` ,'-'),coalesce(`f_usb2` ,'-'),coalesce(`f_usb3` ,'-'),coalesce(`f_rs2` ,'-'),coalesce(`f_rs244` ,'-'),coalesce(`f_dio` ,'-'),coalesce(`f_can` ,'-'),coalesce(`f_vga` ,'-'),coalesce(`f_dvi` ,'-'),coalesce(`f_hdmi` ,'-'),coalesce(`f_dp` ,'-'),coalesce(`f_pcie16` ,'-'),coalesce(`f_pcie8` ,'-'),coalesce(`f_pcie4` ,'-'),coalesce(`f_pcie1` ,'-'),coalesce(`f_mpcie` ,'-'),coalesce(`f_pci` ,'-'),coalesce(`f_voltage` ,'-'),coalesce(`f_connector` ,'-'),coalesce(`f_adapter` ,'-'),concat(coalesce(`f_dim_l`,'-'),' mm x ',coalesce(`f_dim_w`,'-'),' mm x ',coalesce(`f_dim_d`,'-'), ' mm'),concat(coalesce(`f_weight` ,'-'), ' kg'),concat(coalesce(`f_optemp_min`,'-'),'~',coalesce(`f_optemp_max`,'-')),concat(coalesce(`f_sttemp_min`,'-'),'~',coalesce(`f_sttemp_max`,'-')),coalesce(`f_humidity` ,'-'),coalesce(`f_shock` ,'-'),coalesce(`f_vibration` ,'-'),coalesce(`os_id` ,'-'),coalesce(`f_notes`  ,'-')) as vals from vw_allspecs) tt1 INNER JOIN vw_seq n ON char_length(vals) - char_length(replace(vals, '^', '')) >= seq - 1 where id = $proc_id_3)tb3 on tb1.seq = tb3.seq left outer join (SELECT id,n.seq, SUBSTRING_INDEX(SUBSTRING_INDEX(vals, '^', n.seq), '^', -1) op FROM (select id,concat_ws('^',coalesce(app_id_grp ,'-'),coalesce(`f_name` ,'-'),coalesce(`processor_id` ,'-'),coalesce(`f_manufacturer` ,'-'),coalesce(`f_series` ,'-'),coalesce(`f_chipset` ,'-'),coalesce(`f_graphics` ,'-'),concat(coalesce(`f_max_memory`,'-'),'GB ',coalesce(`f_memory_type`,'-')),coalesce(`f_sata25_in` ,'-'),coalesce(`f_sata25_hot` ,'-'),coalesce(`f_sata35_in` ,'-'),coalesce(`f_msata` ,'-'),coalesce(`f_m2` ,'-'),coalesce(`f_raid` ,'-'),coalesce(`f_gbe` ,'-'),coalesce(`f_poe` ,'-'),coalesce(`f_usb2` ,'-'),coalesce(`f_usb3` ,'-'),coalesce(`f_rs2` ,'-'),coalesce(`f_rs244` ,'-'),coalesce(`f_dio` ,'-'),coalesce(`f_can` ,'-'),coalesce(`f_vga` ,'-'),coalesce(`f_dvi` ,'-'),coalesce(`f_hdmi` ,'-'),coalesce(`f_dp` ,'-'),coalesce(`f_pcie16` ,'-'),coalesce(`f_pcie8` ,'-'),coalesce(`f_pcie4` ,'-'),coalesce(`f_pcie1` ,'-'),coalesce(`f_mpcie` ,'-'),coalesce(`f_pci` ,'-'),coalesce(`f_voltage` ,'-'),coalesce(`f_connector` ,'-'),coalesce(`f_adapter` ,'-'),concat(coalesce(`f_dim_l`,'-'),' mm x ',coalesce(`f_dim_w`,'-'),' mm x ',coalesce(`f_dim_d`,'-'), ' mm'),concat(coalesce(`f_weight` ,'-'), ' kg'),concat(coalesce(`f_optemp_min`,'-'),'~',coalesce(`f_optemp_max`,'-')),concat(coalesce(`f_sttemp_min`,'-'),'~',coalesce(`f_sttemp_max`,'-')),coalesce(`f_humidity` ,'-'),coalesce(`f_shock` ,'-'),coalesce(`f_vibration` ,'-'),coalesce(`os_id` ,'-'),coalesce(`f_notes`  ,'-')) as vals from vw_allspecs) tt1 INNER JOIN vw_seq n ON char_length(vals) - char_length(replace(vals, '^', '')) >= seq - 1 where id = $proc_id_4)tb4 on tb1.seq = tb4.seq order by tb1.seq";

						$result = $conn->query($sql);
						if ($result->num_rows > 0) {

							echo "<div class='b-compare__block-inside j-inside'>";

							$cntr_specs=0;
							while($row = $result->fetch_assoc()) {

								//if ($row["col1"] != "-" AND $row["col2"] != "-" AND $row["col3"] != "-" AND $row["col4"] != "-") {

									if ($specifications[$cntr_specs] == "Applications"){
										$table_title = "GENERAL";
									} elseif ($specifications[$cntr_specs] == "Internal 2.5 SATA"){
										$table_title = "STORAGE";
									} elseif ($specifications[$cntr_specs] == "Ethernet"){
										$table_title = "I/O";
									} elseif ($specifications[$cntr_specs] == "VGA"){
										$table_title = "VIDEO";
									} elseif ($specifications[$cntr_specs] == "PCIe x16"){
										$table_title = "EXPANSION";
									} elseif($specifications[$cntr_specs] == "DC Input"){
										$table_title = "POWER";
									} elseif ($specifications[$cntr_specs] == "Dimensions"){
										$table_title = "MECHANICAL";
									} elseif ($specifications[$cntr_specs] == "OS"){
										$table_title = "OPERATING SYSTEM";
									}

									//if ($specifications[$cntr_specs] == "Applications" OR $specifications[$cntr_specs] == "Internal 2.5 SATA" OR $specifications[$cntr_specs] == "Ethernet" OR $specifications[$cntr_specs] == "VGA" OR $specifications[$cntr_specs] == "PCIe x16" OR $specifications[$cntr_specs] == "DC Input" OR $specifications[$cntr_specs] == "Dimensions" OR $specifications[$cntr_specs] == "OS") {
										//echo "<div class='b-compare__block wow zoomInUp' data-wow-delay='0.3s'>";
											//echo "<div class='b-compare__block-title s-whiteShadow'>";
												//echo "<h3 class='s-titleDet'>".$table_title."</h3>";
												//echo "<a class='j-more' href='#'><span class='fa fa-angle-left'></span></a>";
											//echo "</div>";

											
										//}

												echo "<div class='row'>";
													echo "<div class='col-xs-3'><div class='b-compare__block-inside-title'>".$specifications[$cntr_specs]."</div></div>";
													echo "<div class='col-xs-3'><div class='b-compare__block-inside-value'>".str_replace(",", ", ", $row["col1"])."</div></div>";
													echo "<div class='col-xs-3'><div class='b-compare__block-inside-value'>".str_replace(",", ", ", $row["col2"])."</div></div>";
													if ($row["col3"] != "") {
													echo "<div class='col-xs-3'><div class='b-compare__block-inside-value'>".str_replace(",", ", ", $row["col3"])."</div></div>";
													}
													if ($row["col4"] != "") {
													echo "<div class='col-xs-3'><div class='b-compare__block-inside-value'>".str_replace(",", ", ", $row["col4"])."</div></div>";
													}
												echo "</div>";

									//if ($specifications[$cntr_specs] == "Applications" OR $specifications[$cntr_specs] == "Internal 2.5 SATA" OR $specifications[$cntr_specs] == "Ethernet" OR $specifications[$cntr_specs] == "VGA" OR $specifications[$cntr_specs] == "PCIe x16" OR $specifications[$cntr_specs] == "DC Input" OR $specifications[$cntr_specs] == "Dimensions" OR $specifications[$cntr_specs] == "OS") {
											//echo "</div>";
											
									//}
								//}
								$cntr_specs++;  
								echo "</div>";
							}
							//echo "</div>";
						} else {
							echo $result->num_rows." results";
						}

						$conn->close();

				?>
				</div>

				<div class="b-compare__block wow zoomInUp" data-wow-delay="0.3s">
					<div class="b-compare__block-title s-whiteShadow">
						<h3 class="s-titleDet">BASIC INFO</h3>
						<a class="j-more" href="#"><span class="fa fa-angle-left"></span></a>
					</div>
					<div class="b-compare__block-inside j-inside">
						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Make / Year
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									Jaugar 2015
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									Mercedez-Benz 2015
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									Lexus 2015
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Front Head Room
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									39.5 in
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									38.0 in
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									37.9 in
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Width / Height / Length
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									74.8 in  /  57.3 in  /  201.9 in
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									73.0 in  /  57.0 in  /  192.5 in
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									56.4 in  /  73.8 in  /  205.0 in
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Wheel Base
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									119.0 in
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									113.5 in
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									121.6 in
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-3">
								<div class="b-compare__block-inside-title">
									Cargo capacity, all seats in place
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									15.2 cu.ft.
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									12.9 cu.ft.
								</div>
							</div>
							<div class="col-xs-3">
								<div class="b-compare__block-inside-value">
									13.0 cu.ft.
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section><!--b-compare-->

		<div class="b-features">
			<div class="container">
				<div class="row">
					<div class="col-md-9 col-md-offset-3 col-xs-6 col-xs-offset-6">
						<ul class="b-features__items">
							<li class="wow zoomInUp" data-wow-delay="0.3s" data-wow-offset="100">Low Prices, No Haggling</li>
							<li class="wow zoomInUp" data-wow-delay="0.3s" data-wow-offset="100">Largest Car Dealership</li>
							<li class="wow zoomInUp" data-wow-delay="0.3s" data-wow-offset="100">Multipoint Safety Check</li>
						</ul>
					</div>
				</div>
			</div>
		</div><!--b-features-->

		<?php include_once('footer.php');?>
		<!--b-footer-->
		
		<!--Main-->   
		<script src="js/jquery-1.11.3.min.js"></script>
		<script src="js/jquery-ui.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/modernizr.custom.js"></script>

		<script src="assets/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
		<script src="js/waypoints.min.js"></script>
		<script src="js/jquery.easypiechart.min.js"></script>
		<script src="js/classie.js"></script>

		<!--Switcher-->
		<script src="assets/switcher/js/switcher.js"></script>
		<!--Owl Carousel-->
		<script src="assets/owl-carousel/owl.carousel.min.js"></script>
		<!--bxSlider-->
		<script src="assets/bxslider/jquery.bxslider.js"></script>
		<!-- jQuery UI Slider -->
		<script src="assets/slider/jquery.ui-slider.js"></script>

		<!--Theme-->
		<script src="js/jquery.smooth-scroll.js"></script>
		<script src="js/wow.min.js"></script>
		<script src="js/jquery.placeholder.min.js"></script>
		<script src="js/theme.js"></script>
	</body>
</html>