<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<meta name="robots" content="noindex,nofollow">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<title>Computer Store</title>

		<link rel="shortcut icon" type="image/x-icon" href="images/favicon.png" />

		<link href="css/master.css" rel="stylesheet">

		<!-- SWITCHER -->
		<link rel="stylesheet" id="switcher-css" type="text/css" href="assets/switcher/css/switcher.css" media="all" />
		<link rel="alternate stylesheet" type="text/css" href="assets/switcher/css/color1.css" title="color1" media="all" data-default-color="true" />
		<link rel="alternate stylesheet" type="text/css" href="assets/switcher/css/color2.css" title="color2" media="all" />
		<link rel="alternate stylesheet" type="text/css" href="assets/switcher/css/color3.css" title="color3" media="all" />
		<link rel="alternate stylesheet" type="text/css" href="assets/switcher/css/color4.css" title="color4" media="all" />
		<link rel="alternate stylesheet" type="text/css" href="assets/switcher/css/color5.css" title="color5" media="all" />
		<link rel="alternate stylesheet" type="text/css" href="assets/switcher/css/color6.css" title="color6" media="all" />

		<!--[if lt IE 9]>
		<script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

	</head>
	<body class="m-index" data-scrolling-animations="true" data-equal-height=".b-auto__main-item">

		<!-- Loader -->
		<div id="page-preloader"><span class="spinner"></span></div>
		<!-- Loader end -->

		<?php include_once('header.php');?>

		<section class="b-slider"> 
			<div id="carousel" class="slide carousel carousel-fade">
				<div class="carousel-inner">
					<div class="item active">
						<img src="media/main-slider/1.jpg" alt="sliderImg" />
						<div class="container">
							<div class="carousel-caption b-slider__info">
								<h3>Find your dream</h3>
								<h2>Lamborghini Aventador</h2>
								<p>Model 2015 <span>$184,900</span></p>
								<a class="btn m-btn" href="detail.html">see details<span class="fa fa-angle-right"></span></a>
							</div>
						</div>
					</div>
					<div class="item">
						<img src="media/main-slider/2.jpg" alt="sliderImg" />
						<div class="container">
							<div class="carousel-caption b-slider__info">
								<h3>Find your dream</h3>
								<h2>Lamborghini Aventador</h2>
								<p>Model 2015 <span>$184,900</span></p>
								<a class="btn m-btn" href="detail.html">see details<span class="fa fa-angle-right"></span></a>
							</div>
						</div>
					</div>
					<div class="item">
						<img src="media/main-slider/3.jpg"  alt="sliderImg"/>
						<div class="container">
							<div class="carousel-caption b-slider__info">
								<h3>Find your dream</h3>
								<h2>Lamborghini Aventador</h2>
								<p>Model 2015 <span>$184,900</span></p>
								<a class="btn m-btn" href="detail.html">see details<span class="fa fa-angle-right"></span></a>
							</div>
						</div>
					</div>
				</div>
				<a class="carousel-control right" href="#carousel" data-slide="next">
					<span class="fa fa-angle-right m-control-right"></span>
				</a>
				<a class="carousel-control left" href="#carousel" data-slide="prev">
					<span class="fa fa-angle-left m-control-left"></span>
				</a>
			</div>
		</section><!--b-slider-->

		<?php include_once('home_search.php');?>

		<?php include_once('home_featured.php');?>

		<section class="b-welcome">
			<div class="container">
				<div class="row">
					<div class="col-md-5 col-md-offset-2 col-sm-6 col-xs-12">
						<div class="b-welcome__text wow fadeInLeft" data-wow-delay="0.3s" data-wow-offset="100">
							<h2>WORLD'S LEADING CAR DEALER</h2>
							<h3>WELCOME TO AUTOCLUB</h3>
							<p>Curabitur libero. Donec facilisis velit eudsl est. Phasellus consequat. Aenean vita quam. Vivamus et nunc. Nunc consequat sem velde metus imperdiet lacinia. Dui estter neque molestie necd dignissim ac hendrerit quis purus. Etiam sit amet vec convallis massa scelerisque mattis. Sed placerat leo nec.</p>
							<p>Ipsum midne ultrices magn eu tempor quam dolor eustrl sem. Donec quis dolel Donec pede quam placerat alterl tristique faucibus posuere lobortis.</p>
							<ul>
								<li><span class="fa fa-check"></span>Donec facilisis velit eu est phasellus consequat </li>
								<li><span class="fa fa-check"></span>Aenean vitae quam. Vivamus et nunc nunc consequat</li>
								<li><span class="fa fa-check"></span>Sem vel metus imperdiet lacinia enean </li>
								<li><span class="fa fa-check"></span>Dapibus aliquam augue fusce eleifend quisque tels</li>
							</ul>
						</div>
					</div>
					<div class="col-md-5 col-sm-6 col-xs-12">
						<div class="b-welcome__services wow fadeInRight" data-wow-delay="0.3s" data-wow-offset="100">
							<div class="row">
								<div class="col-xs-6 m-padding">
									<div class="b-welcome__services-auto">
										<div class="b-welcome__services-img m-auto">
											<span class="fa fa-cab"></span>
										</div>
										<h3>AUTO LOANS</h3>
									</div>
								</div>
								<div class="col-xs-6 m-padding">
									<div class="b-welcome__services-trade">
										<div class="b-welcome__services-img m-trade">
											<span class="fa fa-male"></span>
										</div>
										<h3>Trade-Ins</h3>
									</div>
								</div>
								<div class="col-xs-12 text-center">
									<span class="b-welcome__services-circle"></span>
								</div>
								<div class="col-xs-6 m-padding">
									<div class="b-welcome__services-buying">
										<div class="b-welcome__services-img m-buying">
											<span class="fa fa-book"></span>
										</div>
										<h3>Buying guide</h3>
									</div>
								</div>
								<div class="col-xs-6 m-padding">
									<div class="b-welcome__services-support">
										<div class="b-welcome__services-img m-support">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												width="45px" height="45px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
												<g>
													<path d="M257.938,336.072c0,17.355-14.068,31.424-31.423,31.424c-17.354,0-31.422-14.068-31.422-31.424
														c0-17.354,14.068-31.423,31.422-31.423C243.87,304.65,257.938,318.719,257.938,336.072z M385.485,304.65
														c-17.354,0-31.423,14.068-31.423,31.424c0,17.354,14.069,31.422,31.423,31.422c17.354,0,31.424-14.068,31.424-31.422
														C416.908,318.719,402.84,304.65,385.485,304.65z M612,318.557v59.719c0,29.982-24.305,54.287-54.288,54.287h-39.394
														C479.283,540.947,379.604,606.412,306,606.412s-173.283-65.465-212.318-173.85H54.288C24.305,432.562,0,408.258,0,378.275v-59.719
														c0-20.631,11.511-38.573,28.46-47.758c0.569-84.785,25.28-151.002,73.553-196.779C149.895,28.613,218.526,5.588,306,5.588
														c87.474,0,156.105,23.025,203.987,68.43c48.272,45.777,72.982,111.995,73.553,196.779C600.489,279.983,612,297.925,612,318.557z
														M497.099,336.271c0-13.969-0.715-27.094-1.771-39.812c-24.093-22.043-67.832-38.769-123.033-44.984
														c7.248,8.15,13.509,18.871,17.306,32.983c-33.812-26.637-100.181-20.297-150.382-79.905c-2.878-3.329-5.367-6.51-7.519-9.417
														c-0.025-0.035-0.053-0.062-0.078-0.096l0.006,0.002c-8.931-12.078-11.976-19.262-12.146-11.31
														c-1.473,68.513-50.034,121.925-103.958,129.46c-0.341,7.535-0.62,15.143-0.62,23.08c0,28.959,4.729,55.352,12.769,79.137
														c30.29,36.537,80.312,46.854,124.586,49.59c8.219-13.076,26.66-22.205,48.136-22.205c29.117,0,52.72,16.754,52.72,37.424
														c0,20.668-23.604,37.422-52.72,37.422c-22.397,0-41.483-9.93-49.122-23.912c-30.943-1.799-64.959-7.074-95.276-21.391
														C198.631,535.18,264.725,568.41,306,568.41C370.859,568.41,497.099,486.475,497.099,336.271z M550.855,264.269
														C547.4,116.318,462.951,38.162,306,38.162S64.601,116.318,61.145,264.269h20.887c7.637-49.867,23.778-90.878,48.285-122.412
														C169.37,91.609,228.478,66.13,306,66.13c77.522,0,136.63,25.479,175.685,75.727c24.505,31.533,40.647,72.545,48.284,122.412
														H550.855L550.855,264.269z"/>
												</g>
											</svg>

										</div>
										<h3>24/7 support</h3>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section><!--b-welcome-->

		<section class="b-world">
			<div class="container">
				<h6 class="wow zoomInLeft" data-wow-delay="0.3s" data-wow-offset="100">EVERYTHING YOU NEED TO KNOW</h6><br />
				<h2 class="s-title wow zoomInRight" data-wow-delay="0.3s" data-wow-offset="100">THE WORLD OF AUTOS</h2>
				<div class="row">
					<div class="col-sm-4 col-xs-12">
						<div class="b-world__item wow zoomInLeft" data-wow-delay="0.3s" data-wow-offset="100">
							<img class="img-responsive" src="media/370x200/wolks.jpg" alt="wolks" />
							<div class="b-world__item-val">
								<span class="b-world__item-val-title">FIRST DRIVE REVIEW</span>
								<div class="b-world__item-val-circles">
									<span></span>
									<span></span>
									<span></span>
									<span></span>
									<span class="m-empty"></span>
								</div>
								<span class="b-world__item-num">4.1</span>
							</div>
							<h2>2016 Volkswagen Golf R SportWagen</h2>
							<p>Curabitur libero. Donec facilisis velit eu est. Phasellus cons quat. Aenean vitae quam. Vivamus et nunc. Nunc consequ
								sem velde metus imperdiet lacinia.</p>
							<a href="article.html" class="btn m-btn">READ MORE<span class="fa fa-angle-right"></span></a>
						</div>
					</div>
					<div class="col-sm-4 col-xs-12">
						<div class="b-world__item wow zoomInUp" data-wow-delay="0.3s" data-wow-offset="100">
							<img class="img-responsive"  src="media/370x200/mazda.jpg" alt="mazda" />
							<div class="b-world__item-val">
								<span class="b-world__item-val-title">INSTRUMENTED TEST</span>
								<div class="b-world__item-val-circles">
									<span></span>
									<span></span>
									<span></span>
									<span></span>
									<span class="m-halfEmpty"></span>
								</div>
								<span class="b-world__item-num">4.5</span>
							</div>
							<h2>2016 Mazda CX-5 2.5L AWD</h2>
							<p>Curabitur libero. Donec facilisis velit eu est. Phasellus cons quat. Aenean vitae quam. Vivamus et nunc. Nunc consequ
								sem velde metus imp         erdiet lacinia.</p>
							<a href="article.html" class="btn m-btn m-active">READ MORE<span class="fa fa-angle-right"></span></a>
						</div>
					</div>
					<div class="col-sm-4 col-xs-12">
						<div class="b-world__item j-item wow zoomInRight" data-wow-delay="0.3s" data-wow-offset="100">
							<img class="img-responsive"  src="media/370x200/chevrolet.jpg" alt="chevrolet" />
							<div class="b-world__item-val">
								<span class="b-world__item-val-title">BUYERS INFO</span>
								<div class="b-world__item-val-circles">
									<span></span>
									<span></span>
									<span></span>
									<span></span>
									<span></span>
								</div>
								<span class="b-world__item-num">5.0</span>
							</div>
							<h2>Advantages of Buying New or Used Vehicle</h2>
							<p>Curabitur libero. Donec facilisis velit eu est. Phasellus cons quat. Aenean vitae quam. Vivamus et nunc. Nunc consequ
								sem velde metus imp         erdiet lacinia.</p>
							<a href="article.html" class="btn m-btn">READ MORE<span class="fa fa-angle-right"></span></a>
						</div>
					</div>
				</div>
			</div>
		</section><!--b-world-->

		<section class="b-asks">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-10 col-sm-offset-1 col-md-offset-0 col-xs-12">
						<div class="b-asks__first wow zoomInLeft" data-wow-delay="0.3s" data-wow-offset="100">
							<div class="b-asks__first-circle">
								<span class="fa fa-search"></span>
							</div>
							<div class="b-asks__first-info">
								<h2>ARE YOU LOOKING FOR A CAR?</h2>
								<p>Search Our Inventory With Thousands Of Cars  And More 
									Cars Are Adding On Daily Basis</p>
							</div>
							<div class="b-asks__first-arrow">
								<a href="listingsTwo.html"><span class="fa fa-angle-right"></span></a>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-sm-10 col-sm-offset-1 col-xs-12 col-md-offset-0">
						<div class="b-asks__first m-second wow zoomInRight" data-wow-delay="0.3s" data-wow-offset="100">
							<div class="b-asks__first-circle">
								<span class="fa fa-usd"></span>
							</div>
							<div class="b-asks__first-info">
								<h2>DO YOU WANT TO SELL A CAR?</h2>
								<p>Search Our Inventory With Thousands Of Cars  And More 
									Cars Are Adding On Daily Basis</p>
							</div>
							<div class="b-asks__first-arrow">
								<a href="listingsTwo.html"><span class="fa fa-angle-right"></span></a>
							</div>
						</div>
					</div>
					<div class="col-xs-12">
						<p class="b-asks__call wow zoomInUp" data-wow-delay="0.3s" data-wow-offset="100">QUESTIONS? CALL US  : <span>1-800- 624-5462</span></p>
					</div>
				</div>
			</div>
		</section><!--b-asks-->

		<?php include_once('home_auto.php');?>

		<section class="b-count">
			<div class="container">
				<div class="row">
					<div class="col-md-11 col-xs-12 percent-blocks m-main" data-waypoint-scroll="true">
						<div class="row">
							<div class="col-sm-3 col-xs-6">
								<div class="b-count__item">
									<div class="b-count__item-circle">
										<span class="fa fa-car"></span>
									</div>
									<div class="chart" data-percent="5000">
										<h2  class="percent">5000</h2>
									</div>
									<h5>vehicles in stock</h5>
								</div>
							</div>
							<div class="col-sm-3 col-xs-6">
								<div class="b-count__item">
									<div class="b-count__item-circle">
										<span class="fa fa-users"></span>
									</div>
									<div class="chart" data-percent="3100">
										<h2  class="percent">3100</h2>
									</div>
									<h5>HAPPY CUSTOMER REVIEWS</h5>
								</div>
							</div>
							<div class="col-sm-3 col-xs-6">
								<div class="b-count__item">
									<div class="b-count__item-circle">
										<span class="fa fa-building-o"></span>
									</div>
									<div class="chart" data-percent="54">
										<h2  class="percent">54</h2>
									</div>
									<h5>DEALER BRANCHES</h5>
								</div>
							</div>
							<div class="col-sm-3 col-xs-6">
								<div class="b-count__item j-last">
									<div class="b-count__item-circle">
										<span class="fa fa-suitcase"></span>
									</div>
									<div class="chart" data-percent="547">
										<h2  class="percent">547</h2>
									</div>
									<h5>FREE PARTS GIVEN</h5>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section><!--b-count-->

		<?php include_once('home_newsletter.php');?>

		<section class="b-review">
			<div class="container">
				<div class="col-sm-10 col-sm-offset-1 col-xs-12">
					<div id="carousel-small-rev" class="owl-carousel enable-owl-carousel" data-items="1" data-navigation="true" data-auto-play="true" data-stop-on-hover="true" data-items-desktop="1" data-items-desktop-small="1" data-items-tablet="1" data-items-tablet-small="1">
						<div class="b-review__main">
							<div class="b-review__main-person">
								<div class="b-review__main-person-inside">
								</div>
							</div>
							<h5><span>DONALD BROOKS</span>, Customer, Ferrari 488 GTB 2 Owner<em>"</em></h5>
							<p>Donec facilisis velit eust. Phasellus cons quat. Aenean vitae quam. Vivamus et nunc. Nunc consequsem
								velde metus imperdiet lacinia.  Nam rutrum congue diam. Vestibulum acda risus eros auctor egestas. Morbids sem magna, viverra quis sollicitudin quis consectetuer quis nec magna.</p>
						</div>
						<div class="b-review__main">
							<div class="b-review__main-person">
								<div class="b-review__main-person-inside">
								</div>
							</div>
							<h5><span>DONALD BROOKS</span>, Customer, Ferrari 488 GTB 2 Owner<em>"</em></h5>
							<p>Donec facilisis velit eust. Phasellus cons quat. Aenean vitae quam. Vivamus et nunc. Nunc consequsem
								velde metus imperdiet lacinia.  Nam rutrum congue diam. Vestibulum acda risus eros auctor egestas. Morbids sem magna, viverra quis sollicitudin quis consectetuer quis nec magna.</p>
						</div>
						<div class="b-review__main">
							<div class="b-review__main-person">
								<div class="b-review__main-person-inside">
								</div>
							</div>
							<h5><span>DONALD BROOKS</span>, Customer, Ferrari 488 GTB 2 Owner<em>"</em></h5>
							<p>Donec facilisis velit eust. Phasellus cons quat. Aenean vitae quam. Vivamus et nunc. Nunc consequsem
								velde metus imperdiet lacinia.  Nam rutrum congue diam. Vestibulum acda risus eros auctor egestas. Morbids sem magna, viverra quis sollicitudin quis consectetuer quis nec magna.</p>
						</div>
					</div>
				</div>
			</div>
			<img src="images/backgrounds/reviews.jpg" alt="" class="img-responsive center-block" />
		</section><!--b-review-->

		<div class="b-features">
			<div class="container">
				<div class="row">
					<div class="col-md-9 col-md-offset-3 col-xs-6 col-xs-offset-6">
						<ul class="b-features__items">
							<li class="wow zoomInUp" data-wow-delay="0.3s" data-wow-offset="100">Low Prices, No Haggling</li>
							<li class="wow zoomInUp" data-wow-delay="0.3s" data-wow-offset="100">Largest Car Dealership</li>
							<li class="wow zoomInUp" data-wow-delay="0.3s" data-wow-offset="100">Multipoint Safety Check</li>
						</ul>
					</div>
				</div>
			</div>
		</div><!--b-features-->

		<?php include_once('footer.php');?>
		<!--b-footer-->

		<!--Main-->   
		<script src="js/jquery-1.11.3.min.js"></script>
		<script src="js/jquery-ui.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/modernizr.custom.js"></script>

		<script src="assets/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
		<script src="js/waypoints.min.js"></script>
		<script src="js/jquery.easypiechart.min.js"></script>
		<script src="js/classie.js"></script>

		<!--Switcher-->
		<script src="assets/switcher/js/switcher.js"></script>
		<!--Owl Carousel-->
		<script src="assets/owl-carousel/owl.carousel.min.js"></script>
		<!--bxSlider-->
		<script src="assets/bxslider/jquery.bxslider.js"></script>
		<!-- jQuery UI Slider -->
		<script src="assets/slider/jquery.ui-slider.js"></script>

		<!--Theme-->
		<script src="js/jquery.smooth-scroll.js"></script>
		<script src="js/wow.min.js"></script>
		<script src="js/jquery.placeholder.min.js"></script>
		<script src="js/theme.js"></script>

	</body>
</html>