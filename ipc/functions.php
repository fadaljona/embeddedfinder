<?php 

function productDescription($p_name, $p_dim_t, $p_form_factor, $p_pciet, $p_pci, $p_graphics_card, $p_poe, $p_manufacturer, $p_mount) {

	$product_description = "";

	// Size (Optional)
	if (strpos($p_name, 'VTC') !== false ) {
		$product_description = "In-Vehicle";
	} elseif (strpos($p_mount, 'DIN') !== false ) {
		$product_description = "DIN Rail";
	} elseif ($p_dim_t < 1000000) {
		$product_description = "Ultra-Compact";
	} elseif ($p_dim_t < 3000000) {
		$product_description = "Compact";
	} elseif (strpos($p_name, 'LP') !== false ) {
		$product_description = "Low-Profile";
	};	  

	// Form Factor

	$p_form_factor = $p_form_factor." Computer";

	// Manufacturer
	if ($p_manufacturer == "Iwill") {
		$p_manufacturer = "Indutrial PC";
	} else {
		$p_manufacturer = $p_manufacturer;
	}

	// Feature
	if ($p_graphics_card > 0) {
		$p_feature = "with graphics card";
	} 
	elseif ($p_poe > 0) {
		$p_feature = "with PoE ports";
	}
	elseif ($p_pciet == 1 and $p_pci == 0) {
		$p_feature = "with PCIe slot";
	}
	elseif ($p_pciet == 2 and $p_pci == 0) {
		$p_feature = "with Dual PCIe slots";
	}
	elseif ($p_pci > 0 and $p_pciet == 0) {
		$p_feature =  "with PCI slot";
	}
	elseif ($p_pci > 0 and $p_pciet > 0) {
		$p_feature =  "with PCIe and PCI slots";
	}


	if ($p_feature != "") {
		$product_description = $product_description." ".$p_form_factor." ".$p_feature;
	} else {
		$product_description = $product_description." ".$p_form_factor;
	}

	  

	echo $product_description;
}

?>