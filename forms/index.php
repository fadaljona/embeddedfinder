<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Forms Plus: PHP - Available pages</title>
        <link href="./font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
        <link href="./bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
        <style>
            .forms .col-sm-3{
                margin-bottom: 30px;
            }
            .forms .row .btn{
                width: 100%;
            }
            .admin{
                margin: 30px 0;
            }
        </style>
    </head>
    <body>
        <div class="container forms">
            <div class="admin text-center">
                <a target="_blank" class="btn btn-lg btn-success" href="admin.php">Admin <i class="fa fa-sign-in"></i></a>
            </div>
            <h2 class="text-center">Demos</h2>
            <?php
                $forms = array(
                    "elements.php"                => "Elements",
                    "js-elements.php"             => "JS Elements",
                    "colorpicker.php"             => "Colorpicker",
                    "datetimepicker.php"          => "Datetimepicker",
                    "js-validation.php"           => "JS Validation",
                    "elements-tooltips.php"       => "Tooltip",
                    "masked-input.php"            => "Masked input",
                    "captcha.php"                 => "Captcha",
                    "slider.php"                  => "Slider",
                    "block-pick.php"              => "Block Pick",
                    "values-and-calculations.php" => "Values and calculations",
                    "ajax-example.php"            => "Ajax example",
                    "js-steps.php"                => "JS Steps",
                    "tabs.php"                    => "Tabs",
                    "popup.php"                   => "Popup",
                    "show-block.php"              => "Show Block",
                    "elements-rtl.php"            => "Elements RTL",
                    "autocomplete.php"            => "Autocomplete",
                    "actions.php"                 => "Actions",
                    "sub-selection.php"           => "Sub Selection"
                );
                $i = 0;
            ?>
            <div class="row">
                <?php foreach ($forms as $link => $title) { ?>
                    <?php if( $i && $i % 4 == 0 ) { ?>
                        </div>
                        <div class="row">
                    <?php } ?>
                    <?php $i++; ?>
                    <div class="col-sm-3"><a target="_blank" class="btn btn-primary" href="<?php echo $link; ?>"><?php echo $title; ?></a></div>
                <?php } ?>
            </div>
            <h2 class="text-center">Forms</h2>
            <?php
                $forms = array(
                    "ajax-form-booking.php"                             => "Ajax Form Booking",
                    "ajax-form-booking-steps.php"                       => "Ajax Form Booking Steps",
                    "ajax-form-checkout-calculations.php"               => "Ajax Form Checkout Calculations",
                    "ajax-form-checkout-steps-calculations.php"         => "Ajax Form Checkout Steps Calculations",
                    "ajax-form-college-admission.php"                   => "Ajax Form College Admission",
                    "ajax-form-comment.php"                             => "Ajax Form Comment",
                    "ajax-form-contact.php"                             => "Ajax Form Contact",
                    "ajax-form-contact-with-icon.php"                   => "Ajax Form Contact With Icon",
                    "ajax-form-contact-with-map.php"                    => "Ajax Form Contact With Map",
                    "ajax-form-contact-with-recaptcha.php"              => "Ajax Form Contact With Recaptcha",
                    "ajax-form-contact-with-language-switcher.php"      => "Ajax Form Contact With Language Switcher",
                    "ajax-form-feedback.php"                            => "Ajax Form Feedback",
                    "ajax-form-flat-booking.php"                        => "Ajax Form Flat Booking",
                    "ajax-form-flat-booking-steps.php"                  => "Ajax Form Flat Booking Steps",
                    "ajax-form-forgot-password.php"                     => "Ajax Form Forgot Password",
                    "ajax-form-job-application.php"                     => "Ajax Form Job Application",
                    "ajax-form-login.php"                               => "Ajax Form Login",
                    "ajax-form-order-car.php"                           => "Ajax Form Order Car",
                    "ajax-form-order-car-steps.php"                     => "Ajax Form Order Car Steps",
                    "ajax-form-order-service.php"                       => "Ajax Form Order Service",
                    "ajax-form-order-service-steps.php"                 => "Ajax Form Order Service Steps",
                    "ajax-form-play-school-admission.php"               => "Ajax Form Play School Admission",
                    "ajax-form-pre-school-admission.php"                => "Ajax Form Pre School Admission",
                    "ajax-form-registration.php"                        => "Ajax Form Registration",
                    "ajax-form-restaurant-booking.php"                  => "Ajax Form Restaurant Booking",
                    "ajax-form-restaurant-booking-steps.php"            => "Ajax Form Restaurant Booking Steps",
                    "ajax-form-review.php"                              => "Ajax Form Review",
                    "ajax-form-school-admission.php"                    => "Ajax Form School Admission",
                    "ajax-form-subscribe.php"                           => "Ajax Form Subscribe",
                    "ajax-form-support-request.php"                     => "Ajax Form Support Request",
                    "ajax-form-t-user.php"                              => "Ajax Form T User",
                    "ajax-form-university-admission.php"                => "Ajax Form University Admission",
                    "ajax-form-website-bug-report.php"                  => "Ajax Form Website Bug Report",
                    "ajax-form-website-review.php"                      => "Ajax Form Website Review",
                    "form-booking.php"                                  => "Form Booking",
                    "form-booking-steps.php"                            => "Form Booking Steps",
                    "form-checkout-calculations.php"                    => "Form Checkout Calculations",
                    "form-checkout-steps-calculations.php"              => "Form Checkout Steps Calculations",
                    "form-college-admission.php"                        => "Form College Admission",
                    "form-comment.php"                                  => "Form Comment",
                    "form-contact.php"                                  => "Form Contact",
                    "form-contact-with-icon.php"                        => "Form Contact With Icon",
                    "form-contact-with-map.php"                         => "Form Contact With Map",
                    "form-contact-with-recaptcha.php"                   => "Form Contact With Recaptcha",
                    "form-contact-with-language-switcher.php"           => "Form Contact With Language Switcher",
                    "form-feedback.php"                                 => "Form Feedback",
                    "form-flat-booking.php"                             => "Form Flat Booking",
                    "form-forgot-password.php"                          => "Form Forgot Password",
                    "form-job-application.php"                          => "Form Job Application",
                    "form-login.php"                                    => "Form Login",
                    "form-order-car.php"                                => "Form Order Car",
                    "form-order-service.php"                            => "Form Order Service",
                    "form-order-service-steps.php"                      => "Form Order Service Steps",
                    "form-play-school-admission.php"                    => "Form Play School Admission",
                    "form-pre-school-admission.php"                     => "Form Pre School Admission",
                    "form-registration.php"                             => "Form Registration",
                    "form-restaurant-booking.php"                       => "Form Restaurant Booking",
                    "form-review.php"                                   => "Form Review",
                    "form-school-admission.php"                         => "Form School Admission",
                    "form-subscribe.php"                                => "Form Subscribe",
                    "form-support-request.php"                          => "Form Support Request",
                    "form-t-user.php"                                   => "Form T User",
                    "form-university-admission.php"                     => "Form University Admission",
                    "form-website-bug-report.php"                       => "Form Website Bug Report",
                    "form-website-review.php"                           => "Form Website Review",
                    "popup-form-t-user.php"                             => "Popup Form T User",
                    "popup-form-subscribe.php"                          => "Popup Form Subscribe",
                    "popup-form-review.php"                             => "Popup Form Review",
                    "popup-form-registration.php"                       => "Popup Form Registration",
                    "popup-form-login.php"                              => "Popup Form Login",
                    "popup-form-job-application.php"                    => "Popup Form Job Application",
                    "popup-form-forgot-password.php"                    => "Popup Form Forgot Password",
                    "popup-form-feedback.php"                           => "Popup Form Feedback",
                    "popup-form-contact.php"                            => "Popup Form Contact",
                    "popup-form-comment.php"                            => "Popup Form Comment",
                    "ajax-popup-form-t-user.php"                        => "Ajax Popup Form T User",
                    "ajax-popup-form-subscribe.php"                     => "Ajax Popup Form Subscribe",
                    "ajax-popup-form-review.php"                        => "Ajax Popup Form Review",
                    "ajax-popup-form-registration.php"                  => "Ajax Popup Form Registration",
                    "ajax-popup-form-login.php"                         => "Ajax Popup Form Login",
                    "ajax-popup-form-job-application.php"               => "Ajax Popup Form Job Application",
                    "ajax-popup-form-forgot-password.php"               => "Ajax Popup Form Forgot Password",
                    "ajax-popup-form-feedback.php"                      => "Ajax Popup Form Feedback",
                    "ajax-popup-form-contact.php"                       => "Ajax Popup Form Contact",
                    "ajax-popup-form-comment.php"                       => "Ajax Popup Form Comment"
                );
                $i = 0;
            ?>
            <div class="row">
                <?php foreach ($forms as $link => $title) { ?>
                    <?php if( $i && $i % 4 == 0 ) { ?>
                        </div>
                        <div class="row">
                    <?php } ?>
                    <?php $i++; ?>
                    <div class="col-sm-3"><a target="_blank" class="btn btn-primary" href="<?php echo $link; ?>"><?php echo $title; ?></a></div>
                <?php } ?>
            </div>
        </div>
    </body>
</html>
