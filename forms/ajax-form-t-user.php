<?php
    require_once __DIR__ . '/forms-plus-framework/vendor/autoload.php';
    use FormsPlus\Framework\FormsPlusFramework as FormsPlusFramework;
    $formLogin = new FormsPlusFramework( 'forms-plus-framework/app/config/forms/ajax-form-t-user-login.yml' );
    $formRegistration = new FormsPlusFramework( 'forms-plus-framework/app/config/forms/ajax-form-t-user-registration.yml' );
    $formForgotPassword = new FormsPlusFramework( 'forms-plus-framework/app/config/forms/ajax-form-t-user-forgot-password.yml' );
    $formNewPassword = new FormsPlusFramework( 'forms-plus-framework/app/config/forms/form-new-password.yml' );
    if( $formLogin->isSubmitted ) {
        header( 'Content-Type: application/json' );
        if( $formLogin->isValid ) {
            $data = array(
                'block'   => 'loginSuccessOutputBlock',
                'content' => '<div class="alert alert-valid"><strong><i class="fa fa-check"></i> You have successfully logged in.</strong> It does nothing for the moment. The code has to be complemented with the needed functionality.</div>'
            );
            $data[ 'content' ] .= FormsPlusFramework::outputDebug( false, '<div class="alert alert-error">%content%</div>' );
        } else {
            $content = '';
            $step = false;
            foreach( $formLogin->errorList as $attributeID => $itemList ) {
                $name = $formLogin->configuration[ 'attribute_list' ][ $attributeID ][ 'name' ];
                $content .= '<div class="alert alert-error"><strong><i class="fa fa-times"></i> ' . $name . ':</strong> ';
                if( count( $itemList ) == 1 ) {
                    foreach( $itemList as $errorList ) $content .=  implode( ' ', $errorList );
                } else {
                    $content .= '<ul>';
                    foreach( $itemList as $errorList ) $content .= '<li>' . implode( ' ', $errorList ) . '</li>';
                    $content .= '</ul>';
                }
                $content .= '</div>';
                if( !empty( $formLogin->configuration[ 'attribute_list' ][ $attributeID ][ 'form_step' ] ) && ( $step === false || $step > $formLogin->configuration[ 'attribute_list' ][ $attributeID ][ 'form_step' ] ) ) {
                    $step = $formLogin->configuration[ 'attribute_list' ][ $attributeID ][ 'form_step' ];
                }
            }
            $data = array( 'errorData' => array(
                'block'   => 'loginFailOutputBlock',
                'content' => $content
            ));
            if( $step !== false ) {
                $data[ 'errorData' ][ 'step' ] = $step;
            }
            $data[ 'errorData' ][ 'content' ] .= FormsPlusFramework::outputDebug( false, '<div class="alert alert-error">%content%</div>' );
        }
        echo json_encode( $data );
        exit;
    }
    if( $formRegistration->isSubmitted ) {
        header( 'Content-Type: application/json' );
        if( $formRegistration->isValid ) {
            $data = array(
                'block'   => 'registrationSuccessOutputBlock',
                'content' => '<div class="alert alert-valid"><strong><i class="fa fa-check"></i> Your account was successfully created.</strong> An email will be sent to the specified email address. Follow the instructions in that email to activate your account.</div>'
            );
            $data[ 'content' ] .= FormsPlusFramework::outputDebug( false, '<div class="alert alert-error">%content%</div>' );
        } else {
            $content = '';
            $step = false;
            foreach( $formRegistration->errorList as $attributeID => $itemList ) {
                $name = $formRegistration->configuration[ 'attribute_list' ][ $attributeID ][ 'name' ];
                $content .= '<div class="alert alert-error"><strong><i class="fa fa-times"></i> ' . $name . ':</strong> ';
                if( count( $itemList ) == 1 ) {
                    foreach( $itemList as $errorList ) $content .=  implode( ' ', $errorList );
                } else {
                    $content .= '<ul>';
                    foreach( $itemList as $errorList ) $content .= '<li>' . implode( ' ', $errorList ) . '</li>';
                    $content .= '</ul>';
                }
                $content .= '</div>';
                if( !empty( $formRegistration->configuration[ 'attribute_list' ][ $attributeID ][ 'form_step' ] ) && ( $step === false || $step > $formRegistration->configuration[ 'attribute_list' ][ $attributeID ][ 'form_step' ] ) ) {
                    $step = $formRegistration->configuration[ 'attribute_list' ][ $attributeID ][ 'form_step' ];
                }
            }
            $data = array( 'errorData' => array(
                'block'   => 'registrationFailOutputBlock',
                'content' => $content
            ));
            if( $step !== false ) {
                $data[ 'errorData' ][ 'step' ] = $step;
            }
            $data[ 'errorData' ][ 'content' ] .= FormsPlusFramework::outputDebug( false, '<div class="alert alert-error">%content%</div>' );
        }
        echo json_encode( $data );
        exit;
    }
    if( $formForgotPassword->isSubmitted ) {
        header( 'Content-Type: application/json' );
        if( $formForgotPassword->isValid ) {
            $data = array(
                'block'   => 'forgotSuccessOutputBlock',
                'content' => '<div class="alert alert-valid"><strong><i class="fa fa-check"></i> An email has been sent.</strong> This email contains a link you need to click so that we can confirm that the correct user is getting the new password.</div>'
            );
            $data[ 'content' ] .= FormsPlusFramework::outputDebug( false, '<div class="alert alert-error">%content%</div>' );
        } else {
            $content = '';
            $step = false;
            foreach( $formForgotPassword->errorList as $attributeID => $itemList ) {
                $name = $formForgotPassword->configuration[ 'attribute_list' ][ $attributeID ][ 'name' ];
                $content .= '<div class="alert alert-error"><strong><i class="fa fa-times"></i> ' . $name . ':</strong> ';
                if( count( $itemList ) == 1 ) {
                    foreach( $itemList as $errorList ) $content .=  implode( ' ', $errorList );
                } else {
                    $content .= '<ul>';
                    foreach( $itemList as $errorList ) $content .= '<li>' . implode( ' ', $errorList ) . '</li>';
                    $content .= '</ul>';
                }
                $content .= '</div>';
                if( !empty( $formForgotPassword->configuration[ 'attribute_list' ][ $attributeID ][ 'form_step' ] ) && ( $step === false || $step > $formForgotPassword->configuration[ 'attribute_list' ][ $attributeID ][ 'form_step' ] ) ) {
                    $step = $formForgotPassword->configuration[ 'attribute_list' ][ $attributeID ][ 'form_step' ];
                }
            }
            $data = array( 'errorData' => array(
                'block'   => 'forgotFailOutputBlock',
                'content' => $content
            ));
            if( $step !== false ) {
                $data[ 'errorData' ][ 'step' ] = $step;
            }
            $data[ 'errorData' ][ 'content' ] .= FormsPlusFramework::outputDebug( false, '<div class="alert alert-error">%content%</div>' );
        }
        echo json_encode( $data );
        exit;
    }
    // define an active tab
    $activeTab = 1;
    // activate a user that got this activation link via an email
    if( !empty( $_GET[ 'action' ] ) && $_GET[ 'action' ] === 'user_activation' && !empty( $_GET[ 'email' ] ) && !empty( $_GET[ 'key' ] ) ) {
        $parameterList = $formRegistration->configuration[ 'mail' ][ 'message' ][ 'registration' ][ 'template_parameter_list' ];
        $filter = array( 'prop_form_config_file' => $formRegistration->configFile );
        $filter[ 'attr_' . $parameterList[ 'email_attribute_id' ] ] = $_GET[ 'email' ];
        $filter[ 'attr_' . $parameterList[ 'hash_key_attribute_id' ] ] = $_GET[ 'key' ];
        $formRegistration->initStorage();
        $collectionSet = $formRegistration->storage->fetchCollectionList( null, 1, 0, null, null, $filter );
        if( $collectionSet[ 'count' ] === 1 ) {
            $collection = $collectionSet[ 'list' ][ '0' ];
            $collection[ 'attribute_list' ][ $parameterList[ 'hash_key_attribute_id' ] ][ 'final_value' ] = '';
            $formRegistration->storage->updateCollection( $collection );
            $actionUserActivation = array( 'valid', 'Your account is now activated.' );
        } elseif( $collectionSet[ 'count' ] === 0 ) {
            $actionUserActivation = array( 'error', 'Your account hasn\'t been activated.', 'The submitted key is invalid.' );
        } elseif( $collectionSet[ 'count' ] > 1 ) {
            $actionUserActivation = array( 'error', 'The account activation has failed.', 'An internal error occurred. Please, contact the website administrator.' );
        }
    }
    if( isset( $actionUserActivation ) ) {
        $activeTab = 2;
    }
    // set a new password for a user who got this recovery link via an email
    if( !empty( $_GET[ 'action' ] ) && $_GET[ 'action' ] === 'user_newpassword' && !empty( $_GET[ 'email' ] ) && !empty( $_GET[ 'key' ] ) ) {
        $parameterList = $formForgotPassword->configuration[ 'mail' ][ 'message' ][ 'forgotpassword' ][ 'template_parameter_list' ];
        $filter = array( 'prop_form_config_file' => $formForgotPassword->configFile );
        $filter[ 'attr_' . $parameterList[ 'email_attribute_id' ] ] = $_GET[ 'email' ];
        $filter[ 'attr_' . $parameterList[ 'hash_key_attribute_id' ] ] = $_GET[ 'key' ];
        $formForgotPassword->initStorage();
        $forgotPasswordCollectionSet = $formForgotPassword->storage->fetchCollectionList( null, 1, 0, null, null, $filter );
        if( $forgotPasswordCollectionSet[ 'count' ] === 1 ) {
            $forgotPasswordCollection = $forgotPasswordCollectionSet[ 'list' ][ '0' ];
            $actionUserNewPassword = array( 'form' => true );
            if( $formNewPassword->isValid ) {
                $newPasswordHash = $formNewPassword->data[ 'attribute_list' ][ 'new_password' ][ 'final_value' ];
                $parameterList = $formRegistration->configuration[ 'mail' ][ 'message' ][ 'registration' ][ 'template_parameter_list' ];
                $filter = array( 'prop_form_config_file' => $formRegistration->configFile );
                $filter[ 'attr_' . $parameterList[ 'email_attribute_id' ] ] = $_GET[ 'email' ];
                $filter[ 'attr_' . $parameterList[ 'hash_key_attribute_id' ] ] = '';
                $formRegistration->initStorage();
                $userCollectionSet = $formRegistration->storage->fetchCollectionList( null, 1, 0, null, null, $filter );
                if( $userCollectionSet[ 'count' ] === 1 ) {
                    $userCollection = $userCollectionSet[ 'list' ][ '0' ];
                    $userCollection[ 'attribute_list' ][ $parameterList[ 'password_attribute_id' ] ][ 'final_value' ] = $newPasswordHash;
                    $formRegistration->storage->updateCollection( $userCollection );
                    $formForgotPassword->storage->removeCollection( $forgotPasswordCollection[ 'collection_id' ] );
                    $actionUserNewPassword[ 'form' ] = array( 'valid', 'Your password has been successfully changed.' );
                } elseif( $userCollectionSet[ 'count' ] === 0 ) {
                    $actionUserNewPassword[ 'form' ] = array( 'error', 'The password recovery is impossible.', 'A user with such email address does not exist.' );
                } elseif( $userCollectionSet[ 'count' ] > 1 ) {
                    $actionUserNewPassword[ 'form' ] = array( 'error', 'The password recovery is impossible.', 'A user with such email address does not exist.' );
                }
            }
        } elseif( $forgotPasswordCollectionSet[ 'count' ] === 0 ) {
            $actionUserNewPassword = array( 'error', 'The password recovery is impossible.', 'The submitted key is invalid.' );
        } elseif( $forgotPasswordCollectionSet[ 'count' ] > 1 ) {
            $actionUserNewPassword = array( 'error', 'The password recovery has failed.', 'An internal error occurred. Please, contact the website administrator.' );
        }
    }
    if( isset( $actionUserNewPassword ) ) {
        $activeTab = 3;
    }
    if( $formNewPassword->isSubmitted ) {
        header( 'Content-Type: application/json' );
        if( $formNewPassword->isValid ) {
            $data = array(
                'block'   => 'newpasswordSuccessOutputBlock',
                'content' => sprintf( '<div class="alert alert-%s"><strong><i class="fa fa-%s"></i> %s</strong> %s</div>', $actionUserNewPassword[ 'form' ][ 0 ], $actionUserNewPassword[ 'form' ][ 0 ] === 'error' ? 'times' : 'check', $actionUserNewPassword[ 'form' ][ 1 ], isset( $actionUserNewPassword[ 'form' ][ 2 ] ) ? $actionUserNewPassword[ 'form' ][ 2 ] : '' )
            );
            $data[ 'content' ] .= FormsPlusFramework::outputDebug( false, '<div class="alert alert-error">%content%</div>' );
        } else {
            $content = '';
            foreach( $formNewPassword->errorList as $attributeID => $itemList ) {
                $name = $formNewPassword->configuration[ 'attribute_list' ][ $attributeID ][ 'name' ];
                $content .= '<div class="alert alert-error"><strong><i class="fa fa-times"></i> ' . $name . ':</strong> ';
                if( count( $itemList ) == 1 ) {
                    foreach( $itemList as $errorList ) $content .=  implode( ' ', $errorList );
                } else {
                    $content .= '<ul>';
                    foreach( $itemList as $errorList ) $content .= '<li>' . implode( ' ', $errorList ) . '</li>';
                    $content .= '</ul>';
                }
                $content .= '</div>';
            }
            $data = array( 'errorData' => array(
                'block'   => 'newpasswordFailOutputBlock',
                'content' => $content
            ));
            $data[ 'errorData' ][ 'content' ] .= FormsPlusFramework::outputDebug( false, '<div class="alert alert-error">%content%</div>' );
        }
        echo json_encode( $data );
        exit;
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Forms Plus: PHP</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php FormsPlusFramework::loadStyles(); ?>
        <link rel="stylesheet" href="<?php echo FormsPlusFramework::getStaticVar( 'asset_directory_uri' ); ?>css/base.css" type="text/css">
    </head>
    <body>
        <div class="container">
            <div class="<?php $formLogin->designCSSClasses(); ?>">
                <div class="p-form p-shadowed p-form-sm">
                    <input type="radio" id="tab-1" name="activeTab" class="p-tab-sel"<?php echo ( $activeTab === 1 ) ? ' checked' : ''; ?>/>
                    <input type="radio" id="tab-2" name="activeTab" class="p-tab-sel"<?php echo ( $activeTab === 2 ) ? ' checked' : ''; ?>/>
                    <input type="radio" id="tab-3" name="activeTab" class="p-tab-sel"<?php echo ( $activeTab === 3 ) ? ' checked' : ''; ?>/>
                    <ul class="nav nav-tabs">
                        <li><label for="tab-1"><i class="fa fa-sign-in"></i></label></li>
                        <li><label for="tab-2"><i class="fa fa-user-plus"></i></label></li>
                        <li><label for="tab-3"><i class="fa fa-question-circle"></i></label></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane">
                            <form method="post" action="<?php echo strtok( $_SERVER[ 'REQUEST_URI' ], '?' ); ?>" data-js-validate="true" data-js-highlight-state-msg="true" data-js-show-valid-msg="true" data-js-ajax-form="" data-js-ajax-before-hide-block="loginSuccessBlock;loginFailBlock" data-js-ajax-fail-show-block="loginFailBlock" data-js-ajax-success-hide-block="loginFormBlock" data-js-ajax-success-show-block="loginSuccessBlock" data-js-ajax-always-hide-block="loginLoadingBlock" data-js-ajax-before-show-block="loginLoadingBlock">
                                <div class="p-title text-left">
                                    <span class="p-title-side">User login</span>
                                </div>
                                <div data-js-block="loginSuccessBlock" class="collapse">
                                    <h4>Form was sent successfully!</h4>
                                    <div data-js-block="loginSuccessOutputBlock" class="collapse"></div>
                                    <div class="text-right">
                                        <a href="<?php echo strtok( $_SERVER[ 'REQUEST_URI' ], '?' ); ?>" class="btn">reload</a>
                                    </div>
                                </div>
                                <div data-js-block="loginFailBlock" class="collapse">
                                    <h4>Failed to send form!</h4>
                                    <div data-js-block="loginFailOutputBlock" class="collapse"></div>
                                </div>
                                <div data-js-block="loginFormBlock">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="p-subtitle p-no-offs text-left">
                                                <span class="p-title-side">sign in with</span>
                                            </div>
                                            <div class="p-social-bar">
                                                <a href="#" class="btn p-social-btn"><i class="fa fa-facebook"></i></a>
                                                <a href="#" class="btn p-social-btn"><i class="fa fa-twitter"></i></a>
                                                <a href="#" class="btn p-social-btn"><i class="fa fa-google-plus"></i></a>
                                                <a href="#" class="btn p-social-btn"><i class="fa fa-linkedin"></i></a>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="p-subtitle p-no-offs text-left">
                                                <span class="p-title-side">or login</span>
                                            </div>
                                            <?php $formLogin->attributeView( 'log_login' ); ?>
                                            <?php $formLogin->attributeView( 'log_password' ); ?>
                                            <label for="tab-3" class="p-colored-link">Forgot password?</label>
                                            <div class="clearfix"></div>
                                            <?php $formLogin->attributeView( 'log_keep_logged_in' ); ?>
                                        </div>
                                    </div>
                                    <div data-js-block="loginLoadingBlock" class="progress collapse">
                                        <div class="progress-bar progress-bar-fp progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                                    </div>
                                    <div class="text-right">
                                        <button class="btn p-ajax-disabled" type="submit" name="confirm"><i class="fa fa-sign-in"></i>&nbsp;login</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane">
                            <form method="post" action="<?php echo strtok( $_SERVER[ 'REQUEST_URI' ], '?' ); ?>" data-js-validate="true" data-js-highlight-state-msg="true" data-js-show-valid-msg="true" data-js-ajax-form="" data-js-ajax-before-hide-block="registrationSuccessBlock;registrationFailBlock" data-js-ajax-fail-show-block="registrationFailBlock" data-js-ajax-success-hide-block="registrationFormBlock" data-js-ajax-success-show-block="registrationSuccessBlock" data-js-ajax-always-hide-block="registrationLoadingBlock" data-js-ajax-before-show-block="registrationLoadingBlock">
                                <div class="p-title text-left">
                                    <span class="p-title-side">Registration</span>
                                </div>
                                <?php if( isset( $actionUserActivation ) ) { ?>
                                    <div class="alert alert-<?php echo $actionUserActivation[ 0 ]; ?>">
                                        <strong><i class="fa fa-<?php echo $actionUserActivation[ 0 ] === 'error' ? 'times' : 'check'; ?>"></i> <?php echo $actionUserActivation[ 1 ]; ?></strong>
                                        <?php echo isset( $actionUserActivation[ 2 ] ) ? $actionUserActivation[ 2 ] : ''; ?>
                                    </div>
                                    <div class="text-right">
                                        <a href="<?php echo strtok( $_SERVER[ 'REQUEST_URI' ], '?' ); ?>" class="btn">reload</a>
                                    </div>
                                <?php } else { ?>
                                    <div data-js-block="registrationSuccessBlock" class="collapse">
                                        <h4>Form was sent successfully!</h4>
                                        <div data-js-block="registrationSuccessOutputBlock" class="collapse"></div>
                                        <div class="text-right">
                                            <a href="<?php echo strtok( $_SERVER[ 'REQUEST_URI' ], '?' ); ?>" class="btn">reload</a>
                                        </div>
                                    </div>
                                    <div data-js-block="registrationFailBlock" class="collapse">
                                        <h4>Failed to send form!</h4>
                                        <div data-js-block="registrationFailOutputBlock" class="collapse"></div>
                                    </div>
                                    <div data-js-block="registrationFormBlock">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="p-subtitle p-no-offs text-left">
                                                    <span class="p-title-side">account details</span>
                                                </div>
                                                <?php $formRegistration->attributeView( 'reg_login' ); ?>
                                                <?php $formRegistration->attributeView( 'reg_email' ); ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="p-subtitle p-no-offs text-left">
                                                    <span class="p-title-side">Your details</span>
                                                </div>
                                                <?php $formRegistration->attributeView( 'reg_first_name' ); ?>
                                                <?php $formRegistration->attributeView( 'reg_last_name' ); ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <?php $formRegistration->attributeView( 'reg_password' ); ?>
                                            </div>
                                            <div class="col-md-6">
                                                <?php $formRegistration->attributeView( 'reg_confirm_password' ); ?>
                                            </div>
                                        </div>
                                        <?php $formRegistration->attributeView( 'reg_gender' ); ?>
                                        <?php $formRegistration->attributeView( 'reg_captcha' ); ?>
                                        <div class="clearfix"></div>
                                        <?php $formRegistration->attributeView( 'reg_agree' ); ?>
                                        <div data-js-block="registrationLoadingBlock" class="progress collapse">
                                            <div class="progress-bar progress-bar-fp progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                                        </div>
                                        <div class="text-right">
                                            <button class="btn p-ajax-disabled" type="submit" name="confirm"><i class="fa fa-check-square-o"></i>&nbsp;register</button>
                                        </div>
                                    </div>
                                <?php } ?>
                            </form>
                        </div>
                        <div class="tab-pane">
                            <?php if( isset( $actionUserNewPassword ) ) { ?>
                                <form method="post" action="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>" data-js-validate="true" data-js-highlight-state-msg="true" data-js-show-valid-msg="true" data-js-ajax-form="" data-js-ajax-before-hide-block="newpasswordSuccessBlock;newpasswordFailBlock" data-js-ajax-fail-show-block="newpasswordFailBlock" data-js-ajax-success-hide-block="newpasswordFormBlock" data-js-ajax-success-show-block="newpasswordSuccessBlock" data-js-ajax-always-hide-block="newpasswordLoadingBlock" data-js-ajax-before-show-block="newpasswordLoadingBlock">
                                    <div class="p-title text-left">
                                        <span class="p-title-side">New password</span>
                                    </div>
                                    <?php if( !isset( $actionUserNewPassword[ 'form' ] ) ) { ?>
                                        <div class="alert alert-<?php echo $actionUserNewPassword[ 0 ]; ?>">
                                            <strong><i class="fa fa-<?php echo $actionUserNewPassword[ 0 ] === 'error' ? 'times' : 'check'; ?>"></i> <?php echo $actionUserNewPassword[ 1 ]; ?></strong>
                                            <?php echo isset( $actionUserNewPassword[ 2 ] ) ? $actionUserNewPassword[ 2 ] : ''; ?>
                                        </div>
                                        <div class="text-right">
                                            <a href="<?php echo strtok( $_SERVER[ 'REQUEST_URI' ], '?' ); ?>" class="btn">reload</a>
                                        </div>
                                    <?php } else { ?>
                                        <div data-js-block="newpasswordSuccessBlock" class="collapse">
                                            <h4>Form was sent successfully!</h4>
                                            <div data-js-block="newpasswordSuccessOutputBlock" class="collapse"></div>
                                            <div class="text-right">
                                                <a href="<?php echo strtok( $_SERVER[ 'REQUEST_URI' ], '?' ); ?>" class="btn">reload</a>
                                            </div>
                                        </div>
                                        <div data-js-block="newpasswordFailBlock" class="collapse">
                                            <h4>Failed to send form!</h4>
                                            <div data-js-block="newpasswordFailOutputBlock" class="collapse"></div>
                                        </div>
                                        <div data-js-block="newpasswordFormBlock">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <?php $formNewPassword->attributeView( 'new_password' ); ?>
                                                </div>
                                                <div class="col-sm-6">
                                                    <?php $formNewPassword->attributeView( 'confirm_new_password' ); ?>
                                                </div>
                                            </div>
                                            <div data-js-block="newpasswordLoadingBlock" class="progress collapse">
                                                <div class="progress-bar progress-bar-fp progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                                            </div>
                                            <div class="text-right">
                                                <button class="btn p-ajax-disabled" type="submit" name="confirm"><i class="fa fa-lock"></i>&nbsp;Update password</button>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </form>
                            <?php } else { ?>
                                <form method="post" action="<?php echo strtok( $_SERVER[ 'REQUEST_URI' ], '?' ); ?>" data-js-validate="true" data-js-highlight-state-msg="true" data-js-show-valid-msg="true" data-js-ajax-form="" data-js-ajax-before-hide-block="forgotSuccessBlock;forgotFailBlock" data-js-ajax-fail-show-block="forgotFailBlock" data-js-ajax-success-hide-block="forgotFormBlock" data-js-ajax-success-show-block="forgotSuccessBlock" data-js-ajax-always-hide-block="forgotLoadingBlock" data-js-ajax-before-show-block="forgotLoadingBlock">
                                    <div class="p-title text-left">
                                        <span class="p-title-side">Forgot password?</span>
                                    </div>
                                    <div data-js-block="forgotSuccessBlock" class="collapse">
                                        <h4>Form was sent successfully!</h4>
                                        <div data-js-block="forgotSuccessOutputBlock" class="collapse"></div>
                                        <div class="text-right">
                                            <a href="<?php echo strtok( $_SERVER[ 'REQUEST_URI' ], '?' ); ?>" class="btn">reload</a>
                                        </div>
                                    </div>
                                    <div data-js-block="forgotFailBlock" class="collapse">
                                        <h4>Failed to send form!</h4>
                                        <div data-js-block="forgotFailOutputBlock" class="collapse"></div>
                                    </div>
                                    <div data-js-block="forgotFormBlock">
                                        <p>Enter your email and we'll send you your recovery details.</p>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <?php $formForgotPassword->attributeView( 'fp_email' ); ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <button class="btn p-ajax-disabled" type="submit" name="confirm"><i class="fa fa-envelope-o"></i>&nbsp;send email</button>
                                            </div>
                                        </div>
                                        <div data-js-block="forgotLoadingBlock" class="progress collapse">
                                            <div class="progress-bar progress-bar-fp progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                                        </div>
                                    </div>
                                </form>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php FormsPlusFramework::loadScripts(); ?>
        <?php FormsPlusFramework::outputDebug(); ?>
    </body>
</html>