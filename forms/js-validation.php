<?php
    require_once __DIR__ . '/forms-plus-framework/vendor/autoload.php';
    use FormsPlus\Framework\FormsPlusFramework as FormsPlusFramework;
    $form = new FormsPlusFramework( 'forms-plus-framework/app/config/demos/js-validation.yml' );
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Forms Plus: PHP</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php FormsPlusFramework::loadStyles(); ?>
        <link rel="stylesheet" href="<?php echo FormsPlusFramework::getStaticVar( 'asset_directory_uri' ); ?>css/base.css" type="text/css">
    </head>
    <body>
        <div class="container">
            <form enctype="multipart/form-data" method="post" action="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>" class="<?php $form->designCSSClasses(); ?>" data-js-validate="true" data-js-highlight-state-msg="true" data-js-show-valid-msg="true">
                <div class="p-form p-shadowed p-form-sm">
                    <div class="p-title text-left">
                        <span class="p-title-side">JS Validation&nbsp;&nbsp;<i class="fa fa-check-square-o"></i></span>
                    </div>
                    <?php if( $form->isValid ) { ?>
                        <div class="alert alert-valid"><strong><i class="fa fa-check"></i> Thank you:</strong> message example.</div>
                    <?php } elseif( $form->isSubmitted ) { ?>
                        <?php foreach( $form->errorList as $attributeID => $itemList ) { ?>
                            <div class="alert alert-error"><strong><i class="fa fa-times"></i> <?php echo $form->configuration[ 'attribute_list' ][ $attributeID ][ 'name' ]; ?>:</strong>
                                <?php if( count( $itemList ) == 1 ) { ?>
                                    <?php foreach( $itemList as $errorList ) echo implode( ' ', $errorList ); ?>
                                <?php } else { ?>
                                    <ul><?php foreach( $itemList as $errorList ) echo '<li>' . implode( ' ', $errorList ) . '</li>'; ?></ul>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <?php if( !$form->isValid ) { ?>
                        <button class="btn" type="submit"><i class="fa fa-check-square-o"></i>&nbsp;run validation</button>
                        <div class="p-subtitle text-left">
                            <span class="p-title-side">Fields</span>
                        </div>
                        <div class="row">
                            <?php
                                $delimiterCounter = 0;
                                $subtitleList = array(
                                    'textarea'                  => array( 12 ),
                                    'file'                      => array( 6 ),
                                    'color'                     => array( 6, 'Colors' ),
                                    'imagepicker_single'        => array( 12, 'Picture pick' ),
                                    'blockpicker_single'        => array( 12, 'Block pick' ),
                                    'checkbox_single'           => array( 12, 'Link' ),
                                    'checkbox_multiple'         => array( 4, 'Checkbox and Radio', 'Columned' ),
                                    'checkbox_multiple_inline'  => array( 6, null, 'Inline' ),
                                    'switch_multiple'           => array( 4, 'Switch', 'Columned' ),
                                    'switch_multiple_inline'    => array( 6, null, 'Inline' ),
                                    'captcha'                   => array( 12 )
                                );
                                $columnSize = 6;
                            ?>
                            <?php foreach( $form->attributeList() as $attributeIdentifier ) { ?>
                                <?php if( isset( $subtitleList[ $attributeIdentifier ] ) ) { ?>
                                    </div>
                                    <?php if( !empty( $subtitleList[ $attributeIdentifier ][ 1 ] ) ) { ?>
                                        <div class="p-subtitle text-left">
                                            <span class="p-title-side"><?php echo $subtitleList[ $attributeIdentifier ][ 1 ]; ?></span>
                                        </div>
                                    <?php } ?>
                                    <?php if( !empty( $subtitleList[ $attributeIdentifier ][ 2 ] ) ) { ?>
                                        <h5><?php echo $subtitleList[ $attributeIdentifier ][ 2 ]; ?></h5>
                                    <?php } ?>
                                    <div class="row">
                                    <?php 
                                        $columnSize = $subtitleList[ $attributeIdentifier ][ 0 ]; 
                                        $delimiterCounter = 0;
                                    ?>
                                <?php } elseif( $delimiterCounter % ( 12 / $columnSize ) == 0 && $delimiterCounter != 0 ) { ?>
                                    </div><div class="row">
                                <?php } ?>
                                <div class="col-sm-<?php echo $columnSize; ?>">
                                    <?php $form->attributeView( $attributeIdentifier ); ?>
                                </div>
                                <?php $delimiterCounter++; ?>
                            <?php } ?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="text-right">
                            <button class="btn" type="submit">submit</button>
                            <button class="btn" type="reset">reset</button>
                        </div>
                    <?php } ?>
                </div>
            </form>
        </div>
        <?php FormsPlusFramework::loadScripts(); ?>
        <?php FormsPlusFramework::outputDebug(); ?>
    </body>
</html>