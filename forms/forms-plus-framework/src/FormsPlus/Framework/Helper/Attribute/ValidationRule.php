<?php

namespace FormsPlus\Framework\Helper\Attribute;
use FormsPlus\Framework\Helper\Configuration;
use FormsPlus\Framework\Helper\Storage;
use FormsPlus\Framework\Helper\Debug;
use FormsPlus\Framework\FormsPlusFramework;

class ValidationRule
{
    public static function uniqueValue( $value, $files, $attributeID, $framework, $parameterList, &$last )
    {
        $last = true;
        $framework->initStorage();
        $collectionSet = $framework->storage->fetchCollectionList( null, 0, 0, null, null, array(
            'prop_form_config_file' => $framework->configFile,
            'attr_' . $attributeID  => $value
        ));
        if( $collectionSet[ 'count' ] > 0 ) {
            return true;
        }
    }

    public static function existingValue( $value, $files, $attributeID, $framework, $parameterList, &$last )
    {
        $last = true;
        $form = isset( $parameterList[ 'form_config_file' ] ) ? FormsPlusFramework::initDummy( $parameterList[ 'form_config_file' ] ) : $framework;
        $storage = $form->initStorage();
        $attributeID = isset( $parameterList[ 'attribute_id' ] ) ? $parameterList[ 'attribute_id' ] : $attributeID;
        $collectionSet = $storage->fetchCollectionList( null, 0, 0, null, null, array(
            'prop_form_config_file' => $storage->framework->configFile,
            'attr_' . $attributeID  => $value
        ));
        if( $collectionSet[ 'count' ] === 0 ) {
            return true;
        }
    }

    public static function passwordVerify( $value, $files, $attributeID, $framework, $parameterList, &$last )
    {
        $last = true;
        if( count( array_diff( array( 'collected_login_attribute_id', 'stored_login_attribute_id', 'stored_password_attribute_id', 'form_config_file' ), array_keys( $parameterList ) ) ) !== 0 ) {
            Debug::errorMessage( 'Some required parameters are missing for FormsPlus\Framework\Helper\Attribute\ValidationRule::passwordVerify method', $framework->configFile );
            return true;
        }
        $form = FormsPlusFramework::initDummy( $parameterList[ 'form_config_file' ] );
        $storage = $form->initStorage();
        $collectionSet = $storage->fetchCollectionList( null, 1, 0, null, null, array(
            'prop_form_config_file'                          => $parameterList[ 'form_config_file' ],
            'attr_' . $parameterList[ 'stored_login_attribute_id' ] => $framework->data[ 'attribute_list' ][ $parameterList[ 'collected_login_attribute_id' ] ][ 'final_value' ]
        ));
        if( $collectionSet[ 'count' ] === 1 ) {
            $passwordHash = $collectionSet[ 'list' ][ 0 ][ 'attribute_list' ][ $parameterList[ 'stored_password_attribute_id' ] ][ 'final_value' ];
            if( substr( $passwordHash, 0, 1 ) === '$' ) {
                if( password_verify( $value, $passwordHash ) ) {
                    return false;
                }
            } else {
                if( md5( $value ) === $passwordHash ) {
                    return false;
                }
            }
        }
        return true;
    }

    public static function collectionByAttributeWithCondition( $value, $files, $attributeID, $framework, $parameterList, &$last )
    {
        $last = true;
        $form = isset( $parameterList[ 'form_config_file' ] ) ? FormsPlusFramework::initDummy( $parameterList[ 'form_config_file' ] ) : $framework;
        $storage = $form->initStorage();
        $attributeID = isset( $parameterList[ 'attribute_id' ] ) ? $parameterList[ 'attribute_id' ] : $attributeID;
        $filter = array(
            'prop_form_config_file' => $storage->framework->configFile,
            'attr_' . $attributeID  => $value
        );
        if( isset( $parameterList[ 'condition_list' ] ) && is_array( $parameterList[ 'condition_list' ] ) ) {
            foreach( $parameterList[ 'condition_list' ] as $id => $value ) {
                $filter[ 'attr_' . $id ] = $value;
            }
        }
        $collectionSet = $storage->fetchCollectionList( null, 0, 0, null, null, $filter );
        if( $collectionSet[ 'count' ] > 0 ) {
            return false;
        }
        return true;
    }
}
