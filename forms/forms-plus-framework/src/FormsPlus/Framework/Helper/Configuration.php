<?php

namespace FormsPlus\Framework\Helper;

use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Exception\ParseException;
use FormsPlus\Framework\FormsPlusFramework;
use FormsPlus\Framework\Helper\Debug;

class Configuration
{
    private $framework;
    private $currentConfig;
    private $defaultConfig;
    private $mergedConfig;
    private $importConfig;
    private $yaml;

    public function __construct( $framework )
    {
        $this->framework = $framework;
        $this->yaml = new Parser();
        $this->currentConfig = $this->parseYamlFile( $this->framework->configFile );
        if( $this->currentConfig === null ) {
            return;
        }
        $this->loadDefaultConfig();
        $this->loadImportConfig();
        $this->mergeConfig();
    }

    private function parseYamlFile( $file )
    {
        $fileContent = @file_get_contents( $file );
        if( $fileContent === false ) {
            $error = error_get_last();
            Debug::errorMessage( 'The content of the "' .$file  . '" config file cannot be gotten. ' . $error[ 'message' ], $this->framework->configFile );
            return;
        }
        try {
            $configArray = $this->yaml->parse( $fileContent );
        } catch ( ParseException $e ) {
            Debug::errorMessage( 'YAML error in the "' . $file  . '" config file. ' . $e->getMessage(), $this->framework->configFile );
            return;
        }
        return $configArray;
    }

    private function loadImportConfig()
    {
        // "imports" only work when you set it in the current config
        if( isset( $this->currentConfig[ 'imports' ] ) && !empty( $this->currentConfig[ 'imports' ] ) && is_array( $this->currentConfig[ 'imports' ] ) ) {
            $this->importConfig = array();
            $configFileDirectory = dirname( $this->framework->configFile ) . '/';
            foreach( $this->currentConfig[ 'imports' ] as $import ) {
                $importConfig = $this->parseYamlFile( ( substr( $import, 0, 1 ) == '.' ) ? $configFileDirectory . $import : $import );
                if( empty( $importConfig ) ) {
                    continue;
                }
                $this->importConfig = array_replace_recursive( $this->importConfig, $importConfig );
            }
        }
    }

    private function loadDefaultConfig()
    {
        $bundleRootDirectory = realpath( __DIR__ . '/..' );
        $this->defaultConfig = array();
        $this->defaultConfig[ 'design' ] = $this->parseYamlFile( $bundleRootDirectory . '/Resources/config/design.yml' );
        $this->defaultConfig[ 'attribute' ] = $this->parseYamlFile( $bundleRootDirectory . '/Resources/config/attribute.yml' );
        $this->defaultConfig[ 'form' ] = $this->parseYamlFile( $bundleRootDirectory . '/Resources/config/form.yml' );
    }

    private function mergeConfig()
    {
        $this->mergedConfig = $this->defaultConfig[ 'form' ];
        if( !empty( $this->importConfig ) ) {
            $this->mergedConfig = array_replace_recursive( $this->mergedConfig, $this->importConfig );
        }
        $this->mergedConfig = array_replace_recursive( $this->mergedConfig, $this->currentConfig );
        if( !empty( $this->framework->variableList[ 'configuration' ] ) ) {
            $this->mergedConfig = array_replace_recursive( $this->mergedConfig, $this->framework->variableList[ 'configuration' ] );
        }
        // if a user sets a wrong design name, it overrides it by default one
        if( !isset( $this->defaultConfig[ 'design' ][ $this->mergedConfig[ 'design' ][ 'name' ] ] ) ) {
            $this->mergedConfig[ 'design' ][ 'name' ] = $this->defaultConfig[ 'form' ][ 'design' ][ 'name' ];
        }
        // if a user sets a wrong color that is not support by the current design, it will be overwritten by default one
        if( !in_array( $this->mergedConfig[ 'design' ][ 'color' ], $this->defaultConfig[ 'design' ][ $this->mergedConfig[ 'design' ][ 'name' ] ][ 'color_list' ] ) ) {
            $this->mergedConfig[ 'design' ][ 'color' ] = reset( $this->defaultConfig[ 'design' ][ $this->mergedConfig[ 'design' ][ 'name' ] ][ 'color_list' ] );
        }
        // add a style list of the design
        if( !empty( $this->defaultConfig[ 'design' ][ $this->mergedConfig[ 'design' ][ 'name' ] ][ 'required_style_list' ] ) ) {
            $this->mergedConfig[ 'design' ][ 'required_style_list' ] = $this->defaultConfig[ 'design' ][ $this->mergedConfig[ 'design' ][ 'name' ] ][ 'required_style_list' ];
        }
        // if the attribute array is empty, there is no need to do something with it.
        if( !empty( $this->mergedConfig[ 'attribute_list' ] ) ) {
            $attributeIdentifierList = array_keys( $this->mergedConfig[ 'attribute_list' ] );
            $usedAttributeIdentifierList = ( $this->framework instanceof FormsPlusFramework ) ? FormsPlusFramework::getStaticVar( 'used_attribute_identifier_list' ) : array();
            foreach( $attributeIdentifierList as $attributeIdentifier ) {
                if( preg_match( '/^(formid|fake|showblock|hash|clone)/i', $attributeIdentifier ) ) {
                    Debug::warningMessage( 'The attribute identifier cannot start with "formid", "fake", "showblock", "hash", "clone" they are reserved by the system.', $this->framework->configFile );
                    unset( $this->mergedConfig[ 'attribute_list' ][ $attributeIdentifier ] );
                    continue;
                }
                if( preg_match( '/[^a-z0-9_\-]/', $attributeIdentifier ) ) {
                    Debug::warningMessage( 'The attribute identifier "' . $attributeIdentifier . '" is wrong, an attribute identifier can only contain small latin letters (a-z), numbers (0-9), dashes and underscores.', $this->framework->configFile );
                    unset( $this->mergedConfig[ 'attribute_list' ][ $attributeIdentifier ] );
                    continue;
                }
                if( strlen( $attributeIdentifier ) > 100 ) {
                    Debug::warningMessage( 'The attribute identifier "' . $attributeIdentifier . '" cannot be longer than 100 characters.', $this->framework->configFile );
                    unset( $this->mergedConfig[ 'attribute_list' ][ $attributeIdentifier ] );
                    continue;
                }
                if( !preg_match( '/^[a-z](.*)[a-z0-9]$/', $attributeIdentifier ) ) {
                    Debug::warningMessage( 'The attribute identifier "' . $attributeIdentifier . '" must start with a small latin letter (a-z) and end with a small latin letter (a-z) or number (0-9).', $this->framework->configFile );
                    unset( $this->mergedConfig[ 'attribute_list' ][ $attributeIdentifier ] );
                    continue;
                }
                if( !isset( $this->mergedConfig[ 'attribute_list' ][ $attributeIdentifier ][ 'datatype' ] ) ) {
                    Debug::warningMessage( 'Datatype is not set for the "' . $attributeIdentifier . '" attribute.', $this->framework->configFile );
                    unset( $this->mergedConfig[ 'attribute_list' ][ $attributeIdentifier ] );
                    continue;
                }
                $datatype = $this->mergedConfig[ 'attribute_list' ][ $attributeIdentifier ][ 'datatype' ];
                if( !isset( $this->defaultConfig[ 'attribute' ][ $datatype ] ) ) {
                    Debug::warningMessage( 'There is not such datatype as "' . $datatype . '".', $this->framework->configFile );
                    unset( $this->mergedConfig[ 'attribute_list' ][ $attributeIdentifier ] );
                    continue;
                }
                if( isset( $usedAttributeIdentifierList[ $attributeIdentifier ] ) ) {
                    Debug::warningMessage( 'The attribute identifier "' . $attributeIdentifier . '" is already used in the ' . $usedAttributeIdentifierList[ $attributeIdentifier ] . ' config file.', $this->framework->configFile );
                    unset( $this->mergedConfig[ 'attribute_list' ][ $attributeIdentifier ] );
                    continue;
                }
                $this->mergedConfig[ 'attribute_list' ][ $attributeIdentifier ] = array_replace_recursive( $this->defaultConfig[ 'attribute' ][ $datatype ], $this->mergedConfig[ 'common_attribute_setting_list' ], $this->mergedConfig[ 'attribute_list' ][ $attributeIdentifier ] );
                $this->mergedConfig[ 'attribute_list' ][ $attributeIdentifier ][ 'name' ] = $this->getAttributeName( $this->mergedConfig[ 'attribute_list' ][ $attributeIdentifier ], $attributeIdentifier );
            }
        }
    }

    private function getAttributeName( $attribute, $attributeID )
    {
        $name = $attributeID;
        if( isset( $attribute[ 'name' ] ) && !empty( $attribute[ 'name' ] ) ) {
            $name = $attribute[ 'name' ];
        } elseif( isset( $attribute[ 'label' ] ) && !empty( $attribute[ 'label' ] ) ) {
            $name = $attribute[ 'label' ];
        } elseif( isset( $attribute[ 'placeholder' ] ) && !empty( $attribute[ 'placeholder' ] ) ) {
            $name = $attribute[ 'placeholder' ];
        }
        return $name;
    }

    public function getConfig()
    {
        return $this->mergedConfig;
    }
}
