<?php

namespace FormsPlus\Framework\Helper\Storage;

use FormsPlus\Framework\Helper\Storage\Database;
use FormsPlus\Framework\Helper\Storage;
use FormsPlus\Framework\Helper\Debug;

class Database extends Storage
{
    protected $connectionParameters = array(
        'host'     => 'localhost',
        'name'     => null,
        'user'     => 'root',
        'password' => '',
        'port'     => null
    );
    protected $isConnected = false;
    private static $preparedDatabaseStructureList = array();

    public static function newInstance( $framework )
    {
        if( function_exists( 'mysqli_connect' ) ) {
            return new Database\MySQLi( $framework );
        } elseif( class_exists( 'PDO' ) && in_array( 'mysql', \PDO::getAvailableDrivers() ) ) {
            return new Database\PDOMySQL( $framework );
        } elseif( function_exists( 'mysql_connect' ) ) {
            return new Database\MySQL( $framework );
        } else {
            Debug::errorMessage( 'The current system does not support: MySQLi, PDOMySQL, MySQL.', $framework->configFile );
            return;
        }
    }

    protected function getConnectionParameters()
    {
        if( !isset( $this->framework->configuration[ 'storage' ][ 'database' ] ) ) {
            Debug::errorMessage( 'Database configuration section is not defined', $this->framework->configFile );
            return;
        }
        $configuration = $this->framework->configuration[ 'storage' ][ 'database' ];
        if( !empty( $configuration[ 'host' ] ) ) {
            $this->connectionParameters[ 'host' ] = $configuration[ 'host' ];
        }
        if( !empty( $configuration[ 'user' ] ) ) {
            $this->connectionParameters[ 'user' ] = $configuration[ 'user' ];
        }
        if( !empty( $configuration[ 'password' ] ) ) {
            $this->connectionParameters[ 'password' ] = $configuration[ 'password' ];
        }
        if( !empty( $configuration[ 'name' ] ) ) {
            $this->connectionParameters[ 'name' ] = $configuration[ 'name' ];
        }
        if( !empty( $configuration[ 'port' ] ) ) {
            $this->connectionParameters[ 'port' ] = $configuration[ 'port' ];
        }
    }

    protected function injectValuesIntoQuery( $query, $values = array() )
    {
        $sqlQuery = str_replace( array_keys( $values ), array_map( array( $this, 'escapeString' ), array_values( $values ) ), $query );
        $this->debug( $sqlQuery );
        return $sqlQuery;
    }

    protected function debug( $query )
    {
        if( isset( $this->framework->configuration[ 'debug' ][ 'database' ] ) && $this->framework->configuration[ 'debug' ][ 'database' ] ) {
            $handlerName = substr( get_class( $this ), strrpos( get_class( $this ), '\\' ) + 1 );
            Debug::noticeMessage( "$handlerName query:<br><pre>$query</pre>", $this->framework->configFile );
        }
    }

    protected function prepareStorage()
    {
        return ( $this->prepareDatabaseStructure() && $this->prepareFilesDirectory() );
    }

    private function prepareDatabaseStructure()
    {
        $instanceID = json_encode( $this->connectionParameters );
        // it prevents multiple preparations for one DB access
        if( in_array( $instanceID, self::$preparedDatabaseStructureList ) ) {
            return true;
        }
        if( !$this->isConnected ) {
            return false;
        }
        $query = "CREATE TABLE IF NOT EXISTS `collected_forms` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `created` datetime DEFAULT NULL,
          `storage_id` varchar(36) NOT NULL DEFAULT '',
          `attribute_hash` longtext NOT NULL,
          `config_file` longtext NOT NULL,
          `config_modified` datetime DEFAULT NULL,
          `configuration` longtext NOT NULL,
          `preference_configuration` longtext NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        if( !$this->query( $query ) ) {
            return false;
        }
        $query = "CREATE TABLE IF NOT EXISTS `collected_information` (
          `id` bigint(20) NOT NULL AUTO_INCREMENT,
          `created` datetime DEFAULT NULL,
          `form_id` int(11) NOT NULL,
          `ip` varchar(40) NOT NULL,
          `data` longtext NOT NULL,
          PRIMARY KEY (`id`),
          KEY `index_form_id` (`form_id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        if( !$this->query( $query ) ) {
            return false;
        }
        $query = "CREATE TABLE IF NOT EXISTS `collected_attributes` (
          `collection_id` bigint(20) NOT NULL,
          `attribute_id` varchar(100) NOT NULL DEFAULT '',
          `value` longtext NOT NULL,
          KEY `index_collection_id` (`collection_id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        if( !$this->query( $query ) ) {
            return false;
        }
        self::$preparedDatabaseStructureList[] = $instanceID;
        return true;
    }

    public function getCollectionFileInfo( $collectionID, $attributeID, $fileIndexList )
    {
        $collection = $this->fetchCollection( $collectionID );
        if( empty( $collection ) || !isset( $collection[ 'collection_data' ][ 'file_list' ][ $attributeID ] ) || !is_array( $fileIndexList ) || empty( $fileIndexList ) ) {
            return false;
        }
        $fileInfo = $collection[ 'collection_data' ][ 'file_list' ][ $attributeID ];
        foreach( $fileIndexList as $fileIndex ) {
            if( !isset( $fileInfo[ $fileIndex ] ) ) {
                return false;
            }
            $fileInfo = $fileInfo[ $fileIndex ];
        }
        $fileInfo[ 'file_path' ] = 'db-' . $collection[ 'form_storage_id' ] . '-' . $collection[ 'form_id' ] . '/' . $collection[ 'form_created_short' ]. '/c' . $collectionID;
        $fileInfo[ 'real_path' ] = $this->filesDirectory . '/' . $fileInfo[ 'file_path' ] . '/' . $fileInfo[ 'per_name' ];
        return $fileInfo;
    }

    public function fetchForm( $id )
    {
        $query = "SELECT * FROM collected_forms WHERE id = :id";
        $form = $this->resultQuery( $query, array( ':id' => $id ) );
        if( $form === false || empty( $form ) ) {
            return false;
        }
        $form[ 0 ][ 'configuration' ] = unserialize( $form[ 0 ][ 'configuration' ] );
        $form[ 0 ][ 'preference_configuration' ] = unserialize( $form[ 0 ][ 'preference_configuration' ] );
        return isset( $form[ 0 ] ) ? $form[ 0 ] : $form;
    }

    public function fetchLatestFormListPerConfig()
    {
        $query = "SELECT *
                  FROM collected_forms, ( SELECT MAX( id ) as form_id FROM collected_forms GROUP BY config_file ) AS latest_form_version_per_config
                  WHERE latest_form_version_per_config.form_id = collected_forms.id";
        $formList = $this->resultQuery( $query );
        if( $formList === false ) {
            return false;
        }
        foreach( $formList as $index => $form ) {
            $formList[ $index ][ 'configuration' ] = unserialize( $formList[ $index ][ 'configuration' ] );
        }
        return $formList;
    }

    public function fetchFormListByConfig( $configFile )
    {
        $query = "SELECT * FROM collected_forms WHERE config_file = :config_file ORDER BY created DESC";
        $formList = $this->resultQuery( $query, array( ':config_file' => $configFile ) );
        if( $formList === false ) {
            return false;
        }
        return $formList;
    }

    public function fetchCollection( $id )
    {
        $query = "SELECT
                    collected_information.id AS collection_id,
                    collected_information.created AS collection_created,
                    collected_information.ip AS collection_ip,
                    collected_information.data AS collection_data,
                    collected_forms.id AS form_id,
                    collected_forms.storage_id AS form_storage_id,
                    collected_forms.attribute_hash AS form_attribute_hash,
                    collected_forms.config_file AS form_config_file,
                    collected_forms.config_modified AS form_config_modified,
                    collected_forms.configuration AS form_configuration,
                    DATE_FORMAT( collected_information.created, '%Y%m' ) AS form_created_short
                  FROM collected_information
                  LEFT JOIN collected_forms ON collected_information.form_id=collected_forms.id
                  WHERE collected_information.id = :id";
        $collection = $this->resultQuery( $query, array( ':id' => $id ) );
        if( $collection === false || empty( $collection ) ) {
            return false;
        }
        $this->prepareCollectionList( $collection );
        $collection[ 0 ][ 'form_configuration' ] = unserialize( $collection[ 0 ][ 'form_configuration' ] );
        return $collection[ 0 ];
    }

    private function getTableNameByPropertyID( $name )
    {
        $list = array( 'collection_id', 'collection_created', 'collection_ip', 'collection_data', 'form_id', 'form_created', 'form_storage_id', 'form_attribute_hash', 'form_config_file', 'form_config_modified', 'configuration', 'preference_configuration' );
        if( !in_array( $name, $list ) ) {
            return false;
        }
        if( substr( $name, 0, 11 ) === 'collection_' ) {
            return 'collected_information.' . substr( $name, 11 ) ;
        } elseif( substr( $name, 0, 5 ) === 'form_' ) {
            return 'collected_forms.' . substr( $name, 5 ) ;
        }
    }

    public function fetchCollectionList( $formID = null, $limit = 50, $offset = 0, $sort = array( 'prop_created', true ), $search = '', $filter = null )
    {
        $result = array();
        $queryValueList = array();
        $querySelect = "collected_information.id AS collection_id,
                        collected_information.created AS collection_created,
                        collected_information.ip AS collection_ip,
                        collected_information.data AS collection_data,
                        collected_information.form_id AS form_id";
        $queryFrom = "collected_information LEFT JOIN collected_forms ON ( collected_information.form_id = collected_forms.id )";
        $queryOrder = '';
        $queryWhere = empty( $formID ) ? '' : "form_id = :form_id";
        $queryValueList[ ':form_id' ] = $formID;
        if( $search != '' ) {
            $searchList = array();
            $querySelect = "DISTINCT " . $querySelect;
            preg_match_all( '/"(?:\\\\.|[^\\\\"])*"|\S+/', $search, $searchPregMatch );
            foreach( $searchPregMatch[ 0 ] as $searchPregMatchIndex => $searchPregMatchItem ) {
                $searchCondition = trim( $searchPregMatchItem, '"' );
                $queryFrom .= " INNER JOIN collected_attributes search_attr_$searchPregMatchIndex ON ( search_attr_$searchPregMatchIndex.collection_id = collected_information.id AND search_attr_$searchPregMatchIndex.value LIKE :search_$searchPregMatchIndex )";
                $queryValueList[ ':search_' . $searchPregMatchIndex ] = '%' . str_replace( '*', '%', str_replace( '%', '\%', $searchCondition ) ) . '%';
                $searchList[] = $searchCondition;
            }
            $result[ 'search' ] = $searchList;
        }
        if( is_array( $sort ) && count( $sort ) === 2 ) {
            $orderFieldType = substr( $sort[ 0 ], 0, 5 );
            $orderFieldName = substr( $sort[ 0 ], 5 );
            $orderFullFieldName = $sort[ 0 ];
            if( $orderFieldType == 'attr_' ) {
                $queryFrom .= " LEFT JOIN collected_attributes sort_collected_attributes ON ( collected_information.id = sort_collected_attributes.collection_id AND sort_collected_attributes.attribute_id = :sort_attribute_id )";
                $queryValueList[ ':sort_attribute_id' ] = $orderFieldName;
                $queryOrder = " ORDER BY sort_collected_attributes.value " . ( $sort[ 1 ] ? "ASC" : "DESC" );
            } elseif( $orderFieldType == 'prop_' ) {
                $tableName = $this->getTableNameByPropertyID( $orderFieldName );
                if( $tableName ) {
                    $queryOrder = " ORDER BY $tableName " . ( $sort[ 1 ] ? "ASC" : "DESC" );
                }
            }
        }
        if( is_array( $filter ) && count( $filter ) > 0 ) {
            $counter = 1;
            foreach( $filter as $filterField => $filterCondition ) {
                $filterFieldType = substr( $filterField, 0, 5 );
                $filterFieldName = substr( $filterField, 5 );
                if( $filterFieldType == 'attr_' ) {
                    $queryFrom .= " LEFT JOIN collected_attributes filter_attr_$counter ON ( collected_information.id = filter_attr_$counter.collection_id AND filter_attr_$counter.attribute_id = :filter_attr_" . $counter . "_id )";
                    $queryWhere .= " AND IFNULL( filter_attr_$counter.value, '' ) = :filter_attr_" . $counter . "_value";
                    $queryValueList[ ":filter_attr_" . $counter . "_id" ] = $filterFieldName;
                    $queryValueList[ ":filter_attr_" . $counter . "_value" ] = $filterCondition;
                } elseif( $filterFieldType == 'prop_' ) {
                    $tableName = $this->getTableNameByPropertyID( $filterFieldName );
                    if( $tableName ) {
                        $queryWhere .= " AND $tableName = :filter_prop_" . $counter . "_value";
                        $queryValueList[ ":filter_prop_" . $counter . "_value" ] = $filterCondition;
                    }
                }
                $counter++;
            }
        }
        if( !empty( $queryWhere ) ) {
            $queryWhere = "WHERE " . preg_replace( '/^ *AND */', '', $queryWhere );
        }
        $query = "SELECT SQL_CALC_FOUND_ROWS $querySelect FROM $queryFrom $queryWhere $queryOrder";
        $query .= " LIMIT $offset, $limit";
        $collectionList = $this->resultQuery( $query, $queryValueList );
        if( $collectionList === false ) {
            return false;
        }
        $query = "SELECT FOUND_ROWS() as count";
        $collectionCount = $this->resultQuery( $query );
        if( $collectionCount === false ) {
            return false;
        }
        $result[ 'count' ] = intval( $collectionCount[ 0 ][ 'count' ] );
        $this->prepareCollectionList( $collectionList );
        $result[ 'list' ] = $collectionList;
        return $result;
    }

    public function storeData()
    {
        $currentTime = time();
        $created = date( 'Y-m-d H:i:s', $currentTime );
        $formID = false;
        $configFile = $this->framework->configFile;
        $attributeHash = $this->framework->variableList[ 'attribute_hash' ];
        $configModified = date( 'Y-m-d H:i:s', filemtime( $this->framework->configFile ) );
        $configuration = serialize( $this->framework->configuration );
        $query = "SELECT * FROM collected_forms WHERE config_file = :config_file AND attribute_hash = :attribute_hash LIMIT 1";
        $form = $this->resultQuery( $query, array( ':config_file' => $configFile, ':attribute_hash' => $attributeHash ) );
        if( $form === false ) {
            return false;
        }
        // if there is not form with such config file and attribute hash then we add it into DB, otherwise just get its ID
        if( empty( $form ) ) {
            $preferenceConfiguration = serialize( array(
                'administration' => array(
                    'collection_table' => array(
                        'default_sort_field' => 'prop_collection_created',
                        'default_sort_order' => true,
                        'field_presence' => array(
                            'attribute_list' => array(),
                            'property_list' => array( 'collection_created', 'collection_ip' )
                        )
                    )
                )
            ));
            $query = "SELECT UUID() as uuid";
            $storageID = $this->resultQuery( $query );
            if( $storageID === false ) {
                return false;
            }
            $storageID = $storageID[ 0 ][ 'uuid' ];
            $query = "INSERT INTO collected_forms ( created, storage_id, attribute_hash, config_file, config_modified, configuration, preference_configuration )
                      VALUES ( :created, :storage_id, :attribute_hash, :config_file, :config_modified, :configuration, :preference_configuration )";
            if( !$this->query( $query, array(
                ':created' => $created,
                ':storage_id' => $storageID,
                ':attribute_hash' => $attributeHash,
                ':config_file' => $configFile,
                ':config_modified' => $configModified,
                ':configuration' => $configuration,
                ':preference_configuration' => $preferenceConfiguration
                ))) {
                return false;
            }
            $formID = $this->queryInsertID();
        } else {
            $formID = $form[ 0 ][ 'id' ];
            $storageID = $form[ 0 ][ 'storage_id' ];
            // if the config file was updated, we store its changes into DB
            if( $form[ 0 ][ 'config_modified' ] != $configModified ) {
                $query = "UPDATE collected_forms
                          SET config_modified = :config_modified, configuration = :configuration
                          WHERE id='$formID'";
                $this->query( $query, array( ':config_modified' => $configModified, ':configuration' => $configuration ) );
            }
        }
        if( !$formID ) {
            Debug::errorMessage( 'Form ID cannot be obtained', $this->framework->configFile );
            return false;
        }
        $collectionID = $this->addCollection( array(
            'form_id'            => $formID,
            'collection_created' => $created,
            'collection_ip'      => $_SERVER[ 'REMOTE_ADDR' ],
            'collection_data'    => $this->getDataToStore(),
            'attribute_list'     => $this->framework->data[ 'attribute_list' ]
        ));
        if( !$collectionID ) {
            return false;
        }
        // Create folder and store files if there are available ones
        $this->storeFiles( "db-$storageID-$formID/" . date( 'Ym', $currentTime ) . "/c$collectionID" );
        return $collectionID;
    }

    public function addCollection( $collection )
    {
        if( empty( $collection[ 'form_id' ] ) ) {
            Debug::errorMessage( 'Collection cannot be added if form ID is missing', $this->framework->configFile );
            return false;
        }
        $form = $this->resultQuery( "SELECT collected_forms.id AS form_id, collected_forms.storage_id AS form_storage_id, collected_forms.attribute_hash AS form_attribute_hash, collected_forms.config_file AS form_config_file, collected_forms.config_modified AS form_config_modified, collected_forms.configuration AS form_configuration FROM collected_forms WHERE id = :id", array( ':id' => intval( $collection[ 'form_id' ] ) ) );
        if( empty( $form ) ) {
            Debug::errorMessage( 'There is no form with "' . $collection[ 'form_id' ] . '" ID', $this->framework->configFile );
            return false;
        }
        $valueList = array(
            ':form_id' => intval( $collection[ 'form_id' ] ),
            ':created' => date( 'Y-m-d H:i:s' ),
            ':ip'      => '',
            ':data'    => ''
        );
        $collection = array_merge( $collection, $form[ 0 ] );
        if( isset( $this->framework->configuration[ 'storage' ][ 'collection' ][ 'handler' ] ) ) {
            call_user_func_array(
                $this->framework->configuration[ 'storage' ][ 'collection' ][ 'handler' ],
                array( &$collection, $this->framework )
            );
        }
        if( !empty( $collection[ 'collection_created' ] ) ) {
            $valueList[ ':created' ] = date( 'Y-m-d H:i:s', strtotime( $collection[ 'collection_created' ] ) );
        }
        if( !empty( $collection[ 'collection_ip' ] ) && filter_var( $collection[ 'collection_ip' ], FILTER_VALIDATE_IP ) !== false ) {
            $valueList[ ':ip' ] = $collection[ 'collection_ip' ];
        }
        if( !empty( $collection[ 'collection_data' ] ) ) {
            $valueList[ ':data' ] = serialize( $collection[ 'collection_data' ] );
        }
        $query = "INSERT INTO collected_information ( created, form_id, ip, data ) VALUES ( :created, :form_id, :ip, :data )";
        if( !$this->query( $query, $valueList ) ) {
            return false;
        }
        $collectionID = $this->queryInsertID();
        if( !$collectionID ) {
            Debug::errorMessage( 'Collection ID cannot be obtained', $this->framework->configFile );
            return false;
        }
        if( !empty( $collection[ 'attribute_list' ] ) ) {
            foreach( $collection[ 'attribute_list' ] as $attributeID => $attributeData ) {
                if( isset( $attributeData[ 'final_value' ] ) && strval( $attributeData[ 'final_value' ] ) !== '' ) {
                    $query = "INSERT INTO collected_attributes ( collection_id, attribute_id, value ) VALUES ( :collection_id, :attribute_id, :value )";
                    if( !$this->query( $query, array( ':collection_id' => $collectionID, ':attribute_id' => $attributeID, ':value' => $attributeData[ 'final_value' ] ) ) ) {
                        return false;
                    }
                }
            }
        }
        return $collectionID;
    }

    public function updateFormPreferenceConfiguration( $formID, $configuration )
    {
        $query = "UPDATE collected_forms SET preference_configuration = :preference_configuration WHERE id = :id";
        if( !$this->query( $query, array( ':preference_configuration' => serialize( $configuration ), ':id' => $formID ) ) ) {
            return false;
        }
    }

    public function removeForm( $id )
    {
        $query = "SELECT collected_forms.id, collected_forms.storage_id
                  FROM collected_forms
                  WHERE collected_forms.id = :id";
        $form = $this->resultQuery( $query, array( ':id' => $id ) );
        if( $form === false || empty( $form ) ) {
            return false;
        }
        $form = $form[ 0 ];
        $query = "DELETE collected_forms, collected_information, collected_attributes
                  FROM collected_forms
                  LEFT JOIN collected_information ON collected_forms.id=collected_information.form_id
                  LEFT JOIN collected_attributes ON collected_information.id=collected_attributes.collection_id
                  WHERE collected_forms.id = :id";
        if( !$this->query( $query, array( ':id' => $id ) ) ) {
            return false;
        }
        $formDirectory = $this->filesDirectory . '/db-' . $form[ 'storage_id' ] . '-' . $form[ 'id' ];
        $this->removeDirectory( $formDirectory );
        return true;
    }

    public function removeCollection( $id )
    {
        $collection = $this->fetchCollection( $id );
        if( empty( $collection ) ) {
            return false;
        }
        $query = "DELETE collected_information, collected_attributes
                  FROM collected_information
                  LEFT JOIN collected_attributes ON collected_information.id=collected_attributes.collection_id
                  WHERE collected_information.id = :id";
        if( !$this->query( $query, array( ':id' => $id ) ) ) {
            return false;
        }
        $dateGroupedCollectionsDirectory = $this->filesDirectory . '/db-' . $collection[ 'form_storage_id' ] . '-' . $collection[ 'form_id' ] . '/' . $collection[ 'form_created_short' ];
        $this->removeDirectory( $dateGroupedCollectionsDirectory . '/c' . $id );
        $this->removeDirectory( $dateGroupedCollectionsDirectory, true );
        return true;
    }

    public function updateCollection( $collection )
    {
        if( !empty( $collection[ 'attribute_list' ] ) ) {
            $selectQuery = "SELECT attribute_id FROM collected_attributes WHERE collection_id = :collection_id";
            $insertQuery = "INSERT INTO collected_attributes ( collection_id, attribute_id, value ) VALUES ( :collection_id, :attribute_id, :value )";
            $updateQuery = "UPDATE collected_attributes SET value = :value WHERE collection_id = :collection_id AND attribute_id = :attribute_id";
            $deleteQuery = "DELETE FROM collected_attributes WHERE collection_id = :collection_id AND attribute_id = :attribute_id";
            $currentAttributeList = $this->resultQuery( $selectQuery, array( ':collection_id' => $collection[ 'collection_id' ] ) );
            $currentAttributeIDList = array();
            foreach( $currentAttributeList as $attribute ) {
                $currentAttributeIDList[] = $attribute[ 'attribute_id' ];
            }
            foreach( $collection[ 'attribute_list' ] as $attributeID => $attributeValue ) {
                $attributeFinalValue = trim( strval( $attributeValue[ 'final_value' ] ) );
                $attributeExists = in_array( $attributeID, $currentAttributeIDList );
                if( $attributeFinalValue === '' ) {
                    if( $attributeExists ) {
                        $this->query( $deleteQuery, array( ':collection_id' => $collection[ 'collection_id' ], ':attribute_id' => $attributeID ) );
                    }
                } else {
                    if( $attributeExists ) {
                        $this->query( $updateQuery, array( ':value' => $attributeValue[ 'final_value' ], ':collection_id' => $collection[ 'collection_id' ], ':attribute_id' => $attributeID ) );
                    } else {
                        $this->query( $insertQuery, array( ':value' => $attributeValue[ 'final_value' ], ':collection_id' => $collection[ 'collection_id' ], ':attribute_id' => $attributeID ) );
                    }
                }
            }
        }
    }

    private function prepareCollectionList( &$collectionList )
    {
        foreach( $collectionList as &$collection ) {
            $query = "SELECT * FROM collected_attributes WHERE collection_id = :collection_id";
            $collection[ 'collection_data' ] = empty( $collection[ 'collection_data' ] ) ? array() : unserialize( $collection[ 'collection_data' ] );
            $attributeList = $this->resultQuery( $query, array( ':collection_id' => $collection[ 'collection_id' ] ) );
            $collection[ 'attribute_list' ] = array();
            if( $attributeList !== false && !empty( $attributeList ) ) {
                foreach( $attributeList as $attribute ) {
                    $collection[ 'attribute_list' ][ $attribute[ 'attribute_id' ] ][ 'final_value' ] = $attribute[ 'value' ];
                }
            }
            if( isset( $collection[ 'collection_data' ][ 'file_list' ] ) ) {
                foreach( $collection[ 'collection_data' ][ 'file_list' ] as $attributeID => $attributeFile ) {
                    $collection[ 'attribute_list' ][ $attributeID ][ 'files' ] = $attributeFile;
                }
            }
            if( isset( $collection[ 'collection_data' ][ 'attribute_list' ] ) ) {
                $collection[ 'attribute_list' ] = array_replace_recursive( $collection[ 'attribute_list' ], $collection[ 'collection_data' ][ 'attribute_list' ] );
            }
        }
    }
}
