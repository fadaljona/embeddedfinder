<?php

namespace FormsPlus\Framework\Helper\Storage;

use FormsPlus\Framework\Helper\Storage;

class None extends Storage
{
    public static function newInstance( $framework )
    {
        return new self( $framework );
    }

    public function prepareStorage()
    {
        return true;
    }

    public function storeData()
    {
        return true;
    }
}
