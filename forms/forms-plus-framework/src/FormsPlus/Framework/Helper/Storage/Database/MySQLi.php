<?php

namespace FormsPlus\Framework\Helper\Storage\Database;

use FormsPlus\Framework\Helper\Storage\Database;
use FormsPlus\Framework\Helper\Debug;

class MySQLi extends Database
{
    private $db;

    public function __construct( $framework )
    {
        $this->framework = $framework;
        $this->getConnectionParameters();
        $this->connect();
    }

    private function connect()
    {
        $this->db = mysqli_connect(
            $this->connectionParameters[ 'host' ],
            $this->connectionParameters[ 'user' ],
            $this->connectionParameters[ 'password' ],
            $this->connectionParameters[ 'name' ],
            $this->connectionParameters[ 'port' ]
        );
        if( mysqli_connect_errno() ) {
            Debug::errorMessage( 'MySQLi connection error: ' . mysqli_connect_error(), $this->framework->configFile );
            return;
        }
        mysqli_set_charset( $this->db, 'utf8' );
        $this->isConnected = true;
    }

    public function escapeString( $value )
    {
        return "'" . mysqli_real_escape_string( $this->db, $value ) . "'";
    }

    public function queryInsertID()
    {
        return mysqli_insert_id( $this->db );
    }

    public function resultQuery( $query, $values = array() )
    {
        $response = mysqli_query( $this->db, $this->injectValuesIntoQuery( $query, $values ) );
        if( !$response ) {
            Debug::errorMessage( 'MySQLi result query error: ' . mysqli_error( $this->db ), $this->framework->configFile );
            return false;
        }
        $result = array();
        while( $row = mysqli_fetch_assoc( $response ) ) {
            $result[] = $row;
        }
        mysqli_free_result( $response );
        return $result;
    }

    public function query( $query, $values = array() )
    {
        if( mysqli_query( $this->db, $this->injectValuesIntoQuery( $query, $values ) ) ) {
            return true;
        } else {
            Debug::errorMessage( 'MySQLi query error: ' . mysqli_error( $this->db ), $this->framework->configFile );
            return false;
        }
    }
}
