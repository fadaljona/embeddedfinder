<?php

namespace FormsPlus\Framework\Helper\Storage\Database;

use FormsPlus\Framework\Helper\Storage\Database;
use FormsPlus\Framework\Helper\Debug;

class MySQL extends Database
{
    private $db;

    public function __construct( $framework )
    {
        $this->framework = $framework;
        $this->getConnectionParameters();
        $this->connect();
    }

    private function connect()
    {
        $this->db = mysql_connect(
            $this->connectionParameters[ 'host' ] . ( !empty( $this->connectionParameters[ 'port' ] ) ? ':' . $this->connectionParameters[ 'port' ] : '' ),
            $this->connectionParameters[ 'user' ],
            $this->connectionParameters[ 'password' ]
        );
        if( !$this->db ) {
            Debug::errorMessage( 'MySQL connection error: ' . mysql_error(), $this->framework->configFile );
            return;
        }
        $dbSelection = mysql_select_db( $this->connectionParameters[ 'name' ], $this->db );
        if( !$dbSelection ) {
            Debug::errorMessage( 'MySQL selection error: ' . mysql_error( $this->db ), $this->framework->configFile );
            return;
        }
        mysql_set_charset( 'utf8', $this->db );
        $this->isConnected = true;
    }

    public function escapeString( $value )
    {
        return "'" . mysql_real_escape_string( $value, $this->db ) . "'";
    }

    public function queryInsertID()
    {
        return mysql_insert_id( $this->db );
    }

    public function resultQuery( $query, $values = array() )
    {
        $response = mysql_query( $this->injectValuesIntoQuery( $query, $values ), $this->db );
        if( !$response ) {
            Debug::errorMessage( 'MySQL result query error: ' . mysql_error( $this->db ), $this->framework->configFile );
            return false;
        }
        $result = array();
        while( $row = mysql_fetch_assoc( $response ) ) {
            $result[] = $row;
        }
        mysql_free_result( $response );
        return $result;
    }

    public function query( $query, $values = array() )
    {
        if( mysql_query( $this->injectValuesIntoQuery( $query, $values ), $this->db ) ) {
            return true;
        } else {
            Debug::errorMessage( 'MySQL query error: ' . mysql_error( $this->db ), $this->framework->configFile );
            return false;
        }
    }
}
