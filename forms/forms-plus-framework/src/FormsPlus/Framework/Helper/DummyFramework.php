<?php

namespace FormsPlus\Framework\Helper;

use FormsPlus\Framework\Helper\Storage;
use FormsPlus\Framework\Helper\Configuration;

class DummyFramework
{
    private $configFile;
    private $configuration;
    private $storage;

    public function __construct( $configFile )
    {
        $this->configFile = $configFile;
        $configuration = new Configuration( $this );
        $this->configuration = $configuration->getConfig();
    }

    public function __get( $name )
    {
        return isset( $this->$name ) ? $this->$name : null;
    }

    public function initStorage()
    {
        if( is_null( $this->storage ) ) {
            $this->storage = Storage::newInstance( $this );
        }
        return $this->storage;
    }
}
