<?php

namespace FormsPlus\Framework\Helper;

class Mail
{
    private $framework;
    private $configuration;

    public function __construct( $framework )
    {
        $this->framework = $framework;
        if( !isset( $framework->configuration[ 'mail' ] ) ) {
            Debug::errorMessage( 'Mail configuration section is not defined in the config file.', $this->framework->configFile );
            return;
        }
        $this->configuration = $framework->configuration[ 'mail' ];
    }

    private function getMailer()
    {
        switch( $this->configuration[ 'transport' ] ) {
            case 'smtp':
                $transport = \Swift_SmtpTransport::newInstance();
                if( !empty( $this->configuration[ 'smtp' ][ 'host' ] ) ) {
                    $transport->setHost( $this->configuration[ 'smtp' ][ 'host' ] );
                }
                if( !empty( $this->configuration[ 'smtp' ][ 'port' ] ) ) {
                    $transport->setPort( $this->configuration[ 'smtp' ][ 'port' ] );
                }
                if( !empty( $this->configuration[ 'smtp' ][ 'username' ] ) ) {
                    $transport->setUsername( $this->configuration[ 'smtp' ][ 'username' ] );
                }
                if( !empty( $this->configuration[ 'smtp' ][ 'password' ] ) ) {
                    $transport->setPassword( $this->configuration[ 'smtp' ][ 'password' ] );
                }
                if( !empty( $this->configuration[ 'smtp' ][ 'encryption' ] ) ) {
                    $transport->setEncryption( $this->configuration[ 'smtp' ][ 'encryption' ] );
                }
                break;
            case 'sendmail':
                $transport = \Swift_SendmailTransport::newInstance();
                if( isset( $this->configuration[ 'sendmail' ][ 'command' ] ) && !empty( $this->configuration[ 'sendmail' ][ 'command' ] ) ) {
                    $transport->setCommand( $this->configuration[ 'sendmail' ][ 'command' ] );
                }
                break;
            case 'mail':
            case 'nativemail':
                $transport = \Swift_MailTransport::newInstance();
                break;
            default:
                throw new Exception( 'Undefined transport [' . $this->configuration[ 'transport' ] . ']' );
        }
        return new \Swift_Mailer( $transport );
    }

    public function sendMail()
    {
        if( empty( $this->configuration[ 'message' ] ) ) {
            return;
        }
        foreach( $this->configuration[ 'message' ] as $groupID => $groupSettings ) {
            if( preg_match( '/[^a-z0-9_\-]/', $groupID ) ) {
                Debug::warningMessage( 'The mail message group identifier "' . $groupID . '" is wrong, it can only contain small latin letters (a-z), numbers (0-9), dashes and underscores.', $this->framework->configFile );
                continue;
            }
            if( !empty( $groupSettings[ 'handler' ] ) ) {
                call_user_func_array( $groupSettings[ 'handler' ], array( &$groupSettings, $this->framework ) );
            }
            $recipientList = !empty( $groupSettings[ 'recipient_list' ] ) ? $this->getEmailAddressList( $groupSettings[ 'recipient_list' ] ) : null;
            $ccRecipientList = !empty( $groupSettings[ 'cc_recipient_list' ] ) ? $this->getEmailAddressList( $groupSettings[ 'cc_recipient_list' ] ) : null;
            $bccRecipientList = !empty( $groupSettings[ 'bcc_recipient_list' ] ) ? $this->getEmailAddressList( $groupSettings[ 'bcc_recipient_list' ] ) : null;
            $sender = !empty( $groupSettings[ 'sender' ] ) ? $this->getEmailAddressList( $groupSettings[ 'sender' ] ) : null;
            $from = !empty( $groupSettings[ 'from' ] ) ? $this->getEmailAddressList( $groupSettings[ 'from' ] ) : null;
            $replyTo = !empty( $groupSettings[ 'reply_to' ] ) ? $this->getEmailAddressList( $groupSettings[ 'reply_to' ] ) : null;
            if( empty( $sender ) && empty( $from ) ) {
                continue;
            }
            if( empty( $from ) ) {
                $from = $sender;
            }
            if( empty( $recipientList ) && empty( $ccRecipientList ) && empty( $bccRecipientList ) ) {
                continue;
            }
            $message = \Swift_Message::newInstance();
            if( !empty( $recipientList ) ) {
                $message->setTo( $recipientList );
            }
            if( !empty( $ccRecipientList ) ) {
                $message->setCc( $ccRecipientList );
            }
            if( !empty( $bccRecipientList ) ) {
                $message->setBcc( $bccRecipientList );
            }
            if( !empty( $groupSettings[ 'subject' ] ) ) {
                $message->setSubject( str_replace( '{name}', $this->framework->configuration[ 'name' ], $this->extractAttributeValue( $groupSettings[ 'subject' ] ) ) );
            }
            if( !empty( $from ) ) {
                $message->setFrom( $from );
            }
            if( !empty( $sender ) ) {
                $message->setSender( $sender );
            }
            if( !empty( $replyTo ) ) {
                $message->setReplyTo( $replyTo );
            }
            $templateType = 'text';
            if( isset( $groupSettings[ 'template_type' ] ) && in_array( $groupSettings[ 'template_type' ], array( 'text', 'html' ) ) ) {
                $templateType = $groupSettings[ 'template_type' ];
            }
            if( !empty( $groupSettings[ 'content_type' ] ) ) {
                $message->setContentType( $groupSettings[ 'content_type' ] );
            } else {
                $message->setContentType( ( $templateType == 'html' ) ? 'text/html' : 'text/plain' );
            }
            $content = $this->framework->getTemplateResult(
                'mail/' . $groupID . 'Message.' . ( ( $templateType == 'html' ) ? 'html' : 'txt' ) . '.php',
                array(
                    'configFile'    => $this->framework->configFile,
                    'data'          => $this->framework->data,
                    'configuration' => $this->framework->configuration,
                    'parameterList' => !empty( $groupSettings[ 'template_parameter_list' ] ) ? $groupSettings[ 'template_parameter_list' ] : array()
                )
            );
            if( isset( $this->framework->configuration[ 'debug' ][ 'mail' ] ) && $this->framework->configuration[ 'debug' ][ 'mail' ] ) {
                Debug::noticeMessage( "Message for <strong>$groupID</strong> email group:<br><pre>$content</pre>", $this->framework->configFile );
            }
            $message->setBody( $content );
            if( !empty( $this->framework->data[ 'file_list' ] ) && isset( $groupSettings[ 'attach_files' ] ) && $groupSettings[ 'attach_files' ] ) {
                foreach( $this->framework->data[ 'file_list' ] as $attributeFileList ) {
                    foreach( $attributeFileList as $attributeFile ) {
                        $fileList = isset( $attributeFile[ 'name' ] ) ? array( $attributeFile ) : $attributeFile;
                        foreach( $fileList as $file ) {
                            $message->attach(
                                \Swift_Attachment::fromPath( $file[ 'tmp_name' ], $file[ 'type' ] )->setFilename( $file[ 'name' ] )
                            );
                        }
                    }
                }
            }
            $mailer = $this->getMailer();
            try {
                $result = $mailer->send( $message );
                if( !$result ) {
                    Debug::errorMessage( 'Email messages were not sent for "' . $groupID . '" group.', $this->framework->configFile );
                }
            } catch( \Swift_TransportException $e ) {
                Debug::errorMessage( 'Sending email error. ' . $e->getMessage(), $this->framework->configFile );
            }
        }
    }

    private function getEmailAddressList( $list )
    {
        if( !is_array( $list ) ) {
            $email = $this->extractAttributeValue( $list );
            return \Swift_Validate::email( $email ) ? $email : null;
        }
        $result = array();
        foreach( $list as $key => $value ) {
            $hasName = is_string( $key );
            $email = $this->extractAttributeValue( $hasName ? $key : $value );
            $name = $hasName ? $this->extractAttributeValue( $value ) : null;
            if( !\Swift_Validate::email( $email ) ) {
                continue;
            }
            if( empty( $name ) ) {
                $result[] = $email;
            } else {
                $result[ $email ] = $name;
            }
        }
        return $result;
    }

    private function extractAttributeValue( $value )
    {
        $dataAttributeList = $this->framework->data[ 'attribute_list' ];
        $configurationAttributeList = $this->framework->configuration[ 'attribute_list' ];
        return trim( preg_replace_callback( "/(@[a-z0-9_\-]+@|@@[a-z0-9_\-]+@@)/", function( $matches ) use ( &$dataAttributeList, &$configurationAttributeList ) {
            $value = $matches[ 0 ];
            if( substr( $value, 0, 2 ) == '@@' ) {
                $datatype = trim( $value, '@' );
                foreach( $configurationAttributeList as $attributeID => $attribute ) {
                    if( $attribute[ 'datatype' ] == $datatype ) {
                        if( !empty( $dataAttributeList[ $attributeID ][ 'string_value' ] ) ) {
                            return $dataAttributeList[ $attributeID ][ 'string_value' ];
                        }
                        break;
                    }
                }
            } else {
                $attributeID = trim( $value, '@' );
                if( !empty( $dataAttributeList[ $attributeID ][ 'string_value' ] ) ) {
                    return $dataAttributeList[ $attributeID ][ 'string_value' ];
                }
            }
            return '';
        }, $value ) );
    }
}
