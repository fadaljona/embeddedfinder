<?php

return array(
    'base.site.title' => 'Forms Plus: 管理パネル',
    'base.site.shortTitle' => 'Forms +',
    'base.header.logOut' => 'ログアウト',
    'base.sidebarMenu.home' => 'ホーム',
    'base.sidebarMenu.search' => '検索',
    'base.sidebarMenu.forms' => 'フォーム',
    'base.sidebarMenu.system' => 'システム',
    'base.sidebarMenu.language' => '言語',
    'base.paginator.previous' => '前へ',
    'base.paginator.next' => '次へ',
    'base.paginator.info' => '全 %total% エントリー中 %first% から %last% まで表示',
    'base.formView.button.selectFormVersion' => 'フォームバージョンを選択',
    'base.formView.button.preferences' => '設定',
    'base.formView.button.removeCurrentVersion' => '現在のバージョンを削除',
    'base.formView.alert.noFormWithID' => 'このIDのフォームがありません',
    'base.formInfo.panel.name' => 'フォーム情報',
    'base.formInfo.label.created' => '作成しました',
    'base.formInfo.label.attributeList' => '属性リスト',
    'base.formInfo.label.collectionNumber' => '登録数',
    'base.collectionExport.panel.name' => 'エクスポート',
    'base.collectionExport.format.htm.name' => 'HTML (.htm)',
    'base.collectionExport.format.htm.title' => 'HTMLへエクスポート',
    'base.collectionExport.format.csv.name' => 'CSV (.csv)',
    'base.collectionExport.format.csv.title' => 'CSVへエクスポート',
    'base.collectionExport.format.ods.name' => 'OpenDocument Spreadsheet (.ods)',
    'base.collectionExport.format.ods.title' => 'OpenDocument Spreadsheetへエクスポート',
    'base.collectionExport.format.pdf.name' => 'PDF (.pdf)',
    'base.collectionExport.format.pdf.title' => 'PDFへエクスポート',
    'base.collectionExport.format.xls.name' => 'Excel 95以降 (.xls)',
    'base.collectionExport.format.xls.title' => 'Excel 95以降へエクスポート',
    'base.collectionExport.format.xlsx.name' => 'Excel 2007以降 (.xlsx)',
    'base.collectionExport.format.xlsx.title' => 'Excel 2007以降へエクスポート',
    'base.collectionImport.panel.name' => 'インポート',
    'base.collectionImport.label.sourceFile' => 'ソースファイル',
    'base.collectionImport.label.columnComparison' => '列をと比較',
    'base.collectionImport.label.attributeID' => 'アトリビュートID',
    'base.collectionImport.label.attributeName' => 'アトリビュート名',
    'base.collectionImport.button.uploadFile' => 'ファイルアップロード',
    'base.collectionList.panel.name' => '収集データ',
    'base.collectionList.column.id' => 'ID',
    'base.collectionList.column.submitted' => '提出済',
    'base.collectionList.column.ip' => 'IP',
    'base.collectionList.field.search' => '検索する',
    'base.collectionList.button.search' => '検索',
    'base.collectionList.button.removeSelected' => '削除選択',
    'base.collectionList.button.sort' => 'ソート',
    'base.collectionList.select.pageLimit' => '1ページに %limit% を表示',
    'base.collectionList.alert.noCollections' => '登録はありません',
    'base.formPreferences.panel.name' => '設定',
    'base.formPreferences.label.defaultSortField' => '既定のソートフィルド',
    'base.formPreferences.label.defaultSortOrder' => '既定のソート順',
    'base.formPreferences.label.ascending' => '昇順',
    'base.formPreferences.label.descending' => '降順',
    'base.formPreferences.button.applyChanges' => '変更を適用',
    'base.formPreferences.button.cancel' => 'キャンセル',
    'base.formRemove.panel.name' => 'フォームバージョン削除',
    'base.formRemove.alert.confirmationMessage' => 'フォームバージョン削除確認、すべてが削除されます。',
    'base.formRemove.button.confirmRemoval' => '削除確認',
    'base.formRemove.button.cancel' => 'キャンセル',
    'base.collectionSearch.heading' => '検索',
    'base.collectionSearch.column.collection' => '登録',
    'base.collectionSearch.column.submitted' => '提出済',
    'base.collectionSearch.column.form' => 'フォーム',
    'base.collectionSearch.column.content' => '内容',
    'base.collectionSearch.alert.noCollections' => '検索基準と一致する登録はありません。',
    'base.systemInfo.heading' => 'システム情報',
    'base.systemInfo.label.formPlusVersion' => 'システムバージョン',
    'base.systemInfo.label.phpVersion' => 'PHPバージョン',
    'base.systemInfo.label.storageType' => 'ストレージタイプ',
    'base.systemInfo.label.databaseHandler' => 'データベースハンドラー',
    'base.formList.heading' => 'データ収集フォーム',
    'base.formList.alert.noCollectedData' => 'データ収集フォーム',
    'base.formList.column.formName' => 'フォーム名',
    'base.formList.column.created' => '作成済',
    'base.formList.column.config' => '設定',
    'base.formDataExport.heading' => '%form% 収集データをファイルへエクスポート',
    'base.formDataExport.panel.name' => 'エクスポート設定',
    'base.formDataExport.label.showColumns' => '列表示（ドラッグ＆ドロップ）',
    'base.formDataExport.label.exportFormat' => 'エクスポート形式',
    'base.formDataExport.label.pageOrientation' => 'ページの向き',
    'base.formDataExport.label.portrait' => '縦',
    'base.formDataExport.label.landscape' => '横',
    'base.formDataExport.label.gridLines' => 'グリッド線',
    'base.formDataExport.label.show' => '表示',
    'base.formDataExport.label.hide' => '隠す',
    'base.formDataExport.button.export' => 'エクスポート',
    'base.formDataExport.button.cancel' => 'キャンセル',
    'base.formDataExport.alert.formatNotSpecified' => 'エクスポート形式は指定されていません。',
    'base.formDataExport.alert.absentColumns' => 'O選択されてる列がないとファイル生成できません。',
    'base.formDataImport.heading' => '%file% ファイルより %form% へインポートする',
    'base.formDataImport.panel.name' => 'インポート設定',
    'base.formDataImport.alert.importFileMissing' => 'インポートするファイルがありません。',
    'base.formDataImport.alert.importDataMissing' => 'インポートするデータがありません。',
    'base.formDataImport.alert.fileFormatNotSupported' => 'このファイル形式がサポートされていません。',
    'base.formDataImport.alert.fileUploadError' => 'ファイルアップロードエラー。',
    'base.formDataImport.alert.importedRows' => '%number% 行がインポート成功しました。',
    'base.formDataImport.alert.noDataInSourceFile' => 'このファイルにはデータがありません。',
    'base.formDataImport.label.notImport' => 'インポートしない',
    'base.formDataImport.label.firstDataRowPresence' => 'Pデータの一行目',
    'base.formDataImport.label.exclude' => '削除',
    'base.formDataImport.label.include' => '挿入',
    'base.formDataImport.button.export' => 'インポート',
    'base.formDataImport.button.cancel' => 'キャンセル',
    'base.formDataImport.panel.name' => 'インポート',
    'base.collectionView.heading' => '登録 # %id%',
    'base.collectionView.panel.name' => '収集データ',
    'base.collectionView.table.formAttributes' => 'フォーム属性',
    'base.collectionView.column.name' => '名',
    'base.collectionView.column.value' => '値',
    'base.collectionView.button.backToList' => 'リストへ戻る',
    'base.collectionView.button.editCollection' => '登録編集',
    'base.collectionView.alert.noCollectionWithID' => 'このIDの登録はありません。',
    'base.collectionEdit.panel.name' => '収集データ編集',
    'base.collectionEdit.button.save' => '保存',
    'base.collectionEdit.button.cancel' => 'キャンセル',
    'base.collectionEdit.alert.noEditableAttributes' => '編集できる属性はありません。',
    'base.collectionEdit.alert.consideredEditable' => '%list% 以外の属性は編集可能と見なされます。',
    'base.fileDownload.panel.name' => 'ダウンロードするファイル',
    'base.fileDownload.table.fileList' => 'ファイルリスト',
    'base.fileDownload.column.name' => '名',
    'base.fileDownload.column.size' => 'サイズ',
    'base.fileDownload.column.type' => 'タイプ',
    'base.fileDownload.column.download' => 'ダウンロード',
    'base.fileDownload.alert.fileNotFoundInData' => '収集データでファイルを見つかりません。',
    'base.fileDownload.alert.fileNotFoundInStorage' => 'ストレージドライブでこのファイルを見つかりません。',
    'base.fileDownload.alert.noFileAttributeWithID' => 'このIDを持つファイル属性はありません。',
    'base.languageSwitch.alert.noLocaleWithID' => 'このIDの地域はありません。',
    'base.datatable.sZeroRecords' => '一致する登録が見つかりません。',
    'base.datatable.sInfo' => '_TOTAL_登録中の _START_ ～ _END_ を表示',
    'base.datatable.sInfoEmpty' => '0登録中の0～0を表示',
    'base.datatable.sInfoFiltered' => '( _MAX_ 全登録フィルター処理しました。)',
    'base.datatable.sLengthMenu' => '登録 _MENU_ 表示',
    'base.datatable.sSearch' => '検索',
    'base.datatable.paginate.next' => '次へ',
    'base.datatable.paginate.previous' => '前へ',
    'login.form.name' => 'ログイン',
    'login.form.field.username' => 'ユーザー名',
    'login.form.field.password' => 'パスワード',
    'login.form.button.login' => 'ログイン',
    'login.form.alert.incorrectUsernamePassword' => 'ユーザー名とパスワードが間違っています。'
);
