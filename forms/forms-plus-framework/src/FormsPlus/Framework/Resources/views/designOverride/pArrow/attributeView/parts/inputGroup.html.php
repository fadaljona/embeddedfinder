<?php
    use FormsPlus\Framework\Helper\Template;
    $inputGroupElement = ( !empty( $input[ 'type' ] ) && $input[ 'type' ] == 'select' ) ?  array( 'tag' => 'span', 'html_attribute_list' => array( 'class' => 'input-group p-custom-arrow' ) ) :  array( 'tag' => 'div', 'html_attribute_list' => array( 'class' => 'input-group' ) );
    if( !empty( $inputGroup[ 'html_attribute_list' ] ) ) $inputGroupElement[ 'html_attribute_list' ] = array_merge( $inputGroupElement[ 'html_attribute_list' ], $inputGroup[ 'html_attribute_list' ] );
?>
<<?php echo $inputGroupElement[ 'tag' ]; ?><?php echo Template::generateHTMLAttributes( $inputGroupElement[ 'html_attribute_list' ] ); ?>>
    <?php /* 'input-addon', {data: {addon : field.textLeft} }) */ ?>
    <?php if( !empty( $attribute[ 'icon' ] ) ) { ?>
        <span class="input-group-addon">
            <span class="p-addon-bg"><i class="<?php echo $attribute[ 'icon' ]; ?>"></i></span>
        </span>
    <?php } ?>
    <?php if( isset( $attribute[ 'browse_button_side' ] ) && $attribute[ 'browse_button_side' ] == 'left' ) { ?>
        <span class="input-group-btn">
            <button type="button" class="btn"><?php echo $attribute[ 'browse_button_name' ]; ?></button>
        </span>
    <?php } ?>
    <span class="p-field-wrap">
        <span class="p-positioned">
            <div class="p-inner-table">
                <?php if( !empty( $input[ 'type' ] ) && $input[ 'type' ] == 'textarea' ) { ?>
                    <textarea <?php echo Template::generateHTMLAttributes( $input ); ?>><?php echo !is_null( $value ) ? htmlentities( $value ) : ''; ?></textarea>
                <?php } elseif( !empty( $input[ 'type' ] ) && $input[ 'type' ] == 'select' ) { ?>
                    <select <?php echo Template::generateHTMLAttributes( $input ); ?>>
                        <?php if( !empty( $attribute[ 'blank_option' ] ) ) { ?>
                            <option class="p-select-default" value="" disabled <?php echo ( is_null( $value ) || $value == '' ) ? ' selected' : ''; ?>><?php echo $attribute[ 'blank_option' ]; ?></option>
                        <?php } ?>
                        <?php foreach( $attribute[ 'option_list' ] as $index => $option ) { ?>
                            <?php if( empty( $option[ 'name' ] ) ) continue; ?>
                            <?php
                                $optionAttributeList = array();
                                Template::setJsAttributesByPrefix( $optionAttributeList, $option );
                            ?>
                            <option value="<?php echo $index; ?>"<?php echo ( empty( $inputGroup[ 'ignore_value' ] ) && Template::isItemActive( $index, $option, 'selected', $value, $isSubmitted ) ) ? ' selected' : ''; ?><?php echo Template::generateHTMLAttributes( $optionAttributeList ); ?>><?php echo $option[ 'name' ]; ?></option>
                        <?php } ?>
                    </select>
                <?php } elseif( !empty( $input ) ) { ?>
                    <input <?php echo Template::generateHTMLAttributes( $input ); ?>/>
                <?php } ?>
                <?php echo !empty( $inputGroup[ 'after_input_html' ] ) ? $inputGroup[ 'after_input_html' ] : ''; ?>
                <?php self::includeTemplate( 'attributeView/parts/inputState.html.php', array( 'attribute' => $attribute ) ); ?>
                <?php self::includeTemplate( 'attributeView/parts/tooltip.html.php', array( 'attribute' => $attribute ) ); ?>
                <div class="p-arrow"></div>
            </div>
        </span>
        <span class="p-field-cb"></span>
    </span>
    <?php if( !empty( $inputGroup[ 'js_addon_html' ] ) ) { ?>
        <span class="input-group-addon">
            <?php echo $inputGroup[ 'js_addon_html' ]; ?>
        </span>
    <?php } ?>
    <?php if( isset( $input[ 'type' ] ) && $input[ 'type' ] == 'select' && empty( $attribute[ 'multiple' ] ) ) { ?>
        <span class="p-select-arrow"><i class="fa fa-caret-down"></i></span>
    <?php } ?>
    <?php if( !empty( $attribute[ 'addon' ] ) ) { ?>
        <span class="input-group-addon">
            <span class="p-addon-bg"><?php echo $attribute[ 'addon' ]; ?></span>
        </span>
    <?php } ?>
    <?php if( isset( $attribute[ 'browse_button_side' ] ) && $attribute[ 'browse_button_side' ] == 'right' ) { ?>
        <span class="input-group-btn">
            <button type="button" class="btn"><?php echo $attribute[ 'browse_button_name' ]; ?></button>
        </span>
    <?php } ?>
</<?php echo $inputGroupElement[ 'tag' ]; ?>>