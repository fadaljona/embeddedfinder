<div class="p-field-value">
    <span class="p-value-label"><?php echo $attribute[ 'name' ]; ?>:</span>
    <span class="p-value-text p-colored-text"><?php echo isset( $data[ 'string_value' ] ) ? $data[ 'string_value' ] : '-'; ?></span>
</div>