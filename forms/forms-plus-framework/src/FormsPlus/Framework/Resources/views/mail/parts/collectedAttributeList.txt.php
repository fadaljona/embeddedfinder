<?php
    $allGroupAttributeIDList = !empty( $configuration[ 'cloning_attribute_group_list' ] ) ? array_unique( call_user_func_array( 'array_merge', $configuration[ 'cloning_attribute_group_list' ] ) ) : array();
    foreach( $data[ 'attribute_list' ] as $attributeID => $attribute ) {
        if( $attribute[ 'final_value' ] != '' && !( in_array( $attributeID, $allGroupAttributeIDList ) && is_array( $attribute[ 'string_value' ] ) ) && ( isset( $configuration[ 'attribute_list' ][ $attributeID ][ 'in_mail' ] ) && $configuration[ 'attribute_list' ][ $attributeID ][ 'in_mail' ] ) ) {
            echo $attribute[ 'name' ] . ': ' . $attribute[ 'final_value' ] . "\n";
        }
    }
    if( !empty( $configuration[ 'cloning_attribute_group_list' ] ) ) {
        echo "\n";
        foreach( $configuration[ 'cloning_attribute_group_list' ] as $groupName => $groupAttributeIDList ) {
            $attributeValueList = array();
            foreach( $groupAttributeIDList as $attributeID ) {
                if( !isset( $data[ 'attribute_list' ][ $attributeID ] ) || !is_array( $data[ 'attribute_list' ][ $attributeID ][ 'string_value' ] ) || empty( $configuration[ 'attribute_list' ][ $attributeID ][ 'in_mail' ] ) ) continue;
                $attributeValue = array_filter( $data[ 'attribute_list' ][ $attributeID ][ 'string_value' ], 'strlen' );
                if( empty( $attributeValue ) ) continue;
                $attributeValueList[ $attributeID ] = $attributeValue;
            }
            if( empty( $attributeValueList ) ) continue;
            $indexList = array_keys( call_user_func_array( 'array_replace', $attributeValueList ) );
            echo $groupName . ":\n";
            foreach( $indexList as $index ) {
                foreach( $attributeValueList as $attributeID => $attributeValue ) {
                    if( !isset( $attributeValue[ $index ] ) ) continue;
                    echo '   ' . $data[ 'attribute_list' ][ $attributeID ][ 'name' ] . ': ' . $attributeValue[ $index ] . "\n";
                }
                echo "\n";
            }
        }
    }
?>
