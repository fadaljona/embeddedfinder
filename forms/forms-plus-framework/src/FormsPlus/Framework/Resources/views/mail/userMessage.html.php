Thank you for contacting us! Here is your data that you left on the website!
<br>
<?php
    self::includeTemplate( 'mail/parts/collectedAttributeList.html.php', array(
        'configuration' => $configuration,
        'data'          => $data
    ));
?>
