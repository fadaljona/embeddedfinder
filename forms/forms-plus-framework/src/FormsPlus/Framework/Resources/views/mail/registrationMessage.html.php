<?php use FormsPlus\Framework\Helper\Template; ?>

Thank you for registering at <a href="<?php echo Template::url( '/' ); ?>"><?php echo $_SERVER[ 'HTTP_HOST' ]; ?></a>.<br><br>
<i>Your account information:</i><br>
<?php
    self::includeTemplate( 'mail/parts/collectedAttributeList.html.php', array(
        'configuration' => $configuration,
        'data'          => $data
    ));
?>
<br>
<?php
    if( ( isset( $parameterList[ 'email_attribute_id' ] ) && isset( $data[ 'attribute_list' ][ $parameterList[ 'email_attribute_id' ] ][ 'final_value' ] ) ) && ( isset( $parameterList[ 'hash_key_attribute_id' ] ) && isset( $data[ 'attribute_list' ][ $parameterList[ 'hash_key_attribute_id' ] ][ 'final_value' ] ) ) ) {
        $query = array(
            'action'                                  => 'user_activation',
            $parameterList[ 'email_attribute_id' ]    => $data[ 'attribute_list' ][ $parameterList[ 'email_attribute_id' ] ][ 'final_value' ],
            $parameterList[ 'hash_key_attribute_id' ] => $data[ 'attribute_list' ][ $parameterList[ 'hash_key_attribute_id' ] ][ 'final_value' ]
        );
        echo "Click the following URL to confirm your account: <br>" . Template::url( false, $query );
    } else {
        echo 'The activation link cannot be generated, please contact the website administrator to confirm your account.';
    }
?>
