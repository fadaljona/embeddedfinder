Thank you for contacting us! Here is your data that you left on the website!

<?php
    self::includeTemplate( 'mail/parts/collectedAttributeList.txt.php', array(
        'configuration' => $configuration,
        'data'          => $data
    ));
?>
