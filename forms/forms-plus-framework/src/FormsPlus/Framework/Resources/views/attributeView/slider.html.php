<?php 
    use FormsPlus\Framework\Helper\Template;
    $inputAttributeList = array();
    Template::setDefaultValue( $value, $attribute, $isSubmitted );
    Template::setInputNumberAttributes( $inputAttributeList, $attribute );
    Template::setJsAttributesByPrefix( $inputAttributeList, $attribute );
    if( !is_null( $value ) ) $inputAttributeList[ 'value' ] = strval( $value );
    $sliderAttributeList = array();
    Template::setPluginAttributes( $sliderAttributeList, $attribute, 'slider_option_list', 'slider' );
    $isVertical = ( isset( $attribute[ 'slider_option_list' ][ 'orientation' ] ) && $attribute[ 'slider_option_list' ][ 'orientation' ] == 'vertical' );
    $activeIndicators = 0;
    if( isset( $attribute[ 'slider_indicator' ][ 'show_before' ] ) && $attribute[ 'slider_indicator' ][ 'show_before' ] ) {
        $activeIndicators++;
    }
    if( isset( $attribute[ 'slider_indicator' ][ 'show_after' ] ) && $attribute[ 'slider_indicator' ][ 'show_after' ] ) {
        $activeIndicators++;
    }
?>
<div class="form-group<?php echo ( isset( $attribute[ 'disabled' ] ) && $attribute[ 'disabled' ] ) ? ' p-field-disabled' : ''; ?><?php echo ( isset( $attribute[ 'view' ] ) && in_array( $attribute[ 'view' ], array( 'simple', 'bordered', 'top', 'modern', 'flat' ) ) ) ? ( ' sl-v-' . $attribute[ 'view' ] ) : ''; ?>">
    <?php if( isset( $attribute[ 'wrapper_field' ][ 'show' ] ) && $attribute[ 'wrapper_field' ][ 'show' ] ) { ?>
        <?php self::includeTemplate( 'attributeView/parts/label.html.php', array( 'attribute' => $attribute, 'identifier' => $clone[ 'id' ] . $identifier, 'class' => 'p-field-label' ) ); ?>
        <?php
            $hasToggleButton = isset( $attribute[ 'wrapper_field' ][ 'toggle_button' ] ) && $attribute[ 'wrapper_field' ][ 'toggle_button' ];
            $inputGroup = array();
            $input = null;
            if( $hasToggleButton ) {
                if( $isVertical ) {
                    $inputGroup[ 'html_attribute_list' ] = array( 'data-js-clickout-remove-class' => 'p-show-js-block' );
                }
                $input = array_merge( $inputAttributeList, array(
                    'type'                 => 'text',
                    'class'                => 'form-control',
                    'data-js-slider-field' => 'true',
                    'data-slider-bound'    => $clone[ 'id' ] . $identifier
                ));
                $inputGroup[ 'js_addon_html' ] = '<span class="p-js-link p-js-full" data-js-toggle-class="p-show-js-block' . ( $isVertical ? '' : ' p-hide-field' ) . '"><i class="' . $attribute[ 'wrapper_field' ][ 'toggle_icon' ] . '"></i></span>';
                unset( $attribute[ 'addon' ] );
            }
            $sliderFieldHTML  = '<div class="p-slider-field p-slider-' . ( $isVertical ? 'vertical' : 'horizontal' ) . '" data-js-slider=""' . ( $hasToggleButton ? ' data-slider-name="' . $clone[ 'id' ] . $identifier . '"' : '' ) . Template::generateHTMLAttributes( $sliderAttributeList ) . '>';
            $sliderFieldHTML .= '<input type="hidden" id="' . $clone[ 'id' ] . $identifier. '" name="' . $identifier . $clone[ 'name' ] . '" data-js-slider-field="true"' . ( $hasToggleButton ? ' data-slider-bound="' . $clone[ 'id' ] . $identifier . '"' : '' ) . Template::generateHTMLAttributes( $inputAttributeList ) . '/>';
            $sliderFieldHTML .= '<div class="p-slider-wrap"><div data-js-slider-block=""></div></div>';
            $sliderFieldHTML .= '</div>';
            $inputGroup[ 'after_input_html' ] = $sliderFieldHTML;
            self::includeTemplate( 'attributeView/parts/inputGroup.html.php', array( 'input' => $input, 'attribute' => $attribute[ 'wrapper_field' ], 'inputGroup' => $inputGroup ));
        ?>
    <?php } else { ?>
        <div class="p-slider" data-js-slider=""<?php echo( ( !empty( $attribute[ 'field_list' ] ) && is_array( $attribute[ 'field_list' ] ) ) ? ' data-slider-name="' . $clone[ 'id' ] . $identifier . '"' : '' ); ?><?php echo Template::generateHTMLAttributes( $sliderAttributeList ); ?>>
            <?php self::includeTemplate( 'attributeView/parts/label.html.php', array( 'attribute' => $attribute, 'identifier' => $clone[ 'id' ] . $identifier ) ); ?>
            <input type="hidden" data-js-slider-field="true" id="<?php echo $clone[ 'id' ] . $identifier; ?>" name="<?php echo $identifier . $clone[ 'name' ]; ?>"<?php echo Template::generateHTMLAttributes( $inputAttributeList ); ?>/>
            <?php if( !empty( $attribute[ 'field_list' ] ) && is_array( $attribute[ 'field_list' ] ) ) { ?>
                <div class="row">
                    <?php foreach( $attribute[ 'field_list' ] as $index => $field ) { ?>
                        <div class="<?php echo ( !empty( $field[ 'grid_class' ] ) ) ? $field[ 'grid_class' ] : 'col-sm-12'; ?>">
                            <?php
                                $fieldAttributeList = array(
                                    'type'              => ( empty( $field[ 'option_list' ] ) ? 'text' : 'select' ),
                                    'class'             => 'form-control',
                                    'data-slider-bound' => $clone[ 'id' ] . $identifier
                                );
                                if( isset( $field[ 'value_format' ] ) ) {
                                    $fieldAttributeList[ 'data-js-value-format' ] = $field[ 'value_format' ];
                                }
                                self::includeTemplate( 'attributeView/parts/inputGroup.html.php', array( 
                                    'input' => $fieldAttributeList,
                                    'attribute' => $field,
                                    'inputGroup' =>  array( 'ignore_value' => true )
                                ));
                            ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
            <div class="p-slider-block p-slider-<?php echo( $isVertical ? 'vertical' : 'horizontal' ); ?><?php echo ( isset( $attribute[ 'size' ] ) && in_array( $attribute[ 'size' ], array( 'lg', 'md', 'sm' ) ) ) ? ( ' p-slider-' . $attribute[ 'size' ] ) : ''; ?>">
                <?php for( $number = 1; $number <= intval( $attribute[ 'slider_quantity' ] ); $number++ ) { ?>
                    <div class="p-slider-item<?php echo( $isVertical ? ( $activeIndicators == 2 ? ' p-slider-sides-blocks' : ( $activeIndicators == 1 ? ' p-slider-side-block' : '' ) ) : ( ( $activeIndicators > 0 && !( isset( $attribute[ 'slider_indicator' ][ 'position' ] ) && in_array( $attribute[ 'slider_indicator' ][ 'position' ], array( 'above', 'below' ) ) ) ) ? ' p-table' : '' ) ); ?>">
                        <?php if( !$isVertical && isset( $attribute[ 'slider_indicator' ][ 'position' ] ) && in_array( $attribute[ 'slider_indicator' ][ 'position' ], array( 'above', 'below' ) ) ) { ?>
                            <?php if( $attribute[ 'slider_indicator' ][ 'position' ] == 'below' ) { ?>
                                <div class="p-slider-wrap">
                                    <div data-js-slider-block=""></div>
                                </div>
                            <?php } ?>
                            <div class="clearfix">
                                <?php if( isset( $attribute[ 'slider_indicator' ][ 'show_before' ] ) && $attribute[ 'slider_indicator' ][ 'show_before' ] ) { ?>
                                    <span class="p-js-value pull-left"<?php echo ( !empty( $attribute[ 'slider_indicator' ][ 'before_value_format' ] ) ) ? ' data-js-value-format="' . htmlentities( $attribute[ 'slider_indicator' ][ 'before_value_format' ], ENT_QUOTES ) . '"' : ''; ?>></span>
                                <?php } ?>
                                <?php if( isset( $attribute[ 'slider_indicator' ][ 'show_after' ] ) && $attribute[ 'slider_indicator' ][ 'show_after' ] ) { ?>
                                    <span class="p-js-value pull-right"<?php echo ( !empty( $attribute[ 'slider_indicator' ][ 'after_value_format' ] ) ) ? ' data-js-value-format="' . htmlentities( $attribute[ 'slider_indicator' ][ 'after_value_format' ], ENT_QUOTES ) . '"' : ''; ?>></span>
                                <?php } ?>
                            </div>
                            <?php if( $attribute[ 'slider_indicator' ][ 'position' ] == 'above' ) { ?>
                                <div class="p-slider-wrap">
                                    <div data-js-slider-block=""></div>
                                </div>
                            <?php } ?>
                        <?php } else { ?>
                            <?php if( isset( $attribute[ 'slider_indicator' ][ 'show_before' ] ) && $attribute[ 'slider_indicator' ][ 'show_before' ] ) { ?>
                                <div class="<?php echo( $isVertical ? 'p-slider-before' : 'p-min-cell' ); ?>">
                                    <span class="p-js-value"<?php echo ( !empty( $attribute[ 'slider_indicator' ][ 'before_value_format' ] ) ) ? ' data-js-value-format="' . htmlentities( $attribute[ 'slider_indicator' ][ 'before_value_format' ], ENT_QUOTES ) . '"' : ''; ?>></span>
                                </div>
                            <?php } ?>
                            <div class="p-slider-wrap">
                                <div data-js-slider-block=""></div>
                            </div>
                            <?php if( isset( $attribute[ 'slider_indicator' ][ 'show_after' ] ) && $attribute[ 'slider_indicator' ][ 'show_after' ] ) { ?>
                                <div class="<?php echo( $isVertical ? '' : 'p-min-cell' ); ?>">
                                    <span class="p-js-value"<?php echo ( !empty( $attribute[ 'slider_indicator' ][ 'after_value_format' ] ) ) ? ' data-js-value-format="' . htmlentities( $attribute[ 'slider_indicator' ][ 'after_value_format' ], ENT_QUOTES ) . '"' : ''; ?>></span>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
</div>