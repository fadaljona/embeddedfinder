<?php
    use FormsPlus\Framework\Helper\Template;
    $inputAttributeList = array( 'class' => 'form-control' );
    Template::setInputBaseAttributes( $inputAttributeList, $attribute );
    Template::setInputStringAttributes( $inputAttributeList, $attribute );
    Template::setInputValidationAttributes( $inputAttributeList, $attribute );
    Template::setPluginAttributes( $inputAttributeList, $attribute, 'realperson_option_list', 'captcha' );
    $inputAttributeList[ 'data-captcha-hash-name' ] = 'hash{n}';
?>
<div class="form-group p-captcha-group">
    <?php self::includeTemplate( 'attributeView/parts/label.html.php', array( 'attribute' => $attribute, 'identifier' => $clone[ 'id' ] . $identifier, 'class' => 'p-field-label' ) ); ?>
    <?php
        self::includeTemplate( 'attributeView/parts/inputGroup.html.php', array( 'input' => array_merge( $inputAttributeList, array(
            'type'                 => 'text',
            'id'                   => $clone[ 'id' ] . $identifier,
            'name'                 => $identifier . $clone[ 'name' ],
            'data-js-captcha'      => 'true'
        )), 'attribute' => $attribute ));
    ?>
</div>