<?php
    use FormsPlus\Framework\Helper\Template;
    $inputAttributeList = array();
    Template::setInputValidationAttributes( $inputAttributeList, $attribute );
    Template::setJsAttributesByPrefix( $inputAttributeList, $attribute );
    if( isset( $attribute[ 'required' ] ) && $attribute[ 'required' ] ) {
        $inputAttributeList[ 'required' ] = true;
    }
    $checkmarkHTML = '';
    if( !empty( $attribute[ 'checkmark' ][ 'type' ] ) ) {
        if( $attribute[ 'checkmark' ][ 'type' ] == 'point' ) {
            $checkmarkHTML = '<span class="p-check-point"></span>';
        } elseif( $attribute[ 'checkmark' ][ 'type' ] == 'button' ) {
            $checkmarkHTML = '<span class="p-check-icon"><span class="p-check-block"></span></span>';
        } elseif( $attribute[ 'checkmark' ][ 'type' ] == 'switch' ) {
            $checkmarkHTML = '<span class="p-switch-icon" data-checked="' . htmlentities( $attribute[ 'checkmark' ][ 'checked_text' ], ENT_QUOTES ) . '" data-unchecked="' . htmlentities( $attribute[ 'checkmark' ][ 'unchecked_text' ], ENT_QUOTES ) . '"></span>';
        }
    }
?>
<div class="form-group">
    <?php self::includeTemplate( 'attributeView/parts/label.html.php', array( 'attribute' => $attribute, 'identifier' => $clone[ 'id' ] . $identifier ) ); ?>
    <?php if( $attribute[ 'display' ][ 'type' ] == 'line' ) { ?>
        <?php foreach( $attribute[ 'block_list' ] as $index => $block ) { ?>
            <?php
                $itemInputAttributeList = $inputAttributeList;
                Template::setJsAttributesByPrefix( $itemInputAttributeList, $block );
            ?>
            <div class="p-block-pick">
                <label>
                    <input <?php echo ( $attribute[ 'multiple' ] ) ? 'type="checkbox" name="' . $identifier . $clone[ 'name' ] . '[]"' : 'type="radio" name="' . $identifier . $clone[ 'name' ] . '"'; ?> class="p-check-next" value="<?php echo $index; ?>"<?php echo Template::isItemActive( $index, $block, 'selected', $value, $isSubmitted ) ? ' checked' : ''; ?><?php echo Template::generateHTMLAttributes( $itemInputAttributeList ); ?>/>
                    <span class="p-check-container<?php echo ( isset( $attribute[ 'view' ] ) && in_array( $attribute[ 'view' ], array( 'highlight', 'bordered' ) ) ) ? ( ' p-check-' . $attribute[ 'view' ] ) : ''; ?>">
                        <span class="row p-sm-row">
                            <span class="p-col<?php echo ( !empty( $attribute[ 'display' ][ 'line' ][ 'image_grid_class' ] ) ) ? ( ' ' . $attribute[ 'display' ][ 'line' ][ 'image_grid_class' ] ) : ''; ?>">
                                <span class="p-picture-pick p-picture-radio p-picture-solo">
                                    <?php echo $checkmarkHTML; ?>
                                    <span class="p-preview">
                                        <img src="<?php echo htmlentities( $block[ 'image' ], ENT_QUOTES ); ?>">
                                    </span>
                                </span>
                            </span>
                            <span class="p-col<?php echo ( !empty( $attribute[ 'display' ][ 'line' ][ 'text_grid_class' ] ) ) ? ( ' ' . $attribute[ 'display' ][ 'line' ][ 'text_grid_class' ] ) : ''; ?>">
                                <?php if( !empty( $block[ 'title' ] ) ) { ?>
                                    <span class="p-block-title p-colored-text"><?php echo $block[ 'title' ]; ?></span>
                                <?php } ?>
                                <?php if( !empty( $block[ 'description' ] ) ) { ?>
                                    <span class="p-block-description"><?php echo $block[ 'description' ]; ?></span>
                                <?php } ?>
                            </span>
                        </span>
                    </span>
                </label>
            </div>
        <?php } ?>
    <?php } elseif( $attribute[ 'display' ][ 'type' ] == 'block' ) { ?>
        <div class="row p-sm-row">
            <?php foreach( $attribute[ 'block_list' ] as $index => $block ) { ?>
                <?php
                    $itemInputAttributeList = $inputAttributeList;
                    Template::setJsAttributesByPrefix( $itemInputAttributeList, $block );
                ?>
                <div class="col-sm-4 p-col">
                    <div class="p-block-pick">
                        <label>
                            <input <?php echo ( $attribute[ 'multiple' ] ) ? 'type="checkbox" name="' . $identifier . $clone[ 'name' ] . '[]"' : 'type="radio" name="' . $identifier . $clone[ 'name' ] . '"'; ?> class="p-check-next" value="<?php echo $index; ?>"<?php echo Template::isItemActive( $index, $block, 'selected', $value, $isSubmitted ) ? ' checked' : ''; ?><?php echo Template::generateHTMLAttributes( $itemInputAttributeList ); ?>/>
                            <span class="p-check-container<?php echo ( isset( $attribute[ 'view' ] ) && in_array( $attribute[ 'view' ], array( 'highlight', 'bordered' ) ) ) ? ( ' p-check-' . $attribute[ 'view' ] ) : ''; ?>">
                                <span class="p-fixed-sm p-check-side">
                                    <?php echo $checkmarkHTML; ?>
                                    <span class="p-content text-center">
                                        <img class="img-responsive" src="<?php echo htmlentities( $block[ 'image' ], ENT_QUOTES ); ?>">
                                        <?php if( !empty( $block[ 'title' ] ) ) { ?>
                                            <span class="p-content-title p-colored-text"><?php echo $block[ 'title' ]; ?></span>
                                        <?php } ?>
                                        <?php if( !empty( $block[ 'description' ] ) ) { ?>
                                            <span class="p-content-description"><?php echo $block[ 'description' ]; ?></span>
                                        <?php } ?>
                                    </span>
                                </span>
                            </span>
                        </label>
                    </div>
                </div>
            <?php } ?>
        </div>
    <?php } ?>
</div>
