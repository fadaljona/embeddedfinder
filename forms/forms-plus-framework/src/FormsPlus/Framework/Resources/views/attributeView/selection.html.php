<?php
    use FormsPlus\Framework\Helper\Template;
    $inputAttributeList = array( 'class' => 'form-control' );
    if( isset( $attribute[ 'multiple' ] ) && $attribute[ 'multiple' ] ) {
        $inputAttributeList[ 'multiple' ] = true;
    }
    Template::setInputBaseAttributes( $inputAttributeList, $attribute );
    Template::setInputValidationAttributes( $inputAttributeList, $attribute );
    Template::setJsAttributesByPrefix( $inputAttributeList, $attribute );
?>
<div class="form-group<?php echo ( isset( $attribute[ 'disabled' ] ) && $attribute[ 'disabled' ] ) ? ' p-field-disabled' : ''; ?>">
    <?php self::includeTemplate( 'attributeView/parts/label.html.php', array( 'attribute' => $attribute, 'identifier' => $clone[ 'id' ] . $identifier, 'class' => 'p-field-label' ) ); ?>
    <?php
        if( isset( $attribute[ 'addon' ] ) ) unset( $attribute[ 'addon' ] );
        self::includeTemplate( 'attributeView/parts/inputGroup.html.php', array( 'input' => array_merge( $inputAttributeList, array(
            'type' => 'select',
            'id'   => $clone[ 'id' ] . $identifier,
            'name' => $identifier . $clone[ 'name' ]
        )), 'attribute' => $attribute, 'value' => $value, 'isSubmitted' => $isSubmitted ));
    ?>
</div>