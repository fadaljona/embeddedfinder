<?php if( ( isset( $attribute[ 'state' ][ 'required' ] ) && $attribute[ 'state' ][ 'required' ] ) || ( isset( $attribute[ 'state' ][ 'validation' ] ) && $attribute[ 'state' ][ 'validation' ] ) ) { ?>
    <span class="input-group-state">
        <span class="p-position">
            <span class="p-text">
                <?php if( isset( $attribute[ 'state' ][ 'required' ] ) && $attribute[ 'state' ][ 'required' ] ) { ?>
                    <span class="p-required-text"><i class="<?php echo ( !empty( $attribute[ 'state' ][ 'icon' ][ 'required' ] ) ) ? $attribute[ 'state' ][ 'icon' ][ 'required' ] : 'fa fa-star'; ?>"></i></span>
                <?php } elseif ( isset( $attribute[ 'state' ][ 'validation' ] ) && $attribute[ 'state' ][ 'validation' ] ) { ?>
                    <span class="p-valid-text"><i class="<?php echo ( !empty( $attribute[ 'state' ][ 'icon' ][ 'valid' ] ) ) ? $attribute[ 'state' ][ 'icon' ][ 'valid' ] : 'fa fa-check'; ?>"></i></span>
                    <span class="p-error-text"><i class="<?php echo ( !empty( $attribute[ 'state' ][ 'icon' ][ 'error' ] ) ) ? $attribute[ 'state' ][ 'icon' ][ 'error' ] : 'fa fa-times'; ?>"></i></span>
                <?php } ?>
            </span>
        </span>
    </span>
<?php } ?>