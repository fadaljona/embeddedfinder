<?php if( !empty( $attribute[ 'label' ] ) ) { ?>
    <label for="<?php echo $identifier; ?>" class="<?php echo isset( $class ) ? $class : ''; ?><?php echo ( isset( $attribute[ 'required' ] ) && $attribute[ 'required' ] ) ? ' p-label-required' : ''; ?>"><?php echo $attribute[ 'label' ]; ?></label>
<?php } ?>