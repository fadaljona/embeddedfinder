<?php
    use FormsPlus\Framework\Helper\Template;
    $inputAttributeList = array( 'class' => 'form-control' );
    Template::setDefaultValue( $value, $attribute, $isSubmitted );
    Template::setInputBaseAttributes( $inputAttributeList, $attribute );
    Template::setInputNumberAttributes( $inputAttributeList, $attribute );
    Template::setInputValidationIgnoreAttributes( $inputAttributeList, $attribute );
    Template::setJsAttributesByList( $inputAttributeList, $attribute, array( 'value_format', 'wheel_spin', 'arrow_spin' ) );
    Template::setJsAttributesByPrefix( $inputAttributeList, $attribute );
    if( !is_null( $value ) ) $inputAttributeList[ 'value' ] = $value;
?>
<div class="form-group">
    <?php self::includeTemplate( 'attributeView/parts/label.html.php', array( 'attribute' => $attribute, 'identifier' => $clone[ 'id' ] . $identifier, 'class' => 'p-field-label' ) ); ?>
    <?php
        $inputGroup = array();
        if( $attribute[ 'box_arrow_spin' ] !== false && !is_null( $attribute[ 'box_arrow_spin' ] ) ) {
            $arrowBoxHTML  = '<span class="p-js-spinner" data-js-spinner="' . $attribute[ 'box_arrow_spin' ] . '">';
            $arrowBoxHTML .= '<span class="p-js-up p-js-link"><i class="' . $attribute[ 'increase_icon' ] . '"></i></span>';
            $arrowBoxHTML .= '<span class="p-js-down p-js-link"><i class="' . $attribute[ 'decrease_icon' ] . '"></i></span>';
            $arrowBoxHTML .= '</span>';
            $inputGroup[ 'js_addon_html' ] = $arrowBoxHTML;
            unset( $attribute[ 'addon' ] );
        }
        self::includeTemplate( 'attributeView/parts/inputGroup.html.php', array( 'input' => array_merge( $inputAttributeList, array(
            'type' => 'text',
            'id'   => $clone[ 'id' ] . $identifier,
            'name' => $identifier . $clone[ 'name' ]
        )), 'attribute' => $attribute, 'inputGroup' => $inputGroup ));
    ?>
</div>
