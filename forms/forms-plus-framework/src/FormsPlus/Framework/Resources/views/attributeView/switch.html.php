<?php
    use FormsPlus\Framework\Helper\Template;
    $inputAttributeList = array();
    Template::setInputValidationAttributes( $inputAttributeList, $attribute );
    if( isset( $attribute[ 'required' ] ) && $attribute[ 'required' ] ) {
        $inputAttributeList[ 'required' ] = true;
    }
    if( isset( $attribute[ 'disabled' ] ) && $attribute[ 'disabled' ] ) {
        $inputAttributeList[ 'disabled' ] = true;
    }
?>
<div class="form-group<?php echo ( isset( $attribute[ 'disabled' ] ) && $attribute[ 'disabled' ] ) ? ' p-field-disabled' : ''; ?>">
    <?php self::includeTemplate( 'attributeView/parts/label.html.php', array( 'attribute' => $attribute, 'identifier' => $clone[ 'id' ] . $identifier ) ); ?>
    <div class="p-form-sg<?php echo ( isset( $attribute[ 'view' ] ) && in_array( $attribute[ 'view' ], array( 'cross-panel', 'panel', 'inline' ) ) ) ? ( ' pt-form-' . $attribute[ 'view' ] ) : ''; ?>">
        <?php foreach( $attribute[ 'switch_list' ] as $index => $switch ) { ?>
            <?php if( !isset( $switch[ 'name' ] ) ) continue; ?>
            <?php
                $itemLabelClassList = array();
                $itemInputAttributeList = $inputAttributeList;
                $checkInput = ( !empty( $switch[ 'check_input' ] ) ) ? $switch[ 'check_input' ] : false;
                Template::setJsAttributesByPrefix( $itemInputAttributeList, $switch );
                if( $checkInput !== false ) {
                    $itemInputAttributeList[ 'class' ] = 'p-check-input';
                    $itemLabelClassList[] = 'p-check-input';
                }
                if( isset( $switch[ 'disabled' ] ) && $switch[ 'disabled' ] ) {
                    $itemInputAttributeList[ 'disabled' ] = true;
                    $itemLabelClassList[] = 'p-field-disabled';
                }
            ?>
            <div class="<?php echo ( $attribute[ 'multiple' ] ) ? 'p-switch' : 'p-radioswitch'; ?>">
                <label<?php echo !empty( $itemLabelClassList ) ? ' class="' . implode( ' ', $itemLabelClassList ) . '"' : ''; ?>>
                    <input <?php echo ( $attribute[ 'multiple' ] ) ? 'type="checkbox" name="' . $identifier . $clone[ 'name' ] . '[check][]"' : 'type="radio" name="' . $identifier . $clone[ 'name' ] . '[check]"'; ?> value="<?php echo $index; ?>"<?php echo Template::isItemActive( $index, $switch, 'checked', isset( $value[ 'check' ] ) ? $value[ 'check' ] : null, $isSubmitted ) ? ' checked' : ''; ?><?php echo Template::generateHTMLAttributes( $itemInputAttributeList ); ?>/>
                    <span class="p-switch-icon" data-checked="<?php echo htmlentities( $attribute[ 'checked_text' ], ENT_QUOTES ); ?>" data-unchecked="<?php echo htmlentities( $attribute[ 'unchecked_text' ], ENT_QUOTES ); ?>"></span>
                    <?php if( $checkInput !== false ) { ?>
                        <span class="form-group">
                            <?php
                                $checkInputAttributeList = array(
                                    'class' => 'form-control',
                                    'type'  => ( ( !empty( $checkInput[ 'type' ] ) ) ?  $checkInput[ 'type' ] : 'text' ),
                                    'name'  => $identifier . $clone[ 'name' ] . '[input][' . $index . ']',
                                    'value' => Template::getSubInputValue( $index, $checkInput, isset( $value[ 'input' ] ) ? $value[ 'input' ] : null, $isSubmitted )
                                );
                                Template::setInputBaseAttributes( $checkInputAttributeList, $checkInput );
                                self::includeTemplate( 'attributeView/parts/inputGroup.html.php', array( 'input' => $checkInputAttributeList, 'attribute' => $checkInput ));
                            ?>
                        </span>
                    <?php } else { ?>
                        <span class="p-label"><?php echo $switch[ 'name' ]; ?></span>
                    <?php } ?>
                </label>
            </div>
        <?php } ?>
    </div>
</div>
