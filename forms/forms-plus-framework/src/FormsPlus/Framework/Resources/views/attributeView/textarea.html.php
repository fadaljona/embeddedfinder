<?php
    use FormsPlus\Framework\Helper\Template;
    $inputAttributeList = array( 'class' => 'form-control' );
    Template::setDefaultValue( $value, $attribute, $isSubmitted );
    Template::setInputBaseAttributes( $inputAttributeList, $attribute );
    Template::setInputStringAttributes( $inputAttributeList, $attribute );
    Template::setInputValidationAttributes( $inputAttributeList, $attribute );
    Template::setJsAttributesByPrefix( $inputAttributeList, $attribute );
?>
<div class="form-group<?php echo ( isset( $attribute[ 'disabled' ] ) && $attribute[ 'disabled' ] ) ? ' p-field-disabled' : ''; ?>">
    <?php self::includeTemplate( 'attributeView/parts/label.html.php', array( 'attribute' => $attribute, 'identifier' => $clone[ 'id' ] . $identifier, 'class' => 'p-field-label' ) ); ?>
    <?php
        self::includeTemplate( 'attributeView/parts/inputGroup.html.php', array( 'input' => array_merge( $inputAttributeList, array(
            'type' => 'textarea',
            'id'   => $clone[ 'id' ] . $identifier,
            'name' => $identifier . $clone[ 'name' ]
        )), 'attribute' => $attribute, 'value' => $value ));
    ?>
</div>