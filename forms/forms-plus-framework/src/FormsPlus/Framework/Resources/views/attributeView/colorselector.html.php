<?php
    use FormsPlus\Framework\Helper\Template;
    $inputAttributeList = array();
    if( isset( $attribute[ 'required' ] ) && $attribute[ 'required' ] ) {
        $inputAttributeList[ 'required' ] = true;
    }
    Template::setInputValidationAttributes( $inputAttributeList, $attribute );
    Template::setJsAttributesByPrefix( $inputAttributeList, $attribute );
    $subOptionClass = $configuration[ 'feature_list' ][ 'sub_selection' ] ? ' p-sub-option' : '';
?>
<div class="form-group">
    <?php self::includeTemplate( 'attributeView/parts/label.html.php', array( 'attribute' => $attribute, 'identifier' => $clone[ 'id' ] . $identifier ) ); ?>
    <?php if( !empty( $attribute[ 'default_sub_option' ] ) && $configuration[ 'feature_list' ][ 'sub_selection' ] ) { ?>
        <div class="p-sub-option" data-js-option-default><?php echo $attribute[ 'default_sub_option' ]; ?></div>
    <?php } ?>
    <div class="p-form-colorpick">
        <?php foreach( $attribute[ 'color_list' ] as $index => $color ) { ?>
            <?php if( empty( $color[ 'color' ] ) ) continue; ?>
            <?php
                $itemInputAttributeList = $inputAttributeList;
                Template::setJsAttributesByPrefix( $itemInputAttributeList, $color );
            ?>
            <div class="p-radio-color<?php echo $subOptionClass; ?>">
                <label>
                    <input <?php echo ( $attribute[ 'multiple' ] ) ? 'type="checkbox" name="' . $identifier . $clone[ 'name' ] . '[]"' : 'type="radio" name="' . $identifier . $clone[ 'name' ] . '"'; ?> value="<?php echo $index; ?>"<?php echo Template::isItemActive( $index, $color, 'selected', $value, $isSubmitted ) ? ' checked' : ''; ?><?php echo Template::generateHTMLAttributes( $itemInputAttributeList ); ?>/>
                    <span class="p-color-block" style="background: <?php echo $color[ 'color' ]; ?>;"></span>
                </label>
            </div>
        <?php } ?>
    </div>
</div>
