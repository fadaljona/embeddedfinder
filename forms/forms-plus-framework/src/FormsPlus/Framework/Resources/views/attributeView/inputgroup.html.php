<?php use FormsPlus\Framework\Helper\Template; ?>
<div class="form-group<?php echo ( isset( $attribute[ 'disabled' ] ) && $attribute[ 'disabled' ] ) ? ' p-field-disabled' : ''; ?>">
    <?php self::includeTemplate( 'attributeView/parts/label.html.php', array( 'attribute' => $attribute, 'identifier' => $clone[ 'id' ] . $identifier, 'class' => 'p-field-label' ) ); ?>
    <div class="p-form-chars">
        <?php foreach( $attribute[ 'input_list' ] as $index => $input ) { ?>
            <?php if( !( isset( $input[ 'size' ] ) && is_int( $input[ 'size' ] ) && $input[ 'size' ] > 0 ) ) continue; ?>
            <div class="p-field-chars-<?php echo $input[ 'size' ]; ?>">
                <?php
                    self::includeTemplate( 'attributeView/parts/inputGroup.html.php', array( 'attribute' => $attribute, 'input' => array(
                        'type'                => 'text',
                        'name'                => $identifier . $clone[ 'name' ] . '[' . $index . ']',
                        'placeholder'         => ( empty( $input[ 'placeholder' ] ) ? str_repeat( 'X', $input[ 'size' ] ) : $input[ 'placeholder' ] ),
                        'minlength'           => $input[ 'size' ],
                        'maxlength'           => $input[ 'size' ],
                        'data-js-input-group' => $clone[ 'id' ] . $identifier,
                        'class'               => 'form-control',
                        'value'               => Template::getSubInputValue( $index, $input, $value, $isSubmitted )

                    ))); 
                ?>
            </div>
        <?php } ?>
    </div>
</div>