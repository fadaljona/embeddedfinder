<?php
    use JasonGrimes\Paginator;
    $queryArgumentList = array( 'action' => 'collectionSearch' );
    $itemsPerPage = 10;
    if( !empty( $_GET[ 'pageLimit' ] ) ) {
        $itemsPerPage = intval( $_GET[ 'pageLimit' ] );
        $queryArgumentList[ 'pageLimit' ] = $itemsPerPage;
    }
    $currentPage = 1;
    if( !empty( $_GET[ 'navigationPage' ] ) ) {
        $currentPage = intval( $_GET[ 'navigationPage' ] );
    }
    $searchText = '';
    if( isset( $_GET[ 'searchText' ] ) && trim( $_GET[ 'searchText' ] ) != '' ) {
        $searchText = trim( $_GET[ 'searchText' ] );
        $queryArgumentList[ 'searchText' ] = $searchText;
    }
    $sort = array( 'prop_created', true );
    if( isset( $_GET[ 'sortColumn' ] ) && isset( $_GET[ 'sortOrder' ] ) ) {
        $columnList = array( 'prop_created', 'prop_form_id' );
        if( in_array( $_GET[ 'sortColumn' ], $columnList ) ) {
            $sort = array( $_GET[ 'sortColumn' ], (bool) $_GET[ 'sortOrder' ] );
            $queryArgumentList[ 'sortColumn' ] = $sort[ 0 ];
            $queryArgumentList[ 'sortOrder' ] = $sort[ 1 ];
        }
    }
    if( $searchText != '' ) {
        $urlQuery = '?' . http_build_query( $queryArgumentList );
        $collectionList = $this->storage->fetchCollectionList( null, $itemsPerPage, ( ( $currentPage - 1 ) * $itemsPerPage ), $sort, $searchText );
        $paginator = new Paginator( $collectionList[ 'count' ], $itemsPerPage, $currentPage, $urlQuery . '&navigationPage=(:num)' );
    }
?>
<div class="page-title">
    <div class="title_left">
        <h3><?php echo $this->translateText( 'base.collectionSearch.heading' ); ?></h3>
    </div>
</div>
<div class="x_panel">
    <div class="x_content">
        <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
            <?php if( isset( $paginator ) ) { ?>
                <form method="get">
                    <input type="hidden" name="action" value="<?php echo $queryArgumentList[ 'action' ]; ?>">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 text-center-xs text-left-sm">
                            <div class="form-group">
                                <div class="input-group">
                                    <select name="pageLimit" class="form-control input-sm">
                                        <?php foreach( array( '100', '50', '25', '10' ) as $pageLimitItem ) { ?>
                                           <option value="<?php echo $pageLimitItem; ?>"<?php echo ( $pageLimitItem == $itemsPerPage ) ? ' selected' : ''; ?>><?php echo $this->translateText( 'base.collectionList.select.pageLimit', array( '%limit%' => $pageLimitItem ) ); ?></option>
                                        <?php } ?>
                                    </select>
                                    <span class="input-group-btn">
                                        <button class="btn btn-warning btn-sm" type="submit" title="<?php echo $this->translateText( 'base.collectionList.button.sort' ); ?>"><i class="fa fa-list"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 text-center-xs text-right-sm">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control input-sm" placeholder="<?php echo $this->translateText( 'base.collectionList.field.search' ); ?>" name="searchText" value="<?php echo htmlspecialchars( $searchText, ENT_QUOTES, 'UTF-8' ) ?>">
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary btn-sm" type="submit" title="<?php echo $this->translateText( 'base.collectionList.button.search' ); ?>"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            <?php } ?>
            <?php if( $searchText == '' ) { ?>
                <form method="get">
                    <input type="hidden" name="action" value="<?php echo $queryArgumentList[ 'action' ]; ?>">
                    <div class="form-group">
                        <div class="input-group rm-mr">
                            <input size="50" type="text" class="form-control input-sm" placeholder="<?php echo $this->translateText( 'base.collectionList.field.search' ); ?>" name="searchText" value="<?php echo htmlspecialchars( $searchText, ENT_QUOTES, 'UTF-8' ) ?>">
                            <span class="input-group-btn">
                                <button class="btn btn-primary btn-sm" type="submit" title="<?php echo $this->translateText( 'base.collectionList.button.search' ); ?>"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </div>
                </form>
            <?php } elseif( $collectionList[ 'count' ] > 0 ) { ?>
                <?php $sortQueryArgumentList = $queryArgumentList; ?>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered dataTable">
                        <thead>
                            <tr>
                                <th class="shrink"><?php echo $this->translateText( 'base.collectionSearch.column.collection' ); ?></th>
                                <?php
                                    $sortQueryArgumentList[ 'sortColumn' ] = 'prop_created';
                                    $sortQueryArgumentList[ 'sortOrder' ] = !$sort[ 1 ];
                                ?>
                                <th><a class="pull-right" href="<?php echo '?' . http_build_query( $sortQueryArgumentList ); ?>"><i class="fa fa-<?php echo ( $sort[ 0 ] == 'prop_created' ) ? ( $sort[ 1 ] ? 'sort-amount-asc' : 'sort-amount-desc' ) : 'sort'; ?>"></i></a><?php echo $this->translateText( 'base.collectionSearch.column.submitted' ); ?></th>
                                <th><?php echo $this->translateText( 'base.collectionSearch.column.form' ); ?></th>
                                <th><?php echo $this->translateText( 'base.collectionSearch.column.content' ); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach( $collectionList[ 'list' ] as $collection ) { ?>
                            <?php $form = $this->storage->fetchForm( $collection[ 'form_id' ] ); ?>
                            <tr>
                                <td class="text-center"><a href="<?php echo $this->pageLink( array( 'action' => 'collectionView', 'collectionID' => $collection[ 'collection_id' ] ) ); ?>"><span class="badge bg-blue">#<?php echo $collection[ 'collection_id' ]; ?></span></a></td>
                                <td><?php echo $collection[ 'collection_created' ]; ?></td>
                                <td><a href="<?php echo $this->pageLink( array( 'action' => 'formView', 'formID' => $collection[ 'form_id' ] ) ); ?>"><?php echo $form[ 'configuration' ][ 'name' ]; ?></a></td>
                                <td>
                                    <?php
                                        $runOnce = true;
                                        foreach( $collection[ 'attribute_list' ] as $attributeID => $attribute ) {
                                            echo !$runOnce ? ' &#8226 ' : '';
                                            echo preg_replace( "/(" . implode( '|', array_map( 'preg_quote', array_map( 'htmlspecialchars', $collectionList[ 'search' ] ) ) ) . ")/ui", "<strong>$0</strong>", htmlspecialchars( $attribute[ 'final_value' ] ) ) . ' ';
                                            $runOnce = false;
                                        }
                                    ?>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            <?php } else { ?>
                <div class="alert alert-warning rm-mr" role="alert">
                    <?php echo $this->translateText( 'base.collectionSearch.alert.noCollections' ); ?>
                </div>
            <?php } ?>
            <?php if( isset( $paginator ) && $collectionList[ 'count' ] > 0 ) { ?>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 text-center-xs text-left-sm">
                        <div class="dataTables_info">
                            <?php echo $this->translateText( 'base.paginator.info', array( '%first%' => $paginator->getCurrentPageFirstItem(), '%last%' => $paginator->getCurrentPageLastItem(), '%total%' => $paginator->getTotalItems() ) ); ?>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 text-center-xs text-right-sm">
                        <?php if( $paginator->getNumPages() > 1) { ?>
                            <?php echo $this->includeTemplate( 'parts/paginator.html.php', array( 'paginator' => $paginator ) ); ?>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<div class="clearfix"></div>
