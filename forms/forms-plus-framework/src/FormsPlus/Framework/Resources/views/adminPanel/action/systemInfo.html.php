<div class="page-title">
    <div class="title_left">
        <h3><?php echo $this->translateText( 'base.systemInfo.heading' ); ?></h3>
    </div>
</div>
<div class="x_panel">
    <div class="x_content rm-mb-last">
        <p><strong><?php echo $this->translateText( 'base.systemInfo.label.formPlusVersion' ); ?>:</strong> 1.3.1</p>
        <p><strong><?php echo $this->translateText( 'base.systemInfo.label.phpVersion' ); ?>:</strong> <?php echo phpversion(); ?></p>
        <?php $storageReflection = new ReflectionClass( $this->storage ); ?>
        <p><strong><?php echo $this->translateText( 'base.systemInfo.label.storageType' ); ?>:</strong> <?php echo $storageReflection->getParentClass()->getShortName(); ?></p>
        <p><strong><?php echo $this->translateText( 'base.systemInfo.label.databaseHandler' ); ?>:</strong> <?php echo $storageReflection->getShortName(); ?></p>
    </div>
</div>
<div class="clearfix"></div>
