<?php
    if( !class_exists( 'PHPExcel' ) ) {
        return array( 'error' => $this->translateText( 'base.action.alert.missingRequiredLibrary' ) );
    }
    $form = !empty( $_GET[ 'formID' ] ) ? $this->storage->fetchForm( $_GET[ 'formID' ] ) : null;
    if( empty( $form  ) ) {
        return array( 'error' => $this->translateText( 'base.formView.alert.noFormWithID' ) );
    }
    $this->variableList[ 'content_info' ][ 'config_file' ] = $form[ 'config_file' ];
    $propertyIDList = array( 'submitted', 'ip' );
    if( isset( $_POST[ 'uploadImportFileAction' ] ) ) {
        if( !isset( $_FILES[ 'sourceFile' ] ) ) {
            return array( 'error' => $this->translateText( 'base.formDataImport.alert.importFileMissing' ) );
        }
        if( $_FILES[ 'sourceFile' ][ 'error' ] !== UPLOAD_ERR_OK ) {
            return array( 'error' => $this->translateText( 'base.formDataImport.alert.fileUploadError' ) );
        }
        $fileName = $_FILES[ 'sourceFile' ][ 'name' ];
        $phpExcel = array( 'filename' => $_FILES[ 'sourceFile' ][ 'tmp_name' ] );
        $phpExcel[ 'filetype' ] = PHPExcel_IOFactory::identify( $phpExcel[ 'filename' ] );
        if( !in_array( $phpExcel[ 'filetype' ], array( 'OOCalc', 'Excel2007', 'Excel5', 'CSV' ) ) ) {
            return array( 'error' => $this->translateText( 'base.formDataImport.alert.fileFormatNotSupported' ) );
        }
        $phpExcel[ 'reader' ] = PHPExcel_IOFactory::createReader( $phpExcel[ 'filetype' ] );
        $phpExcel[ 'excel' ] = $phpExcel[ 'reader' ]->load( $phpExcel[ 'filename' ] );
        $phpExcel[ 'sheet' ] = $phpExcel[ 'excel' ]->getSheet( 0 );
        $phpExcel[ 'highestRow' ] = $phpExcel[ 'sheet' ]->getHighestRow();
        $phpExcel[ 'highestColumn' ] = $phpExcel[ 'sheet' ]->getHighestColumn();
        $importData = array_filter(
            $phpExcel[ 'sheet' ]->toArray( '' ),
            function( $value ) {
                return count( array_filter( $value, 'strlen' ) );
            }
        );
        if( empty( $importData ) ) {
            return array( 'error' => $this->translateText( 'base.formDataImport.alert.noDataInSourceFile' ) );
        }
        $importColumnList = array();
        $columnComparison = empty( $_POST[ 'columnComparison' ] );
        foreach( reset( $importData ) as $importColumnID => $importColumnName ) {
            $importColumnNameID = strtolower( $importColumnName );
            $importColumnList[ $importColumnID ][ 'name' ] = ( $importColumnID + 1 ) . '. ' . ( strlen( trim( $importColumnName ) ) ? trim( $importColumnName ) : '-' );
            $importColumnList[ $importColumnID ][ 'similarity' ] = 0;
            foreach( $form[ 'configuration' ][ 'attribute_list' ] as $attributeID => $attribute ) {
                $similarity = 0;
                similar_text( $importColumnNameID, ( $columnComparison ? $attributeID : $attribute[ 'name' ] ), $similarity );
                if( $similarity > 0 &&  $similarity > $importColumnList[ $importColumnID ][ 'similarity' ] ) {
                    $importColumnList[ $importColumnID ][ 'similarity' ] = $similarity;
                    $importColumnList[ $importColumnID ][ 'attribute_id' ] = $attributeID;
                }
            }
            foreach( $propertyIDList as $collectionID ) {
                $similarity = 0;
                similar_text( $importColumnNameID, $collectionID, $similarity );
                if( $similarity > 0 &&  $similarity > $importColumnList[ $importColumnID ][ 'similarity' ] ) {
                    $importColumnList[ $importColumnID ][ 'similarity' ] = $similarity;
                    $importColumnList[ $importColumnID ][ 'property_id' ] = $collectionID;
                }
            }
        }
        $importData = array_map(
            function( $value ) {
                return array_map( 'strval', array_filter( $value, 'strlen' ) );
            },
            $importData
        );
    } elseif( isset( $_POST[ 'runImportAction' ] ) ) {
        if( empty( $_POST[ 'importData' ] ) ) {
            return array( 'error' => $this->translateText( 'base.formDataImport.alert.importDataMissing' ) );
        }
        $importData = json_decode( $_POST[ 'importData' ], true );
        $attributeImportColumnList = !empty( $_POST[ 'attributeImportColumnList' ] ) ? $_POST[ 'attributeImportColumnList' ] : array();
        $propertyImportColumnList = !empty( $_POST[ 'propertyImportColumnList' ] ) ? $_POST[ 'propertyImportColumnList' ] : array();
        $fileName = !empty( $_POST[ 'fileName' ] ) ? $_POST[ 'fileName' ] : 'none';
        if( empty( $_POST[ 'firstDataRowPresence' ] ) ) {
            reset( $importData );
            unset( $importData[ key( $importData ) ] );
        }
        $createdCollectionCounter = 0;
        foreach( $importData as $rowValueList ) {
            $attributeValueList = array();
            foreach( $attributeImportColumnList as $attributeID => $attributeImportColumn ) {
                if( $attributeImportColumn === '-1' || !isset( $rowValueList[ $attributeImportColumn ] ) ) continue;
                $attributeValueList[ $attributeID ][ 'final_value' ] = $rowValueList[ $attributeImportColumn ];
            }
            $propertyValueList = array_fill_keys( $propertyIDList, '' );
            foreach( $propertyImportColumnList as $attributeID => $propertyImportColumn ) {
                if( $propertyImportColumn === '-1' || !isset( $rowValueList[ $propertyImportColumn ] ) ) continue;
                $propertyValueList[ $attributeID ] = $rowValueList[ $propertyImportColumn ];
            }
            $collectionID = $this->storage->addCollection( array(
                'form_id'            => $form[ 'id' ],
                'collection_created' => $propertyValueList[ 'submitted' ],
                'collection_ip'      => $propertyValueList[ 'ip' ],
                'attribute_list'     => $attributeValueList
            ));
            if( !$collectionID ) continue;
            $createdCollectionCounter++;
        }
    } else {
        return array( 'error' => $this->translateText( 'base.formDataImport.alert.importFileMissing' ) );
    }
?>
<div class="page-title">
    <div class="title_left">
        <h3><?php echo $this->translateText( 'base.formDataImport.heading', array( '%form%' => '<strong>' . $form[ 'configuration' ][ 'name' ] . '</strong>', '%file%' => '<strong>' . $fileName . '</strong>' ) ); ?></h3>
    </div>
</div>
<div class="clearfix"></div>
<div class="x_panel">
    <div class="x_title">
        <h2><i class="fa fa-cog"></i> <?php echo $this->translateText( 'base.formDataImport.panel.name' ); ?></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php if( isset( $createdCollectionCounter ) ) { ?>
            <div class="alert alert-success rm-mr" role="alert">
                <?php echo $this->translateText( 'base.formDataImport.alert.importedRows', array( '%number%' => '<strong>' . $createdCollectionCounter . '</strong>' ) ); ?>
            </div>
        <?php } else { ?>
            <form method="post">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <?php foreach( $propertyIDList as $propertyID ) { ?>
                            <div class="form-group">
                                <label for="collection-property-<?php echo $propertyID; ?>"><?php echo $this->translateText( 'base.collectionList.column.' . $propertyID ); ?>:</label>
                                <select id="collection-property-<?php echo $propertyID; ?>" class="form-control input-sm" name="propertyImportColumnList[<?php echo $propertyID; ?>]">
                                    <option value=""><?php echo $this->translateText( 'base.formDataImport.label.notImport' ); ?></option>
                                    <?php foreach( $importColumnList as $importColumnID => $importColumn ) { ?>
                                        <option value="<?php echo $importColumnID; ?>"<?php echo ( isset( $importColumn[ 'property_id' ] ) && $importColumn[ 'property_id' ] === $propertyID ) ? ' selected' : ''; ?>><?php echo htmlentities( $importColumn[ 'name' ] ); ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        <?php } ?>
                        <?php foreach( $form[ 'configuration' ][ 'attribute_list' ] as $attributeID => $attribute ) { ?>
                            <div class="form-group">
                                <label for="collection-attribute-<?php echo $attributeID; ?>"><?php echo htmlentities( $attribute[ 'name' ] ); ?>:</label>
                                <select id="collection-attribute-<?php echo $attributeID; ?>" class="form-control input-sm" name="attributeImportColumnList[<?php echo $attributeID; ?>]">
                                    <option value=""><?php echo $this->translateText( 'base.formDataImport.label.notImport' ); ?></option>
                                    <?php foreach( $importColumnList as $importColumnID => $importColumn ) { ?>
                                        <option value="<?php echo $importColumnID; ?>"<?php echo ( isset( $importColumn[ 'attribute_id' ] ) && $importColumn[ 'attribute_id' ] === $attributeID ) ? ' selected' : ''; ?>><?php echo htmlentities( $importColumn[ 'name' ] ); ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><?php echo $this->translateText( 'base.formDataImport.label.firstDataRowPresence' ); ?>:</label><br>
                            <label class="radio-inline"><input type="radio" checked value="0" name="firstDataRowPresence"> <?php echo $this->translateText( 'base.formDataImport.label.exclude' ); ?></label>
                            <label class="radio-inline"><input type="radio" value="1" name="firstDataRowPresence"> <?php echo $this->translateText( 'base.formDataImport.label.include' ); ?></label>
                        </div>
                    </div>
                </div>
                <div class="text-center-xs text-left-sm">
                    <button name="runImportAction" class="btn btn-success btn-sm" type="submit"><?php echo $this->translateText( 'base.formDataImport.button.export' ); ?></button>
                    <a class="btn btn-default btn-sm" href="<?php echo $this->pageLink( array( 'action' => 'formView', 'formID' => $form[ 'id' ] ) ); ?>"><?php echo $this->translateText( 'base.formDataImport.button.cancel' ); ?></a>
                    <input type="hidden" name="importData" value='<?php echo htmlspecialchars( json_encode( $importData, JSON_FORCE_OBJECT ), ENT_QUOTES & ~ENT_COMPAT, 'UTF-8' ); ?>'>
                    <input type="hidden" name="fileName" value="<?php echo htmlspecialchars( $fileName, ENT_QUOTES, 'UTF-8' ); ?>">
                </div>
            </form>
        <?php } ?>
    </div>
</div>
