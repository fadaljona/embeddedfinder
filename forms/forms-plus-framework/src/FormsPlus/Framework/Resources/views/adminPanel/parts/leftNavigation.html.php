<?php
    $formConfigList = $this->storage->fetchLatestFormListPerConfig();
?>
<div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
        <a href="<?php echo $_SERVER[ 'PHP_SELF' ]; ?>" class="site_title"><i class="fa fa-edit"></i> <span><?php echo $this->translateText( 'base.site.shortTitle' ); ?></span></a>
    </div>
    <div class="clearfix"></div>
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
        <div class="menu_section">
            <ul class="nav side-menu">
                <?php if( $this->checkPermission( 'formList' ) ) { ?>
                    <li><a href="<?php echo $this->pageLink(); ?>"<?php echo ( $this->variableList[ 'content_info' ][ 'action' ] == 'formList' ) ? ' class="selected"' : ''; ?>><i class="fa fa-home"></i> <?php echo $this->translateText( 'base.sidebarMenu.home' ); ?></a></li>
                <?php } ?>
                <?php if( $this->checkPermission( 'collectionSearch' ) ) { ?>
                    <li><a href="<?php echo $this->pageLink( array( 'action' => 'collectionSearch' ) ); ?>"<?php echo ( $this->variableList[ 'content_info' ][ 'action' ] == 'collectionSearch' ) ? ' class="selected"' : ''; ?>><i class="fa fa-search"></i> <?php echo $this->translateText( 'base.sidebarMenu.search' ); ?></a></li>
                <?php } ?>
                <?php if( !empty( $formConfigList ) && $this->checkPermission( 'formView' ) ) { ?>
                    <li><a><i class="fa fa-edit"></i> <?php echo $this->translateText( 'base.sidebarMenu.forms' ); ?> <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <?php foreach( $formConfigList as $formConfig ) { ?>
                                <li><a href="<?php echo $this->pageLink( array( 'action' => 'formView', 'formID' => $formConfig[ 'id' ] ) ); ?>"<?php echo ( isset( $this->variableList[ 'content_info' ][ 'config_file' ] ) && $this->variableList[ 'content_info' ][ 'config_file' ] == $formConfig[ 'config_file' ] ) ? ' class="selected"' : ''; ?>><?php echo $formConfig[ 'configuration' ][ 'name' ]; ?></a></li>
                            <?php } ?>
                        </ul>
                    </li>
                <?php } ?>
                <?php if( ( count( $this->configuration[ 'locale' ][ 'admin_panel' ] ) > 1 ) && $this->checkPermission( 'languageSwitch' ) ) { ?>
                    <li><a><i class="fa fa-language"></i> <?php echo $this->translateText( 'base.sidebarMenu.language' ); ?> <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <?php foreach( $this->configuration[ 'locale' ][ 'admin_panel' ] as $locale ) { ?>
                                <li><a href="<?php echo $this->pageLink( array( 'action' => 'languageSwitch', 'locale' => $locale, 'redirect' => $_GET ) ); ?>"><?php echo isset( $this->configuration[ 'locale' ][ 'name_list' ][ $locale ] ) ? $this->configuration[ 'locale' ][ 'name_list' ][ $locale ] : $locale; ?></a></li>
                            <?php } ?>
                        </ul>
                    </li>
                <?php } ?>
                <?php if( $this->checkPermission( 'systemInfo' ) ) { ?>
                    <li><a href="<?php echo $this->pageLink( array( 'action' => 'systemInfo' ) ); ?>"<?php echo ( $this->variableList[ 'content_info' ][ 'action' ] == 'systemInfo' ) ? ' class="selected"' : ''; ?>><i class="fa fa-cogs"></i> <?php echo $this->translateText( 'base.sidebarMenu.system' ); ?></a></li>
                <?php } ?>
                <?php if( !empty( $this->configuration[ 'administration' ][ 'navigation' ][ 'menu' ] ) && is_array( $this->configuration[ 'administration' ][ 'navigation' ][ 'menu' ] ) ) { ?>
                    <?php foreach( $this->configuration[ 'administration' ][ 'navigation' ][ 'menu' ] as $menuAction => $menuItem ) { ?>
                        <?php if( $this->checkPermission( $menuAction ) ) { ?>
                            <li><a href="<?php echo $this->pageLink( array( 'action' => $menuAction ) ); ?>"<?php echo ( $this->variableList[ 'content_info' ][ 'action' ] == $menuAction ) ? ' class="selected"' : ''; ?>><?php echo isset( $menuItem[ 'icon' ] ) ? $menuItem[ 'icon' ] : ''; ?> <?php echo isset( $menuItem[ 'text' ] ) ? $this->translateText( $menuItem[ 'text' ] ) : $menuAction; ?></a></li>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>
