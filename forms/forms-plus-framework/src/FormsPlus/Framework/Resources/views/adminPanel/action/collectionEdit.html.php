<?php
    $collection = !empty( $_GET[ 'collectionID' ] ) ? $this->storage->fetchCollection( $_GET[ 'collectionID' ] ) : null;
    if( empty( $collection  ) ) {
        return array( 'error' => $this->translateText( 'base.collectionView.alert.noCollectionWithID' ) );
    }
    $form = $this->storage->fetchForm( $collection[ 'form_id' ] );
    $this->variableList[ 'content_info' ][ 'config_file' ] = $form[ 'config_file' ];
    if( isset( $_POST[ 'saveCollectionAction' ] ) ) {
        foreach( $collection[ 'form_configuration' ][ 'attribute_list' ] as $attributeID => $attributeConfig ) {
            if( in_array( $attributeConfig[ 'datatype' ], array( 'file', 'imageupload', 'captcha' ) ) || !isset( $_POST[ $attributeID . 'Attribute' ] ) ) continue;
            $collection[ 'attribute_list' ][ $attributeID ][ 'final_value' ] = $_POST[ $attributeID . 'Attribute' ];
        }
        $this->storage->updateCollection( $collection );
        header( 'Location: ' . $this->pageLink( array( 'action' => 'collectionView', 'collectionID' => $collection[ 'collection_id' ] ) ) );
        $this->terminateScript();
    }
?>
<div class="page-title">
    <div class="title_left">
        <h3><?php echo $collection[ 'form_configuration' ][ 'name' ]; ?> <small><?php echo $this->translateText( 'base.collectionView.heading', array( '%id%' => $collection[ 'collection_id' ] ) ); ?></small></h3>
    </div>
</div>
<div class="clearfix"></div>
<div class="x_panel">
    <div class="x_title">
        <h2><?php echo $this->translateText( 'base.collectionEdit.panel.name' ); ?></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <form class="form-horizontal form-label-left" method="post">
            <?php $hasEditableAttributes = false; ?>
            <?php foreach( $collection[ 'form_configuration' ][ 'attribute_list' ] as $attributeID => $attributeConfig ) { ?>
                <?php if( in_array( $attributeConfig[ 'datatype' ], array( 'file', 'imageupload', 'captcha' ) ) ) continue; ?>
                <?php
                    $hasEditableAttributes = true;
                    $attributeFinalValue = isset( $collection[ 'attribute_list' ][ $attributeID ][ 'final_value' ] ) ? $collection[ 'attribute_list' ][ $attributeID ][ 'final_value' ] : '';
                ?>
                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"><?php echo $attributeConfig[ 'name' ]; ?></label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="name" class="form-control col-md-7 col-xs-12" name="<?php echo $attributeID; ?>Attribute" type="text" value="<?php echo htmlspecialchars( $attributeFinalValue ); ?>">
                    </div>
                </div>
            <?php } ?>
            <?php if( $hasEditableAttributes ) { ?>
                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-3">
                        <a href="<?php echo $this->pageLink( array( 'action' => 'collectionView', 'collectionID' => $collection[ 'collection_id' ] ) ); ?>" class="btn btn-primary"><?php echo $this->translateText( 'base.collectionEdit.button.cancel' ); ?></a>
                        <button type="submit" class="btn btn-success" name="saveCollectionAction"><?php echo $this->translateText( 'base.collectionEdit.button.save' ); ?></button>
                    </div>
                </div>
            <?php } else { ?>
                <div class="alert alert-warning rm-mr" role="alert">
                    <?php echo $this->translateText( 'base.collectionEdit.alert.noEditableAttributes' ); ?>
                </div>
                <br>
                <div class="alert alert-info rm-mr" role="info">
                    <?php echo $this->translateText( 'base.collectionEdit.alert.consideredEditable', array( '%list%' => '<strong>file</strong>, <strong>imageupload</strong>, <strong>captcha</strong>' ) ); ?>
                </div>
            <?php } ?>
        </form>
    </div>
</div>
