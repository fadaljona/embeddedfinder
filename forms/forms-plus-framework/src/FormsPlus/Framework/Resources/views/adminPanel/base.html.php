<!DOCTYPE html>
<?php use FormsPlus\Framework\Helper\Debug; ?>
<html lang="<?php echo $this->variableList[ 'locale' ]; ?>">
    <head>
        <title><?php echo $this->translateText( 'base.site.title' ); ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php foreach( $this->variableList[ 'style_list' ] as $style ) { ?>
            <link rel="stylesheet" href="<?php echo $this->variableList[ 'asset_directory_uri' ] . $style; ?>" type="text/css">
        <?php } ?>
    </head>
    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col">
                    <?php echo $this->includeTemplate( 'parts/leftNavigation.html.php' ); ?>
                </div>
                <div class="top_nav">
                   <?php echo $this->includeTemplate( 'parts/topNavigation.html.php' ); ?>
                </div>
                <div class="right_col" role="main">
                    <?php if( isset( $content ) ) { ?>
                        <?php echo $content; ?>
                    <?php } elseif( isset( $error ) ) { ?>
                        <div class="clearfix"></div>
                        <div class="alert alert-danger" role="alert">
                            <?php echo $error; ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php echo Debug::getMessageListHTML(); ?>
        <?php foreach( $this->variableList[ 'script_list' ] as $script ) { ?>
            <script src="<?php echo $this->variableList[ 'asset_directory_uri' ] . $script; ?>" type="text/javascript"></script>
        <?php } ?>
    </body>
</html>
