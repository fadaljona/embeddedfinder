<div class="nav_menu">
    <nav class="" role="navigation">
        <div class="nav toggle">
            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <li>
                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><?php echo $this->session->getSegment( 'FormsPlus\AdminPanel' )->get( 'username' ); ?> <span class=" fa fa-angle-down"></span></a>
                <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="<?php echo $_SERVER[ 'PHP_SELF' ]; ?>?action=logout"><i class="fa fa-sign-out pull-right"></i> <?php echo $this->translateText( 'base.header.logOut' ); ?></a></li>
                </ul>
            </li>
        </ul>
    </nav>
</div>
