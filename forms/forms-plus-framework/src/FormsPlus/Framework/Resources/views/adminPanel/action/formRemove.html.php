<?php
    $form = !empty( $_GET[ 'formID' ] ) ? $this->storage->fetchForm( $_GET[ 'formID' ] ) : null;
    if( empty( $form  ) ) {
        return array( 'error' => $this->translateText( 'base.formView.alert.noFormWithID' ) );
    }
    $this->variableList[ 'content_info' ][ 'config_file' ] = $form[ 'config_file' ];
    if( isset( $_POST[ 'confirmFormRemovalAction' ] ) ) {
        $this->storage->removeForm( $_GET[ 'formID' ] );
        header( 'Location: ' . $_SERVER[ 'PHP_SELF' ] );
        $this->terminateScript();
    }
?>
<div class="page-title">
    <div class="title_left">
        <h3><?php echo $form[ 'configuration' ][ 'name' ]; ?></h3>
    </div>
</div>
<div class="clearfix"></div>
<div class="x_panel">
    <div class="x_title">
        <h2><?php echo $this->translateText( 'base.formRemove.panel.name' ); ?></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <p><?php echo $this->translateText( 'base.formRemove.alert.confirmationMessage' ); ?></p>
    </div>
    <form method="post">
        <button type="submit" class="btn btn-danger btn-sm" name="confirmFormRemovalAction"><?php echo $this->translateText( 'base.formRemove.button.confirmRemoval' ); ?></button>
        <a class="btn btn-default btn-sm" href="<?php echo $this->pageLink( array( 'action' => 'formView', 'formID' => $form[ 'id' ] ) ); ?>"><?php echo $this->translateText( 'base.formRemove.button.cancel' ); ?></a>
    </form>
</div>
