<?php
    use FormsPlus\Framework\Helper\Debug;
    $form = !empty( $_GET[ 'formID' ] ) ? $this->storage->fetchForm( $_GET[ 'formID' ] ) : null;
    $supportedFormatList = array(
        'htm'  => $this->translateText( 'base.collectionExport.format.htm.name' ),
        'csv'  => $this->translateText( 'base.collectionExport.format.csv.name' ),
        'ods'  => $this->translateText( 'base.collectionExport.format.ods.name' ),
        'pdf'  => $this->translateText( 'base.collectionExport.format.pdf.name' ),
        'xls'  => $this->translateText( 'base.collectionExport.format.xls.name' ),
        'xlsx' => $this->translateText( 'base.collectionExport.format.xlsx.name' )
    );
    $format = !empty( $_POST[ 'exportFormat' ] ) ? $_POST[ 'exportFormat' ] : ( !empty( $_GET[ 'exportFormat' ] ) ? $_GET[ 'exportFormat' ] : 'none' );
    if( !isset( $supportedFormatList[ $format ] ) ) {
        return array( 'error' => $this->translateText( 'base.formDataExport.alert.formatNotSpecified' ) );
    }
    if( empty( $form ) ) {
        return array( 'error' => $this->translateText( 'base.formView.alert.noFormWithID' ) );
    }
    $this->variableList[ 'content_info' ][ 'config_file' ] = $form[ 'config_file' ];
    array_splice( $this->variableList[ 'script_list' ], 1, 0, array(
        'js/jquery-ui/js/core.js',
        'js/jquery-ui/js/widget.js',
        'js/jquery-ui/js/mouse.js',
        'js/jquery-ui/js/sortable.js'
    ));
    $fieldFullList = array(
        'prop_id'      => array( $this->translateText( 'base.collectionList.column.id' ), 'collection_id', false ),
        'prop_created' => array( $this->translateText( 'base.collectionList.column.submitted' ), 'collection_created', false ),
        'prop_ip'      => array( $this->translateText( 'base.collectionList.column.ip' ), 'collection_ip', false )
    );
    foreach( $form[ 'configuration' ][ 'attribute_list' ] as $attributeID => $attribute ) {
        $fieldFullList[ 'attr_' . $attributeID ] = array( $attribute[ 'name' ], $attributeID, true );
    }
    if( isset( $_POST[ 'runExportAction' ] ) ) {
        if( empty( $_POST[ 'showFields' ] ) ) {
            return array( 'error' => $this->translateText( 'base.formDataExport.alert.absentColumns' ) );
        }
        $fieldList = array();
        $fieldNameList = array();
        foreach( $_POST[ 'showFields' ] as $fieldID ) {
            if( isset( $fieldFullList[ $fieldID ] ) ) {
                $fieldList[ $fieldID ] = $fieldFullList[ $fieldID ];
                $fieldNameList[] = $fieldFullList[ $fieldID ][ 0 ];
            }
        }
        $fieldCount = count( $fieldList );
        $searchText = isset( $_GET[ 'searchText' ] ) ? $_GET[ 'searchText' ] : '';
        $sort = ( isset( $_GET[ 'sort' ] ) && is_array( $_GET[ 'sort' ] ) && count( $_GET[ 'sort' ] ) == 2 ) ? $_GET[ 'sort' ] : array( 'prop_created', true );

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator( 'FormsPlusPHP' )
                       ->setLastModifiedBy( 'FormsPlusPHP' )
                       ->setTitle( 'Collected data for "' . $form[ 'configuration' ][ 'name' ] . '"' )
                       ->setSubject( 'Collected data for "' . $form[ 'configuration' ][ 'name' ] . '"' );
        $objPHPExcel->setActiveSheetIndex( 0 );
        $objPHPExcel->getActiveSheet()->fromArray( $fieldNameList, NULL, 'A1' );
        $collectionCount = false;
        $itemsPerQuery = 50;
        $offsetCounter = 0;
        do {
            $collectionList = $this->storage->fetchCollectionList( $_GET[ 'formID' ], $itemsPerQuery, $offsetCounter, $sort, ( !empty( $searchText ) ? '*' . $searchText . '*' : '' ) );
            if( $collectionCount === false ) {
                $collectionCount = $collectionList[ 'count' ];
            }
            $cellCounter = array( 'column' => 0, 'row' => $offsetCounter + 1 );
            $offsetCounter = $offsetCounter + $itemsPerQuery;
            $collectionValueList = array();
            foreach( $collectionList[ 'list' ] as $collection ) {
                $cellCounter[ 'row' ]++;
                $cellCounter[ 'column' ] = -1;
                $collectionValue = array();
                foreach( $fieldList as $fieldData ) {
                    $cellCounter[ 'column' ]++;
                    if( $fieldData[ 2 ] ) {
                        if( isset( $collection[ 'attribute_list' ][ $fieldData[ 1 ] ][ 'files' ] ) ) {
                            $collectionValue[] = $this->generateFileAttributeHtml( $collection[ 'collection_id' ], $fieldData[ 1 ], $collection[ 'attribute_list' ][ $fieldData[ 1 ] ][ 'files' ], null, true, '%name% (%size%)' );
                            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow( $cellCounter[ 'column' ], $cellCounter[ 'row' ] )->getHyperlink()->setUrl( $this->pageLink( array( 'action' => 'fileDownload', 'collectionID' => $collection[ 'collection_id' ], 'attributeID' => $fieldData[ 1 ] ), true ) );
                        } else {
                            $collectionValue[] = isset( $collection[ 'attribute_list' ][ $fieldData[ 1 ] ][ 'final_value' ] ) ? $collection[ 'attribute_list' ][ $fieldData[ 1 ] ][ 'final_value' ] : '';
                        }
                    } else {
                        $collectionValue[] = $collection[ $fieldData[ 1 ] ];
                    }
                }
                $collectionValueList[] = $collectionValue;
            }
            $objPHPExcel->getActiveSheet()->fromArray( $collectionValueList, '', 'A' . ( $offsetCounter - $itemsPerQuery + 2 ) );
        } while( $offsetCounter < $collectionCount );
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow( 0, 1, $fieldCount - 1, 1 )->getFont()->setBold( true );
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow( 0, 1, $fieldCount - 1, $collectionCount + 1 )->getAlignment()
            ->setHorizontal( PHPExcel_Style_Alignment::HORIZONTAL_LEFT )
            ->setVertical( PHPExcel_Style_Alignment::VERTICAL_TOP );
        foreach( $fieldNameList as $index => $test ) {
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn( $index )->setAutoSize( true );
        }
        $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation( empty( $_POST[ 'pageOrientation' ] ) ? PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT : PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE );
        $objPHPExcel->getActiveSheet()->setTitle( ( mb_strlen( $form[ 'configuration' ][ 'name' ], 'UTF-8' ) ) > 31 ? mb_substr( $form[ 'configuration' ][ 'name' ], 0, 28, 'UTF-8' ) . '...' : $form[ 'configuration' ][ 'name' ] );
        $objPHPExcel->getActiveSheet()->setShowGridLines( !empty( $_POST[ 'gridLines' ] ) );
        $objPHPExcel->setActiveSheetIndex( 0 );
        $fileName = 'CollectedFormData' . $form[ 'id' ];
        switch( $format ) {
            case 'htm':
                header( 'Content-Type: text/csv; charset=utf-8' );
                header( 'Content-Disposition: attachment;filename="' . $fileName . '.htm"' );
                $objWriter = PHPExcel_IOFactory::createWriter( $objPHPExcel, 'HTML' );
                $objWriter->save( 'php://output' );
                break;
            case 'csv':
                header( 'Content-Type: text/csv; charset=utf-8' );
                header( 'Content-Disposition: attachment;filename="' . $fileName . '.csv"' );
                $objWriter = PHPExcel_IOFactory::createWriter( $objPHPExcel, 'CSV' );
                $objWriter->setUseBOM( true );
                $objWriter->save( 'php://output' );
                break;
            case 'ods':
                header( 'Content-Type: application/vnd.oasis.opendocument.spreadsheet' );
                header( 'Content-Disposition: attachment;filename="' . $fileName . '.ods"' );
                $objWriter = PHPExcel_IOFactory::createWriter( $objPHPExcel, 'OpenDocument' );
                $objWriter->save( 'php://output' );
                break;
            case 'pdf':
                header( 'Content-Type: application/pdf' );
                header( 'Content-Disposition: attachment;filename="' . $fileName . '.pdf"' );
                $reflector = new ReflectionClass( 'TCPDF' );
                $rendererLibraryPath = dirname( $reflector->getFileName() );
                PHPExcel_Settings::setPdfRenderer( PHPExcel_Settings::PDF_RENDERER_TCPDF, $rendererLibraryPath );
                $objWriter = PHPExcel_IOFactory::createWriter( $objPHPExcel, 'PDF' );
                $objWriter->save( 'php://output' );
                break;
            case 'xls':
                header( 'Content-Type: application/vnd.ms-excel' );
                header( 'Content-Disposition: attachment;filename="' . $fileName . '.xls"' );
                $objWriter = PHPExcel_IOFactory::createWriter( $objPHPExcel, 'Excel5' );
                $objWriter->save( 'php://output' );
                break;
            case 'xlsx':
                header( 'Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' );
                header( 'Content-Disposition: attachment;filename="' . $fileName . '.xlsx"' );
                $objWriter = PHPExcel_IOFactory::createWriter( $objPHPExcel, 'Excel2007' );
                $objWriter->save( 'php://output' );
                break;
        }
        $this->terminateScript();
    }
?>
<div class="page-title">
    <div class="title_left">
        <h3><?php echo $this->translateText( 'base.formDataExport.heading', array( '%form%' => '<strong>' . $form[ 'configuration' ][ 'name' ] . '</strong>' ) ); ?></h3>
    </div>
</div>
<div class="clearfix"></div>
<div class="x_panel">
    <div class="x_title">
        <h2><i class="fa fa-cog"></i> <?php echo $this->translateText( 'base.formDataExport.panel.name' ); ?></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <form method="post">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <label><?php echo $this->translateText( 'base.formDataExport.label.showColumns' ); ?>:</label>
                    <ul class="to_do sortable">
                        <?php foreach( $fieldFullList as $name => $fieldData ) { ?>
                            <li><p><span class="todo-mark"><i class="fa fa-arrows-v" aria-hidden="true"></i></span><input type="checkbox" name="showFields[]" value="<?php echo $name; ?>" checked> <?php echo $fieldData[ 0 ]; ?></p></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="preference-export-format"><?php echo $this->translateText( 'base.formDataExport.label.exportFormat' ); ?>:</label>
                        <select id="preference-export-format" class="form-control input-sm" name="exportFormat">
                            <?php foreach( $supportedFormatList as $supportedFormatID => $supportedFormatName ) { ?>
                                <option value="<?php echo $supportedFormatID; ?>"<?php echo ( $format == $supportedFormatID ) ? ' selected' : ''; ?>><?php echo $supportedFormatName; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label><?php echo $this->translateText( 'base.formDataExport.label.pageOrientation' ); ?>:</label><br>
                        <label class="radio-inline"><input type="radio" checked value="0" name="pageOrientation"> <?php echo $this->translateText( 'base.formDataExport.label.portrait' ); ?></label>
                        <label class="radio-inline"><input type="radio" value="1" name="pageOrientation"> <?php echo $this->translateText( 'base.formDataExport.label.landscape' ); ?></label>
                    </div>
                    <div class="form-group">
                        <label><?php echo $this->translateText( 'base.formDataExport.label.gridLines' ); ?>:</label><br>
                        <label class="radio-inline"><input type="radio" checked value="1" name="gridLines"> <?php echo $this->translateText( 'base.formDataExport.label.show' ); ?></label>
                        <label class="radio-inline"><input type="radio" value="0" name="gridLines"> <?php echo $this->translateText( 'base.formDataExport.label.hide' ); ?></label>
                    </div>
                </div>
            </div>
            <div class="text-center-xs text-left-sm">
                <button name="runExportAction" class="btn btn-success btn-sm" type="submit"><?php echo $this->translateText( 'base.formDataExport.button.export' ); ?></button>
                <a class="btn btn-default btn-sm" href="<?php echo $this->pageLink( array( 'action' => 'formView', 'formID' => $form[ 'id' ] ) ); ?>"><?php echo $this->translateText( 'base.formDataExport.button.cancel' ); ?></a>
            </div>
        </form>
    </div>
</div>
