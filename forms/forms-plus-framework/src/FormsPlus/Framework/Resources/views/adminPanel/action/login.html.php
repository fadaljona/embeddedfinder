<?php
    $userList = $this->configuration[ 'administration' ][ 'access' ][ 'user_list' ];
    if( isset( $_POST[ 'username' ] ) && isset( $_POST[ 'password' ] ) ) {
        if( ( isset( $userList[ $_POST[ 'username' ] ] ) && $userList[ $_POST[ 'username' ] ] === $_POST[ 'password' ] ) ||
            ( isset( $userList[ $_POST[ 'username' ] ][ 'password' ] ) && $userList[ $_POST[ 'username' ] ][ 'password' ] === $_POST[ 'password' ] ) ) {
            $this->session->clear();
            $this->session->getSegment( 'FormsPlus\AdminPanel' )->set( 'username', $_POST[ 'username' ] );
            $this->session->regenerateId();
            $this->pageRedirect();
        } else {
            $loginPageError = $this->translateText( 'login.form.alert.incorrectUsernamePassword' );
        }
    }
    $this->variableList[ 'layout' ] = 'login.html.php';
?>
<div id="wrapper">
    <div class="form">
        <section class="login_content">
            <form method="post">
                <h1><?php echo $this->translateText( 'login.form.name' ); ?></h1>
                <?php if( !empty( $loginPageError ) ) { ?>
                    <div role="alert" class="alert alert-error"><?php echo $loginPageError; ?></div>
                <?php } ?>
                <div><input type="text" class="form-control" placeholder="<?php echo $this->translateText( 'login.form.field.username' ); ?>" required name="username" /></div>
                <div><input type="password" class="form-control" placeholder="<?php echo $this->translateText( 'login.form.field.password' ); ?>" required name="password" /></div>
                <div><button class="btn btn-default submit" type="submit"><?php echo $this->translateText( 'login.form.button.login' ); ?></button></div>
            </form>
        </section>
    </div>
</div>
