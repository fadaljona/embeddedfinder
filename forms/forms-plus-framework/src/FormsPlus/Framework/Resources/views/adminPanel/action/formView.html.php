<?php
    use JasonGrimes\Paginator;
    $form = !empty( $_GET[ 'formID' ] ) ? $this->storage->fetchForm( $_GET[ 'formID' ] ) : null;
    if( empty( $form  ) ) {
        return array( 'error' => $this->translateText( 'base.formView.alert.noFormWithID' ) );
    }
    $this->variableList[ 'content_info' ][ 'config_file' ] = $form[ 'config_file' ];
    $formConfiguration = $form[ 'configuration' ];
    $collectionTableConfiguration = $form[ 'preference_configuration' ][ 'administration' ][ 'collection_table' ];
    $formVersionList = $this->storage->fetchFormListByConfig( $form[ 'config_file' ] );
    // remove selected items: start
    if( $this->checkPermission( 'collectionRemove' ) ) {
        if( isset( $_POST[ 'selectedCollectionIDs' ] ) && isset( $_POST[ 'removeCollectionAction' ] ) && is_array( $_POST[ 'selectedCollectionIDs' ] ) ) {
            foreach( $_POST[ 'selectedCollectionIDs' ] as $collectionID ) {
                $this->storage->removeCollection( $collectionID );
            }
        }
    }
    // remove selected items: end
    $queryArgumentList = array( 'action' => 'formView', 'formID' => $form[ 'id' ] );
    $itemsPerPage = 10;
    if( !empty( $_GET[ 'pageLimit' ] ) ) {
        $itemsPerPage = intval( $_GET[ 'pageLimit' ] );
        $queryArgumentList[ 'pageLimit' ] = $itemsPerPage;
    }
    $currentPage = 1;
    if( !empty( $_GET[ 'navigationPage' ] ) ) {
        $currentPage = intval( $_GET[ 'navigationPage' ] );
    }
    $searchText = '';
    if( isset( $_GET[ 'searchText' ] ) && trim( $_GET[ 'searchText' ] ) != '' ) {
        $searchText = trim( $_GET[ 'searchText' ] );
        $queryArgumentList[ 'searchText' ] = $searchText;
    }
    $sort = array( $collectionTableConfiguration[ 'default_sort_field' ], $collectionTableConfiguration[ 'default_sort_order' ] );
    if( isset( $_GET[ 'sortColumn' ] ) && isset( $_GET[ 'sortOrder' ] ) ) {
        $columnList = array( 'prop_collection_created', 'prop_collection_ip' );
        foreach( $formConfiguration[ 'attribute_list' ] as $attributeID => $attribute ) {
            $columnList[] = 'attr_' . $attributeID;
        }
        if( in_array( $_GET[ 'sortColumn' ], $columnList ) ) {
            $sort = array( $_GET[ 'sortColumn' ], (bool) $_GET[ 'sortOrder' ] );
            $queryArgumentList[ 'sortColumn' ] = $sort[ 0 ];
            $queryArgumentList[ 'sortOrder' ] = $sort[ 1 ];
        }
    }
    $urlQuery = '?' . http_build_query( $queryArgumentList );
    $collectionList = $this->storage->fetchCollectionList( $form[ 'id' ], $itemsPerPage, ( ( $currentPage - 1 ) * $itemsPerPage ), $sort, $searchText );
    $paginator = new Paginator( $collectionList[ 'count' ], $itemsPerPage, $currentPage, $urlQuery . '&navigationPage=(:num)' );
?>
<div class="page-title">
    <div class="title_left">
        <h3><?php echo $formConfiguration[ 'name' ]; ?></h3>
    </div>
</div>
<div class="clearfix"></div>
<form class="form-inline" method="get">
    <div class="row">
        <div class="col-xs-12 col-sm-6 text-center-xs text-left-sm">
            <div class="input-group">
                <input type="hidden" name="action" value="formView">
                <select name="formID" class="form-control input-sm">
                    <?php foreach( $formVersionList as $formItem ) { ?>
                        <option value="<?php echo $formItem[ 'id' ]; ?>"<?php echo ( $form[ 'id' ] == $formItem[ 'id' ] ) ? ' selected' : ''; ?>><?php echo $formItem[ 'created' ]; ?></option>
                    <?php } ?>
                </select>
                <span class="input-group-btn"><button type="submit" class="btn btn-info btn-sm"><?php echo $this->translateText( 'base.formView.button.selectFormVersion' ); ?></button></span>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 text-center-xs text-right-sm">
            <div class="input-group">
                <?php if( $this->checkPermission( 'formPreferences' ) ) { ?>
                    <a class="btn btn-info btn-sm" title="Preferences" href="<?php echo $this->pageLink( array( 'action' => 'formPreferences', 'formID' => $form[ 'id' ] ) ); ?>"><?php echo $this->translateText( 'base.formView.button.preferences' ); ?> <i class="fa fa-cog"></i></a>
                <?php } ?>
                <?php if( $this->checkPermission( 'formRemove' ) ) { ?>
                    <a class="btn btn-danger btn-sm" href="<?php echo $this->pageLink( array( 'action' => 'formRemove', 'formID' => $form[ 'id' ] ) ); ?>"><?php echo $this->translateText( 'base.formView.button.removeCurrentVersion' ); ?> <i class="fa fa-trash" aria-hidden="true"></i></a>
                <?php } ?>
            </div>
        </div>
    </div>
</form>
<div class="x_panel">
    <div class="x_title">
        <h2><?php echo $this->translateText( 'base.formInfo.panel.name' ); ?></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <strong><?php echo $this->translateText( 'base.formInfo.label.created' ); ?>:</strong> <?php echo $form[ 'created' ]; ?><br>
        <strong><?php echo $this->translateText( 'base.formInfo.label.attributeList' ); ?>:</strong>
        <?php
            $prefix = '';
            foreach( $formConfiguration[ 'attribute_list' ] as $attributeID => $attribute ) {
                echo $prefix . '<code class="info">' . $attribute[ 'name' ] . '</code>';
                $prefix = ', ';
            }
            unset( $prefix );
        ?>
        <br>
        <strong><?php echo $this->translateText( 'base.formInfo.label.collectionNumber' ); ?>:</strong> <?php echo $collectionList[ 'count' ]; ?>
    </div>
</div>
<?php if( class_exists( 'PHPExcel' ) && $this->checkPermission( 'formDataExport' ) ) { ?>
    <div class="x_panel">
        <div class="x_title">
            <h2><?php echo $this->translateText( 'base.collectionExport.panel.name' ); ?></h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <?php $exportQueryArgumentList = array( 'action' => 'formDataExport', 'formID' => $form[ 'id' ], 'sort' => $sort, 'searchText' => $searchText ); ?>
            <a class="btn btn-default btn-sm" href="<?php echo $this->pageLink( array_merge( $exportQueryArgumentList, array( 'exportFormat' => 'htm' ) ) ); ?>" title="<?php echo $this->translateText( 'base.collectionExport.format.htm.title' ); ?>"><?php echo $this->translateText( 'base.collectionExport.format.htm.name' ); ?></i></a>
            <a class="btn btn-default btn-sm" href="<?php echo $this->pageLink( array_merge( $exportQueryArgumentList, array( 'exportFormat' => 'csv' ) ) ); ?>" title="<?php echo $this->translateText( 'base.collectionExport.format.csv.title' ); ?>"><?php echo $this->translateText( 'base.collectionExport.format.csv.name' ); ?></i></a>
            <a class="btn btn-default btn-sm" href="<?php echo $this->pageLink( array_merge( $exportQueryArgumentList, array( 'exportFormat' => 'ods' ) ) ); ?>" title="<?php echo $this->translateText( 'base.collectionExport.format.ods.title' ); ?>"><?php echo $this->translateText( 'base.collectionExport.format.ods.name' ); ?></i></a>
            <a class="btn btn-default btn-sm" href="<?php echo $this->pageLink( array_merge( $exportQueryArgumentList, array( 'exportFormat' => 'pdf' ) ) ); ?>" title="<?php echo $this->translateText( 'base.collectionExport.format.pdf.title' ); ?>"><?php echo $this->translateText( 'base.collectionExport.format.pdf.name' ); ?></i></a>
            <a class="btn btn-default btn-sm" href="<?php echo $this->pageLink( array_merge( $exportQueryArgumentList, array( 'exportFormat' => 'xls' ) ) ); ?>" title="<?php echo $this->translateText( 'base.collectionExport.format.xls.title' ); ?>"><?php echo $this->translateText( 'base.collectionExport.format.xls.name' ); ?></i></a>
            <a class="btn btn-default btn-sm" href="<?php echo $this->pageLink( array_merge( $exportQueryArgumentList, array( 'exportFormat' => 'xlsx' ) ) ); ?>" title="<?php echo $this->translateText( 'base.collectionExport.format.xlsx.title' ); ?>"><?php echo $this->translateText( 'base.collectionExport.format.xlsx.name' ); ?></i></a>
        </div>
    </div>
<?php } ?>
<?php if( class_exists( 'PHPExcel' ) && $this->checkPermission( 'formDataImport' ) ) { ?>
    <div class="x_panel">
        <div class="x_title">
            <h2><?php echo $this->translateText( 'base.collectionImport.panel.name' ); ?></h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <form action="<?php echo $this->pageLink( array( 'action' => 'formDataImport', 'formID' => $form[ 'id' ] ) ); ?>" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="form-data-import-file"><?php echo $this->translateText( 'base.collectionImport.label.sourceFile' ); ?>:</label>
                    <input id="form-data-import-file" class="form-control-file" type="file" name="sourceFile" required>
                </div>
                <div class="form-group">
                    <label><?php echo $this->translateText( 'base.collectionImport.label.columnComparison' ); ?>:</label><br>
                    <label class="radio-inline"><input type="radio" checked value="0" name="columnComparison"> <?php echo $this->translateText( 'base.collectionImport.label.attributeID' ); ?></label>
                    <label class="radio-inline"><input type="radio" value="1" name="columnComparison"> <?php echo $this->translateText( 'base.collectionImport.label.attributeName' ); ?></label>
                </div>
                <div class="text-center-xs text-left-sm">
                    <button name="uploadImportFileAction" class="btn btn-success btn-sm" type="submit"><?php echo $this->translateText( 'base.collectionImport.button.uploadFile' ); ?></button>
                </div>
            </form>
        </div>
    </div>
<?php } ?>
<?php $sortQueryArgumentList = $queryArgumentList; ?>
<div class="x_panel">
    <div class="x_title">
        <h2><?php echo $this->translateText( 'base.collectionList.panel.name' ); ?></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
            <form method="get">
                <input type="hidden" name="action" value="formView">
                <input type="hidden" name="formID" value="<?php echo $form[ 'id' ]; ?>">
                <input type="hidden" name="sortColumn" value="<?php echo $sort[ 0 ]; ?>">
                <input type="hidden" name="sortOrder" value="<?php echo $sort[ 1 ]; ?>">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 text-center-xs text-left-sm">
                        <div class="form-group">
                            <div class="input-group">
                                <select name="pageLimit" class="form-control input-sm">
                                    <?php foreach( array( '100', '50', '25', '10' ) as $pageLimitItem ) { ?>
                                       <option value="<?php echo $pageLimitItem; ?>"<?php echo ( $pageLimitItem == $itemsPerPage ) ? ' selected' : ''; ?>>  <?php echo $this->translateText( 'base.collectionList.select.pageLimit', array( '%limit%' => $pageLimitItem ) ); ?></option>
                                    <?php } ?>
                                </select>
                                <span class="input-group-btn">
                                    <button class="btn btn-warning btn-sm" type="submit" title="<?php echo $this->translateText( 'base.collectionList.button.sort' ); ?>"><i class="fa fa-list"></i></button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 text-center-xs text-right-sm">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" class="form-control input-sm" placeholder="<?php echo $this->translateText( 'base.collectionList.field.search' ); ?>" name="searchText" value="<?php echo htmlspecialchars( $searchText, ENT_QUOTES, 'UTF-8' ) ?>">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary btn-sm" type="submit" title="<?php echo $this->translateText( 'base.collectionList.button.search' ); ?>"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <?php if( $collectionList[ 'count' ] > 0 ) { ?>
                <form method="post">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTable">
                            <thead>
                                <tr>
                                    <?php if( $this->checkPermission( 'collectionRemove' ) ) { ?>
                                        <th class="shrink"><input type="checkbox" data-check-all-toggle="item"></th>
                                    <?php } ?>
                                    <th class="shrink text-center"><?php echo $this->translateText( 'base.collectionList.column.id' ); ?></th>
                                    <?php if( in_array( 'collection_created', $collectionTableConfiguration[ 'field_presence' ][ 'property_list' ] ) ) { ?>
                                        <?php
                                            $sortQueryArgumentList[ 'sortColumn' ] = 'prop_collection_created';
                                            $sortQueryArgumentList[ 'sortOrder' ] = !$sort[ 1 ];
                                        ?>
                                        <th><a class="pull-right" href="<?php echo '?' . http_build_query( $sortQueryArgumentList ); ?>"><i class="fa fa-<?php echo ( $sort[ 0 ] == 'prop_collection_created' ) ? ( $sort[ 1 ] ? 'sort-amount-asc' : 'sort-amount-desc' ) : 'sort'; ?>"></i></a><?php echo $this->translateText( 'base.collectionList.column.submitted' ); ?></th>
                                    <?php } ?>
                                    <?php if( in_array( 'collection_ip', $collectionTableConfiguration[ 'field_presence' ][ 'property_list' ] ) ) { ?>
                                        <?php
                                            $sortQueryArgumentList[ 'sortColumn' ] = 'prop_collection_ip';
                                            $sortQueryArgumentList[ 'sortOrder' ] = !$sort[ 1 ];
                                        ?>
                                        <th><a class="pull-right" href="<?php echo '?' . http_build_query( $sortQueryArgumentList ); ?>"><i class="fa fa-<?php echo ( $sort[ 0 ] == 'prop_collection_ip' ) ? ( $sort[ 1 ] ? 'sort-amount-asc' : 'sort-amount-desc' ) : 'sort'; ?>"></i></a><?php echo $this->translateText( 'base.collectionList.column.ip' ); ?></th>
                                    <?php } ?>
                                    <?php foreach( $formConfiguration[ 'attribute_list' ] as $attributeID => $attribute ) { ?>
                                        <?php if( !in_array( $attributeID, $collectionTableConfiguration[ 'field_presence' ][ 'attribute_list' ] ) ) continue; ?>
                                        <?php
                                            $sortQueryArgumentList[ 'sortColumn' ] = 'attr_' . $attributeID;
                                            $sortQueryArgumentList[ 'sortOrder' ] = !$sort[ 1 ];
                                        ?>
                                        <th><a class="pull-right" href="<?php echo '?' . http_build_query( $sortQueryArgumentList ); ?>"><i class="fa fa-<?php echo ( $sort[ 0 ] == ( 'attr_' . $attributeID ) ) ? ( $sort[ 1 ] ? 'sort-amount-asc' : 'sort-amount-desc' ) : 'sort'; ?>"></i></a><?php echo htmlentities( $attribute[ 'name' ] ); ?></th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach( $collectionList[ 'list' ] as $collection ) { ?>
                                    <tr>
                                        <?php if( $this->checkPermission( 'collectionRemove' ) ) { ?>
                                            <th scope="row"><input name="selectedCollectionIDs[]" type="checkbox" value="<?php echo $collection[ 'collection_id' ]; ?>" data-check-all-group="item"></th>
                                        <?php } ?>
                                        <td class="text-center"><a href="<?php echo $this->pageLink( array( 'action' => 'collectionView', 'collectionID' => $collection[ 'collection_id' ] ) ); ?>"><span class="badge bg-blue"><?php echo $collection[ 'collection_id' ]; ?></span></a></td>
                                        <?php if( in_array( 'collection_created', $collectionTableConfiguration[ 'field_presence' ][ 'property_list' ] ) ) { ?>
                                            <td><?php echo $collection[ 'collection_created' ]; ?></td>
                                        <?php } ?>
                                        <?php if( in_array( 'collection_ip', $collectionTableConfiguration[ 'field_presence' ][ 'property_list' ] ) ) { ?>
                                            <td><?php echo $collection[ 'collection_ip' ]; ?></td>
                                        <?php } ?>
                                        <?php foreach( $formConfiguration[ 'attribute_list' ] as $attributeID => $attribute ) { ?>
                                            <?php if( !in_array( $attributeID, $collectionTableConfiguration[ 'field_presence' ][ 'attribute_list' ] ) ) continue; ?>
                                            <td>
                                                <?php
                                                    if( isset( $collection[ 'attribute_list' ][ $attributeID ][ 'files' ] ) ) {
                                                        echo $this->generateFileAttributeHtml( $collection[ 'collection_id' ], $attributeID, $collection[ 'attribute_list' ][ $attributeID ][ 'files' ] );
                                                    } else {
                                                        echo ( isset( $collection[ 'attribute_list' ][ $attributeID ][ 'final_value' ] ) ? ( $searchText != '' ? preg_replace( "/(" . implode( '|', array_map( 'preg_quote', array_map( 'htmlspecialchars', $collectionList[ 'search' ] ) ) ) . ")/ui", "<strong>$0</strong>", htmlspecialchars( $collection[ 'attribute_list' ][ $attributeID ][ 'final_value' ] ) ) : htmlspecialchars( $collection[ 'attribute_list' ][ $attributeID ][ 'final_value' ] ) ) : '' );
                                                    }
                                                ?>
                                            </td>
                                        <?php } ?>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 text-center-xs text-left-sm">
                            <?php if( $this->checkPermission( 'collectionRemove' ) ) { ?>
                                <button class="btn btn-danger btn-sm rm-mr" type="submit" name="removeCollectionAction"><i class="fa fa-ban"></i> <?php echo $this->translateText( 'base.collectionList.button.removeSelected' ); ?></button>
                                <span class="visible-xs-block"></span>
                            <?php } ?>
                            <div class="dataTables_info">
                                <?php echo $this->translateText( 'base.paginator.info', array( '%first%' => $paginator->getCurrentPageFirstItem(), '%last%' => $paginator->getCurrentPageLastItem(), '%total%' => $paginator->getTotalItems() ) ); ?>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 text-center-xs text-right-sm">
                            <?php if( $paginator->getNumPages() > 1) { ?>
                                <?php echo $this->includeTemplate( 'parts/paginator.html.php', array( 'paginator' => $paginator ) ); ?>
                            <?php } ?>
                        </div>
                    </div>
                </form>
            <?php } else { ?>
                <div class="alert alert-warning rm-mr" role="alert">
                    <?php echo $this->translateText( 'base.collectionList.alert.noCollections' ); ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
