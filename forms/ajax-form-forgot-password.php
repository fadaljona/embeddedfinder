<?php
    require_once __DIR__ . '/forms-plus-framework/vendor/autoload.php';
    use FormsPlus\Framework\FormsPlusFramework as FormsPlusFramework;
    $form = new FormsPlusFramework( 'forms-plus-framework/app/config/forms/ajax-form-forgot-password.yml' );
    $formNewPassword = new FormsPlusFramework( 'forms-plus-framework/app/config/forms/form-new-password.yml' );
    if( $form->isSubmitted ) {
        header( 'Content-Type: application/json' );
        if( $form->isValid ) {
            $data = array(
                'block'   => 'successContentBlock',
                'content' => '<div class="alert alert-valid"><strong><i class="fa fa-check"></i> An email has been sent.</strong> This email contains a link you need to click so that we can confirm that the correct user is getting the new password.</div>'
            );
            $data[ 'content' ] .= FormsPlusFramework::outputDebug( false, '<div class="alert alert-error">%content%</div>' );
        } else {
            $content = '';
            foreach( $form->errorList as $attributeID => $itemList ) {
                $name = $form->configuration[ 'attribute_list' ][ $attributeID ][ 'name' ];
                $content .= '<div class="alert alert-error"><strong><i class="fa fa-times"></i> ' . $name . ':</strong> ';
                if( count( $itemList ) == 1 ) {
                    foreach( $itemList as $errorList ) $content .=  implode( ' ', $errorList );
                } else {
                    $content .= '<ul>';
                    foreach( $itemList as $errorList ) $content .= '<li>' . implode( ' ', $errorList ) . '</li>';
                    $content .= '</ul>';
                }
                $content .= '</div>';
            }
            $data = array( 'errorData' => array(
                'block'   => 'errorContentBlock',
                'content' => $content
            ));
            $data[ 'errorData' ][ 'content' ] .= FormsPlusFramework::outputDebug( false, '<div class="alert alert-error">%content%</div>' );
        }
        echo json_encode( $data );
        exit;
    }
    // set a new password for a user who got this recovery link via an email
    if( !empty( $_GET[ 'action' ] ) && $_GET[ 'action' ] === 'user_newpassword' && !empty( $_GET[ 'email' ] ) && !empty( $_GET[ 'key' ] ) ) {
        $parameterList = $form->configuration[ 'mail' ][ 'message' ][ 'forgotpassword' ][ 'template_parameter_list' ];
        $filter = array( 'prop_form_config_file' => $form->configFile );
        $filter[ 'attr_' . $parameterList[ 'email_attribute_id' ] ] = $_GET[ 'email' ];
        $filter[ 'attr_' . $parameterList[ 'hash_key_attribute_id' ] ] = $_GET[ 'key' ];
        $form->initStorage();
        $forgotPasswordCollectionSet = $form->storage->fetchCollectionList( null, 1, 0, null, null, $filter );
        if( $forgotPasswordCollectionSet[ 'count' ] === 1 ) {
            $forgotPasswordCollection = $forgotPasswordCollectionSet[ 'list' ][ '0' ];
            $actionUserNewPassword = array( 'form' => true );
            if( $formNewPassword->isValid ) {
                $newPasswordHash = $formNewPassword->data[ 'attribute_list' ][ 'new_password' ][ 'final_value' ];
                $formRegistration = FormsPlusFramework::initDummy( 'forms-plus-framework/app/config/forms/ajax-form-registration.yml' );
                $parameterList = $formRegistration->configuration[ 'mail' ][ 'message' ][ 'registration' ][ 'template_parameter_list' ];
                $filter = array( 'prop_form_config_file' => $formRegistration->configFile );
                $filter[ 'attr_' . $parameterList[ 'email_attribute_id' ] ] = $_GET[ 'email' ];
                $filter[ 'attr_' . $parameterList[ 'hash_key_attribute_id' ] ] = '';
                $formRegistration->initStorage();
                $userCollectionSet = $formRegistration->storage->fetchCollectionList( null, 1, 0, null, null, $filter );
                if( $userCollectionSet[ 'count' ] === 1 ) {
                    $userCollection = $userCollectionSet[ 'list' ][ '0' ];
                    $userCollection[ 'attribute_list' ][ $parameterList[ 'password_attribute_id' ] ][ 'final_value' ] = $newPasswordHash;
                    $formRegistration->storage->updateCollection( $userCollection );
                    $form->storage->removeCollection( $forgotPasswordCollection[ 'collection_id' ] );
                    $actionUserNewPassword[ 'form' ] = array( 'valid', 'Your password has been successfully changed.' );
                } elseif( $userCollectionSet[ 'count' ] === 0 ) {
                    $actionUserNewPassword[ 'form' ] = array( 'error', 'The password recovery is impossible.', 'A user with such email address does not exist.' );
                } elseif( $userCollectionSet[ 'count' ] > 1 ) {
                    $actionUserNewPassword[ 'form' ] = array( 'error', 'The password recovery is impossible.', 'A user with such email address does not exist.' );
                }
            }
        } elseif( $forgotPasswordCollectionSet[ 'count' ] === 0 ) {
            $actionUserNewPassword = array( 'error', 'The password recovery is impossible.', 'The submitted key is invalid.' );
        } elseif( $forgotPasswordCollectionSet[ 'count' ] > 1 ) {
            $actionUserNewPassword = array( 'error', 'The password recovery has failed.', 'An internal error occurred. Please, contact the website administrator.' );
        }
    }
    if( $formNewPassword->isSubmitted ) {
        header( 'Content-Type: application/json' );
        if( $formNewPassword->isValid ) {
            $data = array(
                'block'   => 'newpasswordSuccessOutputBlock',
                'content' => sprintf( '<div class="alert alert-%s"><strong><i class="fa fa-%s"></i> %s</strong> %s</div>', $actionUserNewPassword[ 'form' ][ 0 ], $actionUserNewPassword[ 'form' ][ 0 ] === 'error' ? 'times' : 'check', $actionUserNewPassword[ 'form' ][ 1 ], isset( $actionUserNewPassword[ 'form' ][ 2 ] ) ? $actionUserNewPassword[ 'form' ][ 2 ] : '' )
            );
            $data[ 'content' ] .= FormsPlusFramework::outputDebug( false, '<div class="alert alert-error">%content%</div>' );
        } else {
            $content = '';
            foreach( $formNewPassword->errorList as $attributeID => $itemList ) {
                $name = $formNewPassword->configuration[ 'attribute_list' ][ $attributeID ][ 'name' ];
                $content .= '<div class="alert alert-error"><strong><i class="fa fa-times"></i> ' . $name . ':</strong> ';
                if( count( $itemList ) == 1 ) {
                    foreach( $itemList as $errorList ) $content .=  implode( ' ', $errorList );
                } else {
                    $content .= '<ul>';
                    foreach( $itemList as $errorList ) $content .= '<li>' . implode( ' ', $errorList ) . '</li>';
                    $content .= '</ul>';
                }
                $content .= '</div>';
            }
            $data = array( 'errorData' => array(
                'block'   => 'newpasswordFailOutputBlock',
                'content' => $content
            ));
            $data[ 'errorData' ][ 'content' ] .= FormsPlusFramework::outputDebug( false, '<div class="alert alert-error">%content%</div>' );
        }
        echo json_encode( $data );
        exit;
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Forms Plus: PHP</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php FormsPlusFramework::loadStyles(); ?>
        <link rel="stylesheet" href="<?php echo FormsPlusFramework::getStaticVar( 'asset_directory_uri' ); ?>css/base.css" type="text/css">
    </head>
    <body>
        <div class="container">
            <form method="post" action="<?php echo isset( $actionUserNewPassword ) ? $_SERVER[ 'REQUEST_URI' ] : strtok( $_SERVER[ 'REQUEST_URI' ], '?' ); ?>" class="<?php $form->designCSSClasses(); ?>" data-js-validate="true" data-js-highlight-state-msg="true" data-js-show-valid-msg="true" data-js-ajax-form="" <?php if( isset( $actionUserNewPassword ) ) { ?>data-js-ajax-before-hide-block="newpasswordSuccessBlock;newpasswordFailBlock" data-js-ajax-fail-show-block="newpasswordFailBlock" data-js-ajax-success-hide-block="newpasswordFormBlock" data-js-ajax-success-show-block="newpasswordSuccessBlock" data-js-ajax-always-hide-block="newpasswordLoadingBlock" data-js-ajax-before-show-block="newpasswordLoadingBlock"<?php } else { ?>data-js-ajax-before-hide-block="successBlockName;failBlockName" data-js-ajax-before-show-block="loadingBlockName" data-js-ajax-success-show-block="successBlockName" data-js-ajax-success-hide-block="formBlockName" data-js-ajax-fail-show-block="failBlockName" data-js-ajax-always-hide-block="loadingBlockName"<?php } ?>>
                <div class="p-form p-shadowed p-form-sm">
                    <?php if( isset( $actionUserNewPassword ) ) { ?>
                        <div class="p-title text-left">
                            <span class="p-title-side">New password</span>
                        </div>
                        <?php if( !isset( $actionUserNewPassword[ 'form' ] ) ) { ?>
                            <div class="alert alert-<?php echo $actionUserNewPassword[ 0 ]; ?>">
                                <strong><i class="fa fa-<?php echo $actionUserNewPassword[ 0 ] === 'error' ? 'times' : 'check'; ?>"></i> <?php echo $actionUserNewPassword[ 1 ]; ?></strong>
                                <?php echo isset( $actionUserNewPassword[ 2 ] ) ? $actionUserNewPassword[ 2 ] : ''; ?>
                            </div>
                            <div class="text-right">
                                <a href="<?php echo strtok( $_SERVER[ 'REQUEST_URI' ], '?' ); ?>" class="btn">reload</a>
                            </div>
                        <?php } else { ?>
                            <div data-js-block="newpasswordSuccessBlock" class="collapse">
                                <h4>Form was sent successfully!</h4>
                                <div data-js-block="newpasswordSuccessOutputBlock" class="collapse"></div>
                                <div class="text-right">
                                    <a href="<?php echo strtok( $_SERVER[ 'REQUEST_URI' ], '?' ); ?>" class="btn">reload</a>
                                </div>
                            </div>
                            <div data-js-block="newpasswordFailBlock" class="collapse">
                                <h4>Failed to send form!</h4>
                                <div data-js-block="newpasswordFailOutputBlock" class="collapse"></div>
                            </div>
                            <div data-js-block="newpasswordFormBlock">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php $formNewPassword->attributeView( 'new_password' ); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php $formNewPassword->attributeView( 'confirm_new_password' ); ?>
                                    </div>
                                </div>
                                <div data-js-block="newpasswordLoadingBlock" class="progress collapse">
                                    <div class="progress-bar progress-bar-fp progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                                </div>
                                <div class="text-right">
                                    <button class="btn p-ajax-disabled" type="submit" name="confirm"><i class="fa fa-lock"></i>&nbsp;Update password</button>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        <div class="p-title text-left">
                            <span class="p-title-side">Forgot password?</span>
                        </div>
                        <div data-js-block="successBlockName" class="collapse">
                            <h4>Form was sent successfully!</h4>
                            <div data-js-block="successContentBlock" class="collapse"></div>
                            <div class="text-right">
                                <a href="<?php echo strtok( $_SERVER[ 'REQUEST_URI' ], '?' ); ?>" class="btn">reload</a>
                            </div>
                        </div>
                        <div data-js-block="failBlockName" class="collapse">
                            <h4>Failed to send form!</h4>
                            <div data-js-block="errorContentBlock" class="collapse"></div>
                        </div>
                        <div data-js-block="formBlockName">
                            <p>Enter your email and we'll send you your recovery details.</p>
                            <div class="row">
                                <div class="col-sm-6">
                                    <?php $form->attributeView( 'email' ); ?>
                                </div>
                                <div class="col-sm-6">
                                    <button class="btn p-ajax-disabled" type="submit" name="confirm"><i class="fa fa-envelope-o"></i>&nbsp;send email</button>
                                </div>
                            </div>
                            <div data-js-block="loadingBlockName" class="progress collapse">
                                <div class="progress-bar progress-bar-fp progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </form>
        </div>
        <?php FormsPlusFramework::loadScripts(); ?>
        <?php FormsPlusFramework::outputDebug(); ?>
    </body>
</html>