<?php
    require_once __DIR__ . '/forms-plus-framework/vendor/autoload.php';
    use FormsPlus\Framework\FormsPlusFramework as FormsPlusFramework;
    $form = new FormsPlusFramework( 'forms-plus-framework/app/config/demos/values-and-calculations.yml' );
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Forms Plus: PHP</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php FormsPlusFramework::loadStyles(); ?>
        <link rel="stylesheet" href="<?php echo FormsPlusFramework::getStaticVar( 'asset_directory_uri' ); ?>css/base.css" type="text/css">
    </head>
    <body>
        <div class="container">
            <form enctype="multipart/form-data" method="post" action="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>" class="<?php $form->designCSSClasses(); ?>" data-js-validate="true" data-js-highlight-state-msg="true" data-js-show-valid-msg="true">
                <div class="p-form p-shadowed p-form-sm">
                    <div class="p-title text-left">
                        <span class="p-title-side">Values and calculations&nbsp;&nbsp;<i class="fa fa-calculator"></i></span>
                    </div>
                    <?php if( $form->isValid ) { ?>
                        <div class="alert alert-valid"><strong><i class="fa fa-check"></i> Thank you:</strong> message example.</div>
                    <?php } elseif( $form->isSubmitted ) { ?>
                        <?php foreach( $form->errorList as $attributeID => $itemList ) { ?>
                            <div class="alert alert-error"><strong><i class="fa fa-times"></i> <?php echo $form->configuration[ 'attribute_list' ][ $attributeID ][ 'name' ]; ?>:</strong>
                                <?php if( count( $itemList ) == 1 ) { ?>
                                    <?php foreach( $itemList as $errorList ) echo implode( ' ', $errorList ); ?>
                                <?php } else { ?>
                                    <ul><?php foreach( $itemList as $errorList ) echo '<li>' . implode( ' ', $errorList ) . '</li>'; ?></ul>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <?php if( !$form->isValid ) { ?>
                        <div class="p-subtitle text-left">
                            <span class="p-title-side">Watch value</span>
                        </div>
                        <div class="alert alert-error collapse" data-js-watch-value="" data-js-just-calculations="" data-js-group-values="nameGroup:firstName*,lastName*;birthGroup:@date*;emailGroup:email*" data-js-empty-value-class="show">
                            <h4><i class="fa fa-times"></i> Error:</h4>
                            <ul>
                                <li class="collapse" data-js-group-values="nameGroup:firstName*,lastName*" data-js-watch-value="" data-js-just-calculations="" data-js-empty-value-class="show">Please input First name and Last Name</li>
                                <li class="collapse" data-js-group-values="birthGroup:@date*" data-js-watch-value="" data-js-just-calculations="" data-js-empty-value-class="show">Please input Date of birth</li>
                                <li class="collapse" data-js-group-values="emailGroup:email*" data-js-watch-value="" data-js-just-calculations="" data-js-empty-value-class="show">Please input Email</li>
                            </ul>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'first_name' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'last_name' ); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'date_of_birth' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'email' ); ?>
                            </div>
                        </div>
                        <h3 class="p-no-offs">Hello
                            <span data-js-group-values="nameGroup:firstName,lastName" data-js-watch-value="" data-js-value-format="{firstName} {lastName}" data-js-empty-value="there"></span> from
                            <span data-js-group-values="birthGroup:date" data-js-watch-value="" data-js-value-format="{@date:@MMMM YYYY}" data-js-empty-value="2015"></span>.</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="p-field-value">
                                    <span class="p-value-label">First name:</span>
                                    <span class="p-value-text p-colored-text" data-js-value-format="{firstName}" data-js-group-values="nameGroup:firstName" data-js-watch-value="true" data-js-empty-value="-">-</span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="p-field-value">
                                    <span class="p-value-label">Last name:</span>
                                    <span class="p-value-text p-colored-text" data-js-value-format="{lastName}" data-js-group-values="nameGroup:lastName" data-js-watch-value="true" data-js-empty-value="-">-</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="p-field-value">
                                    <span class="p-value-label">Date of birth:</span>
                                    <span class="p-value-text p-colored-text" data-js-value-format="{@date:@DD.MM.YY}" data-js-group-values="birthGroup:@date" data-js-watch-value="true" data-js-empty-value="-">-</span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="p-field-value">
                                    <span class="p-value-label">Email:</span>
                                    <span class="p-value-text p-colored-text" data-js-value-format="{email}" data-js-group-values="emailGroup:email" data-js-watch-value="true" data-js-empty-value="-">-</span>
                                </div>
                            </div>
                        </div>
                        <div class="p-subtitle text-left">
                            <span class="p-title-side">Calculations</span>
                        </div>
                        <script data-js-template="price1ExtraTemplate" type="text/x-tmpl">
                            <div class="row" data-js-toggle-group="price1ExtraGroup">
                                <div class="col-sm-4">
                                    <?php $form->attributeView( 'price_1_extra' ); ?>
                                </div>
                            </div>
                        </script>
                        <div class="panel panel-fp">
                            <div class="panel-body">
                                <p>Rounds results of calculations to &lt;x&gt; digits, for this example 2, by default to integer, with
                                    <a href="http://www.w3schools.com/jsref/jsref_tofixed.asp" target="_blank">toFixed</a>:</p>
                                <ul>
                                    <li>19 - 19.00</li>
                                    <li>19.996 - 20.00</li>
                                    <li>19.992 - 19.99</li>
                                </ul>
                            </div>
                        </div>
                        <div class="p-block">
                            <div class="row">
                                <div class="col-sm-4">
                                    <?php $form->attributeView( 'currency' ); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <?php $form->attributeView( 'price_1' ); ?>
                            </div>
                            <div class="col-sm-5">
                                <?php $form->attributeView( 'quantity_1' ); ?>
                            </div>
                            <div class="col-sm-3">
                                <label>Subtotal:</label>
                                <span class="form-control" data-js-watch-value="" data-js-empty-value="-" data-js-group-values="priceGroup1:#quantity1,#price1;currencyGroup:#convertion,currency" data-js-value-format="{#2#*:quantity1*price1*convertion}{currency}"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <?php $form->attributeView( 'price_2' ); ?>
                            </div>
                            <div class="col-sm-5">
                                <?php $form->attributeView( 'quantity_2' ); ?>
                            </div>
                            <div class="col-sm-3">
                                <label>Subtotal:</label>
                                <span class="form-control" data-js-watch-value="" data-js-empty-value="-" data-js-group-values="priceGroup2:#quantity2,#price2;currencyGroup:#convertion,currency" data-js-value-format="{#2#*:quantity2*price2*convertion}{currency}"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'additional_1' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'additional_2' ); ?>
                            </div>
                        </div>
                        <?php $form->attributeView( 'additional_3' ); ?>
                        <div class="row">
                            <div class="col-sm-4">
                                <?php $form->attributeView( 'additional_3_type_1' ); ?>
                            </div>
                            <div class="col-sm-4">
                                <?php $form->attributeView( 'additional_3_type_2' ); ?>
                            </div>
                            <div class="col-sm-4">
                                <?php $form->attributeView( 'additional_3_type_3' ); ?>
                            </div>
                        </div>
                        <hr class="p-no-offs" />
                        <div class="p-total-block">
                            <div class="p-action-column pull-right hidden-xs"></div>
                            <div class="pull-right">
                                <div class="pull-right">
                                    <div class="pull-left text-right">Product 1 total:</div>
                                    <div class="p-price-column" data-js-watch-value="" data-js-empty-value="-" data-js-group-values="priceGroup1:#quantity1,#price1;currencyGroup:#convertion,currency" data-js-value-format="{#2#*:quantity1*price1*convertion}{currency}"></div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="pull-right">
                                    <div class="pull-left text-right">Product 2 total:</div>
                                    <div class="p-price-column" data-js-watch-value="" data-js-empty-value="-" data-js-group-values="priceGroup2:#quantity2,#price2;currencyGroup:#convertion,currency" data-js-value-format="{#2#*:quantity2*price2*convertion}{currency}"></div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="pull-right">
                                    <div class="pull-left text-right">Additional 1 total:</div>
                                    <div class="p-price-column" data-js-watch-value="" data-js-empty-value="-" data-js-group-values="adGroup1:#ad1Price;currencyGroup:#convertion,currency" data-js-value-format="{#2#*:ad1Price*convertion}{currency}"></div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="pull-right">
                                    <div class="pull-left text-right">Additional 2 total:</div>
                                    <div class="p-price-column" data-js-watch-value="" data-js-empty-value="-" data-js-group-values="adGroup2:#ad2Price;currencyGroup:#convertion,currency" data-js-value-format="{#2#*:ad2Price*convertion}{currency}"></div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="pull-right">
                                    <div class="pull-left text-right">Additional 3 total:</div>
                                    <div class="p-price-column" data-js-watch-value="" data-js-empty-value="-" data-js-group-values="adGroup3:#ad3Price;currencyGroup:#convertion,currency" data-js-value-format="{#2#*:ad3Price*convertion}{currency}"></div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="pull-right">
                                    <div class="pull-left text-right">Subtotal:</div>
                                    <div class="p-price-column" data-js-watch-value="" data-js-empty-value="-" data-js-value-group="subtotalValue" data-js-group-values="priceGroup1:#quantity1,#price1;priceGroup2:#quantity2,price2;adGroup1:#ad1Price;adGroup2:#ad2Price;adGroup3:#ad3Price;currencyGroup:#convertion,currency" data-js-value-format="{#2#subtotal*:(quantity1*price1 + quantity2*price2 + ad1Price + ad2Price + ad3Price)*convertion}{currency}"></div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="pull-right">
                                    <div class="pull-left text-right">Tax:</div>
                                    <div class="p-price-column" data-js-watch-value="" data-js-empty-value="-" data-js-value-group="taxValue" data-js-group-values="subtotalValue:#subtotal,currency" data-js-value-format="{#2#tax*:subtotal*0.2}{currency}"></div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="pull-right p-colored-text">
                                    <div class="pull-left text-right"><i class="fa fa-shopping-cart"></i> Total:</div>
                                    <div class="p-price-column" data-js-watch-value="" data-js-empty-value="-" data-js-group-values="subtotalValue:#subtotal,currency;taxValue:#tax" data-js-value-format="{#2#total*:subtotal+tax}{currency}"></div>
                                </div>
                            </div>
                        </div>
                        <div class="p-subtitle text-left">
                            <span class="p-title-side">Cart</span>
                        </div>
                        <div data-js-watch-value="" data-js-just-calculations="" data-js-group-values="cartItems:#itemsPrice*" data-js-empty-value-class="hide">
                            <table class="table table-striped p-table">
                                <thead>
                                    <tr>
                                        <td>Product name</td>
                                        <td class="text-center">Quantity</td>
                                        <td class="text-center">Price</td>
                                        <td class="p-price-column hidden-xs">Total price</td>
                                        <td class="p-action-column">&nbsp;</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="p-block">
                                        <td>Product name 1</td>
                                        <td class="text-center">2</td>
                                        <td class="p-price-column">100$</td>
                                        <td class="p-price-column hidden-xs">200$</td>
                                        <td class="p-action-column">
                                            <input type="hidden" name="product[0][id]" value="1" />
                                            <input type="hidden" name="product[0][quantity]" value="2" data-js-value-group="cartItems" data-js-value-format="{#quantity*}" data-js-get-value-extra="{#price:100}{#itemsPrice:price*quantity}" />
                                            <a href="#" class="p-action-link" data-js-remove-block=""><i class="fa fa-times"></i></a>
                                        </td>
                                    </tr>
                                    <tr class="p-block">
                                        <td>Product name 2</td>
                                        <td class="text-center">1</td>
                                        <td class="p-price-column">500$</td>
                                        <td class="p-price-column hidden-xs">500$</td>
                                        <td class="p-action-column">
                                            <input type="hidden" name="product[1][id]" value="2" />
                                            <input type="hidden" name="product[1][quantity]" value="1" data-js-value-group="cartItems" data-js-value-format="{#quantity*}" data-js-get-value-extra="{#price:500}{#itemsPrice:price*quantity}" />
                                            <a href="#" class="p-action-link" data-js-remove-block=""><i class="fa fa-times"></i></a>
                                        </td>
                                    </tr>
                                    <tr class="p-block">
                                        <td>Product name 3</td>
                                        <td class="text-center">3</td>
                                        <td class="p-price-column">120$</td>
                                        <td class="p-price-column hidden-xs">360$</td>
                                        <td class="p-action-column">
                                            <input type="hidden" name="product[2][id]" value="3" />
                                            <input type="hidden" name="product[2][quantity]" value="3" data-js-value-group="cartItems" data-js-value-format="{#quantity*}" data-js-get-value-extra="{#price:120}{#itemsPrice:price*quantity}" />
                                            <a href="#" class="p-action-link" data-js-remove-block=""><i class="fa fa-times"></i></a>
                                        </td>
                                    </tr>
                                    <tr class="p-block">
                                        <td>Product name 4</td>
                                        <td class="text-center">2</td>
                                        <td class="p-price-column">180$</td>
                                        <td class="p-price-column hidden-xs">360$</td>
                                        <td class="p-action-column">
                                            <input type="hidden" name="product[3][id]" value="4" />
                                            <input type="hidden" name="product[3][quantity]" value="2" data-js-value-group="cartItems" data-js-value-format="{#quantity*}" data-js-get-value-extra="{#price:180}{#itemsPrice:price*quantity}" />
                                            <a href="#" class="p-action-link" data-js-remove-block=""><i class="fa fa-times"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <hr />
                            <div class="p-total-block">
                                <div class="p-action-column pull-right hidden-xs"></div>
                                <div class="pull-right">
                                    <div class="clearfix"></div>
                                    <div class="pull-right">
                                        <div class="pull-left text-right">Product price:</div>
                                        <div class="p-price-column" data-js-watch-value="" data-js-empty-value="-" data-js-value-group="cartProductPrice" data-js-group-values="cartItems:#itemsPrice*" data-js-value-format="{#2#itemsPrice*}{currency:'$'}"></div>
                                        <input type="hidden" name="price[product]" data-js-watch-value="" data-js-empty-value="-" data-js-value-group="cartProductPrice" data-js-group-values="cartItems:#itemsPrice*" data-js-value-format="{#2#itemsPrice*}{currency:'$'}"/>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="pull-right">
                                        <div class="pull-left text-right">Tax 20%:</div>
                                        <div class="p-price-column" data-js-watch-value="" data-js-empty-value="-" data-js-value-group="cartTax" data-js-group-values="cartProductPrice:#itemsPrice,currency" data-js-value-format="{#2#tax*:itemsPrice*0.2}{currency}"></div>
                                        <input type="hidden" name="price[tax]" data-js-watch-value="" data-js-empty-value="-" data-js-value-group="cartTax" data-js-group-values="cartProductPrice:#itemsPrice,currency" data-js-value-format="{#2#tax*:itemsPrice*0.2}{currency}"/>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="pull-right">
                                        <div class="pull-left text-right">Delivery price:</div>
                                        <div class="p-price-column">10$</div>
                                        <input type="hidden" name="price[delivery]" value="10$"/>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="pull-right p-colored-text">
                                        <div class="pull-left text-right"><i class="fa fa-shopping-cart"></i> Subtotal:</div>
                                        <div class="p-price-column" data-js-watch-value="" data-js-empty-value="-" data-js-group-values="cartTax:#tax*,currency;cartProductPrice:#itemsPrice*" data-js-value-format="{#2#*:itemsPrice+tax+10}{currency}"></div>
                                        <input type="hidden" name="price[subtotal]" data-js-watch-value="" data-js-empty-value="-" data-js-group-values="cartTax:#tax*,currency;cartProductPrice:#itemsPrice*" data-js-value-format="{#2#*:itemsPrice+tax+10}{currency}"/>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="text-right">
                                <button class="btn" type="submit" name="confirm"><i class="fa fa-check-square-o"></i>&nbsp;confirm</button>
                            </div>
                        </div>
                        <div class="collapse" data-js-watch-value="" data-js-just-calculations="" data-js-group-values="cartItems:#itemsPrice*" data-js-empty-value-class="show">
                            <h3>Cart is empty.</h3>
                            <div class="text-right">
                                <a href="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>" class="btn">reload</a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </form>
        </div>
        <?php FormsPlusFramework::loadScripts(); ?>
        <?php FormsPlusFramework::outputDebug(); ?>
    </body>
</html>