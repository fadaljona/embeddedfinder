<?php
    require_once __DIR__ . '/forms-plus-framework/vendor/autoload.php';
    use FormsPlus\Framework\FormsPlusFramework as FormsPlusFramework;
    $formLogin = new FormsPlusFramework( 'forms-plus-framework/app/config/forms/form-t-user-login.yml' );
    $formRegistration = new FormsPlusFramework( 'forms-plus-framework/app/config/forms/form-t-user-registration.yml' );
    $formForgotPassword = new FormsPlusFramework( 'forms-plus-framework/app/config/forms/form-t-user-forgot-password.yml' );
    $formNewPassword = new FormsPlusFramework( 'forms-plus-framework/app/config/forms/form-new-password.yml' );
    // define an active tab
    $activeTab = 1;
    if( $formLogin->isSubmitted ) $activeTab = 1;
    if( $formRegistration->isSubmitted ) $activeTab = 2;
    if( $formForgotPassword->isSubmitted ) $activeTab = 3;
    // activate a user that got this activation link via an email
    if( !empty( $_GET[ 'action' ] ) && $_GET[ 'action' ] === 'user_activation' && !empty( $_GET[ 'email' ] ) && !empty( $_GET[ 'key' ] ) ) {
        $parameterList = $formRegistration->configuration[ 'mail' ][ 'message' ][ 'registration' ][ 'template_parameter_list' ];
        $filter = array( 'prop_form_config_file' => $formRegistration->configFile );
        $filter[ 'attr_' . $parameterList[ 'email_attribute_id' ] ] = $_GET[ 'email' ];
        $filter[ 'attr_' . $parameterList[ 'hash_key_attribute_id' ] ] = $_GET[ 'key' ];
        $formRegistration->initStorage();
        $collectionSet = $formRegistration->storage->fetchCollectionList( null, 1, 0, null, null, $filter );
        if( $collectionSet[ 'count' ] === 1 ) {
            $collection = $collectionSet[ 'list' ][ '0' ];
            $collection[ 'attribute_list' ][ $parameterList[ 'hash_key_attribute_id' ] ][ 'final_value' ] = '';
            $formRegistration->storage->updateCollection( $collection );
            $actionUserActivation = array( 'valid', 'Your account is now activated.' );
        } elseif( $collectionSet[ 'count' ] === 0 ) {
            $actionUserActivation = array( 'error', 'Your account hasn\'t been activated.', 'The submitted key is invalid.' );
        } elseif( $collectionSet[ 'count' ] > 1 ) {
            $actionUserActivation = array( 'error', 'The account activation has failed.', 'An internal error occurred. Please, contact the website administrator.' );
        }
    }
    if( isset( $actionUserActivation ) ) {
        $activeTab = 2;
    }
    // set a new password for a user who got this recovery link via an email
    if( !empty( $_GET[ 'action' ] ) && $_GET[ 'action' ] === 'user_newpassword' && !empty( $_GET[ 'email' ] ) && !empty( $_GET[ 'key' ] ) ) {
        $parameterList = $formForgotPassword->configuration[ 'mail' ][ 'message' ][ 'forgotpassword' ][ 'template_parameter_list' ];
        $filter = array( 'prop_form_config_file' => $formForgotPassword->configFile );
        $filter[ 'attr_' . $parameterList[ 'email_attribute_id' ] ] = $_GET[ 'email' ];
        $filter[ 'attr_' . $parameterList[ 'hash_key_attribute_id' ] ] = $_GET[ 'key' ];
        $formForgotPassword->initStorage();
        $forgotPasswordCollectionSet = $formForgotPassword->storage->fetchCollectionList( null, 1, 0, null, null, $filter );
        if( $forgotPasswordCollectionSet[ 'count' ] === 1 ) {
            $forgotPasswordCollection = $forgotPasswordCollectionSet[ 'list' ][ '0' ];
            $actionUserNewPassword = array( 'form' => true );
            if( $formNewPassword->isValid ) {
                $newPasswordHash = $formNewPassword->data[ 'attribute_list' ][ 'new_password' ][ 'final_value' ];
                $parameterList = $formRegistration->configuration[ 'mail' ][ 'message' ][ 'registration' ][ 'template_parameter_list' ];
                $filter = array( 'prop_form_config_file' => $formRegistration->configFile );
                $filter[ 'attr_' . $parameterList[ 'email_attribute_id' ] ] = $_GET[ 'email' ];
                $filter[ 'attr_' . $parameterList[ 'hash_key_attribute_id' ] ] = '';
                $formRegistration->initStorage();
                $userCollectionSet = $formRegistration->storage->fetchCollectionList( null, 1, 0, null, null, $filter );
                if( $userCollectionSet[ 'count' ] === 1 ) {
                    $userCollection = $userCollectionSet[ 'list' ][ '0' ];
                    $userCollection[ 'attribute_list' ][ $parameterList[ 'password_attribute_id' ] ][ 'final_value' ] = $newPasswordHash;
                    $formRegistration->storage->updateCollection( $userCollection );
                    $formForgotPassword->storage->removeCollection( $forgotPasswordCollection[ 'collection_id' ] );
                    $actionUserNewPassword[ 'form' ] = array( 'valid', 'Your password has been successfully changed.' );
                } elseif( $userCollectionSet[ 'count' ] === 0 ) {
                    $actionUserNewPassword[ 'form' ] = array( 'error', 'The password recovery is impossible.', 'A user with such email address does not exist.' );
                } elseif( $userCollectionSet[ 'count' ] > 1 ) {
                    $actionUserNewPassword[ 'form' ] = array( 'error', 'The password recovery is impossible.', 'A user with such email address does not exist.' );
                }
            }
        } elseif( $forgotPasswordCollectionSet[ 'count' ] === 0 ) {
            $actionUserNewPassword = array( 'error', 'The password recovery is impossible.', 'The submitted key is invalid.' );
        } elseif( $forgotPasswordCollectionSet[ 'count' ] > 1 ) {
            $actionUserNewPassword = array( 'error', 'The password recovery has failed.', 'An internal error occurred. Please, contact the website administrator.' );
        }
    }
    if( isset( $actionUserNewPassword ) ) {
        $activeTab = 3;
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Forms Plus: PHP</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php FormsPlusFramework::loadStyles(); ?>
        <link rel="stylesheet" href="<?php echo FormsPlusFramework::getStaticVar( 'asset_directory_uri' ); ?>css/base.css" type="text/css">
    </head>
    <body>
        <div class="container">
            <div class="<?php $formLogin->designCSSClasses(); ?>">
                <div class="p-form p-shadowed p-form-sm">
                    <input type="radio" id="tab-1" name="activeTab" class="p-tab-sel"<?php echo ( $activeTab === 1 ) ? ' checked' : ''; ?>/>
                    <input type="radio" id="tab-2" name="activeTab" class="p-tab-sel"<?php echo ( $activeTab === 2 ) ? ' checked' : ''; ?>/>
                    <input type="radio" id="tab-3" name="activeTab" class="p-tab-sel"<?php echo ( $activeTab === 3 ) ? ' checked' : ''; ?>/>
                    <ul class="nav nav-tabs">
                        <li><label for="tab-1"><i class="fa fa-sign-in"></i></label></li>
                        <li><label for="tab-2"><i class="fa fa-user-plus"></i></label></li>
                        <li><label for="tab-3"><i class="fa fa-question-circle"></i></label></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane">
                            <form method="post" action="<?php echo strtok( $_SERVER[ 'REQUEST_URI' ], '?' ); ?>" data-js-validate="true" data-js-highlight-state-msg="true" data-js-show-valid-msg="true">
                                <div class="p-title text-left">
                                    <span class="p-title-side">User login</span>
                                </div>
                                <?php if( $formLogin->isValid ) { ?>
                                    <div class="alert alert-valid"><strong><i class="fa fa-check"></i> You have successfully logged in.</strong> It does nothing for the moment. The code has to be complemented with the needed functionality.</div>
                                    <div class="text-right">
                                        <a href="<?php echo strtok( $_SERVER[ 'REQUEST_URI' ], '?' ); ?>" class="btn">reload</a>
                                    </div>
                                <?php } elseif( $formLogin->isSubmitted ) { ?>
                                    <?php foreach( $formLogin->errorList as $attributeID => $itemList ) { ?>
                                        <div class="alert alert-error"><strong><i class="fa fa-times"></i> <?php echo $formLogin->configuration[ 'attribute_list' ][ $attributeID ][ 'name' ]; ?>:</strong>
                                            <?php if( count( $itemList ) == 1 ) { ?>
                                                <?php foreach( $itemList as $errorList ) echo implode( ' ', $errorList ); ?>
                                            <?php } else { ?>
                                                <ul><?php foreach( $itemList as $errorList ) echo '<li>' . implode( ' ', $errorList ) . '</li>'; ?></ul>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                                <?php if( !$formLogin->isValid ) { ?>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="p-subtitle p-no-offs text-left">
                                                <span class="p-title-side">sign in with</span>
                                            </div>
                                            <div class="p-social-bar">
                                                <a href="#" class="btn p-social-btn"><i class="fa fa-facebook"></i></a>
                                                <a href="#" class="btn p-social-btn"><i class="fa fa-twitter"></i></a>
                                                <a href="#" class="btn p-social-btn"><i class="fa fa-google-plus"></i></a>
                                                <a href="#" class="btn p-social-btn"><i class="fa fa-linkedin"></i></a>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="p-subtitle p-no-offs text-left">
                                                <span class="p-title-side">or login</span>
                                            </div>
                                            <?php $formLogin->attributeView( 'log_login' ); ?>
                                            <?php $formLogin->attributeView( 'log_password' ); ?>
                                            <label for="tab-3" class="p-colored-link">Forgot password?</label>
                                            <div class="clearfix"></div>
                                            <?php $formLogin->attributeView( 'log_keep_logged_in' ); ?>
                                        </div>
                                    </div>
                                    <div class="text-right">
                                        <button class="btn" type="submit" name="confirm"><i class="fa fa-sign-in"></i>&nbsp;login</button>
                                    </div>
                                <?php } ?>
                            </form>
                        </div>
                        <div class="tab-pane">
                            <form method="post" action="<?php echo strtok( $_SERVER[ 'REQUEST_URI' ], '?' ); ?>" data-js-validate="true" data-js-highlight-state-msg="true" data-js-show-valid-msg="true">
                                <div class="p-title text-left">
                                    <span class="p-title-side">Registration</span>
                                </div>
                                <?php if( isset( $actionUserActivation ) ) { ?>
                                    <div class="alert alert-<?php echo $actionUserActivation[ 0 ]; ?>">
                                        <strong><i class="fa fa-<?php echo $actionUserActivation[ 0 ] === 'error' ? 'times' : 'check'; ?>"></i> <?php echo $actionUserActivation[ 1 ]; ?></strong>
                                        <?php echo isset( $actionUserActivation[ 2 ] ) ? $actionUserActivation[ 2 ] : ''; ?>
                                    </div>
                                    <div class="text-right">
                                        <a href="<?php echo strtok( $_SERVER[ 'REQUEST_URI' ], '?' ); ?>" class="btn">reload</a>
                                    </div>
                                <?php } else { ?>
                                    <?php if( $formRegistration->isValid ) { ?>
                                        <div class="alert alert-valid"><strong><i class="fa fa-check"></i> Your account was successfully created.</strong> An email will be sent to the specified email address. Follow the instructions in that email to activate your account.</div>
                                        <div class="text-right">
                                            <a href="<?php echo strtok( $_SERVER[ 'REQUEST_URI' ], '?' ); ?>" class="btn">reload</a>
                                        </div>
                                    <?php } elseif( $formRegistration->isSubmitted ) { ?>
                                        <?php foreach( $formRegistration->errorList as $attributeID => $itemList ) { ?>
                                            <div class="alert alert-error"><strong><i class="fa fa-times"></i> <?php echo $formRegistration->configuration[ 'attribute_list' ][ $attributeID ][ 'name' ]; ?>:</strong>
                                                <?php if( count( $itemList ) == 1 ) { ?>
                                                    <?php foreach( $itemList as $errorList ) echo implode( ' ', $errorList ); ?>
                                                <?php } else { ?>
                                                    <ul><?php foreach( $itemList as $errorList ) echo '<li>' . implode( ' ', $errorList ) . '</li>'; ?></ul>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                    <?php if( !$formRegistration->isValid ) { ?>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="p-subtitle p-no-offs text-left">
                                                    <span class="p-title-side">account details</span>
                                                </div>
                                                <?php $formRegistration->attributeView( 'reg_login' ); ?>
                                                <?php $formRegistration->attributeView( 'reg_email' ); ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="p-subtitle p-no-offs text-left">
                                                    <span class="p-title-side">Your details</span>
                                                </div>
                                                <?php $formRegistration->attributeView( 'reg_first_name' ); ?>
                                                <?php $formRegistration->attributeView( 'reg_last_name' ); ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <?php $formRegistration->attributeView( 'reg_password' ); ?>
                                            </div>
                                            <div class="col-md-6">
                                                <?php $formRegistration->attributeView( 'reg_confirm_password' ); ?>
                                            </div>
                                        </div>
                                        <?php $formRegistration->attributeView( 'reg_gender' ); ?>
                                        <?php $formRegistration->attributeView( 'reg_captcha' ); ?>
                                        <div class="clearfix"></div>
                                        <?php $formRegistration->attributeView( 'reg_agree' ); ?>
                                        <div class="text-right">
                                            <button class="btn" type="submit" name="confirm"><i class="fa fa-check-square-o"></i>&nbsp;register</button>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </form>
                        </div>
                        <div class="tab-pane">
                            <?php if( isset( $actionUserNewPassword ) ) { ?>
                                <form method="post" action="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>" data-js-validate="true" data-js-highlight-state-msg="true" data-js-show-valid-msg="true">
                                    <div class="p-title text-left">
                                        <span class="p-title-side">New password</span>
                                    </div>
                                    <?php if( !isset( $actionUserNewPassword[ 'form' ] ) ) { ?>
                                        <div class="alert alert-<?php echo $actionUserNewPassword[ 0 ]; ?>">
                                                <strong><i class="fa fa-<?php echo $actionUserNewPassword[ 0 ] === 'error' ? 'times' : 'check'; ?>"></i> <?php echo $actionUserNewPassword[ 1 ]; ?></strong>
                                                <?php echo isset( $actionUserNewPassword[ 2 ] ) ? $actionUserNewPassword[ 2 ] : ''; ?>
                                            </div>
                                            <div class="text-right">
                                                <a href="<?php echo strtok( $_SERVER[ 'REQUEST_URI' ], '?' ); ?>" class="btn">reload</a>
                                            </div>
                                    <?php } else { ?>
                                        <?php if( is_array( $actionUserNewPassword[ 'form' ] ) ) { ?>
                                            <div class="alert alert-<?php echo $actionUserNewPassword[ 'form' ][ 0 ]; ?>">
                                                <strong><i class="fa fa-<?php echo $actionUserNewPassword[ 'form' ][ 0 ] === 'error' ? 'times' : 'check'; ?>"></i> <?php echo $actionUserNewPassword[ 'form' ][ 1 ]; ?></strong>
                                                <?php echo isset( $actionUserNewPassword[ 'form' ][ 2 ] ) ? $actionUserNewPassword[ 'form' ][ 2 ] : ''; ?>
                                            </div>
                                            <div class="text-right">
                                                <a href="<?php echo strtok( $_SERVER[ 'REQUEST_URI' ], '?' ); ?>" class="btn">reload</a>
                                            </div>
                                        <?php } else { ?>
                                            <?php if( !$formNewPassword->isValid && $formNewPassword->isSubmitted ) { ?>
                                                <?php foreach( $formNewPassword->errorList as $attributeID => $itemList ) { ?>
                                                    <div class="alert alert-error"><strong><i class="fa fa-times"></i> <?php echo $formNewPassword->configuration[ 'attribute_list' ][ $attributeID ][ 'name' ]; ?>:</strong>
                                                        <?php if( count( $itemList ) == 1 ) { ?>
                                                            <?php foreach( $itemList as $errorList ) echo implode( ' ', $errorList ); ?>
                                                        <?php } else { ?>
                                                            <ul><?php foreach( $itemList as $errorList ) echo '<li>' . implode( ' ', $errorList ) . '</li>'; ?></ul>
                                                        <?php } ?>
                                                    </div>
                                                <?php } ?>
                                            <?php } ?>
                                            <?php if( !$formNewPassword->isValid ) { ?>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <?php $formNewPassword->attributeView( 'new_password' ); ?>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <?php $formNewPassword->attributeView( 'confirm_new_password' ); ?>
                                                    </div>
                                                </div>
                                                <div class="text-right">
                                                    <button class="btn" type="submit" name="confirm"><i class="fa fa-lock"></i>&nbsp;Update password</button>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </form>
                            <?php } else { ?>
                                <form method="post" action="<?php echo strtok( $_SERVER[ 'REQUEST_URI' ], '?' ); ?>" data-js-validate="true" data-js-highlight-state-msg="true" data-js-show-valid-msg="true">
                                    <div class="p-title text-left">
                                        <span class="p-title-side">Forgot password?</span>
                                    </div>
                                    <?php if( $formForgotPassword->isValid ) { ?>
                                        <div class="alert alert-valid"><strong><i class="fa fa-check"></i> An email has been sent.</strong> This email contains a link you need to click so that we can confirm that the correct user is getting the new password.</div>
                                        <div class="text-right">
                                            <a href="<?php echo strtok( $_SERVER[ 'REQUEST_URI' ], '?' ); ?>" class="btn">reload</a>
                                        </div>
                                    <?php } elseif( $formForgotPassword->isSubmitted ) { ?>
                                        <?php foreach( $formForgotPassword->errorList as $attributeID => $itemList ) { ?>
                                            <div class="alert alert-error"><strong><i class="fa fa-times"></i> <?php echo $formForgotPassword->configuration[ 'attribute_list' ][ $attributeID ][ 'name' ]; ?>:</strong>
                                                <?php if( count( $itemList ) == 1 ) { ?>
                                                    <?php foreach( $itemList as $errorList ) echo implode( ' ', $errorList ); ?>
                                                <?php } else { ?>
                                                    <ul><?php foreach( $itemList as $errorList ) echo '<li>' . implode( ' ', $errorList ) . '</li>'; ?></ul>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                    <?php if( !$formForgotPassword->isValid ) { ?>
                                        <p>Enter your email and we'll send you your recovery details.</p>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <?php $formForgotPassword->attributeView( 'fp_email' ); ?>
                                            </div>
                                            <div class="col-sm-6">
                                                <button class="btn" type="submit" name="confirm"><i class="fa fa-envelope-o"></i>&nbsp;send email</button>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </form>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php FormsPlusFramework::loadScripts(); ?>
        <?php FormsPlusFramework::outputDebug(); ?>
    </body>
</html>