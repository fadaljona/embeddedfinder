<?php
    require_once __DIR__ . '/forms-plus-framework/vendor/autoload.php';
    use FormsPlus\Framework\FormsPlusFramework as FormsPlusFramework;
    $form = new FormsPlusFramework( 'forms-plus-framework/app/config/demos/sub-selection.yml' );
    $requestUriDir = str_replace( '//', '/', dirname( strtok( $_SERVER[ 'REQUEST_URI' ], '?' ) ) . '/' );
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Forms Plus: PHP</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php FormsPlusFramework::loadStyles(); ?>
        <link rel="stylesheet" href="<?php echo FormsPlusFramework::getStaticVar( 'asset_directory_uri' ); ?>css/base.css" type="text/css">
    </head>
    <body>
        <div class="container">
            <form method="post" action="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>" class="<?php $form->designCSSClasses(); ?>" data-js-validate="true" data-js-highlight-state-msg="true" data-js-show-valid-msg="true">
                <div class="p-form p-shadowed p-form-md">
                    <div class="p-title text-left">
                        <span class="p-title-side">Sub selection&nbsp;&nbsp;<i class="fa fa-list"></i></span>
                    </div>
                    <div class="p-subtitle">Simple</div>
                    <div class="row">
                        <div class="col-sm-4">
                            <?php $form->attributeView( 'device' ); ?>
                        </div>
                        <div class="col-sm-4">
                            <?php $form->attributeView( 'device_model' ); ?>
                        </div>
                        <div class="col-sm-4">
                            <?php $form->attributeView( 'device_color' ); ?>
                        </div>
                    </div>
                    <hr/>
                    <div id="car-model-wrap">
                        <?php $form->attributeView( 'car_model' ); ?>
                    </div>
                    <div data-js-sub-selection="#car-model-wrap [data-js-option-keys]">
                        <?php $form->attributeView( 'car_color' ); ?>
                    </div>
                    <div class="p-subtitle">AJAX</div>
                    <div class="row">
                        <div class="col-sm-4">
                            <?php $form->attributeView( 'clothing_type' ); ?>
                        </div>
                        <div class="col-sm-4">
                            <?php $form->attributeView( 'clothing_size', false, array( 'js_sub_selection_url' => $requestUriDir . 'ajax-demo-data/sub-selection-clothing-size.php' ) ); ?>
                        </div>
                        <div class="col-sm-4">
                            <?php $form->attributeView( 'clothing_color', false, array( 'js_sub_selection_url' => $requestUriDir . 'ajax-demo-data/sub-selection-clothing-color.php' ) ); ?>
                        </div>
                    </div>
                    <hr>
                    <div id="car-model-ajax-wrap">
                        <?php $form->attributeView( 'car_model_ajax' ); ?>
                    </div>
                    <div class="form-group" data-js-sub-selection="#car-model-ajax-wrap [data-js-option-keys]" data-js-sub-selection-url="<?php echo $requestUriDir; ?>ajax-demo-data/sub-selection-car-color.php" data-name="car_color_ajax">
                        <label for="car-color-ajax">Car color</label>
                        <div class="p-sub-option" data-js-option-default>Please select car.</div>
                    </div>
                </div>
            </form>
        </div>
        <?php FormsPlusFramework::loadScripts(); ?>
        <?php FormsPlusFramework::outputDebug(); ?>
    </body>
</html>