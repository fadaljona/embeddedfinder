jQuery(function(){
    jQuery.each( jQuery( '[data-check-all-toggle]' ), function( index, item ) {
        var itemEl = jQuery( item );
        var relatedCheckboxEls = jQuery( '[data-check-all-group="' + itemEl.attr( 'data-check-all-toggle' ) + '"]' );
        itemEl.change(function() {
            relatedCheckboxEls.prop( 'checked', this.checked );
        });
    });
    var sortable = jQuery( '.sortable' );
    if( sortable.length ) {
        sortable.sortable();
        sortable.disableSelection();
    }
    var datatableEl = jQuery( '[data-plugin-datatable]' );
    if( datatableEl.length ) {
        datatableEl.DataTable({
            'dom': "<'row'<'col-xs-12 col-sm-6'l><'col-xs-12 col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'text-center-xs text-left-sm col-xs-12 col-sm-5'i><'text-center-xs text-left-sm col-xs-12 col-sm-7'p>>",
            'language': datatableEl.data( 'pluginDatatable' )
        });
    }
});
