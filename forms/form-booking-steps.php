<?php
    require_once __DIR__ . '/forms-plus-framework/vendor/autoload.php';
    use FormsPlus\Framework\FormsPlusFramework as FormsPlusFramework;
    $form = new FormsPlusFramework( 'forms-plus-framework/app/config/forms/form-booking-steps.yml' );
    $activeStep = 1;
    if( $form->isSubmitted ) {
        if( $form->isValid ) {
            $activeStep = 3;
        } else {
            $errorStep = false;
            foreach( $form->errorList as $attributeID => $error ) {
                if( !empty( $form->configuration[ 'attribute_list' ][ $attributeID ][ 'form_step' ] ) && ( $errorStep === false || $errorStep > $form->configuration[ 'attribute_list' ][ $attributeID ][ 'form_step' ] ) ) {
                    $errorStep = $form->configuration[ 'attribute_list' ][ $attributeID ][ 'form_step' ];
                }
            }
            $activeStep = ( $errorStep !== false ) ? $errorStep : $activeStep;
        }
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Forms Plus: PHP</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php FormsPlusFramework::loadStyles(); ?>
        <link rel="stylesheet" href="<?php echo FormsPlusFramework::getStaticVar( 'asset_directory_uri' ); ?>css/base.css" type="text/css">
    </head>
    <body>
        <div class="container">
            <form method="post" action="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>" class="<?php $form->designCSSClasses(); ?>" data-js-validate="true" data-js-highlight-state-msg="true" data-js-show-valid-msg="true">
                <div class="p-form p-shadowed p-form-sm">
                    <div class="p-form-steps-wrap">
                        <ul class="p-form-steps" data-js-stepper="bookingSteps">
                            <li <?php echo $activeStep == 1 ? 'class="active" ' : ''; ?>data-js-step="detailsBlock">
                                <span class="p-step">
                                    <span class="p-step-text">Details</span>
                                </span>
                            </li>
                            <li <?php echo $activeStep == 2 ? 'class="active" ' : ''; ?>data-js-step="customerBlock">
                                <span class="p-step">
                                    <span class="p-step-text">Customer</span>
                                </span>
                            </li>
                            <li <?php echo $activeStep == 3 ? 'class="active" ' : ''; ?>data-js-step="invoiceBlock" data-js-disable-steps="true">
                                <span class="p-step">
                                    <span class="p-step-text">Invoice</span>
                                </span>
                            </li>
                        </ul>
                    </div>
                    <?php if( $form->isValid ) { ?>
                        <div class="alert alert-valid"><strong><i class="fa fa-check"></i> Thank you:</strong> message example.</div>
                    <?php } elseif( $form->isSubmitted ) { ?>
                        <?php foreach( $form->errorList as $attributeID => $itemList ) { ?>
                            <div class="alert alert-error"><strong><i class="fa fa-times"></i> <?php echo $form->configuration[ 'attribute_list' ][ $attributeID ][ 'name' ]; ?>:</strong>
                                <?php if( count( $itemList ) == 1 ) { ?>
                                    <?php foreach( $itemList as $errorList ) echo implode( ' ', $errorList ); ?>
                                <?php } else { ?>
                                    <ul><?php foreach( $itemList as $errorList ) echo '<li>' . implode( ' ', $errorList ) . '</li>'; ?></ul>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <div data-js-block="detailsBlock" data-js-validation-block="" class="collapse">
                        <div class="p-subtitle text-left">
                            <span class="p-title-side">booking details</span>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'adults' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'children' ); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'check_in_date' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'check_out_date' ); ?>
                            </div>
                        </div>
                        <div class="text-right">
                            <a class="btn" data-js-show-step="bookingSteps:2"><i class="fa fa-check-square-o"></i>&nbsp;confirm</a>
                            <a class="btn" href="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>"><i class="fa fa-ban"></i>&nbsp;cancel</a>
                        </div>
                    </div>
                    <div data-js-block="customerBlock" class="collapse">
                        <div class="p-subtitle text-left">
                            <span class="p-title-side">your details</span>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'first_name' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'last_name' ); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'email' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'contact_phone' ); ?>
                            </div>
                        </div>
                        <?php $form->attributeView( 'additional_message' ); ?>
                        <div class="text-right">
                            <a class="btn" href="#" data-js-show-step="bookingSteps:1"><i class="fa fa-arrow-left"></i>&nbsp;back</a>
                            <button class="btn" type="submit" name="confirm"><i class="fa fa-check-square-o"></i>&nbsp;confirm</button>
                        </div>
                    </div>
                    <div data-js-block="invoiceBlock" class="collapse">
                        <div class="p-subtitle text-left">
                            <span class="p-title-side">booking details</span>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeResult( 'adults' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeResult( 'children' ); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeResult( 'check_in_date' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeResult( 'check_out_date' ); ?>
                            </div>
                        </div>
                        <div class="p-subtitle text-left">
                            <span class="p-title-side">your details</span>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeResult( 'first_name' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeResult( 'last_name' ); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeResult( 'email' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeResult( 'contact_phone' ); ?>
                            </div>
                        </div>
                        <div>
                            <?php $form->attributeResult( 'additional_message' ); ?>
                        </div>
                        <div class="text-right">
                            <a href="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>" class="btn">reload</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <?php FormsPlusFramework::loadScripts(); ?>
        <?php FormsPlusFramework::outputDebug(); ?>
    </body>
</html>