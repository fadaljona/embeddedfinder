<?php
    require_once __DIR__ . '/forms-plus-framework/vendor/autoload.php';
    use FormsPlus\Framework\FormsPlusFramework as FormsPlusFramework;
    $form = new FormsPlusFramework( 'forms-plus-framework/app/config/forms/ajax-form-registration.yml' );
    if( $form->isSubmitted ) {
        header( 'Content-Type: application/json' );
        if( $form->isValid ) {
            $data = array(
                'block'   => 'successContentBlock',
                'content' => '<div class="alert alert-valid"><strong><i class="fa fa-check"></i> Your account was successfully created.</strong> An email will be sent to the specified email address. Follow the instructions in that email to activate your account.</div>'
            );
            $data[ 'content' ] .= FormsPlusFramework::outputDebug( false, '<div class="alert alert-error">%content%</div>' );
        } else {
            $content = '';
            foreach( $form->errorList as $attributeID => $itemList ) {
                $name = $form->configuration[ 'attribute_list' ][ $attributeID ][ 'name' ];
                $content .= '<div class="alert alert-error"><strong><i class="fa fa-times"></i> ' . $name . ':</strong> ';
                if( count( $itemList ) == 1 ) {
                    foreach( $itemList as $errorList ) $content .=  implode( ' ', $errorList );
                } else {
                    $content .= '<ul>';
                    foreach( $itemList as $errorList ) $content .= '<li>' . implode( ' ', $errorList ) . '</li>';
                    $content .= '</ul>';
                }
                $content .= '</div>';
            }
            $data = array( 'errorData' => array(
                'block'   => 'errorContentBlock',
                'content' => $content
            ));
            $data[ 'errorData' ][ 'content' ] .= FormsPlusFramework::outputDebug( false, '<div class="alert alert-error">%content%</div>' );
        }
        echo json_encode( $data );
        exit;
    }
    // activate a user that got this activation link via an email
    if( !empty( $_GET[ 'action' ] ) && $_GET[ 'action' ] === 'user_activation' && !empty( $_GET[ 'email' ] ) && !empty( $_GET[ 'key' ] ) ) {
        $parameterList = $form->configuration[ 'mail' ][ 'message' ][ 'registration' ][ 'template_parameter_list' ];
        $filter = array( 'prop_form_config_file' => $form->configFile );
        $filter[ 'attr_' . $parameterList[ 'email_attribute_id' ] ] = $_GET[ 'email' ];
        $filter[ 'attr_' . $parameterList[ 'hash_key_attribute_id' ] ] = $_GET[ 'key' ];
        $form->initStorage();
        $collectionSet = $form->storage->fetchCollectionList( null, 1, 0, null, null, $filter );
        if( $collectionSet[ 'count' ] === 1 ) {
            $collection = $collectionSet[ 'list' ][ '0' ];
            $collection[ 'attribute_list' ][ $parameterList[ 'hash_key_attribute_id' ] ][ 'final_value' ] = '';
            $form->storage->updateCollection( $collection );
            $actionUserActivation = array( 'valid', 'Your account is now activated.' );
        } elseif( $collectionSet[ 'count' ] === 0 ) {
            $actionUserActivation = array( 'error', 'Your account hasn\'t been activated.', 'The submitted key is invalid.' );
        } elseif( $collectionSet[ 'count' ] > 1 ) {
            $actionUserActivation = array( 'error', 'The account activation has failed.', 'An internal error occurred. Please, contact the website administrator.' );
        }
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Forms Plus: PHP</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php FormsPlusFramework::loadStyles(); ?>
        <link rel="stylesheet" href="<?php echo FormsPlusFramework::getStaticVar( 'asset_directory_uri' ); ?>css/base.css" type="text/css">
    </head>
    <body>
        <div class="container">
            <form method="post" action="<?php echo strtok( $_SERVER[ 'REQUEST_URI' ], '?' ); ?>" class="<?php $form->designCSSClasses(); ?>" data-js-validate="true" data-js-highlight-state-msg="true" data-js-show-valid-msg="true" data-js-ajax-form="" data-js-ajax-before-hide-block="successBlockName;failBlockName" data-js-ajax-before-show-block="loadingBlockName" data-js-ajax-success-show-block="successBlockName" data-js-ajax-success-hide-block="formBlockName" data-js-ajax-fail-show-block="failBlockName" data-js-ajax-always-hide-block="loadingBlockName">
                <div class="p-form p-shadowed p-form-sm">
                    <div class="p-title text-left">
                        <span class="p-title-side">Registration&nbsp;&nbsp;<i class="fa fa-pencil-square-o"></i></span>
                    </div>
                    <?php if( isset( $actionUserActivation ) ) { ?>
                        <div class="alert alert-<?php echo $actionUserActivation[ 0 ]; ?>">
                            <strong><i class="fa fa-<?php echo $actionUserActivation[ 0 ] === 'error' ? 'times' : 'check'; ?>"></i> <?php echo $actionUserActivation[ 1 ]; ?></strong>
                            <?php echo isset( $actionUserActivation[ 2 ] ) ? $actionUserActivation[ 2 ] : ''; ?>
                        </div>
                        <div class="text-right">
                            <a href="<?php echo strtok( $_SERVER[ 'REQUEST_URI' ], '?' ); ?>" class="btn">reload</a>
                        </div>
                    <?php } else { ?>
                        <div data-js-block="successBlockName" class="collapse">
                            <h4>Form was sent successfully!</h4>
                            <div data-js-block="successContentBlock" class="collapse"></div>
                            <div class="text-right">
                                <a href="<?php echo strtok( $_SERVER[ 'REQUEST_URI' ], '?' ); ?>" class="btn">reload</a>
                            </div>
                        </div>
                        <div data-js-block="failBlockName" class="collapse">
                            <h4>Failed to send form!</h4>
                            <div data-js-block="errorContentBlock" class="collapse"></div>
                        </div>
                        <div data-js-block="formBlockName">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="p-subtitle p-no-offs text-left">
                                        <span class="p-title-side">account details</span>
                                    </div>
                                    <?php $form->attributeView( 'login' ); ?>
                                    <?php $form->attributeView( 'email' ); ?>
                                </div>
                                <div class="col-sm-6">
                                    <div class="p-subtitle p-no-offs text-left">
                                        <span class="p-title-side">Your details</span>
                                    </div>
                                    <?php $form->attributeView( 'first_name' ); ?>
                                    <?php $form->attributeView( 'last_name' ); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <?php $form->attributeView( 'password' ); ?>
                                </div>
                                <div class="col-md-6">
                                    <?php $form->attributeView( 'confirm_password' ); ?>
                                </div>
                            </div>
                            <?php $form->attributeView( 'gender' ); ?>
                            <?php $form->attributeView( 'captcha' ); ?>
                            <div class="clearfix"></div>
                            <?php $form->attributeView( 'agree' ); ?>
                            <div data-js-block="loadingBlockName" class="progress collapse">
                                <div class="progress-bar progress-bar-fp progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                            </div>
                            <div class="text-right">
                                <button class="btn p-ajax-disabled" type="submit" name="confirm"><i class="fa fa-check-square-o"></i>&nbsp;register</button>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </form>
        </div>
        <?php FormsPlusFramework::loadScripts(); ?>
        <?php FormsPlusFramework::outputDebug(); ?>
    </body>
</html>