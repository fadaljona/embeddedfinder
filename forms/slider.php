<?php
    require_once __DIR__ . '/forms-plus-framework/vendor/autoload.php';
    use FormsPlus\Framework\FormsPlusFramework as FormsPlusFramework;
    $form = new FormsPlusFramework( 'forms-plus-framework/app/config/demos/slider.yml' );
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Forms Plus: PHP</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php FormsPlusFramework::loadStyles(); ?>
        <link rel="stylesheet" href="<?php echo FormsPlusFramework::getStaticVar( 'asset_directory_uri' ); ?>css/base.css" type="text/css">
    </head>
    <body>
        <div class="container">
            <form method="post" action="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>" class="<?php $form->designCSSClasses(); ?>" data-js-validate="true" data-js-highlight-state-msg="true" data-js-show-valid-msg="true">
                <div class="p-form p-shadowed p-form-sm">
                    <div class="p-title text-left">
                        <span class="p-title-side">Slider&nbsp;&nbsp;<i class="fa fa-sliders"></i></span>
                    </div>
                    <?php if( $form->isValid ) { ?>
                        <div class="alert alert-valid"><strong><i class="fa fa-check"></i> Thank you:</strong> message example.</div>
                    <?php } elseif( $form->isSubmitted ) { ?>
                        <?php foreach( $form->errorList as $attributeID => $itemList ) { ?>
                            <div class="alert alert-error"><strong><i class="fa fa-times"></i> <?php echo $form->configuration[ 'attribute_list' ][ $attributeID ][ 'name' ]; ?>:</strong>
                                <?php if( count( $itemList ) == 1 ) { ?>
                                    <?php foreach( $itemList as $errorList ) echo implode( ' ', $errorList ); ?>
                                <?php } else { ?>
                                    <ul><?php foreach( $itemList as $errorList ) echo '<li>' . implode( ' ', $errorList ) . '</li>'; ?></ul>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <?php if( !$form->isValid ) { ?>
                        <div class="p-subtitle text-left">
                            <span class="p-title-side">single</span>
                        </div>
                        <div class="row">
                            <?php
                                $delimiterCounter = 0;
                                $subtitleList = array(
                                    'icon_default'                  => array( 6, 'icon' ),
                                    'range_default'                 => array( 6, 'range' ),
                                    'field_single'                  => array( 6, 'field' ),
                                    'bound_input'                   => array( 6, 'bound to field' ),
                                    'bound_range_inputs'            => array( 12 ),
                                    'vertical_default'              => array( 4, 'vertical' ),
                                    'sizes_default'                 => array( 3, 'sizes' ),
                                    'group_default'                 => array( 12, 'group' ),
                                    'group_values_left'             => array( 6 ),
                                    'vertical_multiple_default'     => array( 12, 'vertical' ),
                                    'vertical_multiple_value_top'   => array( 6 ),
                                    'multiple_bound_input'          => array( 6, 'bound to field' )
                                );
                                $columnSize = 6;
                            ?>
                            <?php foreach( $form->attributeList() as $attributeIdentifier ) { ?>
                                <?php if( isset( $subtitleList[ $attributeIdentifier ] ) ) { ?>
                                    </div>
                                    <?php if( !empty( $subtitleList[ $attributeIdentifier ][ 1 ] ) ) { ?>
                                        <div class="p-subtitle text-left">
                                            <span class="p-title-side"><?php echo $subtitleList[ $attributeIdentifier ][ 1 ]; ?></span>
                                        </div>
                                    <?php } ?>
                                    <div class="row">
                                    <?php 
                                        $columnSize = $subtitleList[ $attributeIdentifier ][ 0 ]; 
                                        $delimiterCounter = 0;
                                    ?>
                                <?php } elseif( $delimiterCounter % ( 12 / $columnSize ) == 0 && $delimiterCounter != 0 ) { ?>
                                    </div><div class="row">
                                <?php } ?>
                                <div class="col-sm-<?php echo $columnSize; ?>">
                                    <?php $form->attributeView( $attributeIdentifier ); ?>
                                </div>
                                <?php $delimiterCounter++; ?>
                            <?php } ?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="text-right">
                            <button class="btn" type="submit">submit</button>
                            <button class="btn" type="reset">reset</button>
                        </div>
                    <?php } ?>
                </div>
            </form>
        </div>
        <?php FormsPlusFramework::loadScripts(); ?>
        <?php FormsPlusFramework::outputDebug(); ?>
    </body>
</html>