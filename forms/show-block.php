<?php
    require_once __DIR__ . '/forms-plus-framework/vendor/autoload.php';
    use FormsPlus\Framework\FormsPlusFramework as FormsPlusFramework;
    $form = new FormsPlusFramework( 'forms-plus-framework/app/config/demos/show-block.yml' );
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Forms Plus: PHP</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php FormsPlusFramework::loadStyles(); ?>
        <link rel="stylesheet" href="<?php echo FormsPlusFramework::getStaticVar( 'asset_directory_uri' ); ?>css/base.css" type="text/css">
    </head>
    <body>
        <div class="container">
            <form method="post" action="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>" class="<?php $form->designCSSClasses(); ?>">
                <div class="p-form p-shadowed p-form-sm">
                    <div class="p-title text-left">
                        <span class="p-title-side">show block&nbsp;&nbsp;<i class="fa fa-list"></i></span>
                    </div>
                    <div>
                        <div class="p-subtitle text-left">
                            <span class="p-title-side">Simple</span>
                        </div>
                        <div>
                            <?php $form->showBlockHeadingStart( 'simple', true ); ?>
                                <span class="pull-left">
                                    <span class="p-check-icon">
                                        <span class="p-check-block"></span>
                                    </span>
                                    <span class="p-label">Default</span>
                                </span>
                                <hr/>
                            <?php $form->showBlockHeadingEnd(); ?>
                            <div class="p-show-block-content">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'simple_default_text' ); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'simple_default_password' ); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'simple_default_email' ); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'simple_default_url' ); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <?php $form->showBlockHeadingStart( 'simple', true ); ?>
                                <span class="pull-left">
                                    <span class="p-check-icon">
                                        <span class="p-check-middle"><i class="fa fa-cog"></i></span>
                                    </span>
                                    <span class="p-label">With custom icon</span>
                                </span>
                                <hr/>
                            <?php $form->showBlockHeadingEnd(); ?>
                            <div class="p-show-block-content">
                                <?php $form->attributeView( 'simple_icon_search' ); ?>
                                <?php $form->attributeView( 'simple_icon_textarea' ); ?>
                            </div>
                        </div>
                        <div>
                            <?php $form->showBlockHeadingStart( 'simple', true ); ?>
                                <span class="pull-left">
                                    <span class="p-check-active-icon">
                                        <span class="p-check-middle"><i class="fa fa-angle-double-up"></i></span>
                                    </span>
                                    <span class="p-check-icon">
                                        <span class="p-check-middle"><i class="fa fa-angle-double-down"></i></span>
                                    </span>
                                    <span class="p-label">With custom icons</span>
                                </span>
                                <hr/>
                            <?php $form->showBlockHeadingEnd(); ?>
                            <div class="p-show-block-content">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'simple_icons_email' ); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'simple_icons_price' ); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <?php $form->showBlockHeadingStart( 'simple', true ); ?>
                                <span class="pull-right">
                                    <span class="p-label">At right</span>
                                    <span class="p-check-icon">
                                        <span class="p-check-block"></span>
                                    </span>
                                </span>
                                <hr/>
                            <?php $form->showBlockHeadingEnd(); ?>
                            <div class="p-show-block-content">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'simple_right_email' ); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'simple_right_price' ); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <?php $form->showBlockHeadingStart( 'simple', true ); ?>
                                <span class="pull-left">
                                    <span class="p-label">Just text</span>
                                </span>
                                <hr/>
                            <?php $form->showBlockHeadingEnd(); ?>
                            <div class="p-show-block-content">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'simple_text_email' ); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'simple_text_price' ); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="p-subtitle text-left">
                            <span class="p-title-side">Accordion-like</span>
                        </div>
                        <div>
                            <?php $form->showBlockHeadingStart( 'accordion-like', false, true ); ?>
                                <span class="pull-left">
                                    <span class="p-check-icon">
                                        <span class="p-check-block"></span>
                                    </span>
                                    <span class="p-label">Group 1</span>
                                </span>
                                <hr/>
                            <?php $form->showBlockHeadingEnd(); ?>
                            <div class="p-show-block-content">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'accordion_group1_block1_text' ); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'accordion_group1_block1_password' ); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <?php $form->showBlockHeadingStart( 'accordion-like' ); ?>
                                <span class="pull-left">
                                    <span class="p-check-icon">
                                        <span class="p-check-block"></span>
                                    </span>
                                    <span class="p-label">Group 1</span>
                                </span>
                                <hr/>
                            <?php $form->showBlockHeadingEnd(); ?>
                            <div class="p-show-block-content">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'accordion_group1_block2_text' ); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'accordion_group1_block2_password' ); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <?php $form->showBlockHeadingStart( 'accordion-like' ); ?>
                                <span class="pull-left">
                                    <span class="p-check-icon">
                                        <span class="p-check-block"></span>
                                    </span>
                                    <span class="p-label">Group 1</span>
                                </span>
                                <hr/>
                            <?php $form->showBlockHeadingEnd(); ?>
                            <div class="p-show-block-content">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'accordion_group1_block3_text' ); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'accordion_group1_block3_password' ); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <br/>
                        <hr class="p-flat">
                        <br/>
                        <br/>
                        <div>
                            <?php $form->showBlockHeadingStart( 'accordion-like-icon', false, true, 'p-check-big-icons' ); ?>
                                <span class="pull-left">
                                    <span class="p-check-icon">
                                        <span class="p-check-middle"><i class="fa fa-cc-visa"></i></span>
                                    </span>
                                    <span class="p-label">VISA</span>
                                </span>
                                <hr/>
                            <?php $form->showBlockHeadingEnd(); ?>
                            <div class="p-show-block-content">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'accordion_icon_visa_text' ); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'accordion_icon_visa_email' ); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <?php $form->showBlockHeadingStart( 'accordion-like-icon', false, false, 'p-check-big-icons' ); ?>
                                <span class="pull-left">
                                    <span class="p-check-icon">
                                        <span class="p-check-middle"><i class="fa fa-cc-mastercard"></i></span>
                                    </span>
                                    <span class="p-label">MasterCard</span>
                                </span>
                                <hr/>
                            <?php $form->showBlockHeadingEnd(); ?>
                            <div class="p-show-block-content">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'accordion_icon_mastercard_text' ); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'accordion_icon_mastercard_email' ); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <?php $form->showBlockHeadingStart( 'accordion-like-icon', false, false, 'p-check-big-icons' ); ?>
                                <span class="pull-left">
                                    <span class="p-check-icon">
                                        <span class="p-check-middle"><i class="fa fa-cc-paypal"></i></span>
                                    </span>
                                    <span class="p-label">PayPal</span>
                                </span>
                                <hr/>
                            <?php $form->showBlockHeadingEnd(); ?>
                            <div class="p-show-block-content">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'accordion_icon_paypal_text' ); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'accordion_icon_paypal_email' ); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <br/>
                        <hr class="p-flat">
                        <br/>
                        <br/>
                        <div>
                            <?php $form->showBlockHeadingStart( 'accordion-like-arrows', false, true ); ?>
                                <span class="pull-left">
                                    <span class="p-check-active-icon">
                                        <span class="p-check-middle"><i class="fa fa-angle-double-up"></i></span>
                                    </span>
                                    <span class="p-check-icon">
                                        <span class="p-check-middle"><i class="fa fa-angle-double-down"></i></span>
                                    </span>
                                </span>
                                <hr/>
                            <?php $form->showBlockHeadingEnd(); ?>
                            <div class="p-show-block-content">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'accordion_arrows_block1_text' ); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'accordion_arrows_block1_email' ); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <?php $form->showBlockHeadingStart( 'accordion-like-arrows' ); ?>
                                <span class="pull-left">
                                    <span class="p-check-active-icon">
                                        <span class="p-check-middle"><i class="fa fa-angle-double-up"></i></span>
                                    </span>
                                    <span class="p-check-icon">
                                        <span class="p-check-middle"><i class="fa fa-angle-double-down"></i></span>
                                    </span>
                                </span>
                                <hr/>
                            <?php $form->showBlockHeadingEnd(); ?>
                            <div class="p-show-block-content">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'accordion_arrows_block2_text' ); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'accordion_arrows_block2_email' ); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <?php $form->showBlockHeadingStart( 'accordion-like-arrows' ); ?>
                                <span class="pull-left">
                                    <span class="p-check-active-icon">
                                        <span class="p-check-middle"><i class="fa fa-angle-double-up"></i></span>
                                    </span>
                                    <span class="p-check-icon">
                                        <span class="p-check-middle"><i class="fa fa-angle-double-down"></i></span>
                                    </span>
                                </span>
                                <hr/>
                            <?php $form->showBlockHeadingEnd(); ?>
                            <div class="p-show-block-content">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'accordion_arrows_block3_text' ); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'accordion_arrows_block3_email' ); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <br/>
                        <hr class="p-flat">
                        <br/>
                        <br/>
                        <div>
                            <?php $form->showBlockHeadingStart( 'accordion-like-text', false, true ); ?>
                                <span class="pull-left">
                                    <span class="p-label">VISA</span>
                                </span>
                                <hr/>
                            <?php $form->showBlockHeadingEnd(); ?>
                            <div class="p-show-block-content">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'accordion_text_visa_text' ); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'accordion_text_visa_email' ); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <?php $form->showBlockHeadingStart( 'accordion-like-text' ); ?>
                                <span class="pull-left">
                                    <span class="p-label">MasterCard</span>
                                </span>
                                <hr/>
                            <?php $form->showBlockHeadingEnd(); ?>
                            <div class="p-show-block-content">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'accordion_text_mastercard_text' ); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'accordion_text_mastercard_email' ); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <?php $form->showBlockHeadingStart( 'accordion-like-text' ); ?>
                                <span class="pull-left">
                                    <span class="p-label">PayPal</span>
                                </span>
                                <hr/>
                            <?php $form->showBlockHeadingEnd(); ?>
                            <div class="p-show-block-content">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'accordion_text_paypal_text' ); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'accordion_text_paypal_email' ); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <?php FormsPlusFramework::loadScripts(); ?>
        <?php FormsPlusFramework::outputDebug(); ?>
    </body>
</html>