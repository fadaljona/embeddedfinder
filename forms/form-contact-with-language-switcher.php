<?php
    require_once __DIR__ . '/forms-plus-framework/vendor/autoload.php';
    use FormsPlus\Framework\FormsPlusFramework as FormsPlusFramework;
    use Symfony\Component\Yaml\Parser;
    use FormsPlus\Framework\Helper\Template;
    // language switcher code
    class ContactForm {
        public static $language = 'en';
        public static function SetLanguageValue( &$value, &$stringValue ) {
            $value = self::$language;
        }
    }
    $configuration = array();
    if( isset( $_GET[ 'language' ] ) && in_array( $_GET[ 'language' ], array( 'de', 'es' ) ) ) {
        \ContactForm::$language =  $_GET[ 'language' ];
        $yaml = new Parser();
        $configuration = $yaml->parse( file_get_contents( 'forms-plus-framework/app/config/forms/form-contact-with-language-switcher/' . \ContactForm::$language . '.yml' ) );
    }
    $form = new FormsPlusFramework( 'forms-plus-framework/app/config/forms/form-contact-with-language-switcher.yml', array( 'configuration' => $configuration ) );
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Forms Plus: PHP</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php FormsPlusFramework::loadStyles(); ?>
        <link rel="stylesheet" href="<?php echo FormsPlusFramework::getStaticVar( 'asset_directory_uri' ); ?>css/base.css" type="text/css">
    </head>
    <body>
        <div class="container">
            <form method="post" action="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>" class="<?php $form->designCSSClasses(); ?>" data-js-validate="true" data-js-highlight-state-msg="true" data-js-show-valid-msg="true">
                <div class="p-form p-shadowed p-form-sm">
                    <div class="p-title text-left">
                        <span class="p-title-side"><?php echo $form->configuration[ 'translation_message_list' ][ 'title' ]; ?>&nbsp;&nbsp;<i class="fa fa-envelope-o"></i></span>
                    </div>
                    <?php if( $form->isValid ) { ?>
                        <div class="alert alert-valid"><strong><i class="fa fa-check"></i> <?php echo $form->configuration[ 'translation_message_list' ][ 'alert_valid' ]; ?></strong></div>
                    <?php } elseif( $form->isSubmitted ) { ?>
                        <?php foreach( $form->errorList as $attributeID => $itemList ) { ?>
                            <div class="alert alert-error"><strong><i class="fa fa-times"></i> <?php echo $form->configuration[ 'attribute_list' ][ $attributeID ][ 'name' ]; ?>:</strong>
                                <?php if( count( $itemList ) == 1 ) { ?>
                                    <?php foreach( $itemList as $errorList ) echo implode( ' ', $errorList ); ?>
                                <?php } else { ?>
                                    <ul><?php foreach( $itemList as $errorList ) echo '<li>' . implode( ' ', $errorList ) . '</li>'; ?></ul>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <?php if( !$form->isValid ) { ?>
                        <div class="panel panel-fp">
                            <div class="panel-body">
                                <strong>Choose language:</strong>
                                <a href="<?php echo Template::url(); ?>">English</a>,
                                <a href="<?php echo Template::url( false, array( 'language' => 'de' ) ); ?>">Deutsch</a>,
                                <a href="<?php echo Template::url( false, array( 'language' => 'es' ) ); ?>">Español</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'name' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'email' ); ?>
                            </div>
                        </div>
                        <?php $form->attributeView( 'subject' ); ?>
                        <?php $form->attributeView( 'message' ); ?>
                        <?php $form->attributeView( 'captcha' ); ?>
                        <div class="clearfix"></div>
                        <div class="text-right">
                            <button class="btn" type="submit" name="confirm"><i class="fa fa-paper-plane"></i>&nbsp;&nbsp;<?php echo $form->configuration[ 'translation_message_list' ][ 'submit_button' ]; ?></button>
                        </div>
                    <?php } ?>
                </div>
            </form>
        </div>
        <?php FormsPlusFramework::loadScripts(); ?>
        <?php FormsPlusFramework::outputDebug(); ?>
    </body>
</html>