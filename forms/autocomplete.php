<?php
    require_once __DIR__ . '/forms-plus-framework/vendor/autoload.php';
    use FormsPlus\Framework\FormsPlusFramework as FormsPlusFramework;
    $form = new FormsPlusFramework( 'forms-plus-framework/app/config/demos/autocomplete.yml' );
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Forms Plus: PHP</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php FormsPlusFramework::loadStyles(); ?>
        <link rel="stylesheet" href="<?php echo FormsPlusFramework::getStaticVar( 'asset_directory_uri' ); ?>css/base.css" type="text/css">
    </head>
    <body>
        <div class="container">
            <form method="post" action="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>" class="<?php $form->designCSSClasses(); ?>" data-js-validate="true" data-js-highlight-state-msg="true" data-js-show-valid-msg="true">
                <div class="p-form p-shadowed p-form-sm">
                    <div class="p-title text-left">
                        <span class="p-title-side">Autocomplete&nbsp;&nbsp;<i class="fa fa-info"></i></span>
                    </div>
                    <?php if( $form->isValid ) { ?>
                        <div class="alert alert-valid"><strong><i class="fa fa-check"></i> Thank you:</strong> message example.</div>
                    <?php } elseif( $form->isSubmitted ) { ?>
                        <?php foreach( $form->errorList as $attributeID => $itemList ) { ?>
                            <div class="alert alert-error"><strong><i class="fa fa-times"></i> <?php echo $form->configuration[ 'attribute_list' ][ $attributeID ][ 'name' ]; ?>:</strong>
                                <?php if( count( $itemList ) == 1 ) { ?>
                                    <?php foreach( $itemList as $errorList ) echo implode( ' ', $errorList ); ?>
                                <?php } else { ?>
                                    <ul><?php foreach( $itemList as $errorList ) echo '<li>' . implode( ' ', $errorList ) . '</li>'; ?></ul>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <?php if( !$form->isValid ) { ?>
                        <div class="p-subtitle text-left">
                            <span class="p-title-side">Variations</span>
                        </div>
                        <div class="panel panel-fp">
                            <div class="panel-body">
                                <p>
                                    <strong>Autocomplete words:</strong>
                                    <br/> ActionScript, AppleScript, Asp, BASIC, C, C++, Clojure, COBOL, ColdFusion, Erlang, Fortran, Groovy, Haskell, Java, JavaScript, Lisp, Perl, PHP, Python, Ruby, Scala, Scheme</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'autocomplete_default' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'autocomplete_auto_focus' ); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'autocomplete_delay_300' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'autocomplete_min_length' ); ?>
                            </div>
                        </div>
                        <div class="p-subtitle text-left">
                            <span class="p-title-side">Google Places Autocomplete</span>
                        </div>
                        <p>
                            <a href="https://developers.google.com/maps/documentation/javascript/places-autocomplete" target="_blank">
                                <strong>Google Places autocomplete examples</strong>
                            </a>
                        </p>
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'gautocomplete_default' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'gautocomplete_cities' ); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'gautocomplete_bounds_square' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'gautocomplete_bounds_circle' ); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'gautocomplete_limited_usa' ); ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </form>
        </div>
        <?php FormsPlusFramework::loadScripts(); ?>
        <?php FormsPlusFramework::outputDebug(); ?>
    </body>
</html>