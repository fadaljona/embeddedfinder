<?php
    require_once __DIR__ . '/forms-plus-framework/vendor/autoload.php';
    use FormsPlus\Framework\FormsPlusFramework as FormsPlusFramework;
    $form = new FormsPlusFramework( 'forms-plus-framework/app/config/forms/ajax-form-checkout-steps-calculations.yml' );
    if( $form->isSubmitted ) {
        header( 'Content-Type: application/json' );
        if( $form->isValid ) {
            $data = array(
                'block'   => 'successContentBlock',
                'content' => '<div class="alert alert-valid"><strong><i class="fa fa-check"></i> Thank you:</strong> message example.</div>'
            );
            $data[ 'content' ] .= FormsPlusFramework::outputDebug( false, '<div class="alert alert-error">%content%</div>' );
            ob_start();
            ?>
            <div class="p-subtitle text-left">
                <span class="p-title-side">billing details</span>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <?php $form->attributeResult( 'first_name' ); ?>
                </div>
                <div class="col-sm-6">
                    <?php $form->attributeResult( 'last_name' ); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <?php $form->attributeResult( 'email' ); ?>
                </div>
                <div class="col-sm-6">
                    <?php $form->attributeResult( 'contact_phone' ); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <?php $form->attributeResult( 'company' ); ?>
                </div>
                <div class="col-sm-6">
                    <?php $form->attributeResult( 'company_id' ); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <?php $form->attributeResult( 'billing_address' ); ?>
                </div>
                <div class="col-sm-6">
                    <?php $form->attributeResult( 'postal_code' ); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <?php $form->attributeResult( 'city' ); ?>
                </div>
                <div class="col-sm-6">
                    <?php $form->attributeResult( 'country' ); ?>
                </div>
            </div>
            <div class="p-subtitle text-left">
                <span class="p-title-side">payment type</span>
            </div>
            <div class="row">
                <div class="col-sm-5">
                    <?php $form->attributeResult( 'select_card' ); ?>
                </div>
                <div class="col-sm-7">
                    <?php $form->attributeResult( 'credit_card_holder_name' ); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-7 col-sm-push-5">
                    <?php $form->attributeResult( 'credit_card_number' ); ?>
                </div>
                <div class="col-sm-5 col-sm-pull-7">
                    <?php $form->attributeResult( 'validation_date' ); ?>
                </div>
            </div>
            <hr class="p-flat" />
            <div class="row">
                <div class="col-sm-6">
                    <?php $form->attributeResult( 'additional_message' ); ?>
                </div>
                <div class="col-sm-6">
                    <?php $form->attributeResult( 'terms_of_service' ); ?>
                </div>
            </div>
            <div class="p-subtitle text-left">
                <span class="p-title-side">order info</span>
            </div>
            <?php $productAttributeValue = json_decode( $form->attributeValue( 'product' ), true ); ?>
            <table class="table table-striped p-table">
                <thead>
                    <tr>
                        <td>Product name</td>
                        <td class="text-center">Quantity</td>
                        <td class="text-center">Price</td>
                        <td class="p-price-column hidden-xs">Total price</td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach( $productAttributeValue as $productAttributeValueItem ) { ?>
                        <tr>
                            <td><?php echo $productAttributeValueItem[ 'name' ]; ?></td>
                            <td class="text-center"><?php echo $productAttributeValueItem[ 'quantity' ]; ?></td>
                            <td class="p-price-column"><?php echo $productAttributeValueItem[ 'price' ]; ?>$</td>
                            <td class="p-price-column hidden-xs"><?php echo $productAttributeValueItem[ 'total' ]; ?>$</td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <hr />
            <?php $priceAttributeValue = json_decode( $form->attributeValue( 'price' ), true ); ?>
            <div class="p-total-block">
                <div class="pull-right">
                    <div class="pull-right">
                        <div class="pull-left text-right">Product price:</div>
                        <div class="p-price-column"><?php echo $priceAttributeValue[ 'product' ]; ?></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="pull-right">
                        <div class="pull-left text-right">Delivery price:</div>
                        <div class="p-price-column"><?php echo $priceAttributeValue[ 'delivery' ]; ?></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="pull-right p-colored-text">
                        <div class="pull-left text-right"><i class="fa fa-shopping-cart"></i> Subtotal:</div>
                        <div class="p-price-column"><?php echo $priceAttributeValue[ 'subtotal' ]; ?></div>
                    </div>
                </div>
            </div>
            <br/>
            <?php
            $content = ob_get_contents();
            ob_end_clean();
            $data[ 'content' ] .= $content;
        } else {
            $content = '';
            $step = false;
            foreach( $form->errorList as $attributeID => $itemList ) {
                $name = $form->configuration[ 'attribute_list' ][ $attributeID ][ 'name' ];
                $content .= '<div class="alert alert-error"><strong><i class="fa fa-times"></i> ' . $name . ':</strong> ';
                if( count( $itemList ) == 1 ) {
                    foreach( $itemList as $errorList ) $content .=  implode( ' ', $errorList );
                } else {
                    $content .= '<ul>';
                    foreach( $itemList as $errorList ) $content .= '<li>' . implode( ' ', $errorList ) . '</li>';
                    $content .= '</ul>';
                }
                $content .= '</div>';
                if( !empty( $form->configuration[ 'attribute_list' ][ $attributeID ][ 'form_step' ] ) && ( $step === false || $step > $form->configuration[ 'attribute_list' ][ $attributeID ][ 'form_step' ] ) ) {
                    $step = $form->configuration[ 'attribute_list' ][ $attributeID ][ 'form_step' ];
                }
            }
            $data = array( 'errorData' => array(
                'block'   => 'errorContentBlock',
                'content' => $content
            ));
            if( $step !== false ) {
                $data[ 'errorData' ][ 'step' ] = $step;
            }
            $data[ 'errorData' ][ 'content' ] .= FormsPlusFramework::outputDebug( false, '<div class="alert alert-error">%content%</div>' );
        }
        echo json_encode( $data );
        exit;
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Forms Plus: PHP</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php FormsPlusFramework::loadStyles(); ?>
        <link rel="stylesheet" href="<?php echo FormsPlusFramework::getStaticVar( 'asset_directory_uri' ); ?>css/base.css" type="text/css">
    </head>
    <body>
        <div class="container">
            <!DOCTYPE html>
            <form method="post" action="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>" class="<?php $form->designCSSClasses(); ?>" data-js-validate="true" data-js-highlight-state-msg="true" data-js-show-valid-msg="true" data-js-ajax-form="" data-js-ajax-before-hide-block="successBlockName;failBlockName" data-js-ajax-before-show-block="loadingBlockName" data-js-ajax-success-show-block="successBlockName" data-js-ajax-success-hide-block="formBlockName" data-js-ajax-fail-show-block="failBlockName" data-js-ajax-always-hide-block="loadingBlockName">
                <div class="p-form p-shadowed p-form-sm">
                    <div class="p-form-steps-wrap">
                        <ul class="p-form-steps" data-js-stepper="checkoutSteps">
                            <li class="active" data-js-step="cartBlock">
                                <span class="p-step">
                                    <span class="p-step-text">Cart</span>
                                </span>
                            </li>
                            <li data-js-step="billingBlock">
                                <span class="p-step">
                                    <span class="p-step-text">Billing</span>
                                </span>
                            </li>
                            <li data-js-step="paymentBlock">
                                <span class="p-step">
                                    <span class="p-step-text">Payment</span>
                                </span>
                            </li>
                            <li data-js-step="invoiceBlock" data-js-disable-steps="true">
                                <span class="p-step">
                                    <span class="p-step-text">Invoice</span>
                                </span>
                            </li>
                        </ul>
                    </div>
                    <div class="collapse" data-js-watch-value="" data-js-just-calculations="" data-js-group-values="cartItems:#itemsPrice*" data-js-empty-value-class="show">
                        <h3>Cart is empty.</h3>
                        <div class="text-right">
                            <a href="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>" class="btn">reload</a>
                        </div>
                    </div>
                    <div data-js-watch-value="" data-js-just-calculations="" data-js-group-values="cartItems:#itemsPrice*" data-js-empty-value-class="hide">
                        <div data-js-block="errorContentBlock" class="collapse"></div>
                        <div data-js-block="cartBlock" data-js-validation-block="" class="collapse">
                            <div class="p-subtitle text-left">
                                <span class="p-title-side">order info</span>
                            </div>
                            <table class="table table-striped p-table">
                                <thead>
                                    <tr>
                                        <td>Product name</td>
                                        <td class="text-center">Quantity</td>
                                        <td class="text-center">Price</td>
                                        <td class="p-price-column hidden-xs">Total price</td>
                                        <td class="p-action-column">&nbsp;</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="p-block">
                                        <td>Product name 1</td>
                                        <td class="text-center">2</td>
                                        <td class="p-price-column">100$</td>
                                        <td class="p-price-column hidden-xs">200$</td>
                                        <td class="p-action-column">
                                            <input type="hidden" name="product[0][id]" value="1" />
                                            <input type="hidden" name="product[0][name]" value="Product name 1" />
                                            <input type="hidden" name="product[0][price]" value="100" />
                                            <input type="hidden" name="product[0][total]" value="200" />
                                            <input type="hidden" name="product[0][quantity]" value="2" data-js-value-group="cartItems" data-js-value-format="{#quantity*}" data-js-get-value-extra="{#price:100}{#itemsPrice:price*quantity}" />
                                            <a href="#" class="p-action-link" data-js-remove-block=""><i class="fa fa-times"></i></a>
                                        </td>
                                    </tr>
                                    <tr class="p-block">
                                        <td>Product name 2</td>
                                        <td class="text-center">1</td>
                                        <td class="p-price-column">500$</td>
                                        <td class="p-price-column hidden-xs">500$</td>
                                        <td class="p-action-column">
                                            <input type="hidden" name="product[1][id]" value="2" />
                                            <input type="hidden" name="product[1][name]" value="Product name 2" />
                                            <input type="hidden" name="product[1][price]" value="500" />
                                            <input type="hidden" name="product[1][total]" value="500" />
                                            <input type="hidden" name="product[1][quantity]" value="1" data-js-value-group="cartItems" data-js-value-format="{#quantity*}" data-js-get-value-extra="{#price:500}{#itemsPrice:price*quantity}" />
                                            <a href="#" class="p-action-link" data-js-remove-block=""><i class="fa fa-times"></i></a>
                                        </td>
                                    </tr>
                                    <tr class="p-block">
                                        <td>Product name 3</td>
                                        <td class="text-center">3</td>
                                        <td class="p-price-column">120$</td>
                                        <td class="p-price-column hidden-xs">360$</td>
                                        <td class="p-action-column">
                                            <input type="hidden" name="product[2][id]" value="3" />
                                            <input type="hidden" name="product[2][name]" value="Product name 3" />
                                            <input type="hidden" name="product[2][price]" value="120" />
                                            <input type="hidden" name="product[2][total]" value="360" />
                                            <input type="hidden" name="product[2][quantity]" value="3" data-js-value-group="cartItems" data-js-value-format="{#quantity*}" data-js-get-value-extra="{#price:120}{#itemsPrice:price*quantity}" />
                                            <a href="#" class="p-action-link" data-js-remove-block=""><i class="fa fa-times"></i></a>
                                        </td>
                                    </tr>
                                    <tr class="p-block">
                                        <td>Product name 4</td>
                                        <td class="text-center">2</td>
                                        <td class="p-price-column">180$</td>
                                        <td class="p-price-column hidden-xs">360$</td>
                                        <td class="p-action-column">
                                            <input type="hidden" name="product[3][id]" value="4" />
                                            <input type="hidden" name="product[3][name]" value="Product name 4" />
                                            <input type="hidden" name="product[3][price]" value="180" />
                                            <input type="hidden" name="product[3][total]" value="360" />
                                            <input type="hidden" name="product[3][quantity]" value="2" data-js-value-group="cartItems" data-js-value-format="{#quantity*}" data-js-get-value-extra="{#price:180}{#itemsPrice:price*quantity}" />
                                            <a href="#" class="p-action-link" data-js-remove-block=""><i class="fa fa-times"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <hr />
                            <div class="p-total-block">
                                <div class="p-action-column pull-right hidden-xs"></div>
                                <div class="pull-right">
                                    <div class="clearfix"></div>
                                    <div class="pull-right">
                                        <div class="pull-left text-right">Product price:</div>
                                        <div class="p-price-column" data-js-watch-value="" data-js-empty-value="-" data-js-value-group="cartProductPrice" data-js-group-values="cartItems:#itemsPrice*" data-js-value-format="{#2#itemsPrice*}{currency:'$'}"></div>
                                        <input type="hidden" name="price[product]" data-js-watch-value="" data-js-empty-value="-" data-js-group-values="cartItems:#itemsPrice*" data-js-value-format="{#2#itemsPrice*}{currency:'$'}"/>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="pull-right">
                                        <div class="pull-left text-right">Tax 20%:</div>
                                        <div class="p-price-column" data-js-watch-value="" data-js-empty-value="-" data-js-value-group="cartTax" data-js-group-values="cartProductPrice:#itemsPrice,currency" data-js-value-format="{#2#tax*:itemsPrice*0.2}{currency}"></div>
                                        <input type="hidden" name="price[tax]" data-js-watch-value="" data-js-empty-value="-" data-js-group-values="cartProductPrice:#itemsPrice,currency" data-js-value-format="{#2#tax*:itemsPrice*0.2}{currency}"/>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="pull-right">
                                        <div class="pull-left text-right">Delivery price:</div>
                                        <div class="p-price-column">10$</div>
                                        <input type="hidden" name="price[delivery]" value="10$" />
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="pull-right p-colored-text">
                                        <div class="pull-left text-right"><i class="fa fa-shopping-cart"></i> Subtotal:</div>
                                        <div class="p-price-column" data-js-watch-value="" data-js-empty-value="-" data-js-group-values="cartTax:#tax*,currency;cartProductPrice:#itemsPrice*" data-js-value-format="{#2#*:itemsPrice+tax+10}{currency}"></div>
                                        <input type="hidden" name="price[subtotal]" data-js-watch-value="" data-js-empty-value="-" data-js-group-values="cartTax:#tax*,currency;cartProductPrice:#itemsPrice*" data-js-value-format="{#2#*:itemsPrice+tax+10}{currency}"/>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="text-right">
                                <a class="btn" data-js-show-step="checkoutSteps:2"><i class="fa fa-check-square-o"></i>&nbsp;confirm</a>
                                <a class="btn" href="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>"><i class="fa fa-ban"></i>&nbsp;cancel</a>
                            </div>
                        </div>
                        <div data-js-block="billingBlock" data-js-validation-block="" class="collapse">
                            <div class="p-subtitle text-left">
                                <span class="p-title-side">billing details</span>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <?php $form->attributeView( 'first_name' ); ?>
                                </div>
                                <div class="col-sm-6">
                                    <?php $form->attributeView( 'last_name' ); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <?php $form->attributeView( 'email' ); ?>
                                </div>
                                <div class="col-sm-6">
                                    <?php $form->attributeView( 'contact_phone' ); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <?php $form->attributeView( 'company' ); ?>
                                </div>
                                <div class="col-sm-6">
                                    <?php $form->attributeView( 'company_id' ); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <?php $form->attributeView( 'billing_address' ); ?>
                                </div>
                                <div class="col-sm-6">
                                    <?php $form->attributeView( 'postal_code' ); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <?php $form->attributeView( 'city' ); ?>
                                </div>
                                <div class="col-sm-6">
                                    <?php $form->attributeView( 'country' ); ?>
                                </div>
                            </div>
                            <div class="text-right">
                                <a class="btn" href="#" data-js-show-step="checkoutSteps:1"><i class="fa fa-arrow-left"></i>&nbsp;back</a>
                                <a class="btn" data-js-show-step="checkoutSteps:3"><i class="fa fa-check-square-o"></i>&nbsp;confirm</a>
                            </div>
                        </div>
                        <div data-js-block="paymentBlock" class="collapse">
                            <div class="p-subtitle text-left">
                                <span class="p-title-side">payment type</span>
                            </div>
                            <div class="row">
                                <div class="col-sm-5">
                                    <?php $form->attributeView( 'select_card' ); ?>
                                </div>
                                <div class="col-sm-7">
                                    <?php $form->attributeView( 'credit_card_holder_name' ); ?>
                                    <?php $form->attributeView( 'credit_card_number' ); ?>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <?php $form->attributeView( 'validation_date' ); ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?php $form->attributeView( 'cvv2' ); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr class="p-flat" />
                            <?php $form->attributeView( 'additional_message' ); ?>
                            <?php $form->attributeView( 'terms_of_service' ); ?>
                            <div data-js-block="loadingBlockName" class="progress collapse">
                                <div class="progress-bar progress-bar-fp progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                            </div>
                            <div class="text-right">
                                <a class="btn" href="#" data-js-show-step="checkoutSteps:2"><i class="fa fa-arrow-left"></i>&nbsp;back</a>
                                <button class="btn p-ajax-disabled" type="submit" name="confirm" data-js-show-step="checkoutSteps:4"><i class="fa fa-check-square-o"></i>&nbsp;confirm</button>
                            </div>
                        </div>
                        <div data-js-block="invoiceBlock" class="collapse">
                            <h4>Ajax success!</h4>
                            <div data-js-block="successContentBlock" class="collapse"></div>
                            <div class="text-right">
                                <a href="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>" class="btn">reload</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <?php FormsPlusFramework::loadScripts(); ?>
        <?php FormsPlusFramework::outputDebug(); ?>
    </body>
</html>