<?php
    require_once __DIR__ . '/forms-plus-framework/vendor/autoload.php';
    use FormsPlus\Framework\FormsPlusFramework as FormsPlusFramework;
    $form = new FormsPlusFramework( 'forms-plus-framework/app/config/forms/form-contact-with-map.yml' );
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Forms Plus: PHP</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php FormsPlusFramework::loadStyles(); ?>
        <link rel="stylesheet" href="<?php echo FormsPlusFramework::getStaticVar( 'asset_directory_uri' ); ?>css/base.css" type="text/css">
    </head>
    <body>
        <div class="container">
            <form method="post" action="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>" class="<?php $form->designCSSClasses(); ?>" data-js-validate="true" data-js-highlight-state-msg="true" data-js-show-valid-msg="true">
                <div class="p-form p-shadowed p-form-sm">
                    <div class="p-title text-left">
                        <span class="p-title-side">Contact us&nbsp;&nbsp;<i class="fa fa-envelope-o"></i></span>
                    </div>
                    <?php if( $form->isValid ) { ?>
                        <div class="alert alert-valid"><strong><i class="fa fa-check"></i> Thank you:</strong> message example.</div>
                    <?php } elseif( $form->isSubmitted ) { ?>
                        <?php foreach( $form->errorList as $attributeID => $itemList ) { ?>
                            <div class="alert alert-error"><strong><i class="fa fa-times"></i> <?php echo $form->configuration[ 'attribute_list' ][ $attributeID ][ 'name' ]; ?>:</strong>
                                <?php if( count( $itemList ) == 1 ) { ?>
                                    <?php foreach( $itemList as $errorList ) echo implode( ' ', $errorList ); ?>
                                <?php } else { ?>
                                    <ul><?php foreach( $itemList as $errorList ) echo '<li>' . implode( ' ', $errorList ) . '</li>'; ?></ul>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <?php if( !$form->isValid ) { ?>
                        <div class="p-form-with-back">
                            <div class="p-form-back-wrap">
                                <iframe class="p-form-back" scrolling="no" src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d48325679.44497964!2d51.76993221069085!3d42.33046759020831!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sua!4v1447335647539"></iframe>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-sm-push-6">
                                    <div class="p-form-content p-alt-color">
                                        <div class="p-form-bg p-visibility-8"></div>
                                        <div class="p-block">
                                            <?php $form->attributeView( 'name' ); ?>
                                            <?php $form->attributeView( 'email' ); ?>
                                            <?php $form->attributeView( 'subject' ); ?>
                                            <?php $form->attributeView( 'message' ); ?>
                                            <?php $form->attributeView( 'captcha' ); ?>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-right">
                            <button class="btn" type="submit" name="confirm"><i class="fa fa-paper-plane"></i>&nbsp;&nbsp;send message</button>
                        </div>
                    <?php } ?>
                </div>
            </form>
        </div>
        <?php FormsPlusFramework::loadScripts(); ?>
        <?php FormsPlusFramework::outputDebug(); ?>
    </body>
</html>