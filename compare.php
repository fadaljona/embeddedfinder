<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="robots" content="noindex,nofollow">
  <title>Compare Products</title>
  
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
  <link rel="stylesheet" href="css/view.css">

  <style>
  		body {font-size: .7rem;}
  		.rowid_name td {font-weight: bold;}
  		.row_section_header {background-color: #003366; color: #fff; font-weight: bold;}
  		/*.odd {background: #dee2e6;}*/
  		th {width: 15%;}
  		.rowid_applications, .rowid_manufacturer, .rowid_series {display: none;}
  </style>

  <meta http-equiv="Content-Security-Policy" content="default-src &apos;self&apos;; script-src &apos;self&apos; https://ajax.googleapis.com; style-src &apos;self&apos;; img-src &apos;self&apos; data:">

</head>
<body>
  <div class="container">
  <div class="row"><div class="col-12"><a href="view.php">All Products (Grid)</a>&nbsp;|&nbsp;<a href="table.php">All Products (Table)</a></div></div>
  <div class="row">
  <div class="col-12">
  <h2>Product Comparison</h2>
  <p>Compare specifications of up to four computers at a time.</p>
  <p>
  	<strong>Series</strong>:&nbsp;<a href="compare.php?id=25,26">POC-120</a>&nbsp;|&nbsp;<a href="compare.php?id=27,28,29,30">POC-200</a>&nbsp;|&nbsp;<a href="compare.php?id=47,48,49,50">POC-300</a>&nbsp;|&nbsp;<a href="compare.php?id=40,41,42">Nuvo-5000LP</a>&nbsp;|&nbsp;<a href="compare.php?id=34,35,36">Nuvo-5000E</a>&nbsp;|&nbsp;<a href="compare.php?id=37,38,39">Nuvo-5000P</a>&nbsp;|&nbsp;<a href="compare.php?id=19,20">Nuvo-5100VTC</a>&nbsp;|&nbsp;<a href="compare.php?id=60,61,62">MS-9A89</a>&nbsp;|&nbsp;<a href="compare.php?id=22,23">ABOX-5000G</a>&nbsp;|&nbsp;<a href="compare.php?id=114,115,116">ARK-1122</a>&nbsp;|&nbsp;<a href="compare.php?id=107,117,118">ARK-1123</a>&nbsp;|&nbsp;<a href="compare.php?id=119,120,121">ARK-1124</a>&nbsp;|&nbsp;<a href="compare.php?id=76,77,78">BPC-3023</a>&nbsp;|&nbsp;<a href="compare.php?id=79,80,81">BPC-3030</a>&nbsp;|&nbsp;<a href="compare.php?id=51,52,53">BPC-3040</a>&nbsp;|&nbsp;<a href="compare.php?id=82,83">BPC-3060</a>&nbsp;|&nbsp;<a href="compare.php?id=57,58,59">BPC-3070</a>&nbsp;|&nbsp;<a href="compare.php?id=54,55,56">BPC-3072</a>&nbsp;|&nbsp;<a href="compare.php?id=85,86">BPC-5070</a>&nbsp;|&nbsp;<a href="compare.php?id=109,110">EAC Mini EACIL20</a>&nbsp;|&nbsp;<a href="http://entry.embeddedfinder.com/compare.php?id=132,133">QBiX</a>
  	<br/><br/>
  	<strong>CPU Generation</strong>:&nbsp;<a href="compare.php?id=100,40,72">Nuvo-3000/5000/7000LP</a>&nbsp;|&nbsp;<a href="compare.php?id=92,34,63,70">Nuvo-3000/5000/7000E</a>&nbsp;|&nbsp;<a href="compare.php?id=95,37,64">Nuvo-3000/5000/7000P</a>
  	<br/><br/>
  	<strong>Other</strong>:&nbsp;<a href="compare.php?id=124,128,126,127">IPC J1900</a>
  </p>
  </p>
  <?php

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "entry";


	// Create connection
	$conn = new mysqli($servername, $username, $password,$dbname);

	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	} 

	if (isset($_GET['id'])) {
    $proc_id_set = $_GET['id'];
    $array = explode(',', $proc_id_set);
    if (isset($array[0])) {
      $proc_id_1=$array[0];
  		}
      else
      {
      $proc_id_1=-1;
	}
	if (isset($array[1])) {
      $proc_id_2=$array[1];
  }
      else
      {
      $proc_id_2=-1;
	}
	if (isset($array[2])) {
      $proc_id_3=$array[2];
  	}
      else
      {
      $proc_id_3=-1;
	}
	if (isset($array[3])) {
      $proc_id_4=$array[3];
  	}
      else
      {
      $proc_id_4=-1;
	}	
		}
		else
		{
	      $proc_id_1=-1;
	      $proc_id_2=-1;
	      $proc_id_3=-1;
	      $proc_id_4=-1;
		}
		$specifications = array("Applications","Name","Processors","Manufacturer","Series","Chipset","Graphics","Max Memory","Internal 2.5 SATA","Hot-Swap 2.5 SATA","Interal 3.5 SATA","mSATA","M.2","RAID Support","Ethernet (GbE, LAN)","PoE","USB 2.0","USB 3.0","RS-232","RS-232/422/485","DIO/GPIO","CAN Bus","VGA","DVI","HDMI","DisplayPort","PCIe x16","PCIe x8","PCIe x4","PCIe x1","Mini PCIe","PCI","DC Input","DC Connector","Power Adapter","Dimensions","Weight","Operating Temp.","Storage Temp.","Humidity","Shock","Vibration", "OS", "Notes");
		$sql = "select tb1.op as col1,tb2.op as col2,tb3.op as col3 ,tb4.op as col4 from 
        (SELECT id,n.seq, SUBSTRING_INDEX(SUBSTRING_INDEX(vals, '^', n.seq), '^', -1) op 
        FROM (select id,concat_ws('^',coalesce(app_id_grp ,'-'),coalesce(`f_name` ,'-'),coalesce(`processor_id` ,'-'),coalesce(`f_manufacturer` ,'-'),coalesce(`f_series` ,'-'),coalesce(`f_chipset` ,'-'),coalesce(`f_graphics` ,'-'),concat(coalesce(`f_max_memory`,'-'),'GB ',coalesce(`f_memory_type`,'-')),coalesce(`f_sata25_in` ,'-'),coalesce(`f_sata25_hot` ,'-'),coalesce(`f_sata35_in` ,'-'),coalesce(`f_msata` ,'-'),coalesce(`f_m2` ,'-'),coalesce(`f_raid` ,'-'),coalesce(`f_gbe` ,'-'),coalesce(`f_poe` ,'-'),coalesce(`f_usb2` ,'-'),coalesce(`f_usb3` ,'-'),coalesce(`f_rs2` ,'-'),coalesce(`f_rs244` ,'-'),coalesce(`f_dio` ,'-'),coalesce(`f_can` ,'-'),coalesce(`f_vga` ,'-'),coalesce(`f_dvi` ,'-'),coalesce(`f_hdmi` ,'-'),coalesce(`f_dp` ,'-'),coalesce(`f_pcie16` ,'-'),coalesce(`f_pcie8` ,'-'),coalesce(`f_pcie4` ,'-'),coalesce(`f_pcie1` ,'-'),coalesce(`f_mpcie` ,'-'),coalesce(`f_pci` ,'-'),coalesce(`f_voltage` ,'-'),coalesce(`f_connector` ,'-'),coalesce(`f_adapter` ,'-'),concat(coalesce(`f_dim_l`,'-'),' mm x ',coalesce(`f_dim_w`,'-'),' mm x ',coalesce(`f_dim_d`,'-'), ' mm'),concat(coalesce(`f_weight` ,'-'), ' kg'),concat(coalesce(`f_optemp_min`,'-'),'~',coalesce(`f_optemp_max`,'-')),concat(coalesce(`f_sttemp_min`,'-'),'~',coalesce(`f_sttemp_max`,'-')),coalesce(`f_humidity` ,'-'),coalesce(`f_shock` ,'-'),coalesce(`f_vibration` ,'-'),coalesce(`os_id` ,'-'),coalesce(`f_notes`  ,'-')) as vals from vw_allspecs) tt1 
        INNER JOIN vw_seq n ON char_length(vals) - char_length(replace(vals, '^', '')) >= seq - 1 where id = $proc_id_1)tb1
         left outer join (SELECT id,n.seq, SUBSTRING_INDEX(SUBSTRING_INDEX(vals, '^', n.seq), '^', -1) op 
         FROM (select id,concat_ws('^',coalesce(app_id_grp ,'-'),coalesce(`f_name` ,'-'),coalesce(`processor_id` ,'-'),coalesce(`f_manufacturer` ,'-'),coalesce(`f_series` ,'-'),coalesce(`f_chipset` ,'-'),coalesce(`f_graphics` ,'-'),concat(coalesce(`f_max_memory`,'-'),'GB ',coalesce(`f_memory_type`,'-')),coalesce(`f_sata25_in` ,'-'),coalesce(`f_sata25_hot` ,'-'),coalesce(`f_sata35_in` ,'-'),coalesce(`f_msata` ,'-'),coalesce(`f_m2` ,'-'),coalesce(`f_raid` ,'-'),coalesce(`f_gbe` ,'-'),coalesce(`f_poe` ,'-'),coalesce(`f_usb2` ,'-'),coalesce(`f_usb3` ,'-'),coalesce(`f_rs2` ,'-'),coalesce(`f_rs244` ,'-'),coalesce(`f_dio` ,'-'),coalesce(`f_can` ,'-'),coalesce(`f_vga` ,'-'),coalesce(`f_dvi` ,'-'),coalesce(`f_hdmi` ,'-'),coalesce(`f_dp` ,'-'),coalesce(`f_pcie16` ,'-'),coalesce(`f_pcie8` ,'-'),coalesce(`f_pcie4` ,'-'),coalesce(`f_pcie1` ,'-'),coalesce(`f_mpcie` ,'-'),coalesce(`f_pci` ,'-'),coalesce(`f_voltage` ,'-'),coalesce(`f_connector` ,'-'),coalesce(`f_adapter` ,'-'),concat(coalesce(`f_dim_l`,'-'),' mm x ',coalesce(`f_dim_w`,'-'),' mm x ',coalesce(`f_dim_d`,'-'), ' mm'),concat(coalesce(`f_weight` ,'-'), ' kg'),concat(coalesce(`f_optemp_min`,'-'),'~',coalesce(`f_optemp_max`,'-')),concat(coalesce(`f_sttemp_min`,'-'),'~',coalesce(`f_sttemp_max`,'-')),coalesce(`f_humidity` ,'-'),coalesce(`f_shock` ,'-'),coalesce(`f_vibration` ,'-'),coalesce(`os_id` ,'-'),coalesce(`f_notes`  ,'-')) as vals from vw_allspecs) tt1 
         INNER JOIN vw_seq n ON char_length(vals) - char_length(replace(vals, '^', '')) >= seq - 1 where id = $proc_id_2)tb2 on tb1.seq = tb2.seq 
         left outer join (SELECT id,n.seq, SUBSTRING_INDEX(SUBSTRING_INDEX(vals, '^', n.seq), '^', -1) op FROM (select id,concat_ws('^',coalesce(app_id_grp ,'-'),coalesce(`f_name` ,'-'),coalesce(`processor_id` ,'-'),coalesce(`f_manufacturer` ,'-'),coalesce(`f_series` ,'-'),coalesce(`f_chipset` ,'-'),coalesce(`f_graphics` ,'-'),concat(coalesce(`f_max_memory`,'-'),'GB ',coalesce(`f_memory_type`,'-')),coalesce(`f_sata25_in` ,'-'),coalesce(`f_sata25_hot` ,'-'),coalesce(`f_sata35_in` ,'-'),coalesce(`f_msata` ,'-'),coalesce(`f_m2` ,'-'),coalesce(`f_raid` ,'-'),coalesce(`f_gbe` ,'-'),coalesce(`f_poe` ,'-'),coalesce(`f_usb2` ,'-'),coalesce(`f_usb3` ,'-'),coalesce(`f_rs2` ,'-'),coalesce(`f_rs244` ,'-'),coalesce(`f_dio` ,'-'),coalesce(`f_can` ,'-'),coalesce(`f_vga` ,'-'),coalesce(`f_dvi` ,'-'),coalesce(`f_hdmi` ,'-'),coalesce(`f_dp` ,'-'),coalesce(`f_pcie16` ,'-'),coalesce(`f_pcie8` ,'-'),coalesce(`f_pcie4` ,'-'),coalesce(`f_pcie1` ,'-'),coalesce(`f_mpcie` ,'-'),coalesce(`f_pci` ,'-'),coalesce(`f_voltage` ,'-'),coalesce(`f_connector` ,'-'),coalesce(`f_adapter` ,'-'),concat(coalesce(`f_dim_l`,'-'),' mm x ',coalesce(`f_dim_w`,'-'),' mm x ',coalesce(`f_dim_d`,'-'), ' mm'),concat(coalesce(`f_weight` ,'-'), ' kg'),concat(coalesce(`f_optemp_min`,'-'),'~',coalesce(`f_optemp_max`,'-')),concat(coalesce(`f_sttemp_min`,'-'),'~',coalesce(`f_sttemp_max`,'-')),coalesce(`f_humidity` ,'-'),coalesce(`f_shock` ,'-'),coalesce(`f_vibration` ,'-'),coalesce(`os_id` ,'-'),coalesce(`f_notes`  ,'-')) as vals from vw_allspecs) tt1 INNER JOIN vw_seq n ON char_length(vals) - char_length(replace(vals, '^', '')) >= seq - 1 where id = $proc_id_3)tb3 on tb1.seq = tb3.seq left outer join (SELECT id,n.seq, SUBSTRING_INDEX(SUBSTRING_INDEX(vals, '^', n.seq), '^', -1) op FROM (select id,concat_ws('^',coalesce(app_id_grp ,'-'),coalesce(`f_name` ,'-'),coalesce(`processor_id` ,'-'),coalesce(`f_manufacturer` ,'-'),coalesce(`f_series` ,'-'),coalesce(`f_chipset` ,'-'),coalesce(`f_graphics` ,'-'),concat(coalesce(`f_max_memory`,'-'),'GB ',coalesce(`f_memory_type`,'-')),coalesce(`f_sata25_in` ,'-'),coalesce(`f_sata25_hot` ,'-'),coalesce(`f_sata35_in` ,'-'),coalesce(`f_msata` ,'-'),coalesce(`f_m2` ,'-'),coalesce(`f_raid` ,'-'),coalesce(`f_gbe` ,'-'),coalesce(`f_poe` ,'-'),coalesce(`f_usb2` ,'-'),coalesce(`f_usb3` ,'-'),coalesce(`f_rs2` ,'-'),coalesce(`f_rs244` ,'-'),coalesce(`f_dio` ,'-'),coalesce(`f_can` ,'-'),coalesce(`f_vga` ,'-'),coalesce(`f_dvi` ,'-'),coalesce(`f_hdmi` ,'-'),coalesce(`f_dp` ,'-'),coalesce(`f_pcie16` ,'-'),coalesce(`f_pcie8` ,'-'),coalesce(`f_pcie4` ,'-'),coalesce(`f_pcie1` ,'-'),coalesce(`f_mpcie` ,'-'),coalesce(`f_pci` ,'-'),coalesce(`f_voltage` ,'-'),coalesce(`f_connector` ,'-'),coalesce(`f_adapter` ,'-'),concat(coalesce(`f_dim_l`,'-'),' mm x ',coalesce(`f_dim_w`,'-'),' mm x ',coalesce(`f_dim_d`,'-'), ' mm'),concat(coalesce(`f_weight` ,'-'), ' kg'),concat(coalesce(`f_optemp_min`,'-'),'~',coalesce(`f_optemp_max`,'-')),concat(coalesce(`f_sttemp_min`,'-'),'~',coalesce(`f_sttemp_max`,'-')),coalesce(`f_humidity` ,'-'),coalesce(`f_shock` ,'-'),coalesce(`f_vibration` ,'-'),coalesce(`os_id` ,'-'),coalesce(`f_notes`  ,'-')) as vals from vw_allspecs) tt1 INNER JOIN vw_seq n ON char_length(vals) - char_length(replace(vals, '^', '')) >= seq - 1 where id = $proc_id_4)tb4 on tb1.seq = tb4.seq order by tb1.seq";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			echo "<table id='compare_table' class='table striped table-bordered'><thead></thead><tbody>";
			$cntr_specs=0;
			while($row = $result->fetch_assoc()) {
				//if ($row["col1"] != "-" AND $row["col2"] != "-" AND $row["col3"] != "-" AND $row["col4"] != "-") {
					echo "<tr class='rowid_".str_replace(".", "", str_replace(" ", "", strtolower($specifications[$cntr_specs])))."'><th>".$specifications[$cntr_specs]."</th>";
					echo "<td>".str_replace(",", ", ", $row["col1"])."</td>";
					echo "<td>".str_replace(",", ", ", $row["col2"])."</td>";
					if ($row["col3"] != "") {
						echo "<td>".str_replace(",", ", ", $row["col3"])."</td>";
					}
					if ($row["col4"] != "") {
						echo "<td>".str_replace(",", ", ", $row["col4"])."</td>";
					}
					echo "</tr>";
				//}
				$cntr_specs++;  
			}
			echo "</tbody><table>";
		}

	$conn->close();
	
	?>
	</div>
	</div>
	<script src="js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="/js/jquery-latest.js"></script> 
	<script type="text/javascript" src="/js/jquery.tablesorter.js"></script> 
	<script type="text/javascript" src="/js/compare.js"></script> 
  </div>
</body>
</html>