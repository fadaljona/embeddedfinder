$("tr").not(':first').hover(
  function () {
    $(this).css("background","#00FFD9");
  }, 
  function () {
    $(this).css("background","");
  }
);

$("tr:odd").addClass("odd");

// add row between rowid_maxmemory and rowid_internal25sata
// <tr class="row_section_header"><td colspan="4">Storage</td></tr>

// add row between rowid_raidsupport and rowid_gbe
// <tr class="row_section_header"><td colspan="4">I/O</td></tr>

// add row between rowid_canbus and rowid_vga
// <tr class="row_section_header"><td colspan="4">Video</td></tr>

// add row between rowid_displayport and rowid_pciex16
// <tr class="row_section_header"><td colspan="4">Expansion</td></tr>

// add row between rowid_pci and rowid_dcinput
// <tr class="row_section_header"><td colspan="4">Power</td></tr>

// add row between rowid_poweradapter and rowid_dimensions
// <tr class="row_section_header"><td colspan="4">Mechanical</td></tr>

// add row between rowid_weight and rowid_operatingtemp
// <tr class="row_section_header"><td colspan="4">Environmental</td></tr>

// add row between rowid_vibration and rowid_os
// <tr class="row_section_header"><td colspan="4">Operating System</td></tr>
