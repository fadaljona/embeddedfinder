<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="robots" content="noindex,nofollow">
  <title>Embedded Table</title>
  
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

  <style>
  		body {font-size: .7rem;}
  </style>

  <meta http-equiv="Content-Security-Policy" content="default-src &apos;self&apos;; script-src &apos;self&apos; https://ajax.googleapis.com; style-src &apos;self&apos;; img-src &apos;self&apos; data:">

</head>
<body>
  <div class="container-fluid">

  <?php

$servername = "localhost";
$username = "embeddh3_de";
$password = "dremel29001";
$dbname = "embeddh3_entry";

$view = $_GET['v'];
if ($view = ''){ $view = 'table';} 


	// Create connection
	$conn = new mysqli($servername, $username, $password,$dbname);

	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	} 

	if (isset($_GET['cores'])) {
    $ip_cores = $_GET['cores'];
		}
		else
		{
	  $ip_cores = '1,2,3,4,5,6,7,8,9,10';
		}

if (isset($_GET['apps'])) {
    $ip_apps = $_GET['apps'];
    $app_cond = "and app_id in ('".$ip_apps."')";
		}
		else
		{	
    $app_cond = "";
		}


	// starting code to display Input result on screen 
		$sql = "SELECT `id`, `f_name`, `f_series`, `f_manufacturer`, `f_status`, `f_form_factor`, `f_cpu_socket`, `f_chipset`, `f_graphics`, `f_max_memory`, `f_memory_slots`, `f_memory_type`, `f_memory_speed`, `f_audio`, `f_sata3`, `f_sata2`, `f_sata_express`, `f_msata`, `f_m2`, `f_raid`, `f_bp_gbe`, `f_bp_poe`, `f_bp_usb2`, `f_bp_usb3`, `f_bp_rs2`, `f_bp_rs244`, `f_bp_audio`, `f_bp_ps2`, `f_ob_usb2`, `f_ob_usb3`, `f_ob_rs2`, `f_ob_rs244`, `f_ob_gpio`, `f_optemp_min`, `f_optemp_max`, `f_sttemp_min`, `f_sttemp_max`, `f_vga`, `f_vga_max_res`, `f_dvi`, `f_dvi_max_res`, `f_hdmi`, `f_hdmi_max_res`, `f_dp`, `f_dp_max_res`, `f_mdisplay`, `f_pcie16`, `f_pcie8`, `f_pcie4`, `f_pcie1`, `f_mpcie`, `f_m2e`, `f_pci`, `f_sim`, `f_connector`, `f_lifecycle_q`, `f_lifecycle_y`, `f_cost`, `f_lead_time`, `f_odoo`, `f_notes`, `insert_ts` FROM `board_specs` ORDER BY `f_name`";



		//working sql fron nitin
		//$sql = "SELECT tb6.app_id_grp,tb6.app_id,tb1.`f_name`, tb3.`processor_id`,tb3.`cores`,tb5.`os_id`, tb1.`f_manufacturer`, tb1.`f_source`, tb1.`f_chipset`, tb1.`f_graphics`, tb1.`f_max_memory`, tb1.`f_memory_slots`, tb1.`f_memory_type`, tb1.`f_memory_speed`, tb1.`f_sata25_in`, tb1.`f_msata`, tb1.`f_m2`, tb1.`f_raid`, tb1.`f_gbe`, tb1.`f_poe`, tb1.`f_usb2`, tb1.`f_usb3`, tb1.`f_rs2`, tb1.`f_rs244`, tb1.`f_dio`, tb1.`f_can`, tb1.`f_vga`, tb1.`f_dvi`, tb1.`f_hdmi`, tb1.`f_dp`, tb1.`f_pcie16`, tb1.`f_pcie8`, tb1.`f_pcie4`, tb1.`f_pcie1`, tb1.`f_mpcie`, tb1.`f_pci`, tb1.`f_voltage`, tb1.`f_connector`, tb1.`f_adapter`, tb1.`f_dim_l`, tb1.`f_dim_w`, tb1.`f_dim_d`, tb1.`f_weight`, tb1.`f_optemp_min`, tb1.`f_optemp_max`, tb1.`f_sttemp_min`, tb1.`f_sttemp_max`, tb1.`f_humidity`, tb1.`f_shock`, tb1.`f_vibration`, tb1.`f_notes` FROM `comp_specs` tb1 left outer join (select comp_spec_id,GROUP_CONCAT(mount_id SEPARATOR ' ') as mount_id from mount_link group by comp_spec_id)tb2 on tb1.id = tb2.comp_spec_id  left outer join (select comp_spec_id,proc_family,proc_number,cores,threads,cache,group_concat(concat('Intel ',proc_family,' ',proc_number,' Cores(',cores,') Threads(',threads,')')) as processor_id from process_link pl left outer join dim_processor dp on pl.processor_id = dp.proc_id group by comp_spec_id)tb3 on tb1.id = tb3.comp_spec_id  left outer join (select comp_spec_id,group_concat(cert_id) as cert_id from cert_link group by comp_spec_id)tb4 on tb1.id = tb4.comp_spec_id  left outer join (select comp_spec_id,group_concat(os_id) as os_id from os_link group by comp_spec_id)tb5 on tb1.id = tb5.comp_spec_id left outer join (select comp_spec_id,app_id,group_concat(app_id) as app_id_grp from app_link group by comp_spec_id)tb6 on tb1.id = tb6.comp_spec_id where cores in ($ip_cores) $app_cond ORDER BY  tb1.`f_manufacturer`";

		$result = $conn->query($sql);

echo $view;

	if ($result->num_rows > 0 and $view = 'table') {
		echo "<table class='table striped table-bordered'>";
		echo "<thead><tr><th>Model</th><th>Manufacturer</th><th>Form Factor</th><th>Socket</th><th>Chipset</th><th>Max Memory</th><th>Storage</th><th>Lifecycle</th></tr></thead>";
	    // output data of each row
	    while($row = $result->fetch_assoc()) {
			//Display USB
	    	$p_usb2 = $row["f_usb2"];
	    	$p_usb3 = $row["f_usb3"];
	    	$p_usbt = $p_usb2 + $p_usb3;
	    	
	    	$app_id_grp = $row["app_id_grp"];

			//Display Serial
	    	$p_rs2 = $row["f_rs2"];
	    	$p_rs244 = $row["f_rs244"];
	    	$p_rst = $p_rs2 + $p_rs244;

	    	//Display Video
	    	$p_vga = $row["f_vga"];
	    	$p_dvi = $row["f_dvi"];
	    	$p_hdmi = $row["f_hdmi"];
	    	$p_dp = $row["f_dp"];
	    	$p_vid = "";
	    	if ($p_vga > 0){$p_vid = $p_vga."x VGA,";}
	    	if ($p_dvi > 0){$p_vid = $p_vid." ".$p_dvi."x DVI,";}
	    	if ($p_hdmi > 0){$p_vid = $p_vid." ".$p_hdmi."x HDMI,";}
	    	if ($p_dp > 0){$p_vid = $p_vid." ".$p_dp."x DisplayPort";}
	    	$p_vid = rtrim($p_vid,',');


	    	//Display Expansion
	    	$p_pcie16 = $row["f_pcie16"];
	    	$p_pcie8 = $row["f_pcie8"];
	    	$p_pcie4 = $row["f_pcie4"];
	    	$p_pcie1 = $row["f_pcie1"];
	    	$p_pciet = $p_pcie16 + $p_pcie8 + $p_pcie4 + $p_pcie1;
	    	$p_pci = $row["f_pci"];
	    	$p_mpcie = $row["f_mpcie"];
	    	$p_expansion = "";
	    	if ($p_pciet > 0){$p_expansion = $p_pciet."x PCIe,";}
	    	if ($p_mpcie > 0){$p_expansion = $p_expansion." ".$p_mpcie."x Mini PCIe,";}
	    	if ($p_pci > 0){$p_expansion = $p_expansion." ".$p_pci."x PCI";}
	    	$p_expansion = rtrim($p_expansion,',');


	    	//Storage
	    	$p_sata25_in = $row["f_sata25_in"];
	    	$p_sata25_hot = $row["f_sata25_hot"];
	    	$p_sata35_in = $row["f_sata35_in"];
	    	$p_sata_tot = $p_sata25_in + $p_sata25_hot + $p_sata35_in;
	    	$p_msata = $row["f_msata"];
	    	$p_m2 = $row["f_m2"];
	    	$p_raid = $row["f_raid"];	
	    	$p_storage = "";
	    	if ($p_sata_tot > 0){$p_storage = $p_sata_tot."x SATA,";}
	    	if ($p_msata > 0){$p_storage = $p_storage." ".$p_msata."x mSATA,";}
	    	if ($p_m2 > 0){$p_storage = $p_storage." ".$p_m2."x m.2";}
	    	$p_storage = rtrim($p_storage,',');

	    	//$p_storage = $p_sata_tot."x SATA";

	    	//Display Processors
	    	$p_processor_list_raw = $row["processor_id"];

			if (strpos($p_processor_list_raw, 'i7-6') !== false) {
			    $p_processor_list = "6th Generation Intel Core Processors";
			} elseif (strpos($p_processor_list_raw, 'i7-8') !== false) {
				$p_processor_list = "8th Generation Intel Core Processors";
			} elseif (strpos($p_processor_list_raw, 'i7-3') !== false) {
				$p_processor_list = "3rd Generation Intel Core Processors";
			} elseif (strpos($p_processor_list_raw, 'i7-7') !== false) {
				$p_processor_list = "7th Generation Intel Core Processors";
			} elseif (strpos($p_processor_list_raw, 'i7-4') !== false) {
				$p_processor_list = "4th Generation Intel Core Processors";
			} else {
				$p_processor_list = $p_processor_list_raw;
			};

	    	//$p_processor_list = str_replace(",",", ",$row["app_id_grp"]);

			//Dimensions
			$p_dim_l = $row["f_dim_l"];
			$p_dim_w = $row["f_dim_w"];
			$p_dim_h = $row["f_dim_d"];
			$p_dim_l_in = $p_dim_l*0.0393701;
		    $p_dim_w_in = $p_dim_w*0.0393701;
		    $p_dim_h_in = $p_dim_h*0.0393701;
		    $p_dim_t = $p_dim_l*$p_dim_w*$p_dim_h;

		    $p_weight = $row["f_weight"];
		    $p_weight_lb = $p_weight*2.20462;	
		

	    	//if ($p_sata == 0 and $p_msata > 0){
	    		//$p_storage = $p_msata."x mSATA";
	    	//} elseif ($p_sata > 0 and $p_msata > 0) {
	    		//$p_storage = $p_msata."x mSATA<br/>".$p_sata."x SATA";
	    		//if ($p_raid == "Y"){$p_storage = $p_storage." (RAID)";}
	    	//} else {
	    		//$p_storage = "Other";
	    	//}	

	    	echo "<tr>";
	            //echo "<td><a href='".$row["f_source"]."'>".$row["f_name"]."</a></td>";
	    		echo "<td><a href='product.php?id=".$row["id"]."'>".$row["f_name"]."</a></td>";
	    		echo "<td>".$row["f_manufacturer"]."</td>";
				echo "<td>".$row["f_form_factor"]."</td>";
				echo "<td>".$row["f_cpu_socket"]."</td>";
				echo "<td>".$row["f_chipset"]."</td>";
				echo "<td>".$row["f_max_memory"]."GB ".$row["f_memory_type"]."</td>";
				echo "<td>".$row["f_sata3"]."x SATA 3 Ports (".$row["f_raid"].")</td>";
				echo "<td>Qtr ".$row["f_lifecycle_q"]." ".$row["f_lifecycle_y"]."</td>";
			echo "</tr>";
	    }
	  	echo "<table>";
	} else {
	    echo "0 results";
	}

	$conn->close();
	
	?>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript" src="/js/jquery-latest.js"></script> 
	<script type="text/javascript" src="/js/jquery.tablesorter.js"></script> 
  </div>


</body>
</html>