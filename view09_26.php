<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="robots" content="noindex,nofollow">
  <title>Embedded Table</title>
  
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

  <style>
  		body {font-size: .7rem;}
  </style>

  <meta http-equiv="Content-Security-Policy" content="default-src &apos;self&apos;; script-src &apos;self&apos; https://ajax.googleapis.com; style-src &apos;self&apos;; img-src &apos;self&apos; data:">

</head>
<body>
  <div class="container-fluid">

  <?php

	$servername = "localhost";
	$username = "embeddh3_de";
	$password = "dremel29001";
	$dbname = "embeddh3_entry";

	// Create connection
	$conn = new mysqli($servername, $username, $password,$dbname);

	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	} 

	// starting code to display Input result on screen 
		$sql = "SELECT `f_name`, `f_manufacturer`, `f_source`, `f_chipset`, `f_graphics`, `f_max_memory`, `f_memory_slots`, `f_memory_type`, `f_memory_speed`, `f_sata`, `f_msata`, `f_m2`, `f_raid`, `f_gbe`, `f_poe`, `f_usb2`, `f_usb3`, `f_rs2`, `f_rs244`, `f_dio`, `f_gpio`, `f_vga`, `f_dvi`, `f_hdmi`, `f_dp`, `f_pcie16`, `f_pcie8`, `f_pcie4`, `f_pcie1`, `f_mpcie`, `f_pci`, `f_voltage`, `f_connector`, `f_adapter`, `f_dim_l`, `f_dim_w`, `f_dim_d`, `f_weight`, `f_optemp_min`, `f_optemp_max`, `f_sttemp_min`, `f_sttemp_max`, `f_humidity`, `f_shock`, `f_vibration`, `f_notes` FROM `comp_specs` ORDER BY  `f_manufacturer`";
		$result = $conn->query($sql);

	if ($result->num_rows > 0) {
		echo "<table class='table striped table-bordered'>";
		echo "<thead><tr><th>Model</th><th>Manufacturer</th><th>Chipset</th><th>Graphics</th><th>Max Memory</th><th>Storage</th><th>GbE</th><th>USB</th><th>Serial</th><th>Video</th><th>Expansion</th><th>Voltage</th><th>Dimensions</th><th>Weight</th><th>Operating Temp.</th><th>Storage Temp.</th><th>Notes</th></tr></thead>";
	    // output data of each row
	    while($row = $result->fetch_assoc()) {
			//Display USB
	    	$p_usb2 = $row["f_usb2"];
	    	$p_usb3 = $row["f_usb3"];
	    	$p_usbt = $p_usb2 + $p_usb3;
	    	
			//Display Serial
	    	$p_rs2 = $row["f_rs2"];
	    	$p_rs244 = $row["f_rs244"];
	    	$p_rst = $p_rs2 + $p_rs244;

	    	//Display Video
	    	$p_vga = $row["f_vga"];
	    	$p_dvi = $row["f_dvi"];
	    	$p_hdmi = $row["f_hdmi"];
	    	$p_dp = $row["f_dp"];
	    	$p_vid = "";
	    	if ($p_vga > 0){$p_vid = $p_vga."x VGA";}
	    	if ($p_dvi > 0){$p_vid = $p_vid.", ".$p_dvi."x DVI";}
	    	if ($p_hdmi > 0){$p_vid = $p_vid.", ".$p_dvi."x HDMI";}
	    	if ($p_dp > 0){$p_vid = $p_vid.", ".$p_dp."x DisplayPort";}


	    	//Display Expansion
	    	$p_pcie16 = $row["f_pcie16"];
	    	$p_pcie8 = $row["f_pcie8"];
	    	$p_pcie4 = $row["f_pcie4"];
	    	$p_pcie1 = $row["f_pcie1"];
	    	$p_pciet = $p_pcie16 + $p_pcie8 + $p_pcie4 + $p_pcie1;
	    	$p_pci = $row["f_pci"];
	    	$p_mpcie = $row["f_mpcie"];
	    	$p_expansion = "";
	    	if ($p_pciet > 0){$p_expansion = $p_pciet."x PCIe";}
	    	if ($p_mpcie > 0){$p_expansion = $p_expansion.", ".$p_mpcie."x Mini PCIe";}
	    	if ($p_pci > 0){$p_expansion = $p_expansion.", ".$p_pci."x PCI";}

	    	//Storage
	    	$p_sata = $row["f_sata"];
	    	$p_msata = $row["f_msata"];
	    	$p_m2 = $row["f_m2"];
	    	$p_raid = $row["f_raid"];	
	    	$p_storage = "";
	    	$p_storage = $p_sata."/".$p_msata."/".$p_m2."(".$p_raid.")";

	    	//if ($p_sata == 0 and $p_msata > 0){
	    		//$p_storage = $p_msata."x mSATA";
	    	//} elseif ($p_sata > 0 and $p_msata > 0) {
	    		//$p_storage = $p_msata."x mSATA<br/>".$p_sata."x SATA";
	    		//if ($p_raid == "Y"){$p_storage = $p_storage." (RAID)";}
	    	//} else {
	    		//$p_storage = "Other";
	    	//}	

	    	echo "<tr>";
	            //echo "<td><a href='".$row["f_source"]."'>".$row["f_name"]."</a></td>";
	    		echo "<td>".$row["f_name"]."</td>";
				echo "<td>".$row["f_manufacturer"]."</td>";
				echo "<td>".$row["f_chipset"]."</td>";
				echo "<td>".$row["f_graphics"]."</td>";
				echo "<td>".$row["f_max_memory"]."GB ".$row["f_memory_type"]."</td>";
				//echo "<td>".$row["f_sata"]."</td>";
				//echo "<td>".$row["f_msata"]."</td>";
				//echo "<td>".$row["f_m2"]."</td>";
				echo "<td>".$p_storage."</td>";
				echo "<td class='text-right'>".$row["f_gbe"]."</td>";
				//echo "<td>".$row["f_poe"]."</td>";
				echo "<td class='text-right'>".$p_usbt."</td>";
				//echo "<td>".$row["f_usb3"]."</td>";
				echo "<td class='text-right'>".$p_rst."</td>";
				//echo "<td>".$row["f_rs244"]."</td>";
				//echo "<td>".$row["f_dio"]."</td>";
				//echo "<td>".$row["f_gpio"]."</td>";
				echo "<td>".$p_vid."</td>";
				//echo "<td class='text-right'>".$row["f_dvi"]."</td>";
				//echo "<td class='text-right'>".$row["f_hdmi"]."</td>";
				//echo "<td class='text-right'>".$row["f_dp"]."</td>";
				//echo "<td>".$row["f_pcie16"]."</td>";
				//echo "<td>".$row["f_pcie8"]."</td>";
				//echo "<td>".$row["f_pcie4"]."</td>";
				//echo "<td>".$row["f_pcie1"]."</td>";
				//echo "<td>".$row["f_mpcie"]."</td>";
				//echo "<td>".$row["f_pci"]."</td>";
				echo "<td>".$p_expansion."</td>";
				echo "<td>".$row["f_voltage"]." (".$row["f_connector"]." &amp; ".$row["f_adapter"]." adapter)</td>";
				//echo "<td>".$row["f_connector"]."</td>";
				//echo "<td>".$row["f_adapter"]."</td>";
				echo "<td>".$row["f_dim_l"]."mm x ".$row["f_dim_w"]."mm x ".$row["f_dim_d"]."mm</td>";
				//echo "<td>".$row["f_dim_w"]."</td>";
				//echo "<td>".$row["f_dim_d"]."</td>";
				echo "<td>".$row["f_weight"]."kg</td>";
				echo "<td>".$row["f_optemp_min"]."&deg;~".$row["f_optemp_max"]."&deg; C</td>";
				//echo "<td>".$row["f_optemp_max"]."</td>";
				echo "<td>".$row["f_sttemp_min"]."&deg;~".$row["f_sttemp_max"]."&deg; C</td>";
				//echo "<td>".$row["f_sttemp_max"]."</td>";
				//echo "<td>".$row["f_humidity"]."</td>";
				//echo "<td>".$row["f_shock"]."</td>";
				//echo "<td>".$row["f_vibration"]."</td>";
				//echo "<td>".$row["mount_id"]."</td>";
				//echo "<td>".$row["processor_id"]."</td>";
				//echo "<td>".$row["cert_id"]."</td>";
				//echo "<td>".$row["os_id"]."</td>";
				echo "<td>".$row["f_notes"]."</td>";
			echo "</tr>";
	    }
	  	echo "<table>";
	} else {
	    echo "0 results";
	}

	$conn->close();
	
	?>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript" src="/js/jquery-latest.js"></script> 
	<script type="text/javascript" src="/js/jquery.tablesorter.js"></script> 
  </div>
</body>
</html>