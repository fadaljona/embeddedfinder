<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="robots" content="noindex,nofollow">
  <title>Embedded Computers</title>
  <meta name="description" content="Embedded Computers from...">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Archivo+Black" rel="stylesheet">
  <link rel="stylesheet" href="css/normalize-uf.css">
  <link rel="stylesheet" href="css/unifilter.css">
  <link rel="stylesheet" href="css/style-uf.css">
  <link rel="stylesheet" href="css/view.css">
  
  <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
  <script type="text/javascript" src="js/masonry.min.js"></script>
  <script type="text/javascript" src="js/imagesloaded.min.js"></script>
  <script type="text/javascript" src="js/jquery.unifilter.min.js"></script>
  <script type="text/javascript" src="js/scripts.js"></script>
  <script type="text/javascript">
  		$(document).ready(function(){
  		
  			// our online shop UL list
  			var shop = $('#shop');
  			
  			// Masonry Options
			var masonry_options = {
	  			itemSelector: 'li:not(.uf-clone)',
	  			percentPosition: true,
	  			columnWidth: '.masonry-sizer'
	  		};
  		
  			// filter options
  			var filter_options = {
  				"gender": {
					title: "Man/Woman",
					leading: true
				},
				"cpu" : {	
					title: 'CPU',
					tooltip: true,
				},
				"manufacturer": {
					title: 'Manufacturer',
					tooltip: true,
					multiple: true
				}
			};
			
			// search options
			var search_options = {
				"search": {
					title: "Quick Search",
					placeholder: 'Search by name',
					getSearchData: function(){
						var name = $(this).find('.name')[0].firstChild.textContent;
						return name.split(/\s*,\s*/);
					}
				}
			};
			
			// range options
			var range_options = {
				"price": {
					title: "Price",
					scale: '5-50',
					precision: 0,
					prefix: '$',
					getRangeData: function(){
						var price = $(this).find('.price > span').text().replace(/[^0-9\.]/g, '');
						return parseFloat(price);
					}
				}
			};

	  		// general options
	  		var unifilter_options = {
	  			selector: 'li:not(.masonry-sizer)',
  				order: "search, filters, range",
  				animationType: 'scale',
  				bestMatch: true,
  				hideEmptyFilters: true,
  				filters: filter_options,
  				search: search_options,
  				range: range_options
  			};
  			
  			// init UniFilter plugin
  			$('#filters').unifilter(shop, unifilter_options).on('onListUpdate', function(){
  				shop.masonry('destroy').masonry(masonry_options);
	  		});
	  		
	  		// init masonry and hide all list items
	  		shop.masonry(masonry_options).children('.uf-item').css('opacity', 0);
			
			// show images as they keep loading
  			shop.imagesLoaded().progress(function(instance, image){
  				shop.masonry('layout');
  				$(image.img).parents('.uf-item').css('opacity', '');
  			});
  		  			
  		});
  	
  </script>

</head>
<body>

<?php

$servername = "localhost";
$username = "embeddh3_de";
$password = "dremel29001";
$dbname = "embeddh3_entry";

	// Create connection
	$conn = new mysqli($servername, $username, $password,$dbname);

	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	} 

	if (isset($_GET['cores'])) {
    $ip_cores = $_GET['cores'];
		}
		else
		{
	  $ip_cores = '1,2,3,4,5,6,7,8,9,10';
		}

if (isset($_GET['apps'])) {
    $ip_apps = $_GET['apps'];
    $app_cond = "and app_id in ('".$ip_apps."')";
		}
		else
		{	
    $app_cond = "";
		}


	// starting code to display Input result on screen 
		$sql = "SELECT tb6.app_id_grp,tb6.app_id,tb1.`f_name`,tb1.`f_status`,tb1.`id`,tb1.`f_sata35_in`, tb1.`f_sata25_hot`,tb3.`processor_id`,tb3.`cores`,tb5.`os_id`, tb1.`f_manufacturer`, tb1.`f_source`, tb1.`f_series`, tb1.`f_chipset`, tb1.`f_graphics`, tb1.`f_max_memory`, tb1.`f_memory_slots`, tb1.`f_memory_type`, tb1.`f_memory_speed`, tb1.`f_sata25_in`, tb1.`f_msata`, tb1.`f_m2`, tb1.`f_raid`, tb1.`f_gbe`, tb1.`f_poe`, tb1.`f_usb2`, tb1.`f_usb3`, tb1.`f_rs2`, tb1.`f_rs244`, tb1.`f_dio`, tb1.`f_can`, tb1.`f_mdisplay`, tb1.`f_vga`, tb1.`f_dvi`, tb1.`f_hdmi`, tb1.`f_dp`, tb1.`f_pcie16`, tb1.`f_pcie8`, tb1.`f_pcie4`, tb1.`f_pcie1`, tb1.`f_mpcie`, tb1.`f_pci`, tb1.`f_voltage`, tb1.`f_connector`, tb1.`f_adapter`, tb1.`f_dim_l`, tb1.`f_dim_w`, tb1.`f_dim_d`, tb1.`f_weight`, tb1.`f_optemp_min`, tb1.`f_optemp_max`, tb1.`f_sttemp_min`, tb1.`f_sttemp_max`, tb1.`f_humidity`, tb1.`f_shock`, tb1.`f_vibration`, tb1.`f_notes`, tb1.`f_cost`, tb1.`f_odoo` FROM `comp_specs` tb1 left outer join (select comp_spec_id,GROUP_CONCAT(mount_id SEPARATOR ' ') as mount_id from mount_link group by comp_spec_id)tb2 on tb1.id = tb2.comp_spec_id  left outer join (select comp_spec_id,proc_family,proc_number,cores,threads,base_freq,max_freq,cache,group_concat(concat('Intel ',proc_family,' ', proc_number,' (',base_freq,case when max_freq is not null then concat('/',max_freq) else '' end,' GHz, ',cache,'M)',' - ',cores,' Cores') SEPARATOR '<br>') as processor_id from process_link pl left outer join dim_processor dp on pl.processor_id = dp.proc_id group by comp_spec_id)tb3 on tb1.id = tb3.comp_spec_id  left outer join (select comp_spec_id,group_concat(cert_id) as cert_id from cert_link group by comp_spec_id)tb4 on tb1.id = tb4.comp_spec_id  left outer join (select comp_spec_id,group_concat(os_id) as os_id from os_link group by comp_spec_id)tb5 on tb1.id = tb5.comp_spec_id left outer join (select comp_spec_id,app_id,group_concat(app_id) as app_id_grp from app_link group by comp_spec_id)tb6 on tb1.id = tb6.comp_spec_id where tb1.id > 17 AND cores in ($ip_cores) $app_cond ORDER BY  tb1.`f_name`";
		

		$result = $conn->query($sql);
?>


	<div class="header">
		<a href="http://plugins.gravitysign.com/unifilter">
			<strong title="JQUERY PLUGIN FOR FILTERING, SORTING AND SEARCHING"><span>UNI</span>FILTER</strong>
		</a> v3.0
		<a class="purchase" title ="Purchase UniFilter jQuery Plugin on Codecanyon.net for $10" href="https://codecanyon.net/item/unifilter-multipurpose-jquery-plugin-for-filtering-sorting-searching/21801782?ref=flGravity">Purchase for $10</a>
	</div>

	<!-- SHOP WRAP -->
	<div class="shop-wrap">
	
		<div class="mobile-menu">
			<a href="#qwe"></a>
		</div>
	
		<div class="sidebar">
			<div id="filters" class="unifilter">
				<!-- UNIFILTER WILL BE HERE -->
			</div>
		</div>
						
		<!-- ONLINE SHOP -->
		<ul id="shop" class="shop">	
		
			<li class="masonry-sizer"><!-- requred by masonry plugin --></li>
				
<?php

	if ($result->num_rows > 0) {

	    // output data of each row
	    while($row = $result->fetch_assoc()) {
	    	$p_status = $row["f_status"];

	    	$p_manufacturer = $row["f_manufacturer"];
	    	if ($p_manufacturer == "Iwill") {
	    		$p_manufacturer = "Industrial PC, Inc.";
	    	} else {
	    		$p_manufacturer = $p_manufacturer;
	    	}

			$p_series = $row["f_series"];

			//Display USB
	    	$p_usb2 = $row["f_usb2"];
	    	$p_usb3 = $row["f_usb3"];
	    	$p_usbt = $p_usb2 + $p_usb3;
	    	
	    	$app_id_grp = $row["app_id_grp"];

			//Display Serial
	    	$p_rs2 = $row["f_rs2"];
	    	$p_rs244 = $row["f_rs244"];
	    	$p_rst = $p_rs2 + $p_rs244;

	    	//Display Video
	    	$p_vga = $row["f_vga"];
	    	$p_dvi = $row["f_dvi"];
	    	$p_hdmi = $row["f_hdmi"];
	    	$p_dp = $row["f_dp"];
	    	$p_mdisplay = $row["f_mdisplay"];
	    	$p_display_tot = $p_vga + $p_dvi + $p_hdmi + $p_dp;

	    	$p_vid = "";
	    	if ($p_vga > 0){$p_vid = $p_vga."x VGA,";}
	    	if ($p_dvi > 0){$p_vid = $p_vid." ".$p_dvi."x DVI,";}
	    	if ($p_hdmi > 0){$p_vid = $p_vid." ".$p_hdmi."x HDMI,";}
	    	if ($p_dp > 0){$p_vid = $p_vid." ".$p_dp."x DisplayPort";}
	    	$p_vid = rtrim($p_vid,',');


	    	//Display Expansion
	    	$p_pcie16 = $row["f_pcie16"];
	    	$p_pcie8 = $row["f_pcie8"];
	    	$p_pcie4 = $row["f_pcie4"];
	    	$p_pcie1 = $row["f_pcie1"];
	    	$p_pciet = $p_pcie16 + $p_pcie8 + $p_pcie4 + $p_pcie1;
	    	$p_pci = $row["f_pci"];
	    	$p_mpcie = $row["f_mpcie"];
	    	$p_expansion = "";
	    	if ($p_pciet > 0){$p_expansion = $p_pciet."x PCIe,";}
	    	if ($p_mpcie > 0){$p_expansion = $p_expansion." ".$p_mpcie."x Mini PCIe,";}
	    	if ($p_pci > 0){$p_expansion = $p_expansion." ".$p_pci."x PCI";}
	    	$p_expansion = rtrim($p_expansion,',');

	    	//Display Ethernet
	    	$p_gbe = $row["f_gbe"];
	    	$p_poe = $row["f_poe"];
	    	$p_lan = "";
	    	if ($p_gbe > 0 and $p_poe > 0){
	    		$p_lan = $p_gbe."x GbE, ".$p_poe."x PoE, ";
	    	} elseif ($p_poe > 0 and $p_gbe == 0) {
	    		$p_lan = $p_poe."x PoE, ";
	    	} else {
	    		$p_lan = $p_gbe."x GbE, ";
	    	}

	    	//Storage
	    	$p_sata25_in = $row["f_sata25_in"];
	    	$p_sata25_hot = $row["f_sata25_hot"];
	    	$p_sata35_in = $row["f_sata35_in"];
	    	$p_sata25_tot = $row["f_sata25_hot"];
	    	$p_sata_tot = $p_sata25_in + $p_sata25_hot;
	    	$p_msata = $row["f_msata"];
	    	$p_m2 = $row["f_m2"];
	    	$p_raid = $row["f_raid"];	
	    	$p_storage = "";
	    	if ($p_sata_tot > 0){$p_storage = $p_sata_tot."x SATA,";}
	    	if ($p_msata > 0){$p_storage = $p_storage." ".$p_msata."x mSATA,";}
	    	if ($p_m2 > 0){$p_storage = $p_storage." ".$p_m2."x m.2";}
	    	$p_storage = rtrim($p_storage,',');

	    	//$p_storage = $p_sata_tot."x SATA";

	    	$p_bullet_ports = $row["f_gbe"]."x LAN, ".$p_usbt."x USB, ".$p_rst."x Serial";

	    	//Display Processors
	    	$p_processor_list_raw = $row["processor_id"];

			if (strpos($p_processor_list_raw, 'i7-6') !== false) {
			    $p_processor_list = "Dual/Quad-Core 6th Gen. Intel® Core™ Processors";
			    $p_data_cpu = "core";
			} elseif (strpos($p_processor_list_raw, 'i7-8') !== false) {
				$p_processor_list = "Quad/Hexa-Core 8th Gen. Intel® Core™ Processors";
				$p_data_cpu = "core";
			} elseif (strpos($p_processor_list_raw, 'i7-3') !== false) {
				$p_processor_list = "Dual/Quad-Core 3rd Gen. Intel® Core™ Processors";
				$p_data_cpu = "core";
			} elseif (strpos($p_processor_list_raw, 'i7-7') !== false) {
				$p_processor_list = "Dual/Quad-Core 7th Gen. Intel® Core™ Processors";
				$p_data_cpu = "core";
			} elseif (strpos($p_processor_list_raw, 'i7-4') !== false) {
				$p_processor_list = "Dual/Quad-Core 4th Gen. Intel® Core™ Processors";
				$p_data_cpu = "core";
			} elseif (strpos($p_processor_list_raw, 'Atom') !== false) {
				$p_processor_list = str_replace("Intel Atom","Intel® Atom®",$p_processor_list_raw);
				$p_data_cpu = "atom";
			} elseif (strpos($p_processor_list_raw, 'Celeron') !== false) {
				$p_processor_list = str_replace("Intel Celeron","Intel® Celeron®",$p_processor_list_raw);
				$p_data_cpu = "celeron";
			} elseif (strpos($p_processor_list_raw, 'Pentium') !== false) {
				$p_processor_list = str_replace("Intel Pentium","Intel® Pentium®",$p_processor_list_raw);
				$p_data_cpu = "pentium";
			} 
			else {
				$p_processor_list = str_replace("Intel","Intel®",$p_processor_list_raw);
				$p_data_cpu = "other";
			};

	    	//$p_processor_list = str_replace(",",", ",$row["app_id_grp"]);

	    	

			//Dimensions
			$p_dim_l = $row["f_dim_l"];
			$p_dim_w = $row["f_dim_w"];
			$p_dim_h = $row["f_dim_d"];
			$p_dim_l_in = $p_dim_l*0.0393701;
		    $p_dim_w_in = $p_dim_w*0.0393701;
		    $p_dim_h_in = $p_dim_h*0.0393701;
		    $p_dim_t = $p_dim_l*$p_dim_w*$p_dim_h;

		    $p_weight = $row["f_weight"];
		    $p_weight_lb = $p_weight*2.20462;	
		
		    $p_mounting = $row["mount_id"];
		    $p_voltage = $row["f_voltage"];


			//Display Picture
			if (file_exists ("img/products/".$row["f_name"].".jpg") == true) {
				$p_img_name = $row["f_name"].".jpg";
			} elseif (strpos($row["f_name"], 'Nuvo-3') !== false AND strpos($row["f_name"], 'LP') !== false ) {
				$p_img_name = "Nuvo-3000LP.jpg";
			} elseif (strpos($row["f_name"], 'Nuvo-5') !== false AND strpos($row["f_name"], 'LP') !== false ) {
				$p_img_name = "Nuvo-5000LP.jpg";
			} elseif (strpos($row["f_name"], 'Nuvo-7') !== false AND strpos($row["f_name"], 'LP') !== false ) {
				$p_img_name = "Nuvo-7000LP.jpg";
			}  elseif (file_exists ("img/products/".$row["f_series"].".jpg") == true) {
				$p_img_name = $row["f_series"].".jpg";
			} else {
				$p_img_name = "no-image.jpg";
			}

	    	//Bullet CPU
			$p_bullet_cpu = "";
			if (strpos($p_processor_list, '- 2 Cores') !== false) {
			    $p_bullet_cpu = "Dual-Core ".substr($p_processor_list, 0, strpos($p_processor_list, "("));
			} elseif (strpos($p_processor_list, '- 4 Cores') !== false) {
			    $p_bullet_cpu = "Quad-Core ".substr($p_processor_list, 0, strpos($p_processor_list, "("));
			} elseif (strpos($p_processor_list, '- 1 Cores') !== false) {
			    $p_bullet_cpu = "Single-Core ".substr($p_processor_list, 0, strpos($p_processor_list, "("));
			}
			else {
				$p_bullet_cpu = $p_processor_list;
			}

			//Bullet Points
			


			if ($p_rst > 0) {
				$p_bullet_ports = $p_lan.$p_usbt."x USB, ".$p_rst."x Serial";
			} else {
				$p_bullet_ports = $p_lan.$p_usbt."x USB";
		}





			if ($p_pciet > 0 and $p_pci == 0 and $p_mpcie == 0) {
				$p_bullets_expansion = " with PCIe expansion";
			}
			elseif ($p_pciet == 2 and $p_pci == 0 and $p_mpcie > 0) {
				$p_bullets_expansion = " with Dual PCIe and Mini-PCIe expansion";
			}
			elseif ($p_pciet > 0 and $p_pci == 0 and $p_mpcie > 0) {
				$p_bullets_expansion = " with PCIe and Mini-PCIe expansion";
			}
			elseif ($p_pciet == 0 and $p_pci > 0 and $p_mpcie > 0) {
				$p_bullets_expansion =  " with PCI and Mini-PCIe expansion";
			}
			elseif ($p_pciet > 0 and $p_pci > 0 and $p_mpcie > 0) {
				$p_bullets_expansion = " with PCI, PCIe, and Mini-PCIe expansion";
			} 
			elseif ($p_mpcie == 0 and $p_pciet == 0 and $p_pci > 0) {
				$p_bullets_expansion = " with PCI expansion";
			} 
			elseif ($p_mpcie == 0 and $p_pciet > 0 and $p_pci > 0) {
				$p_bullets_expansion = " with PCI and PCIe expansion";
			} 
			elseif ($p_mpcie > 0 and $p_pciet == 0 and $p_pci == 0) {
				$p_bullets_expansion = " with Mini-PCIe expansion";
			} 
			else {$p_bullets_expansion = "";}

			//Bullet - Chassis
			if ($p_dim_t < 1000000) {
				$p_bullet_size = "Ultra-Compact, Fanless Chassis";
			} elseif ($p_dim_t < 3000000) {
				$p_bullet_size = "Compact, Fanless Chassis";
			} elseif (strpos($row["f_name"], 'LP') !== false ) {
				$p_bullet_size = "Low-Profile Fanless Chassis";
			}  else {
				$p_bullet_size = "Fanless Chassis";
			};

			//Bullet - Video -- Dual/Triple Display? Graphics Card
			if ($p_display_tot > 1){
				$p_bullet_video = "Multi Display (".$p_vid.")";
			} elseif ($p_display_tot == 0) {
				$p_bullet_video = "No Video Output";
			} else {
				$p_bullet_video = $p_vid;
			}

			$p_bullet_video = str_replace("( ", "(", $p_bullet_video);

			//Bullet - Storage -- Add mSATA and m.2
			if ($p_sata35_in > 0 and $p_sata_tot == 0) {
				$p_bullet_storage = $p_sata35_in."x 3.5\" SATA";
			} elseif ($p_sata_tot > 0) {
				$p_bullet_storage = $p_sata_tot."x 2.5\" SATA";
			} elseif ($p_sata_tot == 0 AND $p_series == "UNO-1000") {
				$p_bullet_storage = "microSD slot";
			} else {
				$p_bullet_storage =  $p_storage;
			}

			if ($p_raid == "Y") {$p_bullet_storage = $p_bullet_storage." (RAID)";}

			//Application Class 
			if (strpos($row["app_id_grp"], 'In-Vehicle') !== false) {
				$app_class_string = $app_class_string + "invehicle";
			} elseif (strpos($row["app_id_grp"], 'General') !== false) {
				$app_class_string = $app_class_string + "general";
			} else {
				$app_class_string = "";
			}
			

			echo "<li data-manufacturer='".str_replace(",inc.", "", str_replace(" ", "", strtolower($p_manufacturer)))."'
				data-status='".strtolower($p_status)."'
				data-cpu='".$p_data_cpu."'
				data-usb='".$p_usbt."'>
				<div class='product_card'>
					<div class='product_card_img'><a href='product.php?id=".$row["id"]."'><img src='img/products/".$p_img_name."' width=200' height='200'/></a></div>
					<div class='product_card_head'>
						<h4 class='series'>".$row["f_series"]."</h4>
						<h3 class='name'><a href='product.php?id=".$row["id"]."'>".$row["f_name"]."</a></h3>
						<h5 class='manufacturer'>".$p_manufacturer."</h5>
					</div>
					<div class='product_card_details' style='display: none;'>
						<div class='product_card_bullets'>
							<ul>
								<li>".$p_bullet_cpu."</li>
								<li>".$p_bullet_ports."</li>
								<li>".$p_bullet_video."</li>
								<li>".$p_bullet_size."</li>
								<li>".$p_bullets_expansion."</li>
								<li>".$p_bullet_storage."</li>
								<li>".$p_voltage." DC Input</li>
							</ul>
						</div>
					</div>
				</div>
			</li>";
			
	    }
	  	
	} else {
	    echo "0 results";
	}

	$conn->close();
	
	?>


				
		</ul> <!-- .shop -->
					
		<div class="copyright">
			Copyright &copy; 2018 <a href="http://codecanyon.net/user/flGravity">flGravity<a/>
		</div>	
		
	</div> <!-- END SHOP WRAP -->
 
<a class="up" href="#qwe" title="Go top" style=""></a>

  
</body>
</html>
